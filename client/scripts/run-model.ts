import { readFile } from 'fs/promises';
import { projectSchema } from '../src/data-schemas/project';
import { extractInputFromLegacy } from '../src/model/extractor';
import { Model } from '../src/model/model';

async function main() {
  const [assessmentFile, scenarioId] = process.argv.slice(2);
  if (assessmentFile === undefined || scenarioId === undefined) {
    throw new Error('must provide assessment file and scenario ID');
  }

  const assessment = projectSchema.parse(
    JSON.parse(await readFile(assessmentFile, 'utf-8')),
  );
  const scenario = assessment.data[scenarioId];
  if (scenario === undefined) {
    throw new Error('invalid scenario ID');
  }
  const modelInput = extractInputFromLegacy(scenario).unwrap();
  const model = new Model(modelInput);
  console.profile('model');
  console.log(model.normal.fuelRequirements.totalEnergyAnnual);
  console.profileEnd('model');
}

main().catch(console.error);
