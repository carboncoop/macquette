import { writeFileSync } from 'fs';
import { readFile } from 'fs/promises';

import { projectSchema } from '../src/data-schemas/project';
import { ModelBehaviourVersion, Scenario } from '../src/data-schemas/scenario';
import { isTruthy } from '../src/helpers/is-truthy';
import { extractInputFromLegacy } from '../src/model/extractor';
import { Model } from '../src/model/model';

const AS_SPECIFIED = 'as specified';
const MODEL_VERSIONS: Array<ModelBehaviourVersion | 'as specified'> = [
  AS_SPECIFIED,
  'legacy',
  11,
];

function runModel(scenarioData: Scenario) {
  const input = extractInputFromLegacy(scenarioData).unwrap();
  const oldConsoleWarn = console.warn;
  console.warn = () => undefined;
  try {
    const model = new Model(input);
    return {
      energyUseIntensity: {
        model: model.normal.fuelRequirements.totalEnergyAnnualPerArea,
        currentEnergy:
          model.normal.currentEnergy.annualEnergyEndUse /
          model.normal.floors.totalFloorArea,
      },
      spaceHeatingDemand: model.normal.spaceHeating.spaceHeatingDemandAnnualEnergyPerArea,
    };
  } finally {
    console.warn = oldConsoleWarn;
  }
}

async function main() {
  const filenames = process.argv.slice(2);
  console.info('Reading and validating files');
  const assessments = await Promise.all(
    filenames.map(async (filename) => {
      try {
        const data = await readFile(filename, 'utf-8');
        const parsed: unknown = JSON.parse(data);
        const validated = projectSchema.parse(parsed);
        writeFileSync(2, '.');
        return {
          filename,
          scenarios: validated.data,
          name: validated.name,
          organisationName: validated.organisation?.name ?? null,
        };
      } catch (err) {
        console.error(filename);
        throw err;
      }
    }),
  );
  writeFileSync(2, '\n');
  const scenarios = assessments.flatMap(({ scenarios, ...rest }) => {
    return Object.entries(scenarios).flatMap(([scenarioId, scenarioData]) => {
      if (scenarioData === undefined) return [];
      if (!isTruthy(scenarioData.household?.report.signoff)) return [];
      return [
        {
          scenarioId,
          scenarioName: scenarioData.scenario_name,
          scenarioData,
          ...rest,
        },
      ];
    });
  });
  console.info('Running models');
  console.profile('model');
  console.time('model');
  const totalModelRuns = MODEL_VERSIONS.length * scenarios.length;
  let modelRunCount = 0;
  const modelResults = scenarios.flatMap(({ scenarioData, ...rest }) => {
    const outputs = MODEL_VERSIONS.map((version) => {
      const modelInput: Scenario = {
        ...scenarioData,
        ...(version === AS_SPECIFIED ? {} : { modelBehaviourVersion: version }),
      };
      const outputs = runModel(modelInput);
      modelRunCount++;
      return { version, outputs };
    });
    writeFileSync(
      2,
      `\r${modelRunCount}/${totalModelRuns} (${(
        100 *
        (modelRunCount / totalModelRuns)
      ).toFixed(1)}%)`,
    );
    return outputs.map(({ version, outputs }) => ({ ...rest, version, outputs }));
  });
  writeFileSync(2, '\n');
  console.timeEnd('model');
  console.profileEnd('model');
  console.info('Writing output JSON');
  writeFileSync('compare-models-output.json', JSON.stringify(modelResults), 'utf-8');
}

main().catch(console.error);
