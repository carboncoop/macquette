import { confirm } from '@inquirer/prompts';
import chalk from 'chalk';
import { readFileSync } from 'fs';
import { join } from 'path';

import { HTTPClient } from '../src/api/http';
import { librarySchema } from '../src/data-schemas/libraries';
import { finalErrorHandler } from './import-export-libraries/error-handling';
import { readCsv, readJson } from './import-export-libraries/export';
import {
  Manifest,
  RehydratedManifest,
  manifestSchema,
  rehydrateManifest,
} from './import-export-libraries/manifest';
import { getImportParams } from './import-export-libraries/params';
import { LibraryWithOptionalId } from './import-export-libraries/types';
import { readApiKeysForBaseUrl } from './lib/config';

function readManifest(dataDirectory: string): Manifest {
  const json = readFileSync(join(dataDirectory, 'manifest.json'), 'utf-8');
  const raw: unknown = JSON.parse(json);
  return manifestSchema.parse(raw);
}

function parse(manifest: RehydratedManifest): LibraryWithOptionalId[] {
  const splitParsed = manifest.map((libraryData) => {
    let data: Record<string, unknown>;
    const { type, content } = libraryData.dataFile;
    switch (type) {
      case 'json': {
        data = readJson(content);
        break;
      }
      case 'csv': {
        data = readCsv(content);
        break;
      }
    }
    const library = {
      ...libraryData.metadata,
      data,
    };
    return {
      ...librarySchema.parse(library),
      ...(libraryData.metadata.id === undefined ? {} : { id: libraryData.metadata.id }),
    };
  });
  return splitParsed;
}

type ConfirmResult =
  | { type: 'skip' }
  | { type: 'update'; id: string }
  | { type: 'create' };
async function confirmProceed(
  client: Pick<HTTPClient, 'listLibraries'>,
  params: { dryRun: boolean },
) {
  const allLibraries = await client.listLibraries();
  return async (library: LibraryWithOptionalId): Promise<ConfirmResult> => {
    let action: ConfirmResult;

    const existingLibrary = allLibraries.find(
      (existingLibrary) => existingLibrary.id === library.id,
    );
    if (existingLibrary === undefined) {
      console.info(
        `${chalk.green('[INFO]')} New library: ${chalk.green(library.name)} with ${chalk.green(Object.keys(library.data).length)} items`,
      );
      action = { type: 'create' };
    } else {
      console.warn(
        `${chalk.yellow('[WARN]')} Replacing library with ID ${chalk.yellow(existingLibrary.id)}: ${chalk.yellow(library.name)} with ${chalk.yellow(chalk.green(Object.keys(library.data).length))} items`,
      );
      action = { type: 'update', id: library.id };
    }

    if (params.dryRun) {
      console.log('(No action - dry run)');
      return { type: 'skip' };
    } else {
      if (
        action.type === 'update' &&
        (await confirm({ default: false, message: 'Proceed?' }))
      ) {
        return action;
      } else {
        return { type: 'skip' };
      }
    }
  };
}

async function main() {
  const params = await getImportParams();
  const { sessionId, csrfToken } = await readApiKeysForBaseUrl(params.baseUrl);
  const manifest = readManifest(params.dataDirectory);
  const rehydratedManifest = rehydrateManifest(manifest, params.dataDirectory);
  const parsed = parse(rehydratedManifest);
  const client = new HTTPClient({
    sessionId,
    csrfToken,
    baseURL: params.baseUrl,
  });
  const confirm = await confirmProceed(client, params);
  for (const library of parsed) {
    const confirmed = await confirm(library);
    if (confirmed.type === 'skip') {
      continue;
    } else if (confirmed.type === 'update') {
      await client.updateLibrary(confirmed.id, library);
    } else if (confirmed.type === 'create') {
      await client.createLibrary(library);
    }
  }
}

main().catch(finalErrorHandler).catch(console.error);
