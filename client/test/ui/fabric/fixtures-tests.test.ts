import { cloneDeep } from 'lodash';
import { z } from 'zod';

import { projectSchema } from '../../../src/data-schemas/project';
import { scenarioSchema } from '../../../src/data-schemas/scenario';
import { emulateJsonRoundTrip } from '../../../src/helpers/emulate-json-round-trip';
import { compareFloats } from '../../../src/helpers/fuzzy-float-equality';
import { Result } from '../../../src/helpers/result';
import { fabricModule } from '../../../src/ui/modules/fabric';
import { projects } from '../../fixtures';

function isVariationAcceptable() {
  return function isVariationAcceptableInner(
    _path: string,
    modMissing: boolean,
    modValue: unknown,
    origMissing: boolean,
    origValue: unknown,
  ) {
    // Matching values are OK
    if (modMissing === origMissing && origValue === modValue) {
      return true;
    }

    // New values in the modification are OK
    if (origMissing === true) {
      return true;
    }

    if (typeof modValue === 'number' && typeof origValue === 'number') {
      return compareFloats(origValue, modValue);
    }

    return false;
  };
}

function normaliseRawScenario(
  rawProject: z.input<typeof projectSchema>,
  currentScenarioId: string,
) {
  const rawScenario = rawProject.data[currentScenarioId] ?? {};

  rawScenario.fabric = rawScenario.fabric ?? {};
  rawScenario.fabric.elements = rawScenario.fabric.elements ?? [];
  rawScenario.fabric.measures = rawScenario.fabric.measures ?? {};

  // Legacy boolean conversion
  if (rawScenario.fabric.global_TMP === 1) {
    rawScenario.fabric.global_TMP = true;
  }
  if (rawScenario.fabric.global_TMP === undefined) {
    rawScenario.fabric.global_TMP = false;
  }

  // New code doesn't keep around old value if not in use
  if (rawScenario.fabric.global_TMP === false) {
    delete rawScenario.fabric.global_TMP_value;
  }

  if (typeof rawScenario.fabric.thermal_bridging_yvalue === 'string') {
    rawScenario.fabric.thermal_bridging_yvalue = parseFloat(
      rawScenario.fabric.thermal_bridging_yvalue,
    );
  }

  if (
    rawScenario.fabric.thermal_bridging_yvalue === 0.15 &&
    rawScenario.fabric.thermal_bridging_override !== true
  ) {
    delete rawScenario.fabric.thermal_bridging_yvalue;
  }

  const shouldBeLoft = new Set<string | number>();
  const shouldBeRoof = new Set<string | number>();

  for (const [id, measure] of Object.entries(rawScenario.fabric.measures)) {
    const measureAny = measure as any;

    const measureId = parseInt(id, 10);

    // Unnecessary, it's in the key
    delete measureAny.measure.id;

    if (measureAny.measure.min_cost === '' || measureAny.measure.min_cost === undefined) {
      measureAny.measure.min_cost = 0;
    } else {
      measureAny.measure.min_cost = parseInt(measureAny.measure.min_cost, 10);
    }
    measureAny.measure.cost = parseInt(measureAny.measure.cost, 10);

    // We don't care about quantity/cost_total changing anymore
    delete measureAny.measure.cost_total;
    delete measureAny.measure.quantity;

    // 'lib' is where we store the ID of the library item, but some measures also
    // have a tag, which is out of date (often the tag of the original element).
    delete measureAny.measure.tag;

    if (
      measureAny.measure.type === 'Window' ||
      measureAny.measure.type === 'Door' ||
      measureAny.measure.type === 'Roof_light' ||
      measureAny.measure.type === 'window' ||
      measureAny.measure.type === 'door' ||
      measureAny.measure.type === 'roof light'
    ) {
      if (!('ff' in measureAny.measure) || measureAny.measure.ff === '') {
        measureAny.measure.ff = 0;
      } else if (typeof measureAny.measure.ff === 'string') {
        measureAny.measure.ff = parseFloat(measureAny.measure.ff);
      }
      if (!('g' in measureAny.measure) || measureAny.measure.g === '') {
        measureAny.measure.g = 0;
      } else if (typeof measureAny.measure.g === 'string') {
        measureAny.measure.g = parseFloat(measureAny.measure.g);
      }
      if (!('gL' in measureAny.measure) || measureAny.measure.gL === '') {
        measureAny.measure.gL = 0;
      } else if (typeof measureAny.measure.gL === 'string') {
        measureAny.measure.gL = parseFloat(measureAny.measure.gL);
      }
    } else {
      delete measureAny.measure.ff;
      delete measureAny.measure.g;
      delete measureAny.measure.gL;
    }

    // Measures don't contain the results of calculations or anything that the user
    // enters directly
    delete measureAny.measure.area;
    delete measureAny.measure.h;
    delete measureAny.measure.l;
    delete measureAny.measure.netarea;
    delete measureAny.measure.windowarea;
    delete measureAny.measure.wk;

    // Lofts are often stored wrongly as roofs
    if (measureAny.measure.type === 'Roof' && measureAny.measure.tags[0] === 'Loft') {
      shouldBeLoft.add(measureId);
      measureAny.measure.type = 'Loft';
    } else if (
      measureAny.measure.type === 'Loft' &&
      measureAny.measure.tags[0] === 'Roof'
    ) {
      shouldBeRoof.add(measureId);
      measureAny.measure.type = 'Roof';
    }

    // We give all wall measures the EWI flag to simplify the schema
    if (
      measureAny.measure.type === 'Wall' ||
      measureAny.measure.type === 'Loft' ||
      measureAny.measure.type === 'Roof' ||
      measureAny.measure.type === 'Party_wall'
    ) {
      measureAny.measure.EWI = measureAny.measure.EWI ?? false;

      // cost units should always be sqm - that they aren't was a mistake in the
      // library. This makes no impact on the costs calculated, but it will if they
      // are recalculated in future.
      measureAny.measure.cost_units = 'sqm';
    } else {
      delete measureAny.measure.EWI;
    }

    // We don't make use of this, because we get the 'original' element from the
    // parent scenario
    delete measureAny.original_element;

    // Occurs on 3 assessments, not sure why, but it's unused.
    delete measureAny.measure.scenario;

    // Corrections are often made here
    delete measureAny.measure.location;

    // original_elements has to be much simpler, with most data erased
    if ('original_elements' in measureAny) {
      // Normalise to only have ids
      measureAny.original_elements = Object.fromEntries(
        Object.entries(measureAny.original_elements).map(([k, v]) => [
          k,
          { id: (v as any).id },
        ]),
      );
    }
  }

  // Prune away all measures with no elements they're applied to
  for (const key in rawScenario.fabric.measures) {
    if (
      Object.keys(rawScenario?.fabric.measures[key]?.original_elements ?? {}).length === 0
    ) {
      delete rawScenario.fabric.measures[key];
    }
  }

  for (const element of rawScenario.fabric.elements) {
    const elementAny = element as any;

    elementAny.kvalue = parseFloat(elementAny.kvalue);
    if (elementAny.cost === '') {
      delete elementAny.cost;
    } else {
      elementAny.cost = parseFloat(elementAny.cost);
    }

    elementAny.location = elementAny.location.toString();

    // Output shouldn't include calculated fields
    delete elementAny.netarea;
    delete elementAny.windowarea;
    delete elementAny.wk;
    delete elementAny.gain;

    // 'l' and 'h' are legacy and we use areaInputs now
    delete elementAny.l;
    delete elementAny.h;

    if (
      element.type === 'Window' ||
      element.type === 'Door' ||
      element.type === 'Roof_light' ||
      element.type === 'window' ||
      element.type === 'door' ||
      element.type === 'roof light'
    ) {
      if (!('ff' in elementAny) || elementAny.ff === '') {
        elementAny.ff = 0;
      } else if (typeof elementAny.ff === 'string') {
        elementAny.ff = parseFloat(elementAny.ff);
      }
      if (!('g' in elementAny) || elementAny.g === '') {
        elementAny.g = 0;
      } else if (typeof elementAny.g === 'string') {
        elementAny.g = parseFloat(elementAny.g);
      }
      if (!('gL' in elementAny) || elementAny.gL === '') {
        elementAny.gL = 0;
      } else if (typeof elementAny.gL === 'string') {
        elementAny.gL = parseFloat(elementAny.gL);
      }
    } else if (element.type === 'hatch' || element.type === 'Hatch') {
      delete elementAny.orientation;
      delete elementAny.overshading;
      delete elementAny.ff;
      delete elementAny.g;
      delete elementAny.gL;
    } else {
      delete elementAny.ff;
      delete elementAny.g;
      delete elementAny.gL;
    }

    if (
      element.type === 'Window' ||
      element.type === 'Door' ||
      element.type === 'Roof_light' ||
      element.type === 'Hatch' ||
      element.type === 'window' ||
      element.type === 'door' ||
      element.type === 'roof light' ||
      element.type === 'hatch'
    ) {
      if (!Number.isNaN(parseInt(elementAny.subtractfrom, 10))) {
        elementAny.subtractfrom = parseInt(elementAny.subtractfrom, 10);
      }
    }

    // Non-measures shouldn't have these fields
    if (!(elementAny.id in rawScenario.fabric.measures)) {
      delete elementAny.associated_work;
      delete elementAny.benefits;
      delete elementAny.cost;
      delete elementAny.disruption;
      delete elementAny.EWI;
      delete elementAny.maintenance;
      delete elementAny.notes;
      delete elementAny.performance;
      delete elementAny.quantity;
      delete elementAny.who_by;
      delete elementAny.cost_total;
    }

    // Some roofs and lofts have contradictory data.  The correct data source is
    // the 'tag' field from the measures data.
    if (shouldBeLoft.has(element.id)) {
      elementAny.tags = ['Loft'];
      elementAny.type = 'Loft';
    } else if (shouldBeRoof.has(element.id)) {
      elementAny.tags = ['Roof'];
      elementAny.type = 'Roof';
    }

    // floors with old suspended floor version can get their stuff deleted here bc
    // it can't be output in the same format.
    if (
      elementAny.type === 'Floor' &&
      'perFloorTypeSpec' in elementAny &&
      'suspended' in elementAny.perFloorTypeSpec &&
      elementAny.perFloorTypeSpec?.suspended?.version !== 'v2'
    ) {
      delete elementAny.perFloorTypeSpec.suspended;
    }

    if (
      elementAny.type === 'Floor' &&
      'perFloorTypeSpec' in elementAny &&
      'exposed' in elementAny.perFloorTypeSpec &&
      'insulation' in elementAny.perFloorTypeSpec.exposed &&
      elementAny.perFloorTypeSpec?.exposed?.insulation !== 'v2'
    ) {
      delete elementAny.perFloorTypeSpec.exposed.insulation;
    }

    if (Number.isNaN(elementAny.kvalue)) {
      elementAny.kvalue = 0;
    }

    if (!('tags' in elementAny) || elementAny.tags.length === 0) {
      // Tags are only present on measures in the legacy system; to simplify the
      // output logic, we now always include tags as well as types
      elementAny.tags = [elementAny.type];
    }
  }

  rawScenario.fabric.elements = rawScenario.fabric.elements
    .filter((n) => n)
    .sort((a, b) => {
      if (a.id < b.id) {
        return -1;
      } else if (a.id > b.id) {
        return 1;
      }
      // a must be equal to b
      return 0;
    });
}

function normaliseOutputScenario(data: z.input<typeof scenarioSchema>) {
  const sourceForId: Record<string, string> = {};

  for (const [, measure] of Object.entries(data?.fabric?.measures ?? {})) {
    const measureAny = measure as any;

    sourceForId[measureAny.measure.id] = measureAny.measure.source;
  }

  for (const element of Object.values(data?.fabric?.elements ?? [])) {
    const newSource = sourceForId[element.id];
    if (newSource !== undefined) {
      element.source = newSource;
    }
  }

  data = data ?? {};
  data.fabric = data.fabric ?? {};
  data.fabric.elements = (data?.fabric?.elements ?? [])
    .filter((n) => n)
    .sort((a, b) => {
      if (a.id < b.id) {
        return -1;
      } else if (a.id > b.id) {
        return 1;
      }
      // a must be equal to b
      return 0;
    });
}

describe('fabric page extractor & mutator round trip should roundtrip the data adequately', () => {
  test.each(projects)('$fixturePath', (project) => {
    const scenarios = project.parsedData.data;

    const modifiedProject = cloneDeep(project.rawData);
    for (const [currentScenarioId, currentScenario] of Object.entries(scenarios)) {
      if (currentScenario === undefined) {
        throw new Error('scenario was unexpectedly undefined');
      }
      let state = fabricModule.initialState('');

      const updateActions = fabricModule.shims
        .extractUpdateAction(
          {
            project: project.parsedData,
            currentScenario,
            scenarioId: currentScenarioId,
            currentModel: Result.err(new Error('no model')),
            route: {
              type: 'with scenario',
              scenarioId: currentScenarioId,
              page: 'elements',
            },
            appName: 'some app name',
            userId: '0',
          },
          '',
          { inputs: true, outputs: true },
        )
        .unwrap();
      expect(updateActions).toHaveLength(1);
      [state] = fabricModule.reducer(state, updateActions[0]!);

      fabricModule.shims.mutateLegacyData(
        { project: modifiedProject },
        { scenarioId: currentScenarioId },
        state,
        '',
      );

      normaliseRawScenario(project.rawData as any, currentScenarioId);
    }

    for (const [currentScenarioId, currentScenario] of Object.entries(
      modifiedProject.data,
    )) {
      normaliseOutputScenario(currentScenario);

      expect(
        emulateJsonRoundTrip({ [currentScenarioId]: (currentScenario as any)?.fabric }),
      ).toEqualBy(
        {
          [currentScenarioId]: (project.rawData as any).data[currentScenarioId]?.fabric,
        },
        isVariationAcceptable(),
      );
    }

    // Run the model and ensure the outputs for both raw & modified are the same
    // This essentially checks that 'isVariationAcceptable' flags up the right
    // changes.
    // DISABLED: too CPU-intensive for normal runs.
    /*
        import { calcRun } from '../../src/model/model';

        for (const [, scenario] of Object.entries((project.rawData as any).data)) {
            calcRun(scenario);
        }
        for (const [, scenario] of Object.entries((modifiedProject as any).data)) {
            calcRun(scenario);
        }

        expect(modifiedProject).toEqualBy(
            project.rawData,
            isVariationAcceptable(),
        );
        */
  });
});
