/**
 * @jest-environment jsdom
 */

import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import React from 'react';
import { NumberOutput } from '../../../src/ui/output-components/numeric';

describe('NumberOutput', () => {
  test('renders - when empty', async () => {
    render(<NumberOutput />);

    const items = await screen.findAllByText(/-/);
    expect(items).toHaveLength(1);
  });

  test('renders number & units', async () => {
    render(<NumberOutput value={50} unit="rels" />);

    const items = await screen.findAllByText(/50 rels/);
    expect(items).toHaveLength(1);
  });

  test('respects dp argument', async () => {
    render(<NumberOutput value={1.46377} dp={2} />);

    const items = await screen.findAllByText(/1.46/);
    expect(items).toHaveLength(1);
  });

  test('formats large numbers with commas', async () => {
    render(<NumberOutput value={1300200} />);

    const items = await screen.findAllByText(/1,300,200/);
    expect(items).toHaveLength(1);
  });
});
