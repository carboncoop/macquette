import fc from 'fast-check';
import { cloneDeep, omit } from 'lodash';
import assert from 'node:assert';
import {
  Action,
  dimensionsAndOccupancy,
  LoadedState,
  ordinalNumberSuffix,
  State,
} from '../../src/ui/modules/dimensions-and-occupancy';

function arbLoadedState(): fc.Arbitrary<LoadedState> {
  return fc.record({
    isLocked: fc.boolean(),
    model: fc.constant(null),
    scenarioName: fc.string(),
    floors: fc.array(
      fc.record({
        name: fc.string(),
        area: fc.double(),
        height: fc.double(),
      }),
    ),
    occupancyOverride: fc.record({
      enabled: fc.boolean(),
      value: fc.option(fc.double()),
    }),
    volumeReductionOverride: fc.record({
      enabled: fc.boolean(),
      value: fc.option(fc.double()),
    }),
  });
}

function arbSetFloorDataAction(
  numberOfFloors: number,
): fc.Arbitrary<Extract<Action, { type: 'set floor data' }>> {
  return fc.record({
    type: fc.constant('set floor data' as const),
    index: fc.integer({ min: 0, max: numberOfFloors - 1 }),
    floorData: fc.record(
      {
        name: fc.string(),
        area: fc.double(),
        height: fc.double(),
      },
      { withDeletedKeys: true },
    ),
  });
}

describe('dimensions and occupancy', () => {
  describe('reducer', () => {
    test('floor mutation actions do not mutate floors other than the specified floor', () => {
      const arb = arbLoadedState()
        .filter((state) => state.floors.length > 0)
        .chain((state) =>
          fc.record({
            state: fc.constant(state),
            action: arbSetFloorDataAction(state.floors.length),
          }),
        );
      fc.assert(
        fc.property(arb, ({ state, action }) => {
          const [newState] = dimensionsAndOccupancy.reducer(state, action);
          assert(newState !== 'loading');
          expect(omit(newState, 'floors')).toEqual(omit(state, 'floors'));
          function nullifyFloorAt(index: number, state: Exclude<State, 'loading'>) {
            const out: any = cloneDeep(state);
            out.floors[index] = null;
            return out;
          }
          expect(nullifyFloorAt(action.index, newState)).toEqual(
            nullifyFloorAt(action.index, state),
          );
        }),
      );
    });
    test('floor deletion action affects only the specified floor', () => {
      const arb = arbLoadedState()
        .filter((state) => state.floors.length > 0)
        .chain((state) =>
          fc.record({
            state: fc.constant(state),
            action: fc.record({
              type: fc.constant('delete floor'),
              index: fc.integer({ min: 0, max: state.floors.length - 1 }),
            }) satisfies fc.Arbitrary<Action>,
          }),
        );
      fc.assert(
        fc.property(arb, ({ state, action }) => {
          const [newState] = dimensionsAndOccupancy.reducer(state, action);
          assert(newState !== 'loading');
          expect(omit(newState, 'floors')).toEqual(omit(state, 'floors'));
          expect(newState.floors).toHaveLength(state.floors.length - 1);
          /* eslint-disable jest/no-conditional-expect */
          for (let i = 0; i < state.floors.length; i++) {
            if (i < action.index) {
              expect(newState.floors[i]).toEqual(state.floors[i]);
            } else {
              expect(newState.floors[i]).toEqual(state.floors[i + 1]);
            }
          }
          /* eslint-enable */
        }),
      );
    });
    test('floor addition does not affect prior floors', () => {
      fc.assert(
        fc.property(arbLoadedState(), (state) => {
          const [newState] = dimensionsAndOccupancy.reducer(state, { type: 'add floor' });
          assert(newState !== 'loading');
          expect(newState.floors).toHaveLength(state.floors.length + 1);
          expect(newState.floors.slice(0, state.floors.length)).toEqual(state.floors);
        }),
      );
    });
  });
  describe('ordinal number suffixes', () => {
    test.each([
      { n: 0, suffix: 'th' },
      { n: 1, suffix: 'st' },
      { n: 2, suffix: 'nd' },
      { n: 3, suffix: 'rd' },
      { n: 4, suffix: 'th' },
      { n: 10, suffix: 'th' },
      { n: 11, suffix: 'th' },
      { n: 12, suffix: 'th' },
      { n: 13, suffix: 'th' },
      { n: 14, suffix: 'th' },
      { n: 20, suffix: 'th' },
      { n: 21, suffix: 'st' },
      { n: 22, suffix: 'nd' },
      { n: 23, suffix: 'rd' },
      { n: 24, suffix: 'th' },
      { n: 3.141, suffix: 'th' },
      { n: -2, suffix: 'nd' },
      { n: 11.1, suffix: 'th' },
    ])('ordinal number suffix: $n$suffix', ({ n, suffix }) => {
      expect(ordinalNumberSuffix(n)).toBe(suffix);
    });
  });
});
