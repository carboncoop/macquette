import { scenarioSchema } from '../../../../src/data-schemas/scenario';
import { sum } from '../../../../src/helpers/array-reducers';
import { Model } from '../../../../src/model/model';
import { energyUseIntensity } from '../../../../src/ui/modules/reports/graphs';

const input = {
  modelBehaviourVersion: 11,
  fuels: {
    'Standard Tariff': {
      category: 'Electricity',
      standingcharge: 220.04,
      fuelcost: 22.36,
      co2factor: 0.136,
      primaryenergyfactor: 1.501,
      SAP_code: 30,
      notes: 'OfGEM price cap 1 January 2024-31 March 2024',
      updatedAt: '2024-04-11',
    },
    generation: {
      category: 'Generation',
      standingcharge: 0,
      fuelcost: 22.36,
      co2factor: 0.136,
      primaryenergyfactor: 1.501,
      SAP_code: 0,
      notes: 'OfGEM price cap 1 January 2024-31 March 2024',
      updatedAt: '2024-02-08',
    },
  },
  heating_systems: [
    {
      category: 'Heat pumps',
      combi_loss: '0',
      sfp: null,
      responsiveness: 1,
      summer_efficiency: 180,
      winter_efficiency: 310,
      central_heating_pump: 120,
      primary_circuit_loss: 'No',
      fans_and_supply_pumps: 0,
      tag: 'HP001',
      name: 'heat pump',
      source: '',
      id: 1,
      fuel: 'Standard Tariff',
      fraction_space: 1,
      fraction_water_heating: 1,
      main_space_heating_system: 'secondaryHS',
      temperature_adjustment: 0,
      provides: 'heating_and_water',
      instantaneous_water_heating: false,
      heating_controls: 1,
      efficiency: 2.87260072513611,
    },
  ],
  floors: [
    {
      name: 'Ground Floor',
      area: 50,
      height: 3,
      volume: 150,
    },
  ],
  fabric: {
    elements: [
      {
        id: 1,
        lib: 'CW01',
        name: 'Unfilled cavity masonry Wall (50-100mm cavity)',
        source: 'SAP table S6, p.131/ SAP table 1e, p.195',
        description: '',
        uvalue: 1.5,
        kvalue: 110,
        location: 'The whole building',
        type: 'Wall',
        tags: ['Wall'],
        area: 200,
      },
    ],
  },
};

test('energy use intensity outputs', () => {
  const scenario = scenarioSchema.parse(input);
  const model = Model.fromLegacy(input).unwrap();
  const graph = energyUseIntensity([{ id: 'scenario', scenario: scenario!, model }]);
  const totalEUI = sum(graph.bins[1]!.data);

  expect(model.normal.fuelRequirements.totalEnergyAnnualPerArea).toBeApproximately(
    totalEUI,
  );
});
