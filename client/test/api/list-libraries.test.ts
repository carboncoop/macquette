import nock from 'nock';
import { z } from 'zod';
import { HTTPClient } from '../../src/api/http';
import { libraryMetadataSchema } from '../../src/data-schemas/api-metadata';
import { librarySchema } from '../../src/data-schemas/libraries';
import { floor } from '../../src/data-schemas/libraries/fabric-floors';

const baseURL = 'https://example.com';

type LibraryMetadataInput = z.input<typeof libraryMetadataSchema>;
type LibraryInput = z.input<typeof librarySchema>;
const exampleMetadata: Omit<LibraryMetadataInput, 'type'> = {
  id: '',
  name: '',
  created_at: '2000-01-01T00:00:00Z',
  updated_at: '2000-01-01T00:00:00Z',
  permissions: {
    can_write: true,
    can_share: true,
  },
  owner: {
    type: 'global',
    id: null,
    name: '',
  },
  shared_with: [],
};

describe('list libraries', () => {
  afterEach(() => {
    jest.restoreAllMocks();
    nock.cleanAll();
  });
  test('it hits the right endpoint', async () => {
    const scope = nock(baseURL).get('/api/libraries/').reply(200, []);
    const client = new HTTPClient({ sessionId: '', baseURL });
    const response = await client.listLibraries();
    expect(response).toStrictEqual([]);
    scope.done();
  });

  test('when a library element is invalid, it returns the library without the invalid element', async () => {
    const consoleErrorSpy = jest
      .spyOn(global.console, 'error')
      .mockImplementation(() => undefined);
    type Element = z.input<typeof floor>;
    const goodElement: any = {
      source: '',
      description: '',
      uValue: 0,
      kValue: 0,
      tag: '',
      name: '',
    } satisfies Element;
    const badElement: any = { ...goodElement, uValue: 'not_a_number' };
    expect(() => floor.parse(badElement)).toThrow();
    const scope = nock(baseURL)
      .get('/api/libraries/')
      .reply(200, [
        {
          ...exampleMetadata,
          type: 'floors',
          id: '',
          name: '',
          data: {
            good: goodElement,
            bad: badElement,
          },
        } satisfies LibraryInput,
      ]);
    const client = new HTTPClient({ sessionId: '', baseURL });
    const libraries = await client.listLibraries();
    expect(consoleErrorSpy).toHaveBeenCalledTimes(1);
    const elements = libraries.find(({ type }) => type === 'floors');
    expect(elements).toBeDefined();
    const libraryItems = Object.values(elements!.data);
    expect(libraryItems).toStrictEqual([floor.parse(goodElement)]);
    scope.done();
  });
});
