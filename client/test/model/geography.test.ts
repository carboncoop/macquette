import fc from 'fast-check';
import { Geography, GeographyInput } from '../../src/model/modules/geography';
import { sensibleFloat } from '../arbitraries/legacy-values';

describe('test design temperature calculation', () => {
  it('nearest weather station to Stockport should be Manchester', () => {
    const geography = new Geography({
      latLong: [53.41, -2.157],
      elevation: null,
      override: null,
    });
    const station = geography.nearestWeatherStation;
    expect(station?.city).toBe('Manchester');
  });

  it('nearest weather station to Aberdeen should be Edinburgh', () => {
    const geography = new Geography({
      latLong: [57.128, -2.086],
      elevation: null,
      override: null,
    });
    const station = geography.nearestWeatherStation;
    expect(station?.city).toBe('Edinburgh');
  });

  it('cibseExternalDesignTemperature should be 0.6deg above Manchester for High Crompton, Oldham', () => {
    const geography = new Geography({
      latLong: [53.583, -2.108],
      elevation: 207,
      override: null,
    });
    expect(geography.cibseExternalDesignTemperature).toBe(-4.5 - 0.6);
  });

  it('cibseExternalDesignTemperature should be -4 if no coordinates', () => {
    const geography = new Geography({
      latLong: null,
      elevation: null,
      override: null,
    });
    expect(geography.cibseExternalDesignTemperature).toBe(-4);
  });

  test('externalDesignTemperature should take value of override if provided', () => {
    const geography = new Geography({
      latLong: null,
      elevation: null,
      override: -15,
    });
    expect(geography.externalDesignTemperature).toBe(-15);
  });

  test('external design temperature should never be higher than closest weather station temperature, when all inputs are defined', () => {
    const arbDefinedInput: fc.Arbitrary<GeographyInput> = fc.record({
      elevation: sensibleFloat,
      latLong: fc.tuple(
        sensibleFloat.filter((lat) => lat >= -180 && lat <= 180),
        sensibleFloat.filter((long) => long >= -180 && long <= 180),
      ),
      override: fc.oneof(fc.float(), fc.constant(null)),
    });
    fc.assert(
      fc.property(arbDefinedInput, (input) => {
        const geography = new Geography(input);
        expect(geography.cibseExternalDesignTemperature).toBeLessThanOrEqual(
          geography.nearestWeatherStation!.minTemp,
        );
      }),
    );
  });
});
