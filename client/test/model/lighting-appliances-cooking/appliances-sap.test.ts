import fc from 'fast-check';
import { cloneDeep } from 'lodash';
import { scenarioSchema } from '../../../src/data-schemas/scenario';
import { Month } from '../../../src/model/enums/month';
import { FuelName } from '../../../src/model/modules/fuels';
import {
  AppliancesSAP,
  AppliancesSAPDependencies,
  AppliancesSAPInput,
  extractAppliancesSAPInputFromLegacy,
} from '../../../src/model/modules/lighting-appliances-cooking/sap/appliances';
import { sensibleFloat } from '../../arbitraries/legacy-values';
import { ArbParam, resolveArbParam } from '../../helpers/arbitraries';
import { legacyLightingAppliancesCooking } from '../golden-master/lighting-appliances-cooking';

function arbAppliancesSAPInput(opts?: {
  enabled?: ArbParam<boolean>;
}): fc.Arbitrary<AppliancesSAPInput> {
  return fc.record({
    enabled: resolveArbParam(opts?.enabled, fc.boolean()),
    energyEfficient: fc.boolean(),
    fuelFractions: fc.dictionary(fc.string(), sensibleFloat),
  });
}

type TestDependencies = Omit<AppliancesSAPDependencies, 'modelBehaviourFlags'>;
function arbAppliancesSAPDependencies(): fc.Arbitrary<TestDependencies> {
  return fc.record({
    fuels: fc.record({
      names: fc.array(fc.string()),
    }),
    floors: fc.record({
      totalFloorArea: sensibleFloat.filter((value) => value >= 0),
    }),
    occupancy: fc.record({
      occupancy: sensibleFloat.filter((value) => value >= 0),
    }),
  });
}

const arbModelBehaviourFlags: fc.Arbitrary<
  AppliancesSAPDependencies['modelBehaviourFlags']
> = fc.record({
  appliancesSap: fc.record({
    applyEfficiencyReductionForEnergyMonthly: fc.boolean(),
  }),
});

function makeLegacyDataForAppliancesSAP(
  input: AppliancesSAPInput,
  dependencies: TestDependencies,
) {
  const fuelFractionsEntries = Object.entries(input.fuelFractions);
  let fuels_appliances: any[] | undefined;
  if (fuelFractionsEntries.length === 0) {
    fuels_appliances = undefined;
  } else {
    fuels_appliances = fuelFractionsEntries.map(([fuelName, fraction]) => ({
      fuel: fuelName,
      fraction,
    }));
  }
  return {
    occupancy: dependencies.occupancy.occupancy,
    TFA: dependencies.floors.totalFloorArea,
    LAC_calculation_type: input.enabled ? 'SAP' : 'carboncoop_SAPlighting',
    LAC: {
      energy_efficient_appliances: input.energyEfficient,
      fuels_appliances,
    },
    fuel_requirements: {
      cooking: { quantity: 0 },
      lighting: { quantity: 0 },
      appliances: { quantity: 0 },
    },
    gains_W: { Appliances: new Array(12).fill(0) },
    energy_requirements: {
      appliances: { monthly: new Array(12).fill(0) },
    },
    fuels: {},
  };
}

describe('appliances SAP', () => {
  test('golden master', () => {
    function areDependenciesCompatibleWithLegacy(dependencies: TestDependencies) {
      return dependencies.floors.totalFloorArea * dependencies.occupancy.occupancy > 0;
    }
    fc.assert(
      fc.property(
        arbAppliancesSAPInput({ enabled: true }),
        arbAppliancesSAPDependencies().filter(areDependenciesCompatibleWithLegacy),
        (input, dependencies_) => {
          const dependencies: AppliancesSAPDependencies = {
            ...cloneDeep(dependencies_),
            modelBehaviourFlags: {
              appliancesSap: { applyEfficiencyReductionForEnergyMonthly: false },
            },
          };
          dependencies.fuels.names.push(...Object.keys(input.fuelFractions));
          const appliancesSAP = new AppliancesSAP(input, dependencies);
          const legacyData: any = makeLegacyDataForAppliancesSAP(input, dependencies);
          legacyLightingAppliancesCooking(legacyData);
          expect(legacyData.LAC.EA).toBeApproximately(appliancesSAP.energyAnnual);
          for (const month of Month.all) {
            expect(legacyData.gains_W.Appliances[month.index0]).toBeApproximately(
              appliancesSAP.heatGain(month),
            );
            expect(
              legacyData.energy_requirements.appliances.monthly[month.index0],
            ).toBeApproximately(appliancesSAP.energyMonthly(month));
          }
          const fuelsNotCounted = new Set<FuelName>(Object.keys(input.fuelFractions));
          for (const legacyFuel of legacyData.LAC.fuels_appliances) {
            expect(
              appliancesSAP.fuelDemandByFuelAnnual.get(legacyFuel.fuel)?.energyDemand,
            ).toBeApproximately(legacyFuel.demand);
            fuelsNotCounted.delete(legacyFuel.fuel);
          }
          expect(fuelsNotCounted).toEqual(new Set());
          expect(legacyData.fuel_requirements.appliances.quantity).toBeApproximately(
            appliancesSAP.totalFuelRequirement,
          );
        },
      ),
    );
  });

  test('extractor', () => {
    fc.assert(
      fc.property(
        arbAppliancesSAPInput(),
        arbAppliancesSAPDependencies(),
        (input, dependencies) => {
          const roundTripped = extractAppliancesSAPInputFromLegacy(
            scenarioSchema.parse(makeLegacyDataForAppliancesSAP(input, dependencies)),
          );
          expect(roundTripped).toEqual(input);
        },
      ),
    );
  });

  test('when module is disabled, everything returns 0', () => {
    fc.assert(
      fc.property(
        fc.constantFrom(...Month.all),
        arbAppliancesSAPInput({ enabled: false }),
        arbAppliancesSAPDependencies(),
        arbModelBehaviourFlags,
        (month, input, dependencies_, modelBehaviourFlags) => {
          const dependencies: AppliancesSAPDependencies = {
            ...cloneDeep(dependencies_),
            modelBehaviourFlags,
          };
          dependencies.fuels.names.push(...Object.keys(input.fuelFractions));
          const appliancesSAP = new AppliancesSAP(input, dependencies);
          expect(appliancesSAP.heatGain(month)).toBe(0);
          for (const [, { fuelInput }] of appliancesSAP.fuelDemandByFuelAnnual) {
            expect(Math.abs(fuelInput)).toBe(0);
          }
          expect(appliancesSAP.totalFuelRequirement).toBe(0);
        },
      ),
    );
  });
});
