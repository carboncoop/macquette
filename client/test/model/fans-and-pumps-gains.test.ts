import fc from 'fast-check';
import { cloneDeep, mean, merge } from 'lodash';
import { scenarioSchema } from '../../src/data-schemas/scenario';
import { Month } from '../../src/model/enums/month';
import {
  HeatingSystemFansAndPumpsCalculator,
  HeatingSystemFansAndPumpsDependencies,
  HeatingSystemFansAndPumpsParameters,
  extractHeatingSystemFansAndPumpsParameters,
  heatingSystemFansAndPumpsParameters,
} from '../../src/model/modules/heating-systems/fans-and-pumps';
import { FansAndPumpsGains } from '../../src/model/modules/internal-gains/fans-and-pumps';
import {
  VentilationInfiltrationCommon,
  VentilationInfiltrationCommonInput,
} from '../../src/model/modules/ventilation-infiltration/common-input';
import {
  Ventilation,
  VentilationInput,
} from '../../src/model/modules/ventilation-infiltration/ventilation';
import { sensibleFloat } from '../arbitraries/legacy-values';
import { makeArbitrary } from '../helpers/make-arbitrary';
import { makeLegacyHeatingSystemsForFansAndPumps } from './fans-and-pumps-gains/arbitraries';
import { legacyMetabolicLossesFansAndPumpsGains } from './golden-master/metabolic-losses-fans-and-pumps-gains';
import {
  VentilationInfiltrationTestDependencies,
  arbCommonInput as arbVentilationInfiltrationCommonInput,
  arbDependencies as arbVentilationInfiltrationTestDependencies,
  arbVentilationInput,
  makeLegacyDataForVentilation,
} from './ventilation-infiltration-arbitraries';
import { withSuppressedConsoleWarn } from './with-suppressed-console-warn';

const arbHeatingSystemFansAndPumpsInput = makeArbitrary(
  heatingSystemFansAndPumpsParameters,
);

type GoldenMasterStuff = {
  heatingSystemFansAndPumpsInput: HeatingSystemFansAndPumpsParameters[];
  ventilationInfiltrationCommonInput: VentilationInfiltrationCommonInput;
  ventilationInput: VentilationInput;
  ventilationInfiltrationTestDependencies: VentilationInfiltrationTestDependencies;
};

function arbGoldenMaster(): fc.Arbitrary<GoldenMasterStuff & { legacyData: unknown }> {
  return fc
    .record({
      heatingSystemFansAndPumpsInput: fc.array(arbHeatingSystemFansAndPumpsInput),
      ventilationInfiltrationCommonInput: arbVentilationInfiltrationCommonInput,
      ventilationInput: arbVentilationInput,
      ventilationInfiltrationTestDependencies: arbVentilationInfiltrationTestDependencies,
    })
    .chain((goldenMasterStuff) =>
      makeLegacyDataForFansAndPumpsGains(goldenMasterStuff).map((legacyData) => ({
        ...goldenMasterStuff,
        legacyData,
      })),
    );
}

function makeLegacyDataForFansAndPumpsGains({
  heatingSystemFansAndPumpsInput,
  ventilationInfiltrationCommonInput,
  ventilationInput,
  ventilationInfiltrationTestDependencies,
}: GoldenMasterStuff): fc.Arbitrary<any> {
  return fc
    .tuple(
      makeLegacyHeatingSystemsForFansAndPumps(heatingSystemFansAndPumpsInput),
      makeLegacyDataForVentilation(
        ventilationInfiltrationCommonInput,
        ventilationInput,
        ventilationInfiltrationTestDependencies,
      ),
    )
    .map(([{ heating_systems, Vc }, legacyDataForVentilation]) =>
      merge(
        {
          gains_W: {},
          volume: ventilationInfiltrationTestDependencies.floors.totalVolume,
          Vc,
        },
        legacyDataForVentilation,
        { heating_systems },
      ),
    );
}

describe('fans and pumps gains', () => {
  test('golden master', () => {
    fc.assert(
      fc.property(
        arbGoldenMaster(),
        ({
          heatingSystemFansAndPumpsInput,
          ventilationInfiltrationCommonInput,
          ventilationInput,
          ventilationInfiltrationTestDependencies,
          legacyData: legacyData_,
        }) => {
          fc.pre(
            heatingSystemFansAndPumpsInput.some(
              (system) =>
                system.type === 'warm air system' &&
                system.specificFanPower !== null &&
                system.specificFanPower !== 0,
            ),
          );
          const common = new VentilationInfiltrationCommon(
            ventilationInfiltrationCommonInput,
            ventilationInfiltrationTestDependencies,
          );
          const ventilation = new Ventilation(ventilationInput, {
            ...ventilationInfiltrationTestDependencies,
            ventilationInfiltrationCommon: common,
          });
          const heatingSystemsFansAndPumps = heatingSystemFansAndPumpsInput.map(
            (input) =>
              new HeatingSystemFansAndPumpsCalculator(input, {
                floors: ventilationInfiltrationTestDependencies.floors,
                modelBehaviourFlags: {
                  heatingSystems: {
                    fansAndPumps: {
                      fixUndefinedSpecificFanPowerInWarmAirSystems: false,
                      warmAirSystemsZeroGainInSummer: false,
                      allowFansPumpsEnergyToUseSpecificFanPower: false,
                      fixHeatGainsForWarmAirSystems: false,
                    },
                  },
                },
              }),
          );
          const fansAndPumpsGains = new FansAndPumpsGains(null, {
            heatingSystems: { fansAndPumps: heatingSystemsFansAndPumps },
            ventilation,
          });
          const legacyData: any = cloneDeep(legacyData_);
          legacyMetabolicLossesFansAndPumpsGains(legacyData);
          for (const month of Month.all) {
            expect(fansAndPumpsGains.heatGain(month)).toBeApproximately(
              legacyData.gains_W.fans_and_pumps[month.index0],
            );
          }
        },
      ),
    );
  });

  test('extractor', () => {
    withSuppressedConsoleWarn(() => {
      fc.assert(
        fc.property(
          arbGoldenMaster(),
          ({
            legacyData,
            heatingSystemFansAndPumpsInput: fansAndPumpsHeatingSystemInputs,
          }) => {
            const scenario = scenarioSchema.parse(legacyData);
            const roundTripped = (scenario?.heating_systems ?? []).map((system) =>
              extractHeatingSystemFansAndPumpsParameters(
                system,
                scenario?.water_heating?.Vc ?? null,
              ),
            );
            const expected = cloneDeep(fansAndPumpsHeatingSystemInputs);
            // We don't care about combi loss storage capacity for fans and pumps, and our
            // legacyData doesn't thread it through water_heating.Vc properly, so we ignore
            // it if it comes back different here.
            roundTripped.forEach(
              (system) =>
                system?.combiLoss?.type === 'storage' &&
                delete (system.combiLoss as any).capacity,
            );
            expected.forEach(
              (system) =>
                system?.combiLoss?.type === 'storage' &&
                delete (system.combiLoss as any).capacity,
            );
            expect(roundTripped).toEqual(expected);
          },
        ),
      );
    });
  });

  describe('warm air systems new flags', () => {
    const arb = fc.record({
      input: arbHeatingSystemFansAndPumpsInput.filter(
        (
          input: HeatingSystemFansAndPumpsParameters,
        ): input is Extract<
          HeatingSystemFansAndPumpsParameters,
          { type: 'warm air system' }
        > => input.type === 'warm air system',
      ),
      floors: fc.record({
        totalVolume: sensibleFloat,
      }),
      fixUndefinedSpecificFanPowerInWarmAirSystems: fc.boolean(),
    });
    const newFlags: Omit<
      HeatingSystemFansAndPumpsDependencies['modelBehaviourFlags']['heatingSystems']['fansAndPumps'],
      'fixUndefinedSpecificFanPowerInWarmAirSystems'
    > = {
      warmAirSystemsZeroGainInSummer: true,
      allowFansPumpsEnergyToUseSpecificFanPower: true,
      fixHeatGainsForWarmAirSystems: true,
    };

    test('warm air systems are counted towards gains using their SFP', () => {
      let sfpRatioOnce: number | null = null;
      fc.assert(
        fc.property(
          arb,
          ({ input, floors, fixUndefinedSpecificFanPowerInWarmAirSystems }) => {
            const expectedDefaultSFP = fixUndefinedSpecificFanPowerInWarmAirSystems
              ? 1.5
              : 0;
            const system = new HeatingSystemFansAndPumpsCalculator(input, {
              floors,
              modelBehaviourFlags: {
                heatingSystems: {
                  fansAndPumps: {
                    ...newFlags,
                    fixUndefinedSpecificFanPowerInWarmAirSystems,
                  },
                },
              },
            });
            const meanHeatGain = mean(Month.all.map((m) => system.heatGain(m)));
            const inputSfp = input.specificFanPower ?? expectedDefaultSFP;
            if (inputSfp * floors.totalVolume === 0) {
              // eslint-disable-next-line jest/no-conditional-expect
              expect(meanHeatGain).toBe(0);
            } else {
              if (sfpRatioOnce === null) {
                // First non-zero test, define the expected ratio for all the others
                sfpRatioOnce = meanHeatGain / (inputSfp * floors.totalVolume);
              } else {
                // Non-first non-zero test, we have the expected ratio ready
                // eslint-disable-next-line jest/no-conditional-expect
                expect(meanHeatGain / (inputSfp * floors.totalVolume)).toBeApproximately(
                  sfpRatioOnce,
                );
              }
            }
          },
        ),
      );
    });
  });
});
