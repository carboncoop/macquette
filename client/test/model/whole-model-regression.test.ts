import { cloneDeep, sumBy } from 'lodash';
import { coalesceEmptyString } from '../../src/data-schemas/scenario/value-schemas';
import { sum } from '../../src/helpers/array-reducers';
import { Month } from '../../src/model/enums/month';
import { Model } from '../../src/model/model';
import { scenarios } from '../fixtures';
import { legacyHoursOff } from './golden-master/hours-off-calculation';
import { calcRun } from './reference-model';
import { withSuppressedConsoleWarn } from './with-suppressed-console-warn';

const regressionTestScenarios = scenarios
  .filter(
    (s) => s.fixturePath !== 'private/171.json', // Bug in calculating heating hours
  )
  .filter((scenarioFixture) => {
    if (scenarioFixture.data === undefined) return false;
    if (
      // Skip explicitly non-legacy scenarios
      !(
        scenarioFixture.data?.modelBehaviourVersion === undefined ||
        scenarioFixture.data.modelBehaviourVersion === 'legacy'
      )
    ) {
      return false;
    }
    if (
      // Look for fabric elements with a "selectedFloorType" key indicating use of the new
      // FUVC.
      (scenarioFixture.data.fabric?.elements ?? []).some((element) => {
        return element.type === 'floor' && element.selectedFloorType !== undefined;
      })
    ) {
      return false;
    }
    if (
      typeof scenarioFixture.rawData?.region === 'string' &&
      Number.isNaN(parseInt(scenarioFixture.rawData.region))
    ) {
      // Skip scenarios that have the region name (rather than the region code) in the
      // .region property
      return false;
    }
    if ('version' in (scenarioFixture.rawData?.SHW ?? {})) {
      // Skip scenarios with new solar hot water calculations
      return false;
    }
    for (const legacyHeatingSystem of scenarioFixture.data?.heating_systems ?? []) {
      // Skip scenarios with heating systems that cause bugs in the heating systems fuel
      // calculations
      if (legacyHeatingSystem.provides === 'heating_and_water') {
        if (
          legacyHeatingSystem.fraction_space === 0 &&
          legacyHeatingSystem.fraction_water_heating === 0
        ) {
          return false;
        }
        if (legacyHeatingSystem.summer_efficiency === '') return false;
        if (legacyHeatingSystem.winter_efficiency === '') return false;
      }
    }
    for (const rawLegacyHeatingSystem of scenarioFixture.rawData?.heating_systems ?? []) {
      // Skip scenarios that cause bugs in the fans and pumps fuel calculations
      if (rawLegacyHeatingSystem.fans_and_supply_pumps === 'null') return false;
    }
    if (
      scenarioFixture.data?.ventilation?.ventilation_type === 'NV' &&
      (scenarioFixture.data?.ventilation.EVP?.length ?? 0) > 0
    ) {
      // Skip scenarios that cause bugs in the ventilation fuel calculations
      return false;
    }
    if (
      // eslint-disable-next-line eqeqeq
      scenarioFixture.rawData?.use_SHW == true &&
      ((scenarioFixture.rawData.SHW as any) ?? {}).pump === undefined
    ) {
      // Skip scenarios that cause bugs in the fans and pumps fuel calculations
      return false;
    }
    if (
      scenarioFixture.data?.floors === undefined ||
      scenarioFixture.data?.floors?.length === 0 ||
      scenarioFixture.data?.floors.some((floor) => floor.area === null) ||
      sumBy(
        scenarioFixture.data?.floors ?? [],
        (f) => coalesceEmptyString(f.area, 0) ?? 0,
      ) === 0
    ) {
      // Skip scenarios with no floor area
      return false;
    }

    return true;
  });

function runLegacyModel(scenario: any) {
  legacyHoursOff(scenario);
  calcRun(scenario);
  calcRun(scenario);
}

describe('whole model regression tests', () => {
  test.each(regressionTestScenarios)(
    'legacy scenario golden master: $displayName',
    (scenarioFixture) => {
      withSuppressedConsoleWarn(() => {
        const model = Model.fromLegacy(scenarioFixture.rawData).unwrap();
        const legacyData: any = cloneDeep(scenarioFixture.rawData);
        runLegacyModel(legacyData);
        for (const month of Month.all) {
          expect(model.normal.fabric.heatLoss).toBeApproximately(
            legacyData.losses_WK['fabric'][month.index0],
          );
          expect(model.normal.ventilation.heatLossMonthly(month)).toBeApproximately(
            legacyData.losses_WK['ventilation'][month.index0],
          );
          expect(model.normal.infiltration.heatLossMonthly(month)).toBeApproximately(
            legacyData.losses_WK['infiltration'][month.index0],
          );
          expect(model.normal.heatLoss.heatLossMonthly(month)).toBeApproximately(
            sum(
              Object.values(legacyData.losses_WK ?? {}).map(
                (losses: any) => losses[month.index0],
              ),
            ),
          );
          expect(model.normal.heatGain.heatGainSolar(month)).toBeApproximately(
            legacyData.gains_W['solar'][month.index0],
          );
          expect(model.normal.fansAndPumpsGains.heatGain(month)).toBeApproximately(
            legacyData.gains_W['fans_and_pumps'][month.index0],
          );
          expect(
            model.normal.miscellaneousInternalGains.metabolicHeatGainPower,
          ).toBeApproximately(legacyData.gains_W['metabolic'][month.index0]);
          expect(
            model.normal.miscellaneousInternalGains.miscellaneousHeatLossPower,
          ).toBeApproximately(legacyData.gains_W['losses'][month.index0]);
          expect(model.normal.lighting.heatGain(month)).toBeApproximately(
            legacyData.gains_W['Lighting']?.[month.index0] ?? 0,
          );
          expect(
            model.normal.appliancesSap.heatGain(month) +
              model.normal.cookingSap.heatGain +
              model.normal.appliancesCookingLoadCollections.heatGain(month),
          ).toBeApproximately(
            ((legacyData.gains_W['Appliances']?.[month.index0] ?? 0) as number) +
              ((legacyData.gains_W['Cooking']?.[month.index0] ?? 0) as number),
          );
          expect(model.normal.waterHeating.heatGain(month)).toBeApproximately(
            legacyData.gains_W['waterheating'][month.index0],
          );
          expect(model.normal.heatGain.heatGain(month)).toBeApproximately(
            sum(
              Object.values(legacyData.gains_W).map((gains: any) => gains[month.index0]),
            ),
          );

          const meanInternalTemperatureReceived =
            model.normal.meanInternalTemperature.meanInternalTemperatureOverall(month);
          if (!Number.isNaN(meanInternalTemperatureReceived)) {
            // eslint-disable-next-line jest/no-conditional-expect
            expect(meanInternalTemperatureReceived).toBeApproximately(
              legacyData.mean_internal_temperature.m_i_t_whole_dwelling_adjusted[
                month.index0
              ],
            );
          }
        }
        expect(model.normal.heatLoss.totalAverageHeatLoss).toBeApproximately(
          legacyData.totalWK,
        );

        let expected = legacyData.space_heating.annual_heating_demand_m2;
        if (!Number.isFinite(expected)) expected = NaN;
        expect(
          model.normal.spaceHeating.spaceHeatingDemandAnnualEnergyPerArea,
        ).toBeApproximately(expected);
        expect(model.normal.fuelRequirements.totalCostAnnual).toBeApproximately(
          legacyData.total_cost,
        );
        expect(
          model.normal.fuelRequirements.totalCarbonEmissionsAnnual,
        ).toBeApproximately(legacyData.annualco2);
        expect(
          model.normal.fuelRequirements.totalPrimaryEnergyAnnualPerArea,
        ).toBeApproximately(legacyData.primary_energy_use_m2);
        expect(
          model.normal.fuelRequirements.totalCarbonEmissionsAnnualPerArea,
        ).toBeApproximately(legacyData.kgco2perm2);
        expect(
          model.normal.fuelRequirements.energyPerPersonPerDayAverage,
        ).toBeApproximately(legacyData.kwhdpp);
        expect(
          model.normal.fuelRequirements.primaryEnergyPerPersonPerDayAverage,
        ).toBeApproximately(legacyData.primarykwhdpp);
      });
    },
  );

  test.each(regressionTestScenarios)(
    'legacy scenario golden master (regulated): $displayName',
    (scenarioFixture) => {
      withSuppressedConsoleWarn(() => {
        const model = Model.fromLegacy(scenarioFixture.rawData).unwrap();

        const legacyData: any = cloneDeep(scenarioFixture.rawData);
        runLegacyModel(legacyData);

        let expected = legacyData.SAP.data.space_heating.annual_heating_demand_m2;
        if (!Number.isFinite(expected)) expected = NaN;
        expect(
          model.regulated.spaceHeating.spaceHeatingDemandAnnualEnergyPerArea,
        ).toBeApproximately(expected);

        expect(model.regulated.fuelRequirements.totalCostAnnual).toBeApproximately(
          legacyData.SAP.total_costSAP,
        );
      });
    },
  );
});
