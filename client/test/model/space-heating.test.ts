import fc from 'fast-check';
import { cloneDeep } from 'lodash';
import { inspect } from 'node:util';
import { z } from 'zod';
import { sum } from '../../src/helpers/array-reducers';
import { externalTemperature } from '../../src/model/datasets';
import { Month } from '../../src/model/enums/month';
import { ModelBehaviourFlags } from '../../src/model/modules/behaviour-version';
import {
  SpaceHeating,
  SpaceHeatingDependencies,
  SpaceHeatingInput,
} from '../../src/model/modules/space-heating';
import { sensibleFloat } from '../arbitraries/legacy-values';
import { arbMonthly, monthly } from '../arbitraries/monthly';
import { arbitraryRegion } from '../helpers/arbitrary-enums';
import { legacySpaceHeating } from './golden-master/space-heating';

const arbSpaceHeatingInput = fc.record<SpaceHeatingInput>({
  heatingOffInSummer: fc.boolean(),
});

type TestDependencies = Omit<SpaceHeatingDependencies, 'modelBehaviourFlags'>;
function arbTestDependencies(opts?: { makeCompatibleWithLegacy: boolean }) {
  return fc
    .record<TestDependencies>({
      heatLoss: fc.record({
        heatLossBySourceMonthly: arbMonthly(
          fc.record({
            fabric: sensibleFloat,
            ventilation: sensibleFloat,
            infiltration: sensibleFloat,
          }),
        ),
        heatLossMonthly: arbMonthly(fc.constant(0)),
        heatLossParameter: arbMonthly(fc.constant(0)),
      }),
      floors: fc.record({
        totalFloorArea: sensibleFloat,
      }),
      region: arbitraryRegion,
      heatGain: fc
        .record({
          heatGainInternal: arbMonthly(sensibleFloat),
          heatGainSolar: arbMonthly(sensibleFloat),
        })
        .map(({ heatGainInternal, heatGainSolar }) => ({
          heatGainInternal,
          heatGainSolar,
          heatGain: monthly(Month.all.map((m) => heatGainInternal(m) + heatGainSolar(m))),
        })),
      fabric: fc.record({
        thermalMassParameter:
          (opts?.makeCompatibleWithLegacy ?? false)
            ? sensibleFloat.filter((v) => v >= 0)
            : sensibleFloat,
      }),
      meanInternalTemperature: fc.record({
        meanInternalTemperatureOverall: arbMonthly(sensibleFloat),
      }),
    })
    .map((dependencies) => {
      const heatLossMonthlyArr = Month.all.map((m) =>
        sum(Object.values(dependencies.heatLoss.heatLossBySourceMonthly(m))),
      );
      const heatLossParameterArr = heatLossMonthlyArr.map(
        (loss) => loss / dependencies.floors.totalFloorArea,
      );
      return {
        ...dependencies,
        heatLoss: {
          ...dependencies.heatLoss,
          heatLossMonthly: monthly(heatLossMonthlyArr),
          heatLossParameter: monthly(heatLossParameterArr),
        },
      };
    });
}

describe('space heating', () => {
  test('golden master', () => {
    function makeLegacyDataForSpaceHeating(
      input: SpaceHeatingInput,
      dependencies: TestDependencies,
    ): fc.Arbitrary<unknown> {
      const arbLegacyDataFreeVariables = fc.record({
        otherHeatGainsName: fc.constantFrom(
          'Appliances',
          'Lighting',
          'Cooking',
          'waterheating',
          'fans_and_pumps',
          'metabolic',
          'losses',
        ),
      });
      return arbLegacyDataFreeVariables.map((legacyDataFreeVariables) => ({
        internal_temperature: Month.all.map((m) =>
          dependencies.meanInternalTemperature.meanInternalTemperatureOverall(m),
        ),
        external_temperature: Month.all.map((m) =>
          externalTemperature(dependencies.region, m),
        ),
        TMP: dependencies.fabric.thermalMassParameter,
        TFA: dependencies.floors.totalFloorArea,
        losses_WK: {
          fabric: Month.all.map(
            (m) => dependencies.heatLoss.heatLossBySourceMonthly(m).fabric,
          ),
          ventilation: Month.all.map(
            (m) => dependencies.heatLoss.heatLossBySourceMonthly(m).ventilation,
          ),
          infiltration: Month.all.map(
            (m) => dependencies.heatLoss.heatLossBySourceMonthly(m).infiltration,
          ),
        },
        gains_W: {
          solar: Month.all.map((m) => dependencies.heatGain.heatGainSolar(m)),
          [legacyDataFreeVariables.otherHeatGainsName]: Month.all.map((m) =>
            dependencies.heatGain.heatGainInternal(m),
          ),
        },
        energy_requirements: {},
        space_heating: {
          heating_off_summer: input.heatingOffInSummer,
        },
      }));
    }

    const arb = fc
      .record({
        dependencies: arbTestDependencies({ makeCompatibleWithLegacy: true }),
        input: arbSpaceHeatingInput,
      })
      .chain(({ dependencies, input }) =>
        makeLegacyDataForSpaceHeating(input, dependencies).map((legacyData) => ({
          input,
          dependencies,
          legacyData,
        })),
      );

    const legacySpaceHeatingFlags: Pick<ModelBehaviourFlags, 'spaceHeating'> = {
      spaceHeating: {
        utilisationFactor: {
          preserveGammaPrecision: false,
        },
        skipLegacyCoolingCalculation: false,
        clampNegativeTemperatureDifference: false,
      },
    };
    fc.assert(
      fc.property(
        fc.constantFrom(...Month.all),
        arb,
        (month, { input, dependencies, legacyData: legacyData_ }) => {
          const spaceHeating = new SpaceHeating(
            input,
            {
              modelBehaviourFlags: legacySpaceHeatingFlags,
              ...dependencies,
            },
            () => {
              throw new fc.PreconditionFailure();
            },
          );
          const legacyData: any = cloneDeep(legacyData_);
          expect(() => legacySpaceHeating(legacyData)).not.toThrow();
          expect(spaceHeating.heatLossPower(month)).toBeApproximately(
            legacyData.space_heating.total_losses[month.index0],
          );
          expect(spaceHeating.spaceHeatingDemand(month)).toBeApproximately(
            legacyData.space_heating.heat_demand[month.index0],
          );
          expect(spaceHeating.spaceCoolingDemand(month)).toBeApproximately(
            legacyData.space_heating.cooling_demand[month.index0],
          );
          expect(spaceHeating.spaceHeatingDemandEnergy(month)).toBeApproximately(
            legacyData.space_heating.heat_demand_kwh[month.index0],
          );
          expect(spaceHeating.spaceCoolingDemandEnergy(month)).toBeApproximately(
            legacyData.space_heating.cooling_demand_kwh[month.index0],
          );
          expect(spaceHeating.spaceHeatingDemandEnergyAnnual).toBeApproximately(
            legacyData.space_heating.annual_heating_demand,
          );
          expect(spaceHeating.spaceCoolingDemandEnergyAnnual).toBeApproximately(
            legacyData.space_heating.annual_cooling_demand,
          );
          expect(spaceHeating.spaceHeatingDemandAnnualEnergyPerArea).toBeApproximately(
            legacyData.space_heating.annual_heating_demand_m2,
          );
          expect(spaceHeating.spaceCoolingDemandAnnualEnergyPerArea).toBeApproximately(
            legacyData.space_heating.annual_cooling_demand_m2,
          );
        },
      ),
    );
  });

  const allFlagsTrue: Pick<ModelBehaviourFlags, 'spaceHeating'> = {
    spaceHeating: {
      utilisationFactor: {
        preserveGammaPrecision: true,
      },
      skipLegacyCoolingCalculation: true,
      clampNegativeTemperatureDifference: true,
    },
  };

  test('sum of losses <= sum of gains + space heating demand (monthly)', () => {
    fc.assert(
      fc.property(
        fc.constantFrom(...Month.all),
        arbSpaceHeatingInput,
        arbTestDependencies({ makeCompatibleWithLegacy: false }),
        (month, input, dependencies) => {
          const spaceHeating = new SpaceHeating(input, {
            ...dependencies,
            modelBehaviourFlags: allFlagsTrue,
          });
          const heatIn =
            spaceHeating.usefulSolarHeatGain(month) +
            spaceHeating.usefulInternalHeatGain(month) +
            spaceHeating.spaceHeatingDemand(month);
          fc.pre(Number.isFinite(heatIn) && heatIn >= 0);
          const heatOut = spaceHeating.heatLossPower(month);
          fc.pre(Number.isFinite(heatOut) && heatOut >= 0);
          expect(heatOut).toBeLessThanOrEqual(heatIn * 1.0001);
        },
      ),
    );
  });

  test('mutator', () => {
    const typeofNumber = z.custom<number>(
      (val) => typeof val === 'number',
      (val) => ({
        message: `Expected number, received ${inspect(val)}`,
      }),
    );
    const schema = z.object({
      space_heating: z.object({
        delta_T: z.array(typeofNumber).length(12),
        total_losses: z.array(typeofNumber).length(12),
        total_gains: z.array(typeofNumber).length(12),
        utilisation_factor: z.array(typeofNumber).length(12),
        useful_gains: z.array(typeofNumber).length(12),
        heat_demand: z.array(typeofNumber).length(12),
        cooling_demand: z.array(typeofNumber).length(12),
        heat_demand_kwh: z.array(typeofNumber).length(12),
        cooling_demand_kwh: z.array(typeofNumber).length(12),
        annual_heating_demand: typeofNumber,
        annual_cooling_demand: typeofNumber,
        annual_heating_demand_m2: typeofNumber,
        annual_cooling_demand_m2: typeofNumber,
      }),
      annual_useful_gains_kWh_m2: z.object({
        Internal: typeofNumber,
        Solar: typeofNumber,
      }),
      annual_losses_kWh_m2: z.object({
        ventilation: typeofNumber,
        infiltration: typeofNumber,
        fabric: typeofNumber,
      }),
      space_heating_demand_m2: typeofNumber,
      energy_requirements: z.object({
        space_heating: z.object({
          name: z.literal('Space Heating'),
          quantity: typeofNumber,
          monthly: z.array(typeofNumber).length(12),
        }),
      }),
    });
    fc.assert(
      fc.property(
        fc.constantFrom(...Month.all),
        arbSpaceHeatingInput,
        arbTestDependencies({ makeCompatibleWithLegacy: false }),
        (month, input, dependencies) => {
          const spaceHeating = new SpaceHeating(input, {
            ...dependencies,
            modelBehaviourFlags: allFlagsTrue,
          });
          const legacyData: any = {};
          spaceHeating.mutateLegacyData(legacyData);
          expect(() => schema.parse(legacyData)).not.toThrow();
        },
      ),
    );
  });
  test('when heatingOffInSummer is true, heating demand for summer months is 0', () => {
    fc.assert(
      fc.property(
        fc.constantFrom(...Month.all.filter((m) => m.season === 'summer')),
        arbSpaceHeatingInput.filter((input) => input.heatingOffInSummer),
        arbTestDependencies({ makeCompatibleWithLegacy: false }),
        (month, input, dependencies) => {
          const spaceHeating = new SpaceHeating(input, {
            ...dependencies,
            modelBehaviourFlags: allFlagsTrue,
          });
          expect(spaceHeating.spaceHeatingDemand(month)).toBe(0);
        },
      ),
    );
  });
});
