import fc from 'fast-check';
import { HeatingSystemSpaceParameters } from '../../../src/model/modules/heating-systems/space-heating';
import { sensibleFloat } from '../../arbitraries/legacy-values';
import { ArbParam, resolveArbParam } from '../../helpers/arbitraries';

export function arbSpaceHeatingSystem(opts?: {
  type?: ArbParam<HeatingSystemSpaceParameters['type']>;
  fraction?: ArbParam<HeatingSystemSpaceParameters['fraction']>;
}) {
  const arbType = resolveArbParam(
    opts?.type,
    fc.constantFrom(
      'main' as const,
      'second main (whole house)' as const,
      'second main (part of house)' as const,
      'supplementary' as const,
    ),
  );
  const arbFraction = resolveArbParam(
    opts?.fraction,
    sensibleFloat.filter((val) => val >= 0),
  );
  return fc.record<HeatingSystemSpaceParameters>({
    type: arbType,
    responsiveness: sensibleFloat,
    fraction: arbFraction,
    controlType: fc.constantFrom(null, 1 as const, 2 as const, 3 as const),
    temperatureAdjustment: sensibleFloat,
  });
}
