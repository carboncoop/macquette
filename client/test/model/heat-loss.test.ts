import fc from 'fast-check';
import { sum } from '../../src/helpers/array-reducers';
import { Month } from '../../src/model/enums/month';
import { HeatLoss, HeatLossDependencies } from '../../src/model/modules/heat-loss';
import { sensibleFloat } from '../arbitraries/legacy-values';
import { arbMonthly } from '../arbitraries/monthly';

describe('HeatLoss', () => {
  it('correctly calculates peak heat load', () => {
    const deps: HeatLossDependencies = {
      geography: {
        externalDesignTemperature: -4,
      },
      floors: {
        totalFloorArea: 100,
      },
      ventilation: {
        heatLossAverage: 25,
        heatLossMonthly: () => 25,
      },
      infiltration: {
        heatLossAverage: 25,
        heatLossMonthly: () => 25,
      },
      fabric: {
        heatLoss: 250,
      },
    };
    const heatLoss = new HeatLoss(null, deps);
    expect(heatLoss.totalAverageHeatLoss).toBe(
      deps.fabric.heatLoss +
        deps.ventilation.heatLossAverage +
        deps.infiltration.heatLossAverage,
    ); // 300
    expect(heatLoss.peakHeatLoad).toBe(
      300 * (20 - deps.geography.externalDesignTemperature),
    );
    expect(heatLoss.peakHeatLoadPerArea).toBe((300 * 24) / deps.floors.totalFloorArea);
  });

  const arbDependencies = fc.record<HeatLossDependencies>({
    geography: fc.record({
      externalDesignTemperature: sensibleFloat,
    }),
    floors: fc.record({
      totalFloorArea: sensibleFloat,
    }),
    ventilation: fc
      .record({
        heatLossMonthly: arbMonthly(sensibleFloat),
      })
      .map(({ heatLossMonthly }) => ({
        heatLossMonthly,
        heatLossAverage: sum(Month.all.map((m) => heatLossMonthly(m) * m.days)) / 365,
      })),
    infiltration: fc
      .record({
        heatLossMonthly: arbMonthly(sensibleFloat),
      })
      .map(({ heatLossMonthly }) => ({
        heatLossMonthly,
        heatLossAverage: sum(Month.all.map((m) => heatLossMonthly(m) * m.days)) / 365,
      })),
    fabric: fc.record({
      heatLoss: sensibleFloat,
    }),
  });

  test('all sources are included in total average heat loss', () => {
    fc.assert(
      fc.property(arbDependencies, (dependencies) => {
        const heatLoss = new HeatLoss(null, dependencies);
        const expected =
          sum(Month.all.map((m) => heatLoss.heatLossMonthly(m) * m.days)) / 365;
        const received = heatLoss.totalAverageHeatLoss;
        expect(received).toBeApproximately(expected);
      }),
    );
  });
});
