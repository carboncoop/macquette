import fc from 'fast-check';
import { scenarioSchema } from '../../../src/data-schemas/scenario';
import { extractHeatingSystemSpaceParameters } from '../../../src/model/modules/heating-systems/space-heating';
import { extractMeanInternalTemperatureInputFromLegacy } from '../../../src/model/modules/mean-internal-temperature/extract-from-legacy';
import {
  arbMeanInternalTemperatureDependencies,
  arbMeanInternalTemperatureInput,
} from './arbitraries';
import { makeLegacyDataForMeanInternalTemperature } from './make-legacy-data';

describe('extractor', () => {
  describe('space heating systems', () => {
    test('round trips the legacy data builder', () => {
      fc.assert(
        fc.property(
          arbMeanInternalTemperatureInput(),
          arbMeanInternalTemperatureDependencies(),
          (input, dependencies) => {
            const spaceHeatingSystems =
              scenarioSchema
                .parse(makeLegacyDataForMeanInternalTemperature(input, dependencies))
                ?.heating_systems?.map(extractHeatingSystemSpaceParameters) ?? [];
            expect(spaceHeatingSystems).toEqual(
              dependencies.heatingSystems.spaceHeatingSystems,
            );
          },
        ),
      );
    });
  });
  test('round trips the legacy data builder', () => {
    fc.assert(
      fc.property(
        arbMeanInternalTemperatureInput(),
        arbMeanInternalTemperatureDependencies(),
        (input, dependencies) => {
          const extracted = extractMeanInternalTemperatureInputFromLegacy(
            scenarioSchema.parse(
              makeLegacyDataForMeanInternalTemperature(input, dependencies),
            ),
          );
          expect(extracted).toEqual(input);
        },
      ),
    );
  });
});
