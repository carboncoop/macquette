import { Month } from '../../../src/model/enums/month';
import {
  MeanInternalTemperatureDependencies,
  MeanInternalTemperatureInput,
} from '../../../src/model/modules/mean-internal-temperature';
import { mapWeekly } from '../../../src/model/modules/mean-internal-temperature/weekly';

export function makeLegacyDataForMeanInternalTemperature(
  input: MeanInternalTemperatureInput,
  dependencies: Omit<MeanInternalTemperatureDependencies, 'modelBehaviourFlags'>,
) {
  // Assumes the input was created from the arbitrary with makeCompatibleWithLegacy: true
  return {
    temperature: {
      target: input.targetTemperature,
      ...(input.livingArea !== null ? { living_area: input.livingArea } : {}),
      hours_off: mapWeekly(
        dependencies.heatingHours,
        ({ hoursOffPattern }) => hoursOffPattern,
      ),
    },
    TFA: dependencies.floors.totalFloorArea,
    TMP: dependencies.fabric.thermalMassParameter,
    losses_WK: {
      all: Month.all.map((m) => dependencies.heatLoss.heatLossMonthly(m)),
    },
    gains_W: {
      all: Month.all.map((m) => dependencies.heatGain.heatGain(m)),
    },
    region: dependencies.region.index0,
    heating_systems: dependencies.heatingSystems.spaceHeatingSystems.map((system) => {
      let main_space_heating_system: string;
      switch (system.type) {
        case 'main':
          main_space_heating_system = 'mainHS1';
          break;
        case 'second main (whole house)':
          main_space_heating_system = 'mainHS2_whole_house';
          break;
        case 'second main (part of house)':
          main_space_heating_system = 'mainHS2_part_of_the_house';
          break;
        case 'supplementary':
          main_space_heating_system = 'secondaryHS';
          break;
      }
      return {
        provides: 'heating',
        fraction_space: system.fraction,
        responsiveness: system.responsiveness,
        main_space_heating_system,
        heating_controls: system.controlType ?? 0,
        temperature_adjustment: system.temperatureAdjustment,
        fuel: '',
      };
    }),
    mean_internal_temperature: {},
  };
}
