import fc from 'fast-check';
import { Month } from '../../../src/model/enums/month';
import { HeatingSystemSpaceParameters } from '../../../src/model/modules/heating-systems/space-heating';
import {
  MeanInternalTemperature,
  MeanInternalTemperatureFlags,
  WholeDwellingCalculator,
  WholeDwellingCalculatorInput,
} from '../../../src/model/modules/mean-internal-temperature';
import { sensibleFloat } from '../../arbitraries/legacy-values';
import { legacyTemperature } from '../golden-master/mean-internal-temperature';
import { arbSpaceHeatingSystem } from '../heating-systems/arbitraries';
import { withSuppressedConsoleWarn } from '../with-suppressed-console-warn';
import {
  arbMeanInternalTemperatureDependencies,
  arbMeanInternalTemperatureInput,
} from './arbitraries';
import { makeLegacyDataForMeanInternalTemperature } from './make-legacy-data';

const arbMonth = fc.constantFrom(...Month.all);

describe('mean internal temperature', () => {
  test('golden master', () => {
    withSuppressedConsoleWarn(() => {
      fc.assert(
        fc.property(
          arbMonth,
          arbMeanInternalTemperatureInput(),
          arbMeanInternalTemperatureDependencies({
            makeCompatibleWithLegacy: true,
            mode: 'normal',
          }),
          (month, input, dependencies) => {
            fc.pre(
              input.livingArea === null ||
                Math.abs(input.livingArea / dependencies.floors.totalFloorArea - 1) >
                  2 ** -17,
            );
            const meanInternalTemperature = new MeanInternalTemperature(input, {
              ...dependencies,
              modelBehaviourFlags: {
                meanInternalTemperature: {
                  includeSolarGains: true,
                  utilisationFactor: {
                    preserveGammaPrecision: false,
                    etaIs1IfGammaIsLessThanOrEqualTo0: false,
                  },
                  fixRestOfDwellingTemperatureCalculation: false,
                  treatDifferentHeatingControlsCorrectly: false,
                  normaliseHeatingFractionsForTemperatureAdjustment: false,
                  whenNoHeatingSystems: {
                    correctRestOfDwellingTemperature: false,
                    correctRestOfDwellingUtilisationFactor: false,
                    useSAPTemperatureAdjustment: false,
                  },
                  disallowPre2025StandardisedHeatingMode: false,
                },
              },
            });
            const legacyData: any = makeLegacyDataForMeanInternalTemperature(
              input,
              dependencies,
            );
            legacyTemperature(legacyData);
            expect(meanInternalTemperature.mainHeatingResponsiveness).toBeApproximately(
              legacyData.temperature.responsiveness,
            );

            // Skip checking the utilisation factor, because the legacy code calculates a
            // different value for the mean_internal_temperature.u_factor_* outputs to
            // what it actually uses for the rest of its calculations. If we are
            // calculating it wrong, it will come out in the mean internal temperature
            // numbers.

            expect(
              meanInternalTemperature.meanInternalTemperatureLivingArea(month),
            ).toBeApproximately(
              legacyData.mean_internal_temperature.m_i_t_living_area[month.index0],
            );

            expect(
              meanInternalTemperature.temperatureWhenHeatedRestOfDwelling(month),
            ).toBeApproximately(
              legacyData.mean_internal_temperature.t_heating_periods_rest_of_dwelling[
                month.index0
              ] ?? NaN,
            );

            expect(
              meanInternalTemperature.meanInternalTemperatureRestOfDwelling(month),
            ).toBeApproximately(
              legacyData.mean_internal_temperature.m_i_t_rest_of_dwelling[month.index0],
            );

            expect(
              meanInternalTemperature.meanInternalTemperatureOverallUnadjusted(month),
            ).toBeApproximately(
              legacyData.mean_internal_temperature.m_i_t_whole_dwelling[month.index0] ??
                NaN,
            );

            expect(
              meanInternalTemperature.meanInternalTemperatureOverall(month),
            ).toBeApproximately(legacyData.internal_temperature[month.index0]);
          },
        ),
      );
    });
  });

  const allFlagsTrue: MeanInternalTemperatureFlags = {
    includeSolarGains: true,
    utilisationFactor: {
      preserveGammaPrecision: true,
      etaIs1IfGammaIsLessThanOrEqualTo0: true,
    },
    fixRestOfDwellingTemperatureCalculation: true,
    treatDifferentHeatingControlsCorrectly: true,
    normaliseHeatingFractionsForTemperatureAdjustment: true,
    whenNoHeatingSystems: {
      correctRestOfDwellingTemperature: true,
      correctRestOfDwellingUtilisationFactor: true,
      useSAPTemperatureAdjustment: true,
    },
    disallowPre2025StandardisedHeatingMode: true,
  };
  describe('rest of dwelling', () => {
    const arbInput: fc.Arbitrary<WholeDwellingCalculatorInput> = fc
      .record({
        heatingSystems: fc.oneof(
          fc.record({
            main: arbSpaceHeatingSystem({ type: 'main' }),
            secondMain: fc.option(
              arbSpaceHeatingSystem({
                type: fc.constantFrom<HeatingSystemSpaceParameters['type']>(
                  'second main (part of house)',
                  'second main (whole house)',
                ),
              }),
            ),
          }),
          fc.constant({ main: null, secondMain: null }),
        ),
        targetTemperature: sensibleFloat,
        livingAreaFraction: fc.float({ min: 0, max: 1 }),
        externalTemperature: sensibleFloat,
        thermalMassParameter: sensibleFloat,
        heatLossParameter: sensibleFloat,
        heatLoss: sensibleFloat,
        heatGain: sensibleFloat,
        mainHeatingResponsiveness: sensibleFloat,
        hoursOffPattern: fc.array(sensibleFloat.filter((val) => val > 0)),
      })
      .map((input) => ({
        ...input,
        fractionOfMainHeatingFromSecondMainSystem:
          (input.heatingSystems.secondMain?.fraction ?? 0) /
          ((input.heatingSystems.main?.fraction ?? 0) +
            (input.heatingSystems.secondMain?.fraction ?? 0)),
      }));
    test('temperature is always between rest of dwelling and rest of dwelling with main system controls', () => {
      withSuppressedConsoleWarn(() => {
        fc.assert(
          fc.property(arbInput, (input) => {
            const calculator = new WholeDwellingCalculator(input, allFlagsTrue);
            const min = Math.min(
              calculator.restOfDwellingCalculator.meanInternalTemperature,
              calculator.restOfDwellingWithMainSystemControls.meanInternalTemperature,
            );
            const max = Math.max(
              calculator.restOfDwellingCalculator.meanInternalTemperature,
              calculator.restOfDwellingWithMainSystemControls.meanInternalTemperature,
            );
            const val = calculator.meanInternalTemperatureRestOfDwelling;
            fc.pre(Number.isFinite(min));
            fc.pre(Number.isFinite(max));
            fc.pre(Number.isFinite(val));
            const errorTolerance =
              Math.max(Math.abs(min), Math.abs(max), Math.abs(val)) * 2 ** -17;
            expect(val).toBeGreaterThanOrEqual(min - errorTolerance);
            expect(val).toBeLessThanOrEqual(max + errorTolerance);
          }),
        );
      });
    });
    test('for certain fixed parameters, temperature is the expected weighted average', () => {
      withSuppressedConsoleWarn(() => {
        fc.assert(
          fc.property(
            arbInput.filter(
              (input) =>
                input.heatingSystems.main !== null &&
                input.heatingSystems.secondMain !== null,
            ),
            (input) => {
              const livingAreaFraction = 0.6;
              const mainHeatingFraction = 0.7 / 2;
              const secondMainHeatingFraction = (1 - 0.7) / 2;
              const calculator = new WholeDwellingCalculator(
                {
                  ...input,
                  livingAreaFraction,
                  heatingSystems: {
                    main: {
                      ...input.heatingSystems.main!,
                      fraction: mainHeatingFraction,
                    },
                    secondMain: {
                      ...input.heatingSystems.secondMain!,
                      type: 'second main (part of house)',
                      fraction: secondMainHeatingFraction,
                    },
                  },
                  fractionOfMainHeatingFromSecondMainSystem:
                    secondMainHeatingFraction /
                    (mainHeatingFraction + secondMainHeatingFraction),
                },
                allFlagsTrue,
              );
              expect(calculator.meanInternalTemperatureRestOfDwelling).toBeApproximately(
                0.75 * calculator.restOfDwellingCalculator.meanInternalTemperature +
                  0.25 *
                    calculator.restOfDwellingWithMainSystemControls
                      .meanInternalTemperature,
              );
            },
          ),
        );
      });
    });
    test('when heating system control types are set to null, rest of dwelling temperature is equal to living area temperature', () => {
      fc.assert(
        fc.property(
          arbInput.filter((input) => {
            if (input.heatingSystems.main === null) return false;
            const mainControls = input.heatingSystems.main.controlType;
            const secondaryControls =
              input.heatingSystems.secondMain?.controlType ?? null;
            return mainControls === null && secondaryControls === null;
          }),
          (input) => {
            const calculator = new WholeDwellingCalculator(input, allFlagsTrue);
            expect(calculator.restOfDwellingCalculator.temperatureWhenHeated).toBe(
              calculator.livingAreaCalculator.temperatureWhenHeated,
            );
          },
        ),
      );
    });
  });

  test('no heating systems', () => {
    // Should behave as if there was a single main system with the following parameters,
    // see SAP 10.2 pages 163, 171
    const imaginaryMainSystem: HeatingSystemSpaceParameters = {
      type: 'main',
      responsiveness: 1,
      controlType: 2,
      fraction: 1,
      temperatureAdjustment: 0.3,
    };
    fc.assert(
      fc.property(
        arbMonth,
        arbMeanInternalTemperatureInput(),
        arbMeanInternalTemperatureDependencies({ mode: 'normal' }),
        (month, input, dependencies) => {
          const meanInternalTemperatureNoHeatingSystems = new MeanInternalTemperature(
            input,
            {
              ...dependencies,
              heatingSystems: { spaceHeatingSystems: [] },
              modelBehaviourFlags: { meanInternalTemperature: allFlagsTrue },
            },
          );
          const meanInternalTemperatureImaginaryMainSystem = new MeanInternalTemperature(
            input,
            {
              ...dependencies,
              heatingSystems: { spaceHeatingSystems: [imaginaryMainSystem] },
              modelBehaviourFlags: { meanInternalTemperature: allFlagsTrue },
            },
          );
          expect(
            meanInternalTemperatureNoHeatingSystems.meanInternalTemperatureOverall(month),
          ).toEqual(
            meanInternalTemperatureImaginaryMainSystem.meanInternalTemperatureOverall(
              month,
            ),
          );
        },
      ),
    );
  });
  test('in standardised heating mode, the overall mean internal temperature is always 20°C', () => {
    fc.assert(
      fc.property(
        arbMonth,
        arbMeanInternalTemperatureInput(),
        arbMeanInternalTemperatureDependencies({ mode: 'standardised heating' }),
        (month, input, dependencies) => {
          withSuppressedConsoleWarn(() => {
            const meanInternalTemperature = new MeanInternalTemperature(input, {
              ...dependencies,
              modelBehaviourFlags: { meanInternalTemperature: allFlagsTrue },
            });
            expect(meanInternalTemperature.meanInternalTemperatureOverall(month)).toBe(
              20,
            );
          });
        },
      ),
    );
  });
});
