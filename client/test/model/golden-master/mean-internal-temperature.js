/* eslint-disable */
/* eslint-enable no-debugger, no-warning-comments */
import { emulateJsonRoundTrip } from '../../../src/helpers/emulate-json-round-trip';
import { datasets } from '../reference-datasets';
let z;
function isNaN() {
    // Override builtin `isNaN` so that NaNs propagate rather than being coalesced.
    // This is so that new code can handle them differently.
    return false;
}

/*---------------------------------------------------------------------------------------------
 // TEMPERATURE
 //
 // Inputs from user:
 //      - data.temperature.target
 //      - data.temperature.living_area
 //
 // Inputs from other modules:
 //      - data.TFA
 //      - data.TMP
 //      - data.losses_WK
 //      - data.gains_W
 //      - data.altitude
 //      - data.region
 //	- data.heating_systems
 //	- data.temperature.hours_off
 //
 // Global Outputs:
 //      - data.internal_temperature
 //      - data.external_temperature
 //      - data.HLP
 //	- data.mean_internal_temperature.u_factor_living_area
 //      - data.mean_internal_temperature.m_i_t_living_area
 //      - data.mean_internal_temperature.t_heating_periods_rest_of_dwelling
 //      - data.mean_internal_temperature.u_factor_rest_of_dwelling
 //      - data.mean_internal_temperature.m_i_t_rest_of_dwelling
 //      - data.mean_internal_temperature.fLA
 //      - data.mean_internal_temperature.m_i_t_whole_dwelling
 //      - data.temperature.temperature_adjustment
 //	- data.mean_internal_temperature.m_i_t_whole_dwelling_adjusted
 //
 //
 // Module Variables:
 //	- data.temperature.responsiveness
 //
 // Datasets:
 //      - datasets.table_u1
 //
 // Uses external function:
 //      - calc_utilisation_factor
 //	- calc_MeanInternalTemperature
 //	- calc_Th2
 //
 //---------------------------------------------------------------------------------------------*/
export function legacyTemperature(data) {
    if (data.temperature == undefined) {
        data.temperature = {};
    }
    if (data.temperature.living_area == undefined) {
        data.temperature.living_area = data.TFA;
    }
    if (data.temperature.target == undefined) {
        data.temperature.target = 21;
    }
    if (data.temperature.temperature_adjustment == undefined) {
        data.temperature.temperature_adjustment = 0;
    }
    if (data.temperature.hours_off == undefined) {
        data.temperature.hours_off = { weekday: [7, 8], weekend: [8] };
    }

    // Get Main heating systems
    let mainHSs = {}; // It will take the form of: mainHSs = {mainHS1: systemObject, mainHS2: systemObject}
    data.heating_systems.forEach(function (system) {
        if (
            (system.provides == 'heating' || system.provides == 'heating_and_water') &&
            system.fraction_space > 0
        ) {
            switch (system.main_space_heating_system) {
                case 'mainHS1':
                    mainHSs.mainHS1 = system;
                    break;
                case 'mainHS2_whole_house':
                    mainHSs.mainHS2 = system;
                    mainHSs.mainHS2.whole_house = true;
                    break;
                case 'mainHS2_part_of_the_house':
                    mainHSs.mainHS2 = system;
                    mainHSs.mainHS2.whole_house = false;
                    break;
            }
        }
    });

    // In case of two main heating systems, calculate their fraction of "main heating" (different than fraction of "space heating")
    if (mainHSs.mainHS1 != undefined && mainHSs.mainHS2 != undefined) {
        var fraction_MHS1 =
            mainHSs.mainHS1.fraction_space /
            (mainHSs.mainHS1.fraction_space + mainHSs.mainHS2.fraction_space);
        var fraction_MHS2 =
            mainHSs.mainHS2.fraction_space /
            (mainHSs.mainHS1.fraction_space + mainHSs.mainHS2.fraction_space);
    }

    // Calculate responsiveness - SAP21012, table 9b, p. 220
    data.temperature.responsiveness = 1; // In case no 1st Main heating system has been selected
    // if there is only one main system
    if (mainHSs.mainHS1 != undefined && mainHSs.mainHS2 == undefined) {
        data.temperature.responsiveness = mainHSs.mainHS1.responsiveness;
    } else if (mainHSs.mainHS1 != undefined && mainHSs.mainHS2 != undefined) {
        // if there are two
        data.temperature.responsiveness =
            fraction_MHS1 * mainHSs.mainHS1.responsiveness +
            fraction_MHS2 * mainHSs.mainHS2.responsiveness;
    }

    // Ohter preprationn for the formula
    let R = data.temperature.responsiveness;
    let Th = data.temperature.target;
    let Th_monthly = [Th, Th, Th, Th, Th, Th, Th, Th, Th, Th, Th, Th];
    let TMP = data.TMP; // data.TMP;

    let fLA = data.temperature.living_area / data.TFA;
    if (isNaN(fLA)) {
        fLA = 0;
    }

    let H = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    let HLP = [];
    let G = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for (m = 0; m < 12; m++) {
        for (z in data.losses_WK) {
            H[m] += data.losses_WK[z][m];
            HLP[m] = H[m] / data.TFA;
        }

        for (z in data.gains_W) {
            G[m] += data.gains_W[z][m];
        }
    }

    let Te = [];
    for (var m = 0; m < 12; m++) {
        //Te[m] = datasets.table_u1[data.region][m] - (0.3 * data.altitude / 50);
        Te[m] = datasets.table_u1[data.region][m];
    }

    //----------------------------------------------------------------------------------------------------------------
    // 7. Mean internal temperature (heating season)
    //----------------------------------------------------------------------------------------------------------------

    // Bring calculation of (96)m forward as its used in section 7.
    // Monthly average external temperature from Table U1
    // for (var i=1; i<13; i++) data['96-'+i] = table_u1[i.region][i-1]-(0.3 * i.altitude / 50);

    // See utilisationfactor.js for calculation
    // Calculation is described on page 159 of SAP document
    // Would be interesting to understand how utilisation factor equation
    // can be derived

    let utilisation_factor_A = [];
    for (var m = 0; m < 12; m++) {
        utilisation_factor_A[m] = calc_utilisation_factor(
            TMP,
            HLP[m],
            H[m],
            Th,
            Te[m],
            G[m],
        );
    }

    // Table 9c: Heating requirement

    // Living area
    let Ti_livingarea = calc_MeanInternalTemperature(
        Th_monthly,
        data.temperature.hours_off,
        TMP,
        HLP,
        H,
        Te,
        G,
        R,
    );

    // rest of dwelling - SAP2012, table 9, p.221
    let Th2 = [];
    let Ti_restdwelling = [];
    if (mainHSs.mainHS1 != undefined) {
        if (mainHSs.mainHS2 == undefined || mainHSs.mainHS2.whole_house == true) {
            Th2 = calc_Th2(mainHSs.mainHS1.heating_controls, Th, HLP);
            Ti_restdwelling = calc_MeanInternalTemperature(
                Th2,
                data.temperature.hours_off,
                TMP,
                HLP,
                H,
                Te,
                G,
                R,
            );
        }
        if (mainHSs.mainHS2 != undefined && mainHSs.mainHS2.whole_house == false) {
            // The 2nd main heating system heats a different part of the house
            if (mainHSs.mainHS2.fraction_space > 1 - fLA) {
                Th2 = calc_Th2(mainHSs.mainHS2.heating_controls, Th, HLP);
                Ti_restdwelling = calc_MeanInternalTemperature(
                    Th2,
                    data.temperature.hours_off,
                    TMP,
                    HLP,
                    H,
                    Te,
                    G,
                    R,
                );
            } else {
                Th2 = calc_Th2(mainHSs.mainHS1.heating_controls, Th, HLP);
                let Ti_restdwelling_S1 = calc_MeanInternalTemperature(
                    Th2,
                    data.temperature.hours_off,
                    TMP,
                    HLP,
                    H,
                    Te,
                    G,
                    R,
                );

                Th2 = calc_Th2(mainHSs.mainHS2.heating_controls, Th, HLP);
                let Ti_restdwelling_S2 = calc_MeanInternalTemperature(
                    Th2,
                    data.temperature.hours_off,
                    TMP,
                    HLP,
                    H,
                    Te,
                    G,
                    R,
                );

                for (var m = 0; m < 12; m++) {
                    /* WARNING: This whole calculation is wrong:

                     SAP cell 203 is fraction_MHS2, but we are writing in terms of
                     fraction_MHS1. These sum to 1 (by the normalisation above), so the
                     term for T1 is actually correctly rearranged, but in T2 the numerator
                     should be multiplied by fraction_MHS2 so that the weights sum to 1.
                     Using fraction_MHS1 means the temps are double counted rather than
                     properly weighted, and this error is actually masked by the also
                     erroneous divison by 2.
                    */

                    let T1 = (Ti_restdwelling_S1[m] * (fraction_MHS1 - fLA)) / (1 - fLA);
                    let T2 = (Ti_restdwelling_S2[m] * fraction_MHS1) / (1 - fLA);
                    Ti_restdwelling[m] = (T1 + T2) / 2;
                }
            }
        }
    }
    if (Ti_restdwelling.length == 0) {
        Ti_restdwelling = Ti_livingarea;
    }

    let utilisation_factor_B = [];
    for (var m = 0; m < 12; m++) {
        let Ti = Th2[m];
        let tmpHLP = HLP[m];
        // NOTE: The following clamping of HLP is erroneous (HLP is only supposed to be
        // clamped for the calculation of Th2, not the utilisation factor), and only
        // affects utilisation_factor_B, which is not used anywhere other than the SAP
        // worksheet. We could add a flag to preserve the erroneous behaviour in the new
        // model, but since it doesn't affect anything else, modifying the golden master
        // seems like the best solution.
        //
        // if (tmpHLP > 6.0) {
        //     tmpHLP = 6.0;
        // }

        // TMP,HLP,H,Ti,Te,G
        utilisation_factor_B[m] = calc_utilisation_factor(
            TMP,
            tmpHLP,
            H[m],
            Ti,
            Te[m],
            G[m],
        );
    }

    data.internal_temperature = [];
    for (var m = 0; m < 12; m++) {
        data.internal_temperature[m] =
            fLA * Ti_livingarea[m] + (1 - fLA) * Ti_restdwelling[m];
    }

    data.HLP = HLP;
    data.mean_internal_temperature.u_factor_living_area = utilisation_factor_A;
    data.mean_internal_temperature.m_i_t_living_area = Ti_livingarea;
    data.mean_internal_temperature.t_heating_periods_rest_of_dwelling = Th2;
    data.mean_internal_temperature.u_factor_rest_of_dwelling = utilisation_factor_B;
    data.mean_internal_temperature.m_i_t_rest_of_dwelling = Ti_restdwelling;
    data.mean_internal_temperature.fLA = fLA;
    // data.mean_internal_temperature.m_i_t_whole_dwelling = JSON.parse(
    //     JSON.stringify(data.internal_temperature),
    // );
    data.mean_internal_temperature.m_i_t_whole_dwelling = emulateJsonRoundTrip(
        data.internal_temperature,
    );
    data.external_temperature = Te;

    // Temperature adjustment
    // if there is only one main system
    if (mainHSs.mainHS1 != undefined && mainHSs.mainHS2 == undefined) {
        data.temperature.temperature_adjustment = mainHSs.mainHS1.temperature_adjustment;
    } else if (mainHSs.mainHS1 != undefined && mainHSs.mainHS2 != undefined) {
        // if there are two
        if (mainHSs.mainHS2.whole_house == true) {
            data.temperature.temperature_adjustment =
                mainHSs.mainHS1.temperature_adjustment;
        } else {
            data.temperature.temperature_adjustment =
                mainHSs.mainHS1.fraction_space * mainHSs.mainHS1.temperature_adjustment +
                mainHSs.mainHS2.fraction_space * mainHSs.mainHS2.temperature_adjustment;
        }
    }

    for (var m = 0; m < 12; m++) {
        data.internal_temperature[m] =
            data.internal_temperature[m] + data.temperature.temperature_adjustment;
    }
    data.mean_internal_temperature.m_i_t_whole_dwelling_adjusted =
        data.internal_temperature;
    return data;
}

// Calculation of mean internal temperature for heating
// Calculation of mean internal temperature is based on the heating patterns defined in Table 9.

function calc_utilisation_factor(TMP, HLP, H, Ti, Te, G) {
    /*
     Symbols and units
     H = heat transfer coefficient, (39)m (W/K)
     G = total gains, (84)m (W)
     Ti = internal temperature (°C)
     Te = external temperature, (96)m (°C)
     TMP = Thermal Mass Parameter, (35), (kJ/m2K) (= Cm for building / total floor area)
     HLP = Heat Loss Parameter, (40)m (W/m2K)
     τ = time constant (h)
     η = utilisation factor
     L = heat loss rate (W)
     */

    // Calculation of utilisation factor

    // TMP = thermal Mass / Total floor area
    // HLP = heat transfer coefficient (H) / Total floor area

    let tau = TMP / (3.6 * HLP);
    let a = 1.0 + tau / 15.0;
    // calc losses
    let L = H * (Ti - Te);
    // ratio of gains to losses
    let y = G / L;
    // Note: to avoid instability when γ is close to 1 round γ to 8 decimal places
    // y = y.toFixed(8);
    y = Math.round(y * 100000000.0) / 100000000.0;
    let n = 0.0;
    if (y > 0.0 && y != 1.0) {
        n = (1.0 - Math.pow(y, a)) / (1.0 - Math.pow(y, a + 1.0));
    }
    if (y == 1.0) {
        n = a / (a + 1.0);
    }
    if (y <= 0.0) {
        n = 1.0;
    }
    if (isNaN(n)) {
        n = 0;
    }
    return n;
}

function calc_MeanInternalTemperature(Th, hours_off, TMP, HLP, H, Te, G, R) {
    let Ti_area = [];
    for (let m = 0; m < 12; m++) {
        let Thm = Th[m];
        let Ti = Th[m];
        // (TMP,HLP,H,Ti,Te,G, R,Th,toff)
        let temp = { weekday: 0, weekend: 0 };
        for (let type in hours_off) {
            for (z in hours_off[type]) {
                temp[type] += calc_temperature_reduction(
                    TMP,
                    HLP[m],
                    H[m],
                    Ti,
                    Te[m],
                    G[m],
                    R,
                    Thm,
                    hours_off[type][z],
                );
            }
        }
        let Tweekday = Th[m] - temp.weekday;
        let Tweekend = Th[m] - temp.weekend;
        Ti_area[m] = (5 * Tweekday + 2 * Tweekend) / 7;
    }
    return Ti_area;
}

function calc_Th2(control_type, Th, HLP) {
    let temp = [];
    for (let m = 0; m < 12; m++) {
        let tmpHLP = HLP[m];
        if (tmpHLP > 6.0) {
            tmpHLP = 6.0;
        }
        if (control_type == 1) {
            temp[m] = Th - 0.5 * tmpHLP;
        }
        if (control_type == 2) {
            temp[m] = Th - tmpHLP + Math.pow(tmpHLP, 2) / 12;
        }
        if (control_type == 3) {
            temp[m] = Th - tmpHLP + Math.pow(tmpHLP, 2) / 12;
        }
        if (isNaN(temp[m])) {
            temp[m] = Th;
        }
    }
    return temp;
}

function calc_temperature_reduction(TMP, HLP, H, Ti, Te, G, R, Th, toff) {
    // Calculation of utilisation factor
    let tau = TMP / (3.6 * HLP);
    let a = 1.0 + tau / 15.0;
    let L = H * (Ti - Te);
    let y = G / L;
    // Note: to avoid instability when γ is close to 1 round γ to 8 decimal places
    // y = y.toFixed(8);
    y = Math.round(y * 100000000.0) / 100000000.0;
    let n = 0.0;
    if (y > 0.0 && y != 1.0) {
        n = (1.0 - Math.pow(y, a)) / (1.0 - Math.pow(y, a + 1.0));
    }
    if (y == 1.0) {
        n = a / (a + 1.0);
    }
    let tc = 4.0 + 0.25 * tau;
    let Tsc = (1.0 - R) * (Th - 2.0) + R * (Te + (n * G) / H);
    let u;
    if (toff <= tc) {
        u = (0.5 * toff * toff * (Th - Tsc)) / (24 * tc);
    }
    if (toff > tc) {
        u = ((Th - Tsc) * (toff - 0.5 * tc)) / 24;
    }
    if (isNaN(u)) {
        u = 0;
    }
    return u;
}
