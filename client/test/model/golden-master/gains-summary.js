/* eslint-disable */
/* eslint-enable no-debugger */
let m, g;

/*---------------------------------------------------------------------------------------------
 // gains_summary
 // Calculates total solar gains, total internal gains and both together
 //      - Solar gains are calculated in calc.fabric
 //      - Lighting, cooking and appliances are calculated in new code
 //      - Water heating gains are calculated in water_heating calc.water_heating()
 //      - Gains for fans and pumps, losses and metabolice are calculated in calc.metabolic_losses_fans_and_pumps_gains()
 //      - Useful gains (after applying utilisation factor) are calculated in calc.space_heating()
 //
 //
 // Inputs from other modules:
 //      - data.gains_W
 //
 // Global Outputs:
 //	- data.total_internal_gains
 //	- data.total_solar_gains
 //	- data.total_internal_and_solar_gains
 //
 //---------------------------------------------------------------------------------------------*/

export function legacyGainsSummary(data) {
    data.total_internal_gains = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    data.total_internal_and_solar_gains = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    data.total_solar_gains = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for (m = 0; m < 12; m++) {
        for (g in data.gains_W) {
            data.total_internal_and_solar_gains[m] += data.gains_W[g][m];
            if (g != 'solar') {
                data.total_internal_gains[m] += data.gains_W[g][m];
            } else {
                data.total_solar_gains[m] += data.gains_W[g][m];
            }
        }
    }

    return data;
}
