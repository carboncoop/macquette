/* eslint-disable */
/* eslint-enable no-debugger */

export function legacyHoursOff(data) {
    data.household ??= {};
    data.temperature ??= {};
    data.temperature.hours_off = {
        weekday: get_hours_off_weekday(data),
        weekend: get_hours_off_weekend(data),
    };
}

/************************
 **  Hours off
 ************************/
function get_hours_off_weekday(data) {
    var hours_off = [];
    if (
        data.household['heating_weekday_off3_hours'] != undefined &&
        data.household['heating_weekday_off3_mins'] != undefined &&
        (data.household['heating_weekday_on3_hours'] !=
            data.household['heating_weekday_off3_hours'] ||
            data.household['heating_weekday_on3_mins'] !=
                data.household['heating_weekday_off3_mins'])
    ) {
        var time_on_1 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_on1_hours'],
            data.household['heating_weekday_on1_mins'],
            0,
            0,
        );
        var time_off_1 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_off1_hours'],
            data.household['heating_weekday_off1_mins'],
            0,
            0,
        );
        var time_on_2 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_on2_hours'],
            data.household['heating_weekday_on2_mins'],
            0,
            0,
        );
        var time_off_2 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_off2_hours'],
            data.household['heating_weekday_off2_mins'],
            0,
            0,
        );
        var time_on_3 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_on3_hours'],
            data.household['heating_weekday_on3_mins'],
            0,
            0,
        );
        var time_off_3 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_off3_hours'],
            data.household['heating_weekday_off3_mins'],
            0,
            0,
        );
        hours_off = get_hours_three_periods(
            time_on_1,
            time_off_1,
            time_on_2,
            time_off_2,
            time_on_3,
            time_off_3,
        );
    } else if (
        data.household['heating_weekday_off2_hours'] != undefined &&
        data.household['heating_weekday_off2_mins'] != undefined &&
        (data.household['heating_weekday_on2_hours'] !=
            data.household['heating_weekday_off2_hours'] ||
            data.household['heating_weekday_on2_mins'] !=
                data.household['heating_weekday_off2_mins'])
    ) {
        var time_on_1 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_on1_hours'],
            data.household['heating_weekday_on1_mins'],
            0,
            0,
        );
        var time_off_1 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_off1_hours'],
            data.household['heating_weekday_off1_mins'],
            0,
            0,
        );
        var time_on_2 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_on2_hours'],
            data.household['heating_weekday_on2_mins'],
            0,
            0,
        );
        var time_off_2 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_off2_hours'],
            data.household['heating_weekday_off2_mins'],
            0,
            0,
        );
        hours_off = get_hours_two_periods(time_on_1, time_off_1, time_on_2, time_off_2);
    } else if (
        data.household['heating_weekday_off1_hours'] !=
            data.household['heating_weekday_on1_hours'] ||
        data.household['heating_weekday_off1_mins'] !=
            data.household['heating_weekday_on1_mins']
    ) {
        var time_off = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_off1_hours'],
            data.household['heating_weekday_off1_mins'],
            0,
            0,
        );
        var time_on = new Date(
            2000,
            1,
            1,
            data.household['heating_weekday_on1_hours'],
            data.household['heating_weekday_on1_mins'],
            0,
            0,
        );
        hours_off.push(get_hours_off_one_period(time_on, time_off));
    } else {
        hours_off.push(0);
    }
    return hours_off;
}
function get_hours_off_weekend(data) {
    var hours_off = [];
    if (
        data.household['heating_weekend_off3_hours'] != undefined &&
        data.household['heating_weekend_off3_mins'] != undefined &&
        (data.household['heating_weekend_on3_hours'] !=
            data.household['heating_weekend_off3_hours'] ||
            data.household['heating_weekend_on3_mins'] !=
                data.household['heating_weekend_off3_mins'])
    ) {
        var time_on_1 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_on1_hours'],
            data.household['heating_weekend_on1_mins'],
            0,
            0,
        );
        var time_off_1 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_off1_hours'],
            data.household['heating_weekend_off1_mins'],
            0,
            0,
        );
        var time_on_2 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_on2_hours'],
            data.household['heating_weekend_on2_mins'],
            0,
            0,
        );
        var time_off_2 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_off2_hours'],
            data.household['heating_weekend_off2_mins'],
            0,
            0,
        );
        var time_on_3 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_on3_hours'],
            data.household['heating_weekend_on3_mins'],
            0,
            0,
        );
        var time_off_3 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_off3_hours'],
            data.household['heating_weekend_off3_mins'],
            0,
            0,
        );
        hours_off = get_hours_three_periods(
            time_on_1,
            time_off_1,
            time_on_2,
            time_off_2,
            time_on_3,
            time_off_3,
        );
    } else if (
        data.household['heating_weekend_off2_hours'] != undefined &&
        data.household['heating_weekend_off2_mins'] != undefined &&
        (data.household['heating_weekend_on2_hours'] !=
            data.household['heating_weekend_off2_hours'] ||
            data.household['heating_weekend_on2_mins'] !=
                data.household['heating_weekend_off2_mins'])
    ) {
        var time_on_1 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_on1_hours'],
            data.household['heating_weekend_on1_mins'],
            0,
            0,
        );
        var time_off_1 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_off1_hours'],
            data.household['heating_weekend_off1_mins'],
            0,
            0,
        );
        var time_on_2 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_on2_hours'],
            data.household['heating_weekend_on2_mins'],
            0,
            0,
        );
        var time_off_2 = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_off2_hours'],
            data.household['heating_weekend_off2_mins'],
            0,
            0,
        );
        hours_off = get_hours_two_periods(time_on_1, time_off_1, time_on_2, time_off_2);
    } else if (
        data.household['heating_weekend_off1_hours'] !=
            data.household['heating_weekend_on1_hours'] ||
        data.household['heating_weekend_off1_mins'] !=
            data.household['heating_weekend_on1_mins']
    ) {
        var time_off = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_off1_hours'],
            data.household['heating_weekend_off1_mins'],
            0,
            0,
        );
        var time_on = new Date(
            2000,
            1,
            1,
            data.household['heating_weekend_on1_hours'],
            data.household['heating_weekend_on1_mins'],
            0,
            0,
        );
        hours_off.push(get_hours_off_one_period(time_on, time_off));
    } else {
        hours_off.push(0);
    }
    return hours_off;
}
function get_hours_off_one_period(time_on, time_off) {
    // heating is on before midnight and off after midnight
    if (time_on > time_off) {
        return Math.abs(time_off - time_on) / 36e5;
    } else {
        time_on.setDate(time_on.getDate() + 1);
        return Math.abs(time_on - time_off) / 36e5;
    }
}
function get_hours_two_periods(time_on_1, time_off_1, time_on_2, time_off_2) {
    var hours_off = [];
    hours_off.push((time_on_2 - time_off_1) / 36e5);
    hours_off.push(get_hours_off_one_period(time_on_1, time_off_2));
    return hours_off;
}
function get_hours_three_periods(
    time_on_1,
    time_off_1,
    time_on_2,
    time_off_2,
    time_on_3,
    time_off_3,
) {
    var hours_off = [];
    hours_off.push((time_on_2 - time_off_1) / 36e5);
    hours_off.push((time_on_3 - time_off_2) / 36e5);
    hours_off.push(get_hours_off_one_period(time_on_1, time_off_3));
    return hours_off;
}
