/* eslint-disable */
/* eslint-enable no-debugger */

export function legacyUtilisationFactorMeanInternalTemperature(TMP, HLP, H, Ti, Te, G) {
    // Calculation of utilisation factor
    let tau = TMP / (3.6 * HLP);
    let a = 1.0 + tau / 15.0;
    let L = H * (Ti - Te);
    let y = G / L;
    // Note: to avoid instability when γ is close to 1 round γ to 8 decimal places
    // y = y.toFixed(8);
    y = Math.round(y * 100000000.0) / 100000000.0;
    let n = 0.0;
    if (y > 0.0 && y != 1.0) {
        n = (1.0 - Math.pow(y, a)) / (1.0 - Math.pow(y, a + 1.0));
    }
    if (y == 1.0) {
        n = a / (a + 1.0);
    }
    return n;
}
