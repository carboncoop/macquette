export type UtilisationFactorV8Params = {
  thermalMassParameter: number;
  heatLossParameter: number;
  heatLoss: number;
  heatGain: number;
  temperatureWhenHeated: number;
  externalTemperature: number;
};

function timeConstant(params: {
  thermalMassParameter: number;
  heatLossParameter: number;
}) {
  return params.thermalMassParameter / (3.6 * params.heatLossParameter);
}

export function utilisationFactorV8(input: UtilisationFactorV8Params): number {
  const timeConstant_ = timeConstant(input);
  const a = 1 + timeConstant_ / 15;
  const heatLossRateWatts =
    input.heatLoss * (input.temperatureWhenHeated - input.externalTemperature);
  // SAP says that if heatLossRateWatts = 0, then we should replace gamma with 10^6 to
  // avoid a division by zero. The problem with this is that the limit as the loss rate
  // approaches 0 is different depending on whether it approaches 0 from the negative side
  // or the positive side. Additionally, JavaScript handles division by 0 by using
  // floating point +/-Infinity, which actually resolves the issue cleanly. Therefore, we
  // skip the replacement with 10^6 step and handle the case of +/-Infinity explicitly.
  const gamma = input.heatGain / heatLossRateWatts;
  if (gamma === Infinity) {
    // eta -> 0 as gamma -> +infinity
    return 0;
  }
  if (gamma <= 0) {
    return 1;
  }
  // If gamma is close to 1, the general case formula is either undefined due to a
  // division by 0 or erratic due to FP instability. SAP says to solve this by rounding
  // gamma to 8 decimal places and completing the function at gamma = 1 with its limit.
  //
  // Completing with the limit is a useful approach, but rounding to 8 decimal places is
  // a poor way of dealing with the FP instability. Instead, we replace the function
  // with its Taylor series when gamma is close (in floating point terms) to 1.
  if (1 - 1e-21 < gamma && gamma < 1 + 1e-21) {
    return a / (a + 1) - (a * (gamma - 1)) / (2 * (a + 1)); // First two terms of the Taylor series
  } else {
    const eta = (1 - gamma ** a) / (1 - gamma ** (a + 1));
    return eta;
  }
}
