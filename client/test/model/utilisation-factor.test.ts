import fc from 'fast-check';
import {
  UtilisationFactorFlags,
  UtilisationFactorParams,
  utilisationFactor,
} from '../../src/model/modules/heat-gain-utilisation-factor';
import { sensibleFloat } from '../arbitraries/legacy-values';
import { legacyUtilisationFactorMeanInternalTemperature } from './golden-master/utilisation-factor/legacy-mean-internal-temperature';
import { legacyUtilisationFactorSpaceHeating } from './golden-master/utilisation-factor/legacy-space-heating';
import { utilisationFactorV5 } from './golden-master/utilisation-factor/v5';
import { utilisationFactorV8 } from './golden-master/utilisation-factor/v8';

const arbUtilisationFactorParams: fc.Arbitrary<UtilisationFactorParams> = fc.record({
  thermalMassParameter: sensibleFloat,
  heatLossParameter: sensibleFloat,
  heatLoss: sensibleFloat,
  heatGain: sensibleFloat,
  temperatureWhenHeated: sensibleFloat,
  externalTemperature: sensibleFloat,
});

describe('heat gain utilisation factor', () => {
  test('golden master legacy space heating', () => {
    const legacySpaceHeatingFlags: UtilisationFactorFlags = {
      preserveGammaPrecision: false,
      etaIs1IfGammaIsLessThanOrEqualTo0: true,
    };
    fc.assert(
      fc.property(arbUtilisationFactorParams, (params) => {
        const received = utilisationFactor(params, legacySpaceHeatingFlags, () => {
          throw new fc.PreconditionFailure();
        });
        const expected = legacyUtilisationFactorSpaceHeating(
          params.thermalMassParameter,
          params.heatLossParameter,
          params.heatLoss,
          params.temperatureWhenHeated,
          params.externalTemperature,
          params.heatGain,
        );
        expect(received).toBeApproximately(expected);
      }),
    );
  });
  test('golden master legacy mean internal temperature', () => {
    const legacyMeanInternalTemperatureFlags: UtilisationFactorFlags = {
      preserveGammaPrecision: false,
      etaIs1IfGammaIsLessThanOrEqualTo0: false,
    };
    fc.assert(
      fc.property(arbUtilisationFactorParams, (params) => {
        const received = utilisationFactor(
          params,
          legacyMeanInternalTemperatureFlags,
          () => {
            throw new fc.PreconditionFailure();
          },
        );
        const expected = legacyUtilisationFactorMeanInternalTemperature(
          params.thermalMassParameter,
          params.heatLossParameter,
          params.heatLoss,
          params.temperatureWhenHeated,
          params.externalTemperature,
          params.heatGain,
        );
        expect(received).toBeApproximately(expected);
      }),
    );
  });
  test('golden master v5', () => {
    const v5Flags: UtilisationFactorFlags = {
      preserveGammaPrecision: true,
      etaIs1IfGammaIsLessThanOrEqualTo0: true,
    };
    fc.assert(
      fc.property(arbUtilisationFactorParams, (params) => {
        const received = utilisationFactor(params, v5Flags, () => {
          throw new fc.PreconditionFailure();
        });
        const expected = utilisationFactorV5(params);
        expect(received).toBeApproximately(expected);
      }),
    );
  });
  test('golden master v8', () => {
    const v8Flags: UtilisationFactorFlags = {
      preserveGammaPrecision: true,
      etaIs1IfGammaIsLessThanOrEqualTo0: true,
    };
    fc.assert(
      fc.property(arbUtilisationFactorParams, (params) => {
        const received = utilisationFactor(params, v8Flags, () => {
          throw new fc.PreconditionFailure();
        });
        const expected = utilisationFactorV8(params);
        expect(received).toBeApproximately(expected);
      }),
    );
  });
});
