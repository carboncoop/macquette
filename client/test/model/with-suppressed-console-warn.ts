export function withSuppressedConsoleWarn<T>(fn: () => T): T {
  const oldConsoleWarn = console.warn;
  console.warn = () => undefined;
  try {
    return fn();
  } finally {
    console.warn = oldConsoleWarn;
  }
}
