import { periodLength, periodsOverlapping } from '../src/periods/core';
import { parseTime, timeToString } from '../src/periods/strings';

function time(hour: number, minute = 0) {
  return hour * 60 + minute;
}
describe('periods', () => {
  describe('core', () => {
    describe('periodLength', () => {
      const cases = [
        { start: time(0, 0), end: time(1, 30), expected: 90 },
        { start: time(1, 53), end: time(2, 23), expected: 30 },
        { start: time(23, 0), end: time(1, 0), expected: 120 },
        { start: time(23), end: time(0), expected: 60 },
      ];
      it.each(cases)(
        'should calculate the length correctly - [$start, $end) = $expected',
        (period) => {
          expect(periodLength(period)).toBe(period.expected);
        },
      );
    });

    describe('periodsOverlapping', () => {
      test('A overlaps B on the left', () => {
        const a = { start: time(0), end: time(10) };
        const b = { start: time(5), end: time(15) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });
      test('A overlaps B on the left and A crosses midnight', () => {
        const a = { start: time(23), end: time(10) };
        const b = { start: time(5), end: time(15) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });
      test('A overlaps B on the left and B crosses midnight', () => {
        const a = { start: time(5), end: time(20) };
        const b = { start: time(10), end: time(7) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });
      test('A overlaps B on the left and both cross midnight', () => {
        const a = { start: time(20), end: time(10) };
        const b = { start: time(10), end: time(5) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });

      test('A overlaps B on the right', () => {
        const a = { start: time(10), end: time(20) };
        const b = { start: time(5), end: time(15) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });
      test('A overlaps B on the right and B crosses midnight', () => {
        const a = { start: time(10), end: time(20) };
        const b = { start: time(23), end: time(15) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });
      test('A overlaps B on the right and A crosses midnight', () => {
        const a = { start: time(10), end: time(0) };
        const b = { start: time(5), end: time(15) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });
      test('A overlaps B on the right and both cross midnight', () => {
        const a = { start: time(10), end: time(0) };
        const b = { start: time(23), end: time(15) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });

      test('A contains B', () => {
        const a = { start: time(10), end: time(15) };
        const b = { start: time(12), end: time(13) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });
      test('A contains B and A crosses midnight', () => {
        const a = { start: time(20), end: time(10) };
        const b = { start: time(0), end: time(5) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });
      test('A contains B and both cross midnight', () => {
        const a = { start: time(20), end: time(10) };
        const b = { start: time(22), end: time(5) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });

      test('B contains A', () => {
        const a = { start: time(12), end: time(13) };
        const b = { start: time(10), end: time(15) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });
      test('B contains A and B crosses midnight', () => {
        const a = { start: time(12), end: time(13) };
        const b = { start: time(22), end: time(15) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });
      test('B contains A and both cross midnight', () => {
        const a = { start: time(23), end: time(1) };
        const b = { start: time(22), end: time(2) };
        expect(periodsOverlapping(a, b)).toBe(true);
      });

      test('A is later than B', () => {
        const a = { start: time(15), end: time(20) };
        const b = { start: time(5), end: time(10) };
        expect(periodsOverlapping(a, b)).toBe(false);
      });
      test('A is later than B and B crosses midnight', () => {
        const a = { start: time(15), end: time(20) };
        const b = { start: time(23), end: time(10) };
        expect(periodsOverlapping(a, b)).toBe(false);
      });

      test('B is later than A', () => {
        const a = { start: time(5), end: time(10) };
        const b = { start: time(15), end: time(20) };
        expect(periodsOverlapping(a, b)).toBe(false);
      });
      test('B is later than A and A crosses midnight', () => {
        const a = { start: time(23), end: time(10) };
        const b = { start: time(15), end: time(20) };
        expect(periodsOverlapping(a, b)).toBe(false);
      });
    });
  });
  describe('strings', () => {
    test('timeToString', () => {
      expect(timeToString(60, true)).toBe('01:00');
      expect(timeToString(60, false)).toBe('1:00');
    });

    describe('parseTime', () => {
      it('empty string parses to null with no error', () => {
        expect(parseTime('')).toEqual([null, null]);
      });
      it('single digit parsed to hour', () => {
        expect(parseTime('1')).toEqual([1 * 60, null]);
      });
      it('two digits parsed to hour', () => {
        expect(parseTime('12')).toEqual([12 * 60, null]);
      });
      it('hh:mm parsed correctly', () => {
        expect(parseTime('12:34')).toEqual([12 * 60 + 34, null]);
      });
      it('hh:m rejected', () => {
        expect(parseTime('12:3')).toEqual([null, 'Not a valid time (hh:mm)']);
      });
      it('text rejected', () => {
        expect(parseTime('ab:cd')).toEqual([null, 'Not a valid time (hh:mm)']);
      });
      it('hour too big', () => {
        expect(parseTime('24:20')).toEqual([null, 'Hour should be between 0 and 23']);
      });
      it('minute too big', () => {
        expect(parseTime('11:60')).toEqual([null, 'Minute should be between 0 and 59']);
      });
    });
  });
});
