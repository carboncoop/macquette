import { LinkError, NodeNotFoundError, Tree, TreeNode } from '../../src/helpers/tree';

describe('TreeNode', () => {
  it('should add children in order', () => {
    const a = new TreeNode('A', 'A');
    const b = new TreeNode('B', 'B');
    const c = new TreeNode('C', 'C');

    a.addChild(b);
    a.addChild(c);

    expect(a.children).toEqual([b, c]);
  });

  it('should replace children correctly', () => {
    const a = new TreeNode('A', 'A');
    const b = new TreeNode('B', 'B');
    const c = new TreeNode('C', 'C');
    const d = new TreeNode('D', 'D');
    const e = new TreeNode('E', 'E');

    a.addChild(b);
    a.addChild(c);
    a.addChild(d);

    a.replaceChild(c, [e]);

    expect(a.children).toEqual([b, e, d]);
  });

  it('should fail to replace a non-child', () => {
    const a = new TreeNode('A', 'A');
    const b = new TreeNode('B', 'B');

    expect(() => a.replaceChild(b, [])).toThrow(NodeNotFoundError);
  });
});

describe('Tree', () => {
  it('can be instantiated', () => {
    expect(
      () =>
        new Tree([
          new TreeNode('A', 'A'),
          new TreeNode('B', 'B'),
          new TreeNode('C', 'C'),
        ]),
    ).not.toThrow();
  });
});

describe('Tree.link', () => {
  it('should fail with unknown child', () => {
    const tree = new Tree([new TreeNode('A', 'A')]);
    expect(() => tree.link('B', 'A')).toThrow(NodeNotFoundError);
  });

  it('should fail with unknown parent', () => {
    const tree = new Tree([new TreeNode('A', 'A')]);
    expect(() => tree.link('A', 'B')).toThrow(LinkError);
  });

  it('should succeed with known nodes', () => {
    const tree = new Tree([new TreeNode('A', 'A'), new TreeNode('B', 'B')]);
    expect(() => tree.link('A', 'B')).not.toThrow();
  });
});

describe('Tree.removeNodeAndPromoteChildren', () => {
  it('should work', () => {
    const a = new TreeNode('A', 'A');
    const b = new TreeNode('B', 'B');
    const c = new TreeNode('C', 'C');
    const d = new TreeNode('D', 'D');
    const tree = new Tree([a, b, c, d]);
    tree.link('B', 'A');
    tree.link('C', 'B');
    tree.link('D', 'B');

    tree.removeNodeAndPromoteChildren('B');

    expect(a.children).toEqual([c, d]);
    expect(tree.nodes).not.toContain(b);
  });

  it('should fail for an unknown node', () => {
    const a = new TreeNode('A', 'A');
    const tree = new Tree([a]);

    expect(() => tree.removeNodeAndPromoteChildren('B')).toThrow(NodeNotFoundError);
  });
});

describe('Tree.linearise', () => {
  it('should produce a list of tree nodes by insertion order', () => {
    const tree = new Tree([
      new TreeNode('A', 'A'),
      new TreeNode('B', 'B'),
      new TreeNode('C', 'C'),
    ]);

    expect(tree.linearise()).toEqual({
      A: 'A',
      B: 'B',
      C: 'C',
    });
  });
});
