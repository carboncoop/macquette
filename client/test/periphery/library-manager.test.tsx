/** @jest-environment jsdom */

import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { LibraryManager } from '../../src/periphery/library-manager';
import { FakeLibraryListStore } from '../../src/periphery/stores/library-list';
import { FakeOrganisationListStore } from '../../src/periphery/stores/organisation-list';
import { fakeLibrary, fakeOrganisation } from './fakes';

describe('LibraryManager', () => {
  // eslint-disable-next-line jest/expect-expect
  test('renders', () => {
    const fakeStore = new FakeLibraryListStore([fakeLibrary()]);
    render(<LibraryManager libraryStore={fakeStore} />);
  });

  describe('create a library', () => {
    test('can create a private library', async () => {
      const fakeLibraryStore = new FakeLibraryListStore([]);
      const fakeOrganisationListStore = new FakeOrganisationListStore([]);
      render(
        <LibraryManager
          libraryStore={fakeLibraryStore}
          organisationStore={fakeOrganisationListStore}
        />,
      );

      const user = userEvent.setup();
      await user.click(screen.getByRole('button', { name: 'New library' }));

      const name = screen.getByLabelText('Name:');
      expect(name).toHaveFocus();
      await user.keyboard('ZX24');
      await user.selectOptions(screen.getByLabelText('Type:'), ['Fabric (wall-like)']);
      await user.click(screen.getByLabelText('Private'));
      await user.click(screen.getByRole('button', { name: /create/i }));

      const deleteButtons = screen.queryAllByRole('button', { name: /delete/i });
      expect(deleteButtons).toHaveLength(1);
    });
  });

  describe('share a library', () => {
    describe('permissions', () => {
      test('org canShareLibraries makes no diff to whether it shows up in sharing list', async () => {
        const organisations = [
          fakeOrganisation({ canShareLibraries: true }),
          fakeOrganisation({ canShareLibraries: false }),
        ];
        const library = fakeLibrary({ can_share: true });
        const fakeOrganisationListStore = new FakeOrganisationListStore(organisations);
        const fakeLibraryStore = new FakeLibraryListStore([library]);
        render(
          <LibraryManager
            libraryStore={fakeLibraryStore}
            organisationStore={fakeOrganisationListStore}
          />,
        );

        const user = userEvent.setup();
        await user.click(
          screen.getByRole('button', { name: `Edit sharing for ${library.name}` }),
        );

        const orgOnScreen = screen.queryAllByRole('rowheader', {
          name: organisations[0]!.name,
        });
        expect(orgOnScreen).toHaveLength(2);
      });

      test("library can_share=false doesn't show share button", () => {
        const fakeLibraryStore = new FakeLibraryListStore([
          fakeLibrary({ can_share: false }),
        ]);
        render(<LibraryManager libraryStore={fakeLibraryStore} />);

        const shareButtons = screen.queryAllByRole('button', { name: /edit sharing/i });
        expect(shareButtons).toHaveLength(0);
      });

      test('library can_share=true shows share button', () => {
        const fakeLibraryStore = new FakeLibraryListStore([
          fakeLibrary({ can_share: true }),
        ]);
        render(<LibraryManager libraryStore={fakeLibraryStore} />);

        const shareButtons = screen.queryAllByRole('button', { name: /edit sharing/i });
        expect(shareButtons).toHaveLength(1);
      });
    });

    describe('button functionality', () => {
      test('can share with an organisation', async () => {
        const organisation = fakeOrganisation({ canShareLibraries: true });
        const library = fakeLibrary({ can_share: true });
        const fakeOrganisationListStore = new FakeOrganisationListStore([organisation]);
        const fakeLibraryStore = new FakeLibraryListStore([library]);
        render(
          <LibraryManager
            libraryStore={fakeLibraryStore}
            organisationStore={fakeOrganisationListStore}
          />,
        );

        const user = userEvent.setup();
        await user.click(
          screen.getByRole('button', { name: `Edit sharing for ${library.name}` }),
        );
        await user.click(
          screen.getByRole('button', { name: `Share with ${organisation.name}` }),
        );

        const shareButtons = screen.queryAllByRole('button', { name: /share /i });
        const stopSharingButtons = screen.queryAllByRole('button', {
          name: /stop sharing/i,
        });

        expect(shareButtons).toHaveLength(0);
        expect(stopSharingButtons).toHaveLength(1);
      });

      test('can unshare with an organisation', async () => {
        const organisation = fakeOrganisation({ canShareLibraries: true });
        const library = fakeLibrary({ can_share: true, shared_with: [organisation] });
        const fakeOrganisationListStore = new FakeOrganisationListStore([organisation]);
        const fakeLibraryStore = new FakeLibraryListStore([library]);
        render(
          <LibraryManager
            libraryStore={fakeLibraryStore}
            organisationStore={fakeOrganisationListStore}
          />,
        );

        const user = userEvent.setup();
        await user.click(
          screen.getByRole('button', { name: `Edit sharing for ${library.name}` }),
        );
        await user.click(
          screen.getByRole('button', { name: `Stop sharing with ${organisation.name}` }),
        );

        const shareButtons = screen.queryAllByRole('button', { name: /share /i });
        const stopSharingButtons = screen.queryAllByRole('button', {
          name: /stop sharing/i,
        });

        expect(shareButtons).toHaveLength(1);
        expect(stopSharingButtons).toHaveLength(0);
      });
    });
  });

  describe('delete a library', () => {
    let confirmSpy: jest.SpyInstance | null = null;
    beforeEach(() => {
      confirmSpy = jest.spyOn(window, 'confirm');
      confirmSpy.mockImplementation(jest.fn(() => true));
    });

    test('delete button deletes if library is writable', async () => {
      const library = fakeLibrary({ can_write: true });
      const fakeStore = new FakeLibraryListStore([library]);
      render(<LibraryManager libraryStore={fakeStore} />);

      const user = userEvent.setup();
      await user.click(screen.getByRole('button', { name: `Delete ${library.name}` }));

      const deleteButtons = screen.queryAllByRole('button', { name: /delete/i });
      expect(deleteButtons).toHaveLength(0);
    });

    test("delete button doesn't delete if library is not writable", async () => {
      const library = fakeLibrary({ can_write: false });
      const fakeStore = new FakeLibraryListStore([library]);
      render(<LibraryManager libraryStore={fakeStore} />);

      const user = userEvent.setup();
      await user.click(screen.getByRole('button', { name: `Delete ${library.name}` }));

      const deleteButtons = screen.queryAllByRole('button', { name: /delete/i });
      expect(deleteButtons).toHaveLength(1);
    });

    afterEach(() => confirmSpy?.mockRestore());
  });
});
