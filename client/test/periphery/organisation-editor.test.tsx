/** @jest-environment jsdom */

import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { OrganisationEditor } from '../../src/periphery/organisation-editor';
import { FakeOrganisationStore } from '../../src/periphery/stores/organisation';
import { fakeOrganisation } from './fakes';

describe('OrganisationEditor', () => {
  // eslint-disable-next-line jest/expect-expect
  test('renders when there is no organisation yet', () => {
    const fakeStore = new FakeOrganisationStore(null);
    render(<OrganisationEditor organisationStore={fakeStore} userId={''} />);
  });

  // eslint-disable-next-line jest/expect-expect
  test('renders a fake organisation', () => {
    const fakeStore = new FakeOrganisationStore(fakeOrganisation());
    render(<OrganisationEditor organisationStore={fakeStore} userId={''} />);
  });

  test('can invite a user', async () => {
    const fakeStore = new FakeOrganisationStore(
      fakeOrganisation({ canEditMembersAndRoles: true }),
    );
    render(<OrganisationEditor organisationStore={fakeStore} userId={''} />);

    const actionButtonsPre = screen.queryAllByRole('button', { name: /actions/i });
    const prevButtons = actionButtonsPre.length;

    const user = userEvent.setup();
    await user.click(screen.getByRole('button', { name: /invite user/i }));

    const name = screen.getByLabelText('Name:');
    expect(name).toHaveFocus();
    await user.keyboard('fred');
    await user.click(screen.getByLabelText('Email:'));
    await user.keyboard('eric@test.com');
    await user.click(screen.getByRole('button', { name: 'Invite' }));

    const actionButtonsPost = screen.queryAllByRole('button', { name: /actions/i });
    expect(actionButtonsPost).toHaveLength(prevButtons + 1);
  });

  test('can promote admin & demote again', async () => {
    const fakeStore = new FakeOrganisationStore(
      fakeOrganisation({
        canEditMembersAndRoles: true,
        members: [
          {
            id: 'admin',
            name: 'fake user name',
            email: 'admin@email.address',
            lastLogin: 'never',
            isAdmin: true,
            isLibrarian: true,
          },
          {
            id: 'user',
            name: 'fake user name',
            email: 'user@email.address',
            lastLogin: 'never',
            isAdmin: false,
            isLibrarian: false,
          },
        ],
      }),
    );
    render(<OrganisationEditor organisationStore={fakeStore} userId={'admin'} />);

    const user = userEvent.setup();

    // Promote
    let actionButtons = screen.queryAllByRole('button', { name: /actions/i });
    await user.click(actionButtons[1]!);
    await user.click(screen.getByRole('menuitem', { name: /make admin/i }));

    let roleCells = screen.queryAllByRole('cell', { name: /Admin/ });
    expect(roleCells).toHaveLength(2);

    // Demote again
    actionButtons = screen.queryAllByRole('button', { name: /actions/i });
    await user.click(actionButtons[1]!);
    await user.click(screen.getByRole('menuitem', { name: /demote admin/i }));

    roleCells = screen.queryAllByRole('cell', { name: /Admin/ });
    expect(roleCells).toHaveLength(1);
  });

  test('can promote librarian & demote again', async () => {
    const fakeStore = new FakeOrganisationStore(
      fakeOrganisation({
        canEditMembersAndRoles: true,
        members: [
          {
            id: 'admin',
            name: 'fake user name',
            email: 'admin@email.address',
            lastLogin: 'never',
            isAdmin: true,
            isLibrarian: false,
          },
        ],
      }),
    );
    render(<OrganisationEditor organisationStore={fakeStore} userId={'admin'} />);

    const user = userEvent.setup();

    // Promote
    let actionButtons = screen.queryAllByRole('button', { name: /actions/i });
    await user.click(actionButtons[0]!);
    await user.click(screen.getByRole('menuitem', { name: /make librarian/i }));

    let roleCells = screen.queryAllByRole('cell', { name: /Librarian/ });
    expect(roleCells).toHaveLength(1);

    // Demote again
    actionButtons = screen.queryAllByRole('button', { name: /actions/i });
    await user.click(actionButtons[0]!);
    await user.click(screen.getByRole('menuitem', { name: /demote librarian/i }));

    roleCells = screen.queryAllByRole('cell', { name: /Librarian/ });
    expect(roleCells).toHaveLength(0);
  });

  test('can remove user from organisation', async () => {
    const fakeStore = new FakeOrganisationStore(
      fakeOrganisation({
        canEditMembersAndRoles: true,
        members: [
          {
            id: 'admin',
            name: 'fake user name',
            email: 'admin@email.address',
            lastLogin: 'never',
            isAdmin: true,
            isLibrarian: false,
          },
          {
            id: 'normal',
            name: 'fake user name',
            email: 'normal@email.address',
            lastLogin: 'never',
            isAdmin: false,
            isLibrarian: false,
          },
        ],
      }),
    );
    render(<OrganisationEditor organisationStore={fakeStore} userId={'admin'} />);

    const user = userEvent.setup();

    const actionButtons = screen.queryAllByRole('button', { name: /actions/i });
    await user.click(actionButtons[1]!);
    await user.click(screen.getByRole('menuitem', { name: /remove/i }));

    const roleCells = screen.queryAllByRole('row');
    expect(roleCells).toHaveLength(2);
  });
});
