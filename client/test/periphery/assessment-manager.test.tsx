/** @jest-environment jsdom */

import '@testing-library/jest-dom';
import { act, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import { AssessmentManager } from '../../src/periphery/assessment-manager';
import { FakeAssessmentListStore } from '../../src/periphery/stores/assessment-list';
import { FakeOrganisationListStore } from '../../src/periphery/stores/organisation-list';
import { fakeAssessment, fakeOrganisation } from './fakes';

describe('AssessmentManager', () => {
  // eslint-disable-next-line jest/expect-expect
  test('renders when the fetch is in progress', () => {
    const assessmentStore = new FakeAssessmentListStore({ fetchInProgress: true });
    const organisationStore = new FakeOrganisationListStore([]);
    render(
      <AssessmentManager
        organisationStore={organisationStore}
        assessmentStore={assessmentStore}
      />,
    );
  });

  // eslint-disable-next-line jest/expect-expect
  test('renders when there are some assessments', () => {
    const fakes = [fakeAssessment(), fakeAssessment(), fakeAssessment()];
    const assessmentStore = new FakeAssessmentListStore({ list: fakes });
    const organisationStore = new FakeOrganisationListStore([]);
    render(
      <AssessmentManager
        organisationStore={organisationStore}
        assessmentStore={assessmentStore}
      />,
    );
  });

  describe('create an assessment', () => {
    test('can create a private assessment', async () => {
      const assessmentStore = new FakeAssessmentListStore({ fetchInProgress: true });
      const organisationStore = new FakeOrganisationListStore([]);
      render(
        <AssessmentManager
          organisationStore={organisationStore}
          assessmentStore={assessmentStore}
        />,
      );

      const user = userEvent.setup();
      await user.click(screen.getByRole('button', { name: 'Create assessment' }));

      const name = screen.getByLabelText('Name:');
      expect(name).toHaveFocus();
      await user.keyboard('PEQ4123');
      await user.click(screen.getByLabelText('Private'));
      await user.click(screen.getByRole('button', { name: 'Create' }));

      const actionButtons = screen.queryAllByRole('button', { name: /actions/i });
      expect(actionButtons).toHaveLength(1);
    });
  });

  describe('delete an assessment', () => {
    let confirmSpy: jest.SpyInstance | null = null;
    beforeEach(() => {
      confirmSpy = jest.spyOn(window, 'confirm');
      confirmSpy.mockImplementation(jest.fn(() => true));
    });

    test('can delete an assessment', async () => {
      const assessmentStore = new FakeAssessmentListStore({ list: [fakeAssessment()] });
      const organisationStore = new FakeOrganisationListStore([]);

      render(
        <AssessmentManager
          organisationStore={organisationStore}
          assessmentStore={assessmentStore}
        />,
      );

      const user = userEvent.setup();

      const actionButtons = screen.queryAllByRole('button', { name: /actions/i });
      await user.click(actionButtons[0]!);
      await user.click(screen.getByRole('menuitem', { name: /delete/i }));

      const tableRows = screen.queryAllByRole('row');
      expect(tableRows).toHaveLength(0);
    });

    afterEach(() => confirmSpy?.mockRestore());
  });

  describe('rename an assessment', () => {
    let confirmSpy: jest.SpyInstance | null = null;
    beforeEach(() => {
      confirmSpy = jest.spyOn(window, 'prompt');
      confirmSpy.mockImplementation(jest.fn(() => 'new name'));
    });

    test('can rename an assessment', async () => {
      const assessmentStore = new FakeAssessmentListStore({ list: [fakeAssessment()] });
      const organisationStore = new FakeOrganisationListStore([]);

      render(
        <AssessmentManager
          organisationStore={organisationStore}
          assessmentStore={assessmentStore}
        />,
      );

      const user = userEvent.setup();

      const actionButtons = screen.queryAllByRole('button', { name: /actions/i });
      await user.click(actionButtons[0]!);
      await user.click(screen.getByRole('menuitem', { name: /rename/i }));

      const cells = screen.queryAllByRole('cell', { name: /new name/i });
      expect(cells).toHaveLength(1);
    });

    afterEach(() => confirmSpy?.mockRestore());
  });

  describe('search functionality', () => {
    test('search by name/description', async () => {
      const assessmentStore = new FakeAssessmentListStore({});
      const organisationStore = new FakeOrganisationListStore([]);

      render(
        <AssessmentManager
          organisationStore={organisationStore}
          assessmentStore={assessmentStore}
        />,
      );

      const user = userEvent.setup();
      const name = screen.getByLabelText(/name\/description/i);
      await user.click(name);
      await user.keyboard('test');
      const somewhereElse = screen.queryAllByRole('heading')[0]!;
      await user.click(somewhereElse);

      // There's a debounce to wait 100ms less than this.
      await act(async () => await new Promise((r) => setTimeout(r, 500)));

      expect(assessmentStore.getSnapshot().lastQuery.search).toBe('test');
    });

    test('search by organisation', async () => {
      const org = fakeOrganisation();
      const assessmentStore = new FakeAssessmentListStore({});
      const organisationStore = new FakeOrganisationListStore([org]);

      render(
        <AssessmentManager
          organisationStore={organisationStore}
          assessmentStore={assessmentStore}
        />,
      );

      const user = userEvent.setup();
      await user.selectOptions(screen.getByLabelText(/organisation/i), [org.name]);

      // There's a debounce to wait 100ms less than this.
      await act(async () => await new Promise((r) => setTimeout(r, 500)));

      expect(assessmentStore.getSnapshot().lastQuery.organisation).toEqual(org.id);
    });
  });

  test('duplicate an assessment', async () => {
    const assessmentStore = new FakeAssessmentListStore({ list: [fakeAssessment()] });
    const organisationStore = new FakeOrganisationListStore([]);

    render(
      <AssessmentManager
        organisationStore={organisationStore}
        assessmentStore={assessmentStore}
      />,
    );

    const user = userEvent.setup();

    const actionButtons = screen.queryAllByRole('button', { name: /actions/i });
    await user.click(actionButtons[0]!);
    await user.click(screen.getByRole('menuitem', { name: /duplicate/i }));

    const tableRows = screen.queryAllByRole('row');
    expect(tableRows).toHaveLength(1 + 2);
  });
});
