import '@testing-library/jest-dom';
import {
  AssessmentMetadata,
  LibraryMetadata,
  libraryTypes,
} from '../../src/data-schemas/api-metadata';
import type { Organisation } from '../../src/data-schemas/organisations';

let id = 1;
function generateId() {
  return (id++).toString();
}

export function fakeAssessment(): AssessmentMetadata {
  return {
    id: generateId(),
    name: 'name',
    description: 'description',
    status: 'In progress',
    updatedAt: new Date(),
    createdAt: new Date(),
    owner: { id: generateId(), name: 'Name', email: 'fake@email.address' },
    organisation: null,
  };
}

export function fakeOrganisation(opts?: {
  canShareLibraries?: boolean;
  canEditMembersAndRoles?: boolean;
  members?: Organisation['members'];
}): Organisation {
  return {
    id: generateId(),
    name: 'fake organisation name',
    assessments: 412,
    members: opts?.members ?? [
      {
        id: 'FAKE-ID',
        name: 'fake user name',
        email: 'fake@email.address',
        lastLogin: 'never',
        isAdmin: false,
        isLibrarian: false,
      },
    ],
    permissions: {
      canEditMembersAndRoles: opts?.canEditMembersAndRoles ?? false,
      canEditLibraries: false,
      canShareLibraries: opts?.canShareLibraries ?? false,
    },
  };
}

export function fakeLibrary(opts?: {
  can_write?: boolean;
  can_share?: boolean;
  shared_with?: { id: string; name: string }[];
}): LibraryMetadata {
  return {
    id: generateId(),
    name: 'fake library name',
    type: libraryTypes[0],
    created_at: new Date(),
    updated_at: new Date(),
    permissions: {
      can_write: opts?.can_write ?? false,
      can_share: opts?.can_share ?? false,
    },
    owner: {
      type: 'personal',
      id: 'FAKE-ID',
      name: 'fake owner name',
      email: 'fake.email@address',
    },
    shared_with: opts?.shared_with ?? [],
  };
}
