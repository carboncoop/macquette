import React from 'react';

import { PropsOf } from '../../helpers/props-of';
import { Shadow } from '../../helpers/shadow-object-type';
import { LockableContext } from './lockable-context';

type Option<T> = {
  value: T;
  display: string;
  disabled?: boolean;
  legacy?: boolean;
};

type Section<T> = {
  name: string;
  options: Option<T>[];
};

type BasicSelectProps<T> = {
  value: T | null;
  onChange: (value: T) => void;
  error?: string | undefined;
  options?: Option<T>[];
  sections?: Section<T>[];
};

export type SelectProps<T> = Shadow<PropsOf<'select'>, BasicSelectProps<T>>;

const UNSELECTED_VALUE = '__unselected__';

function Option<T extends string>({
  option,
  value,
}: {
  option: Option<T>;
  value: T | null;
}) {
  if (option.legacy === true && option.value !== value) {
    return null;
  }

  return (
    <option key={option.value} value={option.value} disabled={option.disabled}>
      {option.display}
    </option>
  );
}

function Section<T extends string>({
  name,
  options,
  value,
}: {
  name: string;
  options: Option<T>[];
  value: T | null;
}) {
  return (
    <optgroup label={name}>
      {options.map((option, idx) => (
        <Option key={idx} option={option} value={value} />
      ))}
    </optgroup>
  );
}

export function Select<T extends string>({
  onChange,
  value,
  error,
  options,
  sections,
  style: styleProp,
  ...passthroughProps
}: SelectProps<T>) {
  const { locked } = React.useContext(LockableContext);

  function castTargetValue(evt: React.ChangeEvent<HTMLSelectElement>) {
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    return evt.target.value as T;
  }
  let unselected;
  if (value === null) {
    unselected = (
      <option value={UNSELECTED_VALUE} disabled>
        Select...
      </option>
    );
  } else {
    unselected = <></>;
  }
  const style: React.CSSProperties = {
    ...(styleProp ?? {}),
    ...(error === undefined ? {} : { borderColor: 'red' }),
  };
  const { disabled: disabledProp, ...rest } = passthroughProps;
  const disabled = disabledProp ?? locked ?? false;

  return (
    <select
      onChange={(evt) => onChange(castTargetValue(evt))}
      value={value ?? UNSELECTED_VALUE}
      style={style}
      title={error}
      disabled={disabled}
      {...rest}
    >
      {unselected}
      {options?.map((entry, idx) => <Option key={idx} option={entry} value={value} />)}
      {sections?.map((section, idx) => (
        <Section key={idx} name={section.name} options={section.options} value={value} />
      ))}
    </select>
  );
}
