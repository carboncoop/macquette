import React from 'react';

import { omit } from 'lodash';
import { PropsOf } from '../../helpers/props-of';
import { Shadow } from '../../helpers/shadow-object-type';
import { LockableContext } from './lockable-context';

type BasicCheckboxProps = {
  value: boolean;
  onChange: (checked: boolean) => void;
};

export type CheckboxProps = Shadow<PropsOf<'input'>, BasicCheckboxProps>;

export function CheckboxInput({ value, onChange, ...props }: CheckboxProps) {
  const { locked } = React.useContext(LockableContext);

  const disabled = props.disabled ?? locked ?? false;

  return (
    <input
      type="checkbox"
      checked={value}
      className="big-checkbox"
      onChange={(evt) => {
        if (props.readOnly === true || disabled === true) {
          return;
        } else {
          onChange(evt.target.checked);
        }
      }}
      disabled={disabled}
      {...omit(props, ['disabled'])}
    />
  );
}
