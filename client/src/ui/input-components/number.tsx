import React from 'react';

import { PropsOf } from '../../helpers/props-of';
import { Shadow } from '../../helpers/shadow-object-type';
import { LockableContext } from './lockable-context';
import { useStateTracker } from './use-state-tracker';

type BasicNumberInputProps = {
  value: number | null;
  onChange: (value: number | null) => void;
  unit?: string;
  error?: string | undefined;
};

export type NumberInputProps = Shadow<PropsOf<'input'>, BasicNumberInputProps>;

export function NumberInput({
  value,
  onChange,
  style: styleProp,
  unit,
  error,
  ...passthroughProps
}: NumberInputProps) {
  const { locked } = React.useContext(LockableContext);
  const [innerState, setInnerState] = useStateTracker<string>(value?.toString(10) ?? '');

  const parseable = innerState !== null && /^-?([1-9]\d*|0)?(\.\d+)?$/.test(innerState);
  const parseableOrEmpty = innerState === null || innerState === '' || parseable;
  function handleBlur() {
    if (innerState !== value?.toString(10)) {
      if (innerState === '') {
        onChange(null);
      } else if (parseable) {
        onChange(parseFloat(innerState));
      }
    }
  }
  const style: React.CSSProperties = {
    ...(styleProp ?? {}),
    ...(parseableOrEmpty && error === undefined ? {} : { borderColor: 'red' }),
  };
  let unitFragment: JSX.Element | null = null;
  if (unit !== undefined) {
    unitFragment = <>&nbsp;{unit}</>;
  }
  const { className, disabled: disabledProp, ...rest } = passthroughProps;
  const disabled = (disabledProp ?? false) || locked;

  return (
    <span style={{ whiteSpace: 'nowrap' }}>
      <input
        type="text"
        className={`input--number ${className ?? ''}`}
        value={innerState}
        onChange={(evt) => setInnerState(evt.target.value)}
        onBlur={handleBlur}
        style={style}
        title={error}
        disabled={disabled}
        {...rest}
      />
      {unitFragment}
    </span>
  );
}
