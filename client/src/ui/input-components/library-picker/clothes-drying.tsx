import React from 'react';

import {
  ClothesDryingMeasure,
  isClothesDryingMeasuresLibrary,
} from '../../../data-schemas/libraries/clothes-drying-facilities';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectClothesDryingFacilityParams = {
  onSelect: (item: ClothesDryingMeasure) => void;
  onClose: () => void;
  forMeasure: boolean;
};

export function SelectClothesDryingFacility({
  onSelect,
  onClose,
  forMeasure,
}: SelectClothesDryingFacilityParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isClothesDryingMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No generation measure libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Clothes drying facility"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={null}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={
        forMeasure
          ? [
              {
                title: '£',
                type: 'number' as const,
                value: (system: ClothesDryingMeasure) => system.cost,
              },
            ]
          : []
      }
      getFullItemData={(system: ClothesDryingMeasure) =>
        forMeasure
          ? [
              {
                title: 'Description',
                value: nl2br(system.description),
              },
              { title: 'Associated work', value: system.associated_work },
              { title: 'Key risks', value: system.key_risks },
              { title: 'Notes', value: nl2br(system.notes) },
            ]
          : []
      }
    />
  );
}
