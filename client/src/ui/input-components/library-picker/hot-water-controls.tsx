import React from 'react';

import {
  HotWaterControlTypeMeasure,
  isHotWaterControlTypeMeasuresLibrary,
} from '../../../data-schemas/libraries/hot-water-control-type';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectWaterStorageControlTypeParams = {
  onSelect: (item: HotWaterControlTypeMeasure) => void;
  onClose: () => void;
  currentItemTag: string | null;
};

export function SelectWaterStorageControlType({
  onSelect,
  onClose,
  currentItemTag,
}: SelectWaterStorageControlTypeParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isHotWaterControlTypeMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No hot water control type measure libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Hot water control type"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: '£',
          type: 'number' as const,
          value: (system: HotWaterControlTypeMeasure) => system.cost,
        },
      ]}
      getFullItemData={(system: HotWaterControlTypeMeasure) => [
        {
          title: 'Description',
          value: nl2br(system.description),
        },
        { title: 'Associated work', value: system.associated_work },
        { title: 'Key risks', value: system.key_risks },
        { title: 'Notes', value: nl2br(system.notes) },
      ]}
    />
  );
}
