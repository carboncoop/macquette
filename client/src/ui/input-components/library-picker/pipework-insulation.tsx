import React from 'react';

import { omit } from 'lodash';
import {
  PipeworkInsulationMeasure,
  isPipeworkInsulationMeasuresLibrary,
} from '../../../data-schemas/libraries/pipework-insulation';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { SelectLibraryItem, useLibraries } from './generic';

export type FixedPipeworkInsulationMeasure = Omit<PipeworkInsulationMeasure, 'SELECT'> & {
  pipework_insulation: PipeworkInsulationMeasure['SELECT'];
};

type SelectPipeworkInsulationMeasureParams = {
  onSelect: (item: FixedPipeworkInsulationMeasure) => void;
  onClose: () => void;
};

export function SelectPipeworkInsulationMeasure({
  onSelect,
  onClose,
}: SelectPipeworkInsulationMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isPipeworkInsulationMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No hot water control type measure libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Hot water control type"
      onSelect={(libraryItem) =>
        onSelect({
          ...omit(libraryItem, 'SELECT'),
          pipework_insulation: libraryItem.SELECT,
        })
      }
      onClose={onClose}
      currentItemTag={null}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: '£',
          type: 'number' as const,
          value: (system: PipeworkInsulationMeasure) => system.cost,
        },
      ]}
      getFullItemData={(system: PipeworkInsulationMeasure) => [
        {
          title: 'Description',
          value: nl2br(system.description),
        },
        { title: 'Associated work', value: system.associated_work },
        { title: 'Key risks', value: system.key_risks },
        { title: 'Notes', value: nl2br(system.notes) },
      ]}
    />
  );
}
