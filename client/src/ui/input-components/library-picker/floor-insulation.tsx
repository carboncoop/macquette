import React from 'react';

import {
  FloorInsulationMaterial,
  isFloorInsulationMaterialLibrary,
} from '../../../data-schemas/libraries/floor-insulation';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectFloorInsulationMaterialParams = {
  onSelect: (item: FloorInsulationMaterial) => void;
  onClose: () => void;
  currentItemTag?: string | null;
};

export function SelectFloorInsulationMaterial({
  onSelect,
  onClose,
  currentItemTag = null,
}: SelectFloorInsulationMaterialParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isFloorInsulationMaterialLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing error'}>
        No floor material libraries found
      </ErrorModal>
    );
  }

  function searchText(material: FloorInsulationMaterial) {
    return material.name;
  }

  return SelectLibraryItem<FloorInsulationMaterial>({
    title: 'floor material',
    onSelect,
    onClose,
    currentItemTag,
    libraries: filtered,
    searchText,
    tableColumns: [
      { title: 'Type', type: 'text', value: (material) => material.type },
      {
        title: 'Conductivity',
        type: 'text',
        value: (material) =>
          material.mechanism === 'conductivity' ? material.conductivity.toString() : '',
      },
      {
        title: 'Resistance',
        type: 'text',
        value: (material) =>
          material.mechanism === 'resistance' ? material.resistance.toString() : '',
      },
    ],
    getFullItemData: (item) =>
      item.description === ''
        ? []
        : [
            {
              title: 'Description',
              value: nl2br(item.description),
            },
          ],
  });
}
