import React from 'react';

import {
  HotWaterStorage,
  HotWaterStorageMeasure,
  isHotWaterStorageLibrary,
  isHotWaterStorageMeasuresLibrary,
} from '../../../data-schemas/libraries/storage-type';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectHotWaterStorageParams = {
  onSelect: (item: HotWaterStorage) => void;
  onClose: () => void;
};

export function SelectHotWaterStorage({
  onSelect,
  onClose,
}: SelectHotWaterStorageParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isHotWaterStorageLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No vents and flues libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="vent or flue"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={null}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[]}
      getFullItemData={() => []}
    />
  );
}

type SelectHotWaterStorageMeasureParams = {
  onSelect: (item: HotWaterStorageMeasure) => void;
  onClose: () => void;
};

export function SelectHotWaterStorageMeasure({
  onSelect,
  onClose,
}: SelectHotWaterStorageMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isHotWaterStorageMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No vents and flues libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="vent or flue"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={null}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: '£',
          type: 'number' as const,
          value: (system: HotWaterStorageMeasure) => system.cost,
        },
      ]}
      getFullItemData={(system: HotWaterStorageMeasure) => [
        {
          title: 'Description',
          value: nl2br(system.description),
        },
        { title: 'Associated work', value: system.associated_work },
        { title: 'Key risks', value: system.key_risks },
        { title: 'Notes', value: nl2br(system.notes) },
      ]}
    />
  );
}
