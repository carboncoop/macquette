import React from 'react';

import {
  DraughtProofingMeasure,
  isDraughtProofingMeasuresLibrary,
} from '../../../data-schemas/libraries/draught-proofing-measures';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectDraughtProofingMeasureParams = {
  onSelect: (item: DraughtProofingMeasure) => void;
  onClose: () => void;
};

export function SelectDraughtProofingMeasure({
  onSelect,
  onClose,
}: SelectDraughtProofingMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isDraughtProofingMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No generation measure libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="Draughtproofing measure"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={null}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: 'AP50',
          type: 'number' as const,
          value: (system: DraughtProofingMeasure) => system.q50,
        },
        {
          title: '£',
          type: 'number' as const,
          value: (system: DraughtProofingMeasure) => system.cost,
        },
      ]}
      getFullItemData={(system: DraughtProofingMeasure) => [
        { title: 'Description', value: nl2br(system.description) },
        { title: 'Associated work', value: system.associated_work },
        { title: 'Key risks', value: system.key_risks },
        { title: 'Notes', value: nl2br(system.notes) },
      ]}
    />
  );
}
