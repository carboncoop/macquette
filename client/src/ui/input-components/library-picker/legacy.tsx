import { createElement, StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import { HeatingSystemMeasure } from '../../../data-schemas/libraries/heating-systems';
import { HeatingControlMeasure } from '../../../data-schemas/libraries/space-heating-control-type';
import { SelectHeatingControlMeasure } from './heating-controls';
import { SelectHeatingSystemMeasure } from './heating-systems';

export function selectHeatingSystemMeasure(
  mountpoint: Element,
  onSelect: (item: unknown) => void,
) {
  const root = createRoot(mountpoint);
  const element = createElement(
    StrictMode,
    null,
    createElement(SelectHeatingSystemMeasure, {
      onSelect: (item: HeatingSystemMeasure) => {
        onSelect({ ...item });
        root.unmount();
      },
      onClose: () => {
        root.unmount();
      },
      showCost: false,
      currentItemTag: null,
    }),
  );
  root.render(element);
}

export function selectHeatingControlMeasure(
  mountpoint: Element,
  onSelect: (item: unknown) => void,
) {
  const root = createRoot(mountpoint);
  const element = createElement(
    StrictMode,
    null,
    createElement(SelectHeatingControlMeasure, {
      onSelect: (item: HeatingControlMeasure) => {
        onSelect({ ...item });
        root.unmount();
      },
      onClose: () => {
        root.unmount();
      },
      currentItemTag: null,
    }),
  );
  root.render(element);
}
