import { pickBy } from 'lodash';
import React from 'react';

import type {
  WallLike,
  WallLikeMeasure,
} from '../../../data-schemas/libraries/fabric-walls';
import {
  isFabricWallMeasuresLibrary,
  isFabricWallsLibrary,
} from '../../../data-schemas/libraries/fabric-walls';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { NumberOutput } from '../../output-components/numeric';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectWallParams = {
  onSelect: (item: WallLike) => void;
  onClose: () => void;
  type: 'external wall' | 'party wall' | 'roof' | 'loft';
  currentItemTag?: string | null;
};

export function SelectWallLike({
  onSelect,
  onClose,
  type,
  currentItemTag = null,
}: SelectWallParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isFabricWallsLibrary).map((library) => ({
    ...library,
    data: pickBy(library.data, (item) => item.type === type),
  }));
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No {type} libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title={type}
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={filtered}
      searchText={(wall) => wall.name}
      tableColumns={[
        { title: 'U', type: 'number', value: (wall) => wall.uValue.toString() },
        { title: 'k', type: 'number', value: (wall) => wall.kValue.toString() },
      ]}
      getFullItemData={(wall: WallLike) =>
        wall.description === ''
          ? []
          : [
              {
                title: 'Description',
                value: nl2br(wall.description),
              },
            ]
      }
    />
  );
}

type SelectWallLikeMeasureParams = {
  onSelect: (item: WallLikeMeasure) => void;
  onClose: () => void;
  type: 'external wall' | 'party wall' | 'roof' | 'loft';
  areaSqm: number | null;
  currentItemTag: string | null;
};

export function SelectWallLikeMeasure({
  onSelect,
  onClose,
  type,
  areaSqm,
  currentItemTag,
}: SelectWallLikeMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isFabricWallMeasuresLibrary).map((library) => ({
    ...library,
    data: pickBy(library.data, (item) => item.type === type),
  }));
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No {type} measure libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title={`${type} measure`}
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={currentItemTag}
      libraries={filtered}
      searchText={(row) => row.name}
      tableColumns={[
        { title: 'U', type: 'number', value: (row) => row.uValue.toString() },
        ...(areaSqm !== null
          ? [
              {
                title: '£',
                type: 'number' as const,
                value: (row: WallLikeMeasure) => (
                  <NumberOutput
                    dp={0}
                    value={Math.round(
                      row.min_cost +
                        areaSqm * row.cost * (row.isExternalWallInsulation ? 1.15 : 1),
                    )}
                  />
                ),
              },
            ]
          : []),
      ]}
      getFullItemData={(row: WallLikeMeasure) => [
        ...(row.description !== ''
          ? [
              {
                title: 'Description',
                value: nl2br(row.description),
              },
            ]
          : []),
        {
          title: 'Cost',
          value: `£${row.min_cost} + £${row.cost} per ${row.cost_units} ${
            row.isExternalWallInsulation ? '(x1.15 to account for EWI)' : ''
          }`,
        },
        { title: 'Associated work', value: row.associated_work },
        { title: 'Key risks', value: row.key_risks },
        { title: 'Notes', value: nl2br(row.notes) },
      ]}
    />
  );
}
