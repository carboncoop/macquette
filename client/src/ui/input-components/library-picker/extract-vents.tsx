import React from 'react';

import {
  ExtractVent,
  ExtractVentMeasure,
  isExtractVentMeasuresLibrary,
} from '../../../data-schemas/libraries/extract-ventilation-points';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { isNonEmpty } from '../../../helpers/non-empty-array';
import { ErrorModal } from '../../output-components/modal';
import { nl2br } from '../../output-components/nl2br';
import { NumberOutput } from '../../output-components/numeric';
import { SelectLibraryItem, useLibraries } from './generic';

type SelectExtractVentParams = {
  onSelect: (item: ExtractVent) => void;
  onClose: () => void;
};

export function SelectExtractVent({ onSelect, onClose }: SelectExtractVentParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isExtractVentMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No extract ventilation point libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="extract ventilation point"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={null}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: 'Ventilation rate',
          type: 'number' as const,
          value: (system: ExtractVent) => (
            <NumberOutput
              value={coalesceEmptyString(system.ventilation_rate, null)}
              dp={0}
              unit="m³/h"
            />
          ),
        },
      ]}
      getFullItemData={() => []}
    />
  );
}

type SelectExtractVentMeasureParams = {
  onSelect: (item: ExtractVentMeasure) => void;
  onClose: () => void;
};

export function SelectExtractVentMeasure({
  onSelect,
  onClose,
}: SelectExtractVentMeasureParams) {
  const libraries = useLibraries();
  if (libraries.type === 'waiting') {
    return libraries.modal(onClose);
  }

  const filtered = libraries.data.filter(isExtractVentMeasuresLibrary);
  if (!isNonEmpty(filtered)) {
    return (
      <ErrorModal onClose={onClose} title={'Library missing'}>
        No extract ventilation point libraries found
      </ErrorModal>
    );
  }

  return (
    <SelectLibraryItem
      title="extract ventilation point measure"
      onSelect={onSelect}
      onClose={onClose}
      currentItemTag={null}
      libraries={filtered}
      searchText={(system) => system.name}
      tableColumns={[
        {
          title: 'Ventilation rate',
          type: 'number' as const,
          value: (system: ExtractVentMeasure) => (
            <NumberOutput
              value={coalesceEmptyString(system.ventilation_rate, null)}
              dp={0}
              unit="m³/h"
            />
          ),
        },
        {
          title: '£',
          type: 'number' as const,
          value: (system: ExtractVentMeasure) => system.cost,
        },
      ]}
      getFullItemData={(system: ExtractVentMeasure) => [
        {
          title: 'Description',
          value: nl2br(system.description),
        },
        { title: 'Associated work', value: system.associated_work },
        { title: 'Key risks', value: system.key_risks },
        { title: 'Notes', value: nl2br(system.notes) },
      ]}
    />
  );
}
