import React, { useId, useState } from 'react';

export type CheckboxGroupProps<T> = {
  options: {
    value: T;
    display: string;
  }[];
  value: T[] | null;
  onChange: (value: T[]) => void;
  checkboxClasses?: string[];
  labelClasses?: string[];
  disabled?: boolean;
  ariaLabelledBy?: string;
};

export function CheckboxGroup<T extends string>({
  options,
  onChange,
  value,
  checkboxClasses = ['big-checkbox'],
  labelClasses = [],
  disabled = false,
  ariaLabelledBy,
}: CheckboxGroupProps<T>) {
  const groupId = useId();
  const [selection, setSelection] = useState<T[]>(value ?? []);

  function update(value: T, newState: boolean) {
    if (newState === true) {
      setSelection([...selection, value]);
    } else {
      setSelection(selection.filter((id) => id !== value));
    }
    onChange(selection);
  }

  return (
    <>
      {options.map(({ value: optionValue, display }, idx) => {
        const checked = selection.includes(optionValue);
        return (
          <label
            key={idx}
            id={`${groupId}${idx}`}
            className={['checkbox', 'd-flex', 'gap-7', ...labelClasses].join(' ')}
            htmlFor={`${groupId}-${idx}`}
          >
            <input
              type="checkbox"
              className={checkboxClasses.join(' ')}
              id={`${groupId}-${idx}`}
              aria-labelledby={`${ariaLabelledBy ?? ''} ${groupId}${idx}`}
              value={optionValue}
              checked={checked}
              disabled={disabled}
              onChange={() => update(optionValue, !checked)}
            />
            {display}
          </label>
        );
      })}
    </>
  );
}
