import React, { ReactNode } from 'react';

export function nl2br(input: string): ReactNode {
  const lines = input.split('\n');

  const outLines = [
    ...lines.slice(0, -1).map((line) => {
      return [line, null];
    }),
    lines.slice(-1),
  ]
    .flat(1)
    .map((lineOrBr, idx) => (lineOrBr === null ? <br key={idx} /> : lineOrBr));

  return outLines;
}
