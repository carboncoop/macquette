import { omit } from 'lodash';
import React from 'react';

import { Project } from '../../data-schemas/project';
import { Scenario } from '../../data-schemas/scenario';
import { GenericMeasure } from '../../data-schemas/scenario/measures';
import { coalesceEmptyString } from '../../data-schemas/scenario/value-schemas';
import { assertNever } from '../../helpers/assert-never';
import { Result } from '../../helpers/result';
import { getScenarioMeasures } from '../../measures';
import type { UiModule } from '../module-management/module-type';
import { questionnaireAnswerChoices } from '../questionnaire-answer-choices';

type State = {
  measures: (GenericMeasure & { scenarioId: string })[];
  scopeOfWorksData: Record<string, string | undefined>;
};
type Action = {
  type: 'external data update';
  measures: (GenericMeasure & { scenarioId: string })[];
  scopeOfWorksData: Record<string, string | undefined>;
};

export const scopeOfWorksModule: UiModule<State, Action, never> = {
  name: 'scope of works',
  component: function ScopeOfWorks({ state }) {
    function handleMeasuresClick() {
      const csv = measuresCsv(state.measures);
      downloadCsv(csv, 'measures.csv');
    }
    function handleScopeOfWorksClick() {
      const csv = scopeOfWorksCsv(state.scopeOfWorksData);
      downloadCsv(csv, 'scopeofworks.csv');
    }
    return (
      <>
        <section className="mb-30">
          <h5>Export Appendix A (Scenario Measures Complete Tables) to CSV format</h5>
          <button onClick={handleMeasuresClick} className="btn">
            Export CSV
          </button>
        </section>

        <section>
          <h5>Export non-measures data</h5>
          <button onClick={handleScopeOfWorksClick} className="btn">
            Export CSV
          </button>
        </section>
      </>
    );
  },
  initialState: () => {
    return { measures: [], scopeOfWorksData: {} };
  },
  reducer: (_state, action) => {
    switch (action.type) {
      case 'external data update':
        return [omit(action, 'type')];
    }
  },
  effector: assertNever,
  shims: {
    extractUpdateAction({ project }): Result<Action[], Error> {
      return Result.ok([
        {
          type: 'external data update' as const,
          ...extractData(project),
        },
      ]);
    },
    mutateLegacyData: () => undefined,
  },
};

function extractData(project: Project): State {
  let outMeasures: (GenericMeasure & { scenarioId: string })[] = [];
  for (const [scenarioId, scenario] of Object.entries(project.data)) {
    if (scenarioId === 'master') continue;
    const measures = getScenarioMeasures(scenario);
    outMeasures = [
      ...outMeasures,
      ...measures.map((measure) => ({ ...measure, scenarioId })),
    ];
  }
  const scopeOfWorksData = extractScopeOfWorksData(project.data['master']);
  return { measures: outMeasures, scopeOfWorksData };
}

function downloadCsv(csv: string, filename: string) {
  const downloadData = new Blob([csv], { type: 'text/csv' });
  const downloadLink = document.createElement('a');
  downloadLink.href = window.URL.createObjectURL(downloadData);
  downloadLink.setAttribute('download', filename);
  downloadLink.click();
}

function formatCsv(cells: string[][]): string {
  return cells
    .map((line) =>
      line
        .map((cell) => {
          return '"' + cell.replace(/"/g, '""') + '"';
        })
        .join(','),
    )
    .join('\n');
}

function measuresCsv(measures: (GenericMeasure & { scenarioId: string })[]): string {
  const cells = [
    [
      'Scenario',
      'Code',
      'Measure',
      'Label/location',
      'Performance target',
      'Key risks',
      'Associated work',
      'Maintenance',
    ],
    ...measures.map((measure) =>
      [
        measure.scenarioId,
        measure.tag,
        measure.name,
        measure.location,
        measure.performance,
        measure.key_risks,
        measure.associated_work,
        measure.maintenance,
      ].map((s) => s.replace(/\s+/g, ' ').trim()),
    ),
  ];
  return formatCsv(cells);
}

function scopeOfWorksCsv(scopeOfWorksData: Record<string, string | undefined>): string {
  const header = Object.keys(scopeOfWorksData);
  const record = header.map((key) =>
    (scopeOfWorksData[key] ?? '').replace(/\s+/g, ' ').trim(),
  );
  return formatCsv([header, record]);
}

function lookupWithUndefined<Key extends string, Value>(
  table: Record<Key, Value>,
  key: Key | undefined | null,
): Value | undefined {
  if (key === undefined || key === null) return undefined;
  return table[key];
}

function stringifyNullableBoolean(val: boolean | null | undefined) {
  switch (val) {
    case true:
      return 'yes';
    case false:
      return 'no';
    case undefined:
    case null:
      return "don't know";
  }
}

export function extractScopeOfWorksData(
  baselineScenario: Scenario,
): Record<string, string | undefined> {
  if (baselineScenario === undefined) return {};
  const { household, ventilation, TFA } = baselineScenario;

  return {
    house_type: household?.propertyType ?? undefined,
    house_nr_bedrooms: household?.numBedrooms,
    previous_works: household?.priorWork,
    expected_other_works_note: household?.otherWorksNotes,
    expected_start_note: household?.deadlinesNotes,
    construct_note_floors: household?.construct_note_floors,
    construct_note_roof: household?.construct_note_roof,
    construct_note_openings: household?.construct_note_openings,
    construct_note_drainage: household?.construct_note_drainage,
    construct_note_ventiliation: household?.construct_note_ventiliation,
    construct_note_walls: household?.construct_note_walls,
    construct_note_ingress: household?.construct_note_ingress,
    structural_issues: household?.structuralIssuesNotes,
    structural_issues_note: household?.structuralIssuesNotesLegacy,
    damp: household?.dampCondensationMould,
    damp_note: household?.dampCondensationMouldLegacy,
    ventilation_adequate_paths: lookupWithUndefined(
      questionnaireAnswerChoices.YES_NO_NA,
      household?.ventilation_adequate_paths,
    ),
    ventilation_purge_vents: lookupWithUndefined(
      questionnaireAnswerChoices.YES_NO_NA,
      household?.ventilation_purge_vents,
    ),
    ventilation_gaps: lookupWithUndefined(
      questionnaireAnswerChoices.YES_NO_NA,
      household?.ventilation_gaps,
    ),
    ventilation_note: household?.ventilation_note,
    ventilation_suggestion: lookupWithUndefined(
      questionnaireAnswerChoices.VENTILATION_SUGGESTION,
      household?.ventilation_suggestion,
    ),
    radon_risk: lookupWithUndefined(
      questionnaireAnswerChoices.RADON_RISK,
      household?.radonRisk,
    ),
    flooding_rivers_sea: lookupWithUndefined(
      questionnaireAnswerChoices.FLOODING_RISK,
      household?.floodingRiversAndSea,
    ),
    flooding_surface_water: lookupWithUndefined(
      questionnaireAnswerChoices.FLOODING_RISK,
      household?.floodingSurfaceWater,
    ),
    flooding_reservoirs: lookupWithUndefined(
      questionnaireAnswerChoices.FLOODING_RISK_RESERVOIRS,
      household?.floodingReservoirs,
    ),
    flooding_history: stringifyNullableBoolean(household?.floodingHistory),
    overheating_note: household?.overheatingNotesLegacy,
    historic_age_band: lookupWithUndefined(
      questionnaireAnswerChoices.HISTORIC_AGE_BAND,
      household?.builtBand,
    ),
    historic_conserved: stringifyNullableBoolean(household?.inConservationArea),
    historic_listed: lookupWithUndefined(
      questionnaireAnswerChoices.HISTORIC_LISTED,
      household?.listedStatus,
    ),
    context_and_other_points: household?.generalContextNotes,
    TFA: coalesceEmptyString(TFA, undefined)?.toString(10),
    ventilation_type: ventilation?.ventilation_type,
    ventilation_name: ventilation?.ventilation_name,
  };
}
