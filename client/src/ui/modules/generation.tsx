import React, { useId, useState } from 'react';
import { z } from 'zod';
import { GenerationMeasure } from '../../data-schemas/libraries/generation-measures';
import { projectSchema } from '../../data-schemas/project';
import { coalesceEmptyString } from '../../data-schemas/scenario/value-schemas';
import { assertNever } from '../../helpers/assert-never';
import { Result } from '../../helpers/result';
import { calcMeasureQtyAndCost } from '../../measures';
import { Orientation, OrientationName } from '../../model/enums/orientation';
import { Overshading, OvershadingName } from '../../model/enums/overshading';
import { Model } from '../../model/model';
import { guessOvershadingFromFactor } from '../../model/modules/generation';
import { DeleteIcon, HammerIcon, PlusIcon } from '../icons';
import { Button } from '../input-components/button';
import { CheckboxInput } from '../input-components/checkbox';
import { SelectGenerationMeasure } from '../input-components/library-picker/generation';
import { LockableContext } from '../input-components/lockable-context';
import { NumberInput } from '../input-components/number';
import { RadioGroup } from '../input-components/radio-group';
import { AppContext } from '../module-management/app-context';
import type { UiModule } from '../module-management/module-type';
import { NumberOutput } from '../output-components/numeric';
import { colourForStatus } from './fabric/components/measure-status';

type BasicSystem = {
  fractionUsedOnsite: number | null;
  payments: 'none' | 'fit';
  fitGeneration: number | null; // £/kWh
  fitExport: number | null; // £/kWh
};

type PVSystem = BasicSystem & { type: 'pv' } & (
    | {
        calculationType: 'kWh';
        measure: null;
        generationkWh: number | null;
      }
    | {
        calculationType: 'calculator';
        measure: null;
        capacitykWp: number | null;
        inclination: number | null;
        orientation: OrientationName | null;
        overshading: OvershadingName | null;
      }
    | {
        calculationType: 'measure';
        measure: GenerationMeasure;
        inclination: number | null;
        orientation: OrientationName | null;
        overshading: OvershadingName | null;
      }
  );

type HydroSystem = BasicSystem & { type: 'hydro'; generationkWh: number | null };
type WindSystem = BasicSystem & { type: 'wind'; generationkWh: number | null };

type GenerationSystem = PVSystem | HydroSystem | WindSystem;

type State = {
  systems: {
    pv: PVSystem | null;
    hydro: HydroSystem | null;
    wind: WindSystem | null;
  };
  currentScenarioIsBaseline: boolean;
  scenarioLocked: boolean;
  model: Model | null;
};

type Action =
  | { type: 'external data update'; data: Partial<State> }
  | { type: 'delete system'; systemType: GenerationSystem['type'] }
  | { type: 'add system'; systemType: GenerationSystem['type'] }
  | {
      type: 'update hydro data';
      update: Partial<Omit<HydroSystem, 'type'>>;
    }
  | {
      type: 'update wind data';
      update: Partial<Omit<WindSystem, 'type'>>;
    }
  | { type: 'apply pv measure'; measure: GenerationMeasure }
  | { type: 'change pv calculation type'; calculationType: 'kWh' | 'calculator' }
  | {
      type: 'update pv data';
      update: Partial<
        {
          generationkWh: number | null;
          capacitykWp: number | null;
          inclination: number | null;
          orientation: OrientationName | null;
          overshading: OvershadingName | null;
        } & BasicSystem
      >;
    };

function systemName(system: GenerationSystem) {
  switch (system.type) {
    case 'pv':
      return 'PV system';
    case 'hydro':
      return 'Hydro system';
    case 'wind':
      return 'Wind system';
  }
}

function PVSystem({
  system,
  currentScenarioIsBaseline,
  dispatch,
  pickMeasure,
  model,
}: {
  system: PVSystem;
  currentScenarioIsBaseline: boolean;
  dispatch: (action: Action) => void;
  pickMeasure: () => void;
  model: Model | null;
}) {
  const id = useId();
  const status = system.measure !== null ? 'new measure' : 'none';
  return (
    <div
      className="card"
      style={{
        borderLeft: `10px solid var(${colourForStatus[status]})`,
      }}
    >
      <div className="d-flex justify-content-between align-items-center mb-15">
        <div>
          <div>
            <b>{systemName(system)}</b>
          </div>
          {system.calculationType !== 'measure' && (
            <div>
              <CheckboxInput
                id={`${id}-calculator`}
                value={system.calculationType === 'calculator'}
                onChange={(useCalculator) =>
                  dispatch({
                    type: 'change pv calculation type',
                    calculationType: useCalculator ? 'calculator' : 'kWh',
                  })
                }
              />{' '}
              <label className="d-i" htmlFor={`${id}-calculator`}>
                Use calculator
              </label>
            </div>
          )}
          {system.measure !== null && (
            <div className="d-flex gap-15">
              <span>{system.measure.tag}</span>
              <span>
                Capacity: <NumberOutput value={system.measure.kWp} unit="kWp" />
              </span>
              <span>
                Measure cost: £
                <NumberOutput
                  value={
                    calcMeasureQtyAndCost({
                      costUnits: system.measure.cost_units,
                      costPerUnit: system.measure.cost,
                      kWp: system.measure.kWp,
                      baseCost: system.measure.min_cost,
                    })[1]
                  }
                  dp={0}
                />
              </span>
            </div>
          )}
        </div>

        <div>
          {currentScenarioIsBaseline ? (
            <Button
              title="Delete"
              icon={DeleteIcon}
              className="ml-15"
              onClick={() => dispatch({ type: 'delete system', systemType: 'pv' })}
            />
          ) : (
            <Button
              title={system.measure !== null ? 'Replace measure' : 'Apply measure'}
              icon={HammerIcon}
              className="ml-15"
              onClick={() => pickMeasure()}
            />
          )}
        </div>
      </div>

      <div className="d-flex flex-wrap gap-30">
        {system.calculationType !== 'kWh' && (
          <>
            <div className="d-flex flex-v">
              {system.calculationType === 'calculator' && (
                <div>
                  <label className="small-caps d-i" htmlFor={`${id}-capacity`}>
                    Capacity
                  </label>
                  <br />
                  <NumberInput
                    id={`${id}-capacity`}
                    value={system.capacitykWp}
                    onChange={(capacitykWp) =>
                      dispatch({ type: 'update pv data', update: { capacitykWp } })
                    }
                    unit="kWp"
                  />
                </div>
              )}

              <div>
                <label className="small-caps d-i" htmlFor={`${id}-capacity`}>
                  Inclination
                </label>
                <br />
                <NumberInput
                  id={`${id}-capacity`}
                  value={system.inclination}
                  onChange={(inclination) =>
                    dispatch({ type: 'update pv data', update: { inclination } })
                  }
                  unit="degrees"
                />
              </div>
            </div>

            <div>
              <span className="small-caps">Orientation</span>
              <RadioGroup
                options={[
                  { value: 'North', display: 'North' },
                  { value: 'NE/NW', display: 'NE/NW' },
                  { value: 'East/West', display: 'East/West' },
                  { value: 'SE/SW', display: 'SE/SW' },
                  { value: 'South', display: 'South' },
                ]}
                labelClasses={['mb-0']}
                radioClasses={['mr-3']}
                onChange={(orientation) =>
                  dispatch({ type: 'update pv data', update: { orientation } })
                }
                value={system.orientation}
              />
            </div>

            <div>
              <span className="small-caps">% overshading</span>
              <table className="table-unstyled">
                <tbody>
                  <tr>
                    <td>
                      <input
                        type="radio"
                        className="mt-0"
                        name={`${id}-overshading`}
                        id={`${id}-overshading-0`}
                        checked={system.overshading === '>80%'}
                        onChange={() =>
                          dispatch({
                            type: 'update pv data',
                            update: { overshading: '>80%' },
                          })
                        }
                      />
                    </td>
                    <td className="pl-7">
                      <label className="d-i" htmlFor={`${id}-overshading-0`}>
                        &gt; 80
                      </label>
                    </td>
                    <td className="pl-7">
                      <small>Heavy</small>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input
                        type="radio"
                        className="mt-0"
                        name={`${id}-overshading`}
                        id={`${id}-overshading-1`}
                        checked={system.overshading === '60-80%'}
                        onChange={() =>
                          dispatch({
                            type: 'update pv data',
                            update: { overshading: '60-80%' },
                          })
                        }
                      />
                    </td>
                    <td className="pl-7">
                      <label className="d-i text-nowrap" htmlFor={`${id}-overshading-1`}>
                        60–80
                      </label>
                    </td>
                    <td className="pl-7">
                      <small>Above average</small>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input
                        type="radio"
                        className="mt-0"
                        name={`${id}-overshading`}
                        id={`${id}-overshading-2`}
                        checked={system.overshading === '20-60%'}
                        onChange={() =>
                          dispatch({
                            type: 'update pv data',
                            update: { overshading: '20-60%' },
                          })
                        }
                      />
                    </td>
                    <td className="pl-7">
                      <label className="d-i text-nowrap" htmlFor={`${id}-overshading-2`}>
                        20–60
                      </label>
                    </td>
                    <td className="pl-7">
                      <small>Average/unknown</small>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input
                        type="radio"
                        className="mt-0"
                        name={`${id}-overshading`}
                        id={`${id}-overshading-3`}
                        checked={system.overshading === '<20%'}
                        onChange={() =>
                          dispatch({
                            type: 'update pv data',
                            update: { overshading: '<20%' },
                          })
                        }
                      />
                    </td>
                    <td className="pl-7">
                      <label className="d-i" htmlFor={`${id}-overshading-3`}>
                        &lt; 20
                      </label>
                    </td>
                    <td className="pl-7">
                      <small>Very little</small>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </>
        )}

        <div className="d-flex flex-v">
          {system.calculationType === 'kWh' ? (
            <div>
              <label className="small-caps d-i" htmlFor={`${id}-amount`}>
                Annual generation
              </label>
              <br />
              <NumberInput
                id={`${id}-amount`}
                value={system.generationkWh}
                onChange={(generationkWh) =>
                  dispatch({ type: 'update pv data', update: { generationkWh } })
                }
                unit="kWh"
              />
            </div>
          ) : (
            <div className="mb-7">
              <span className="small-caps d-i">Annual generation</span>
              <br />
              <NumberOutput
                value={model?.normal.generation.solar.energyAnnual ?? null}
                unit="kWh"
                dp={0}
              />
            </div>
          )}

          <div>
            <label className="small-caps d-i" htmlFor={`${id}-fraction`}>
              Fraction used onsite
            </label>
            <br />
            <NumberInput
              id={`${id}-fraction`}
              value={system.fractionUsedOnsite}
              onChange={(fractionUsedOnsite) =>
                dispatch({ type: 'update pv data', update: { fractionUsedOnsite } })
              }
            />
          </div>
        </div>

        <div>
          <span className="small-caps d-i">Payment</span>
          <br />
          <div className="d-flex flex-v" style={{ alignSelf: 'end' }}>
            <RadioGroup<'none' | 'fit'>
              options={[
                { value: 'none', display: 'None' },
                { value: 'fit', display: 'Feed-in tariff' },
              ]}
              labelClasses={['mb-0']}
              radioClasses={['mr-3']}
              value={system.payments}
              onChange={(payments) =>
                dispatch({ type: 'update pv data', update: { payments } })
              }
            />
          </div>
        </div>

        {system.payments === 'fit' && (
          <div className="d-flex flex-v gap-7">
            <div>
              <label className="small-caps d-i" htmlFor={`${id}-fit-generation`}>
                FIT generation rate
              </label>
              <br />
              <NumberInput
                id={`${id}-fit-generation`}
                value={system.fitGeneration}
                onChange={(fitGeneration) =>
                  dispatch({ type: 'update pv data', update: { fitGeneration } })
                }
                unit="£/kWh"
              />
            </div>

            <div>
              <label className="small-caps d-i" htmlFor={`${id}-fit-export`}>
                FIT export rate
              </label>
              <br />
              <NumberInput
                id={`${id}-fit-export`}
                value={system.fitExport}
                onChange={(fitExport) =>
                  dispatch({ type: 'update pv data', update: { fitExport } })
                }
                unit="£/kWh"
              />
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

function SimpleSystem({
  system,
  currentScenarioIsBaseline,
  onUpdate,
  onDelete,
}: {
  system: WindSystem | HydroSystem;
  currentScenarioIsBaseline: boolean;
  onUpdate: (update: Partial<WindSystem | HydroSystem>) => void;
  onDelete: () => void;
}) {
  const id = useId();
  return (
    <div
      className="card"
      style={{
        borderLeft: `10px solid var(${colourForStatus.none})`,
      }}
    >
      <div className="d-flex justify-content-between align-items-center mb-15">
        <div>
          <b>{systemName(system)}</b>
        </div>

        <div>
          {currentScenarioIsBaseline && (
            <Button
              title="Delete"
              icon={DeleteIcon}
              className="ml-15"
              onClick={onDelete}
            />
          )}
        </div>
      </div>

      <div className="d-flex flex-wrap gap-30">
        <div className="d-flex flex-v">
          <div>
            <label className="small-caps d-i" htmlFor={`${id}-amount`}>
              Annual generation
            </label>
            <br />
            <NumberInput
              id={`${id}-amount`}
              value={system.generationkWh}
              onChange={(generationkWh) => onUpdate({ generationkWh })}
              unit="kWh"
            />
          </div>

          <div>
            <label className="small-caps d-i" htmlFor={`${id}-fraction`}>
              Fraction used onsite
            </label>
            <br />
            <NumberInput
              id={`${id}-fraction`}
              value={system.fractionUsedOnsite}
              onChange={(fractionUsedOnsite) => onUpdate({ fractionUsedOnsite })}
            />
          </div>
        </div>

        <div>
          <span className="small-caps d-i">Payment</span>
          <br />
          <div className="d-flex flex-v" style={{ alignSelf: 'end' }}>
            <RadioGroup<'none' | 'fit'>
              options={[
                { value: 'none', display: 'None' },
                { value: 'fit', display: 'Feed-in tariff' },
              ]}
              labelClasses={['mb-0']}
              radioClasses={['mr-3']}
              value={system.payments}
              onChange={(payments) => onUpdate({ payments })}
            />
          </div>
        </div>

        {system.payments === 'fit' && (
          <div className="d-flex flex-v gap-7">
            <div>
              <label className="small-caps d-i" htmlFor={`${id}-fit-generation`}>
                FIT generation rate
              </label>
              <br />
              <NumberInput
                id={`${id}-fit-generation`}
                value={system.fitGeneration}
                onChange={(fitGeneration) => onUpdate({ fitGeneration })}
                unit="£/kWh"
              />
            </div>

            <div>
              <label className="small-caps d-i" htmlFor={`${id}-fit-export`}>
                FIT export rate
              </label>
              <br />
              <NumberInput
                id={`${id}-fit-export`}
                value={system.fitExport}
                onChange={(fitExport) => onUpdate({ fitExport })}
                unit="£/kWh"
              />
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

function legacyFraction(overshading: Overshading): 0.5 | 0.65 | 0.8 | 1 {
  const legacyFractions = [0.5, 0.65, 0.8, 1] as const;
  const index0 = overshading.index0;
  if (index0 < 0 || index0 >= legacyFractions.length) {
    throw new Error('bad fraction');
  }
  // SAFETY: limits checked above
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  return legacyFractions[index0]!;
}

type ModalState = null | 'apply pv measure';

export const generationModule: UiModule<State, Action, never> = {
  name: 'generation',
  component: function Generation({ state, dispatch }) {
    function addSystem(systemType: GenerationSystem['type']) {
      dispatch({ type: 'add system', systemType });
    }
    const [modalState, setModalState] = useState<ModalState>(null);

    return (
      <LockableContext.Provider value={{ locked: state.scenarioLocked }}>
        <section className="line-top mb-45 d-flex flex-v gap-15">
          <h3 className="ma-0">Electricity sources</h3>

          <div className="d-flex gap-7">
            {state.currentScenarioIsBaseline ? (
              <>
                <Button
                  icon={PlusIcon}
                  title="Add PV system"
                  onClick={() => addSystem('pv')}
                  disabled={state.systems.pv !== null}
                />
                <Button
                  icon={PlusIcon}
                  title="Add hydro system"
                  onClick={() => addSystem('hydro')}
                  disabled={state.systems.hydro !== null}
                />
                <Button
                  icon={PlusIcon}
                  title="Add wind system"
                  onClick={() => addSystem('wind')}
                  disabled={state.systems.wind !== null}
                />
              </>
            ) : (
              <>
                {state.systems.pv === null && (
                  <Button
                    icon={HammerIcon}
                    title="Add PV measure"
                    onClick={() => setModalState('apply pv measure')}
                  />
                )}
              </>
            )}
          </div>

          {modalState === 'apply pv measure' && (
            <SelectGenerationMeasure
              onClose={() => setModalState(null)}
              onSelect={(measure: GenerationMeasure) => {
                setModalState(null);
                dispatch({
                  type: 'apply pv measure',
                  measure,
                });
              }}
              currentItemTag={state.systems.pv?.measure?.tag ?? null}
            />
          )}

          {state.systems.pv !== null && (
            <PVSystem
              system={state.systems.pv}
              currentScenarioIsBaseline={state.currentScenarioIsBaseline}
              dispatch={dispatch}
              pickMeasure={() => setModalState('apply pv measure')}
              model={state.model}
            />
          )}
          {state.systems.wind !== null && (
            <SimpleSystem
              system={state.systems.wind}
              currentScenarioIsBaseline={state.currentScenarioIsBaseline}
              onUpdate={(update) => {
                dispatch({
                  type: 'update wind data',
                  update,
                });
              }}
              onDelete={() => dispatch({ type: 'delete system', systemType: 'wind' })}
            />
          )}
          {state.systems.hydro !== null && (
            <SimpleSystem
              system={state.systems.hydro}
              currentScenarioIsBaseline={state.currentScenarioIsBaseline}
              onUpdate={(update) => {
                dispatch({
                  type: 'update hydro data',
                  update,
                });
              }}
              onDelete={() => dispatch({ type: 'delete system', systemType: 'hydro' })}
            />
          )}
        </section>
      </LockableContext.Provider>
    );
  },
  initialState: () => {
    return {
      currentScenarioIsBaseline: true,
      scenarioLocked: false,
      model: null,
      systems: {
        pv: null,
        wind: null,
        hydro: null,
      },
    };
  },
  reducer: (state: State, action: Action): [State] => {
    switch (action.type) {
      case 'external data update': {
        return [{ ...state, ...action.data }];
      }
      case 'delete system': {
        return [{ ...state, [action.systemType]: null }];
      }
      case 'add system': {
        switch (action.systemType) {
          case 'pv':
            state.systems.pv = {
              type: 'pv',
              calculationType: 'calculator',
              measure: null,
              capacitykWp: null,
              fractionUsedOnsite: 0.25,
              inclination: null,
              orientation: null,
              overshading: null,
              payments: 'none',
              fitGeneration: null,
              fitExport: null,
            };
            break;
          case 'hydro':
            state.systems.hydro = {
              type: action.systemType,
              generationkWh: null,
              fractionUsedOnsite: 0.25,
              payments: 'none',
              fitGeneration: null,
              fitExport: null,
            };
            break;
          case 'wind':
            state.systems.wind = {
              type: 'wind',
              generationkWh: null,
              fractionUsedOnsite: 0.25,
              payments: 'none',
              fitGeneration: null,
              fitExport: null,
            };
            break;
        }
        return [state];
      }
      case 'update hydro data': {
        if (state.systems.hydro === null) return [state];
        state.systems.hydro = {
          ...state.systems.hydro,
          ...action.update,
        };
        return [state];
      }
      case 'update wind data': {
        if (state.systems.wind === null) return [state];
        state.systems.wind = {
          ...state.systems.wind,
          ...action.update,
        };
        return [state];
      }
      case 'change pv calculation type': {
        if (
          state.systems.pv !== null &&
          state.systems.pv.calculationType !== 'measure' &&
          state.systems.pv.calculationType !== action.calculationType
        ) {
          if (action.calculationType === 'kWh') {
            state.systems.pv = {
              generationkWh: null,
              ...state.systems.pv,
              calculationType: 'kWh',
            };
          } else {
            state.systems.pv = {
              capacitykWp: null,
              inclination: null,
              orientation: null,
              overshading: null,
              ...state.systems.pv,
              calculationType: 'calculator',
            };
          }
        }
        return [state];
      }
      case 'update pv data': {
        if (state.systems.pv === null) return [state];
        state.systems.pv = { ...state.systems.pv, ...action.update };
        return [state];
      }
      case 'apply pv measure': {
        if (state.systems.pv === null) {
          state.systems.pv = {
            type: 'pv',
            calculationType: 'measure',
            measure: action.measure,
            fractionUsedOnsite: 0.25,
            inclination: null,
            orientation: null,
            overshading: null,
            payments: 'none',
            fitGeneration: null,
            fitExport: null,
          };
          return [state];
        } else {
          state.systems.pv = {
            inclination: null,
            orientation: null,
            overshading: null,
            ...state.systems.pv,
            calculationType: 'measure',
            measure: action.measure,
          };
          return [state];
        }
      }
    }
  },
  effector: assertNever,
  shims: {
    extractUpdateAction({ scenarioId, currentScenario, currentModel }: AppContext) {
      if (scenarioId === null) {
        return Result.err(new Error('scenarioId was null'));
      }
      if (currentScenario === undefined) {
        return Result.err(new Error('no currentScenario'));
      }

      const newState: State = {
        currentScenarioIsBaseline: scenarioId === 'master',
        scenarioLocked: currentScenario?.locked ?? false,
        model: currentModel.mapErr(() => null).coalesce(),
        systems: {
          pv: null,
          wind: null,
          hydro: null,
        },
      };

      if (currentScenario.generation === undefined) {
        return Result.ok([
          {
            type: 'external data update',
            data: newState,
          },
        ]);
      }

      function extractPaymentFields(
        payments: 'none' | 'fit' | undefined,
        fitGeneration: number | '' | null | undefined,
        fitExport: number | '' | null | undefined,
      ) {
        if (payments !== undefined) {
          return {
            payments,
            fitGeneration: coalesceEmptyString(fitGeneration, 0) ?? 0,
            fitExport: coalesceEmptyString(fitExport, 0) ?? 0,
          };
        } else {
          if (
            (fitGeneration === 0 || fitGeneration === undefined) &&
            (fitExport === 0 || fitExport === undefined)
          ) {
            return { payments: 'none' as const, fitGeneration: null, fitExport: null };
          } else {
            return {
              payments: 'fit' as const,
              fitGeneration: coalesceEmptyString(fitGeneration, null) ?? null,
              fitExport: coalesceEmptyString(fitExport, null) ?? null,
            };
          }
        }
      }

      /*
       * Historical projects will have a load of prefilled fields (with 0s, apart from
       * the fractions, which are 0.25 for wind&hydro and 0.5 for PV). If all the fields
       * are in a default state then we use that as a sign that there is no 'system'.
       *
       * Newer projects (i.e. the ones from the introduction of this code onwards) will
       * not have prefilled fields, so we can differentiate an 'added' system because it
       * will have 'null' in relevant fields, and if there is no system, the fields will
       * be undefined (or absent).
       */

      if (
        currentScenario.generation.hydro_annual_kwh === 0 &&
        currentScenario.generation.hydro_fraction_used_onsite === 0.25 &&
        currentScenario.generation.hydro_FIT === 0 &&
        currentScenario.generation.hydro_export_FIT === 0
      ) {
        // No system (old defaults)
      } else if (currentScenario.generation.hydro_annual_kwh === undefined) {
        // No system (new system, sentinel value)
      } else {
        newState.systems.hydro = {
          type: 'hydro',
          generationkWh: coalesceEmptyString(
            currentScenario.generation.hydro_annual_kwh,
            null,
          ),
          fractionUsedOnsite: coalesceEmptyString(
            currentScenario.generation.hydro_fraction_used_onsite ?? null,
            null,
          ),
          ...extractPaymentFields(
            currentScenario.generation.hydro_payments,
            currentScenario.generation.hydro_FIT,
            currentScenario.generation.hydro_export_FIT,
          ),
        };
      }

      if (
        currentScenario.generation.wind_annual_kwh === 0 &&
        currentScenario.generation.wind_fraction_used_onsite === 0.25 &&
        currentScenario.generation.wind_FIT === 0 &&
        currentScenario.generation.wind_export_FIT === 0
      ) {
        // No system (old defaults)
      } else if (currentScenario.generation.wind_annual_kwh === undefined) {
        // No system (new system, sentinel value)
      } else {
        newState.systems.wind = {
          type: 'wind',
          generationkWh: coalesceEmptyString(
            currentScenario.generation.wind_annual_kwh,
            null,
          ),
          fractionUsedOnsite: coalesceEmptyString(
            currentScenario.generation.wind_fraction_used_onsite ?? null,
            null,
          ),
          ...extractPaymentFields(
            currentScenario.generation.wind_payments,
            currentScenario.generation.wind_FIT,
            currentScenario.generation.wind_export_FIT,
          ),
        };
      }

      if (
        currentScenario.generation.solar_annual_kwh === 0 &&
        currentScenario.generation.solar_fraction_used_onsite === 0.5 &&
        currentScenario.generation.solar_FIT === 0 &&
        currentScenario.generation.solar_export_FIT === 0
      ) {
        // No system (old defaults)
      } else if (currentScenario.generation.solar_fraction_used_onsite === undefined) {
        // No system (new system, sentinel value)
        // We don't use solar_annual_kwh because the model will always set it to 0 if no
        // data is provided.
      } else {
        let measure: GenerationMeasure | null = null;

        if (currentScenario.measures?.PV_generation?.measure !== undefined) {
          measure = {
            ...currentScenario.measures.PV_generation.measure,
            kWp: coalesceEmptyString(
              currentScenario.measures.PV_generation.measure.kWp,
              0,
            ),
          };
        }

        const paymentFields = extractPaymentFields(
          currentScenario.generation.solar_payments,
          currentScenario.generation.solar_FIT,
          currentScenario.generation.solar_export_FIT,
        );

        if (measure !== null) {
          newState.systems.pv = {
            type: 'pv',
            calculationType: 'measure',
            measure,
            inclination:
              coalesceEmptyString(currentScenario.generation.solarpv_inclination, null) ??
              null,
            orientation:
              Orientation.optionalFromIndex0(
                coalesceEmptyString(currentScenario.generation.solarpv_orientation, -1) ??
                  -1,
              )?.name ?? null,
            overshading:
              typeof currentScenario.generation.solarpv_overshading === 'number'
                ? (guessOvershadingFromFactor(
                    currentScenario.generation.solarpv_overshading,
                  )?.name ?? null)
                : null,
            fractionUsedOnsite: coalesceEmptyString(
              currentScenario.generation.solar_fraction_used_onsite ?? null,
              null,
            ),
            ...paymentFields,
          };
        } else if (currentScenario.generation.use_PV_calculator === true) {
          newState.systems.pv = {
            type: 'pv',
            calculationType: 'calculator',
            measure: null,
            capacitykWp:
              coalesceEmptyString(
                currentScenario.generation.solarpv_kwp_installed,
                null,
              ) ?? null,
            inclination:
              coalesceEmptyString(currentScenario.generation.solarpv_inclination, null) ??
              null,
            orientation:
              Orientation.optionalFromIndex0(
                coalesceEmptyString(currentScenario.generation.solarpv_orientation, -1) ??
                  -1,
              )?.name ?? null,
            overshading:
              typeof currentScenario.generation.solarpv_overshading === 'number'
                ? (guessOvershadingFromFactor(
                    currentScenario.generation.solarpv_overshading,
                  )?.name ?? null)
                : null,
            fractionUsedOnsite: coalesceEmptyString(
              currentScenario.generation.solar_fraction_used_onsite ?? null,
              null,
            ),
            ...paymentFields,
          };
        } else {
          newState.systems.pv = {
            type: 'pv',
            calculationType: 'kWh',
            measure: null,
            generationkWh:
              coalesceEmptyString(currentScenario.generation.solar_annual_kwh, null) ??
              null,
            fractionUsedOnsite: coalesceEmptyString(
              currentScenario.generation.solar_fraction_used_onsite ?? null,
              null,
            ),
            ...paymentFields,
          };
        }
      }

      return Result.ok([
        {
          type: 'external data update',
          data: newState,
        },
      ]);
    },
    mutateLegacyData(
      { project: projectRaw }: { project: unknown },
      { scenarioId }: { scenarioId: string | null },
      state: State,
    ) {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const project = projectRaw as z.input<typeof projectSchema>;
      if (scenarioId === null) {
        throw new Error("can't mutate nonexistent scenario");
      }
      const data = project.data[scenarioId];
      if (data === undefined) {
        throw new Error("can't mutate nonexistent scenario");
      }

      const pvShared = {
        solar_fraction_used_onsite: state.systems.pv?.fractionUsedOnsite,
        solar_payments: state.systems.pv?.payments,
        solar_FIT:
          state.systems.pv?.payments === 'fit' ? state.systems.pv.fitGeneration : null,
        solar_export_FIT:
          state.systems.pv?.payments === 'fit' ? state.systems.pv.fitExport : null,
      };

      const pv =
        state.systems.pv === null
          ? {}
          : state.systems.pv.calculationType !== 'kWh'
            ? {
                use_PV_calculator: true,
                solarpv_kwp_installed:
                  state.systems.pv.measure !== null
                    ? state.systems.pv.measure.kWp
                    : state.systems.pv.capacitykWp,
                solarpv_orientation:
                  state.systems.pv.orientation === null
                    ? undefined
                    : new Orientation(state.systems.pv.orientation).index0,
                solarpv_inclination: state.systems.pv.inclination,
                solarpv_overshading:
                  state.systems.pv.overshading === null
                    ? undefined
                    : legacyFraction(new Overshading(state.systems.pv.overshading)),
                ...pvShared,
              }
            : {
                use_PV_calculator: false,
                solar_annual_kwh: state.systems.pv.generationkWh,
                ...pvShared,
              };

      const wind = {
        wind_annual_kwh: state.systems.wind?.generationkWh,
        wind_fraction_used_onsite: state.systems.wind?.fractionUsedOnsite,
        wind_payments: state.systems.wind?.payments,
        wind_FIT:
          state.systems.wind?.payments === 'fit'
            ? state.systems.wind.fitGeneration
            : null,
        wind_export_FIT:
          state.systems.wind?.payments === 'fit' ? state.systems.wind.fitExport : null,
      };

      const hydro = {
        hydro_annual_kwh: state.systems.hydro?.generationkWh,
        hydro_fraction_used_onsite: state.systems.hydro?.fractionUsedOnsite,
        hydro_payments: state.systems.hydro?.payments,
        hydro_FIT:
          state.systems.hydro?.payments === 'fit'
            ? state.systems.hydro.fitGeneration
            : null,
        hydro_export_FIT:
          state.systems.hydro?.payments === 'fit' ? state.systems.hydro.fitExport : null,
      };

      data.generation = { ...pv, ...wind, ...hydro };

      if (state.systems.pv !== null && state.systems.pv.measure !== null) {
        const [quantity, costTotal] = calcMeasureQtyAndCost({
          costUnits: state.systems.pv.measure.cost_units,
          costPerUnit: state.systems.pv.measure.cost,
          kWp: state.systems.pv.measure.kWp,
          baseCost: state.systems.pv.measure.min_cost,
        });
        data.measures = data.measures ?? {};
        data.measures.PV_generation = {
          measure: {
            ...state.systems.pv.measure,
            quantity,
            cost_total: costTotal,
          },
        };
      } else {
        delete data.measures?.PV_generation;
      }
    },
  },
};
