import React from 'react';
import { z } from 'zod';

import { projectSchema } from '../../data-schemas/project';
import { sum } from '../../helpers/array-reducers';
import { assertNever } from '../../helpers/assert-never';
import { ensureNonEmpty } from '../../helpers/non-empty-array';
import { Result } from '../../helpers/result';
import { Model } from '../../model/model';
import { FuelDemandByFuel } from '../../model/modules/fuel-requirements';
import { FuelName, Fuels } from '../../model/modules/fuels';
import { DeleteIcon, PlusIcon } from '../icons';
import { Button } from '../input-components/button';
import { InfoTooltip } from '../input-components/forms';
import { NumberInput } from '../input-components/number';
import { Select } from '../input-components/select';
import type { Dispatcher, UiModule } from '../module-management/module-type';
import { NumberOutput } from '../output-components/numeric';

export type State = {
  model: Model | null;
  fansAndPumpsFuelFractions: { fraction: number | null; fuelName: FuelName }[];
};
export type Action =
  | {
      type: 'external update';
      model: Model | null;
      fansAndPumpsFuelFractions: State['fansAndPumpsFuelFractions'];
    }
  | { type: 'fp/add fraction'; validFuelNames: string[] }
  | { type: 'fp/remove fraction'; index: number }
  | {
      type: 'fp/update';
      index: number;
      update: Partial<State['fansAndPumpsFuelFractions'][number]>;
    };

function AreaUseSection({ module, name }: { name: string; module: FuelDemandByFuel }) {
  const entries = Array.from(module.entries());

  const rows = entries.map(([fuelName, params], idx) => (
    <tr key={idx}>
      {idx === 0 && <td rowSpan={entries.length > 1 ? entries.length + 1 : 1}>{name}</td>}
      <td>{fuelName}</td>
      <td className="text-tabular-nums">
        <NumberOutput value={params.relativeFraction} dp={2} />
      </td>
      <td className="text-tabular-nums text-right">
        <NumberOutput value={params.fuelInput} dp={0} unit="kWh" />
      </td>
      {params.fuelInput !== params.energyDemand ? (
        <td>
          (meets <NumberOutput value={params.energyDemand} dp={0} unit="kWh" /> of demand)
        </td>
      ) : (
        <td></td>
      )}
    </tr>
  ));

  let totals: React.JSX.Element[] = [];
  if (entries.length > 1) {
    totals = [
      <tr key="totals">
        <td className="text-italic">Total</td>
        <td className="text-italic">
          {sum(entries.map(([, { relativeFraction }]) => relativeFraction))}
        </td>
        <td className="text-tabular-nums text-italic text-right">
          <NumberOutput
            value={sum(entries.map(([, { fuelInput }]) => fuelInput))}
            dp={0}
            unit="kWh"
          />
        </td>
        <td></td>
      </tr>,
    ];
  }

  return [...rows, ...totals];
}

function FansPumpsSection({
  model,
  state,
  dispatch,
}: {
  model: Model;
  state: State['fansAndPumpsFuelFractions'];
  dispatch: Dispatcher<Action>;
}) {
  const allowDeletion = state.length > 1;

  const modelData = model.normal.fansPumpsElectricKeepHot.fuelDemandByFuelAnnual;

  const electricityOptions = Object.entries(model.normal.fuels.fuels)
    .filter(([, fuel]) => fuel.category === 'electricity')
    .map(([name]) => ({ value: name, display: name }));

  return state.map(({ fuelName, fraction }, idx) => {
    const fuelInput = modelData.get(fuelName)?.fuelInput;
    return (
      <tr key={`lighting-${fuelName}`}>
        {idx === 0 && <td rowSpan={state.length}>Fans and pumps</td>}
        <td>
          <Select
            className="mb-0"
            value={fuelName}
            options={electricityOptions}
            onChange={(fuelName) =>
              dispatch({ type: 'fp/update', index: idx, update: { fuelName } })
            }
          />
        </td>
        <td className="text-tabular-nums">
          <NumberInput
            className="mb-0"
            value={fraction}
            onChange={(fraction) =>
              dispatch({ type: 'fp/update', index: idx, update: { fraction } })
            }
          />
          {allowDeletion && (
            <Button
              className="ml-7"
              icon={DeleteIcon}
              onClick={() => dispatch({ type: 'fp/remove fraction', index: idx })}
            />
          )}
        </td>
        <td className="text-tabular-nums text-right">
          <NumberOutput value={fuelInput} dp={0} unit="kWh" />
        </td>
        <td></td>
      </tr>
    );
  });
}

function EnergyUseByArea({
  model,
  dispatch,
  fansAndPumpsFuelFractionsState,
}: {
  model: Model;
  dispatch: Dispatcher<Action>;
  fansAndPumpsFuelFractionsState: State['fansAndPumpsFuelFractions'];
}) {
  return (
    <section className="line-top mb-30">
      <h3 className="mt-0">Annual energy requirement by source and by area</h3>

      <table className="table" style={{ width: 'auto' }}>
        <thead>
          <tr style={{ backgroundColor: 'var(--grey-800)' }}>
            <th>Area</th>
            <th>Energy source</th>
            <th>Fraction</th>
            <th>Quantity</th>
            <th></th>
          </tr>
        </thead>

        <tbody>
          <AreaUseSection
            name="Lighting"
            module={model.normal.fuelRequirements.fuelDemandsByFuelByModule.lighting}
          />
          <AreaUseSection
            name="Appliances"
            module={model.normal.fuelRequirements.fuelDemandsByFuelByModule.appliances}
          />
          <AreaUseSection
            name="Cooking"
            module={model.normal.fuelRequirements.fuelDemandsByFuelByModule.cooking}
          />
          <AreaUseSection
            name="Appliances (SAP)"
            module={model.normal.fuelRequirements.fuelDemandsByFuelByModule.appliancesSap}
          />
          <AreaUseSection
            name="Cooking (SAP)"
            module={model.normal.fuelRequirements.fuelDemandsByFuelByModule.cookingSap}
          />
          <AreaUseSection
            name="Water heating"
            module={model.normal.fuelRequirements.fuelDemandsByFuelByModule.waterHeating}
          />
          <AreaUseSection
            name="Space heating"
            module={model.normal.fuelRequirements.fuelDemandsByFuelByModule.spaceHeating}
          />
          <FansPumpsSection
            model={model}
            state={fansAndPumpsFuelFractionsState}
            dispatch={dispatch}
          />
        </tbody>
      </table>

      <Button
        icon={PlusIcon}
        title="Add fan and pump fraction"
        onClick={() => {
          const allElectricityFuelNames = Object.entries(model?.normal.fuels.fuels ?? {})
            .filter(([, fuel]) => fuel.category === 'electricity')
            .map(([name]) => name);
          dispatch({
            type: 'fp/add fraction',
            validFuelNames: allElectricityFuelNames,
          });
        }}
      />
    </section>
  );
}

function OnsiteGeneration({ model }: { model: Model }) {
  const generationFuels = [
    { name: 'Solar PV', data: model.normal.generation.solar },
    { name: 'Wind', data: model.normal.generation.wind },
    { name: 'Hydro', data: model.normal.generation.hydro },
  ].filter((fuel) => fuel.data.energyAnnual > 0);

  return (
    <section className="line-top mb-30">
      <h3 className="mt-0">Annual energy generation</h3>

      {generationFuels.length === 0 ? (
        <div>No onsite generation</div>
      ) : (
        <table className="table" style={{ width: 'auto' }}>
          <thead>
            <tr style={{ backgroundColor: 'var(--grey-800)' }}>
              <th>Source</th>
              <th>Quantity</th>
              <th>% used onsite</th>
              <th>Qty used onsite</th>
            </tr>
          </thead>
          <tbody>
            {generationFuels.map((fuel, idx) => (
              <tr key={idx}>
                <td>{fuel.name}</td>
                <td className="text-tabular-nums text-right">
                  <NumberOutput value={fuel.data.energyAnnual} dp={0} unit="kWh" />
                </td>
                <td className="text-tabular-nums text-right">
                  <NumberOutput
                    value={fuel.data.fractionUsedOnsite * 100}
                    dp={0}
                    unit="%"
                  />
                </td>
                <td className="text-tabular-nums text-right">
                  <NumberOutput
                    value={fuel.data.energyUsedOnsiteAnnual}
                    dp={0}
                    unit="kWh"
                  />
                </td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr>
              <td className="text-bold">Total</td>
              <td className="text-tabular-nums text-right">
                <NumberOutput
                  value={model.normal.generation.energyAnnual}
                  dp={0}
                  unit="kWh"
                />
              </td>
              <td />
              <td className="text-tabular-nums text-right">
                <NumberOutput
                  value={model.normal.generation.energyUsedOnsiteAnnual}
                  dp={0}
                  unit="kWh"
                />
              </td>
            </tr>
          </tfoot>
        </table>
      )}
    </section>
  );
}

function EnergyUseBySource({ model }: { model: Model }) {
  return (
    <section className="line-top mb-30">
      <h3 className="mt-0">Annual energy use by source</h3>

      <table className="table" style={{ width: 'auto' }}>
        <thead>
          <tr style={{ backgroundColor: 'var(--grey-800)' }}>
            <th>Energy source</th>
            <th>Quantity</th>
            <th>Carbon factor</th>
            <th>Emissions</th>
            <th>Standing charge</th>
            <th>Unit cost</th>
            <th>Total cost</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {Array.from(model.normal.fuelRequirements.totalsByFuelAnnual.entries()).map(
            ([name, usage]) => {
              const fuel = model.normal.fuels.fuels[name];
              return (
                <tr key={name}>
                  <td>{name}</td>
                  <td className="text-tabular-nums text-right">
                    <NumberOutput value={usage.quantity} dp={0} unit="kWh" />
                  </td>
                  <td>
                    <NumberOutput value={fuel?.carbonEmissionsFactor} dp={3} />
                  </td>
                  <td className="text-tabular-nums text-right">
                    <NumberOutput value={usage.carbonEmissions} unit="kgCO₂e" dp={0} />
                  </td>
                  <td className="text-tabular-nums text-right">
                    £<NumberOutput value={fuel?.standingCharge} dp={0} />
                  </td>
                  <td className="text-tabular-nums text-right">
                    <NumberOutput value={fuel?.unitPrice} unit="p/kWh" dp={2} />
                  </td>
                  <td className="text-tabular-nums text-right">
                    {name === 'generation' && (
                      <InfoTooltip>Cost accounts for % used onsite</InfoTooltip>
                    )}
                    £<NumberOutput value={usage.cost} dp={2} />
                  </td>
                </tr>
              );
            },
          )}
          <tr>
            <th>Totals</th>
            <td className="text-tabular-nums text-right">
              <NumberOutput
                value={model.normal.fuelRequirements.totalEnergyAnnual}
                unit="kWh"
                dp={0}
              />
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td className="text-tabular-nums text-right">
              £
              <NumberOutput
                value={model.normal.fuelRequirements.totalCostAnnual}
                dp={2}
              />
            </td>
          </tr>
          {model.normal.generation.incomeAnnual > 0 ? (
            <>
              <tr>
                <th>Total energy income</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td className="text-tabular-nums text-right">
                  £
                  <NumberOutput value={model.normal.generation.incomeAnnual} dp={2} />
                </td>
              </tr>
              <tr>
                <th>Net cost</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td className="text-tabular-nums text-right">
                  £
                  <NumberOutput
                    value={model.normal.fuelRequirements.totalNetCostAnnual}
                    dp={2}
                  />
                </td>
              </tr>
            </>
          ) : null}
        </tbody>
      </table>
    </section>
  );
}

export const energyUseModule: UiModule<State, Action, never> = {
  name: 'energy use',
  component: function EnergyUse({ state, dispatch }) {
    const { model, fansAndPumpsFuelFractions: fansAndPumps } = state;

    if (model === null) {
      return <div>Error running model, no data to display</div>;
    }

    return (
      <>
        <EnergyUseByArea
          model={model}
          fansAndPumpsFuelFractionsState={fansAndPumps}
          dispatch={dispatch}
        />
        <OnsiteGeneration model={model} />
        <EnergyUseBySource model={model} />
      </>
    );
  },
  initialState: () => {
    return {
      model: null,
      fansAndPumpsFuelFractions: [{ fuelName: Fuels.STANDARD_TARIFF, fraction: 1.0 }],
    };
  },
  reducer: (state, action) => {
    const defaultElement = { fuelName: Fuels.STANDARD_TARIFF, fraction: 1.0 };
    switch (action.type) {
      case 'external update':
        return [
          {
            model: action.model,
            fansAndPumpsFuelFractions: ensureNonEmpty(
              action.fansAndPumpsFuelFractions,
              defaultElement,
            ),
          },
        ];

      case 'fp/remove fraction': {
        state.fansAndPumpsFuelFractions = ensureNonEmpty(
          state.fansAndPumpsFuelFractions.toSpliced(action.index, 1),
          defaultElement,
        );
        return [state];
      }

      case 'fp/update': {
        if (action.index >= state.fansAndPumpsFuelFractions.length) {
          console.warn('failed update due to out-of-range index');
          return [state];
        }
        state.fansAndPumpsFuelFractions[action.index] = {
          // SAFETY: checked above
          // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
          ...state.fansAndPumpsFuelFractions[action.index]!,
          ...action.update,
        };
        return [state];
      }

      case 'fp/add fraction': {
        // Find first unused fuel name if exists
        const existingFuelNames = Array.from(
          state.fansAndPumpsFuelFractions.map(({ fuelName }) => fuelName),
        );
        let chosenFuelName: string | null = null;
        for (const candidateFuelName of action.validFuelNames) {
          if (existingFuelNames.includes(candidateFuelName)) {
            continue;
          } else {
            chosenFuelName = candidateFuelName;
          }
        }

        if (chosenFuelName !== null) {
          state.fansAndPumpsFuelFractions.push({
            fuelName: chosenFuelName,
            fraction: 0,
          });
        }

        state.fansAndPumpsFuelFractions = ensureNonEmpty(
          state.fansAndPumpsFuelFractions,
          defaultElement,
        );
        return [state];
      }
    }
  },
  effector: assertNever,
  shims: {
    extractUpdateAction: ({ currentModel, currentScenario }) => {
      const fansAndPumpsFuelFractions = (currentScenario?.fans_and_pumps ?? []).map(
        ({ fuel, fraction }) => ({
          fuelName: fuel,
          fraction,
        }),
      );

      return Result.ok([
        {
          type: 'external update',
          fansAndPumpsFuelFractions,
          model: currentModel.mapErr(() => null).coalesce(),
        },
      ]);
    },
    mutateLegacyData: (
      { project: projectRaw }: { project: unknown },
      { scenarioId }: { scenarioId: string | null },
      state: State,
    ) => {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const project = projectRaw as z.input<typeof projectSchema>;
      if (scenarioId === null) {
        throw new Error("can't mutate nonexistent scenario");
      }
      const data = project.data[scenarioId];
      if (data === undefined) {
        throw new Error("can't mutate nonexistent scenario");
      }

      data.fans_and_pumps = state.fansAndPumpsFuelFractions.map((entry) => ({
        fuel: entry.fuelName,
        fraction: entry.fraction,
      }));
    },
  },
};
