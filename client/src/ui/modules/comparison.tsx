import React from 'react';

import { z } from 'zod';
import { resultSchema } from '../../data-schemas/helpers/result';
import { GenericMeasure } from '../../data-schemas/scenario/measures';
import { assertNever } from '../../helpers/assert-never';
import { Result } from '../../helpers/result';
import { embodiedCarbonFromMeasure, getScenarioMeasures } from '../../measures';
import { Model } from '../../model/model';
import type { UiModule } from '../module-management/module-type';
import { NumberOutput } from '../output-components/numeric';

type ComparisonScenario = {
  displayName: string;
  measures: GenericMeasure[];
  model: Model;
};

type State = {
  scenarios: Record<string, ComparisonScenario>;
};
type Action = { type: 'new state'; state: State };

function getUsedFuelNames(scenarios: ComparisonScenario[]): string[] {
  const fuelNames = new Set<string>();
  for (const scenario of scenarios) {
    for (const fuelName of scenario.model.normal.fuelRequirements.totalsByFuelAnnual.keys()) {
      fuelNames.add(fuelName);
    }
  }
  return [...fuelNames];
}

type ScenarioTableRow = {
  title: string;
  value: (scenario: ComparisonScenario) => number | undefined | null;
};

type ScenarioTableProps = {
  title: string;
  rows: ScenarioTableRow[];
  scenarios: ComparisonScenario[];
  dp?: number;
};

function ScenarioTable({ title, rows, scenarios, dp = 1 }: ScenarioTableProps) {
  return (
    <table className="table" style={{ width: 'auto' }}>
      <thead>
        <tr>
          <th style={{ width: '15rem' }}>{title}</th>
          {scenarios.map((scenario, idx) => (
            <th key={idx}>{idx === 0 ? 'Baseline' : `Scen.${idx}`}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {rows.map((row, idx) => (
          <tr key={idx}>
            <td>{row.title}</td>
            {scenarios.map((scenario, idx) => (
              <td className="text-right" key={idx}>
                <NumberOutput value={row.value(scenario)} dp={dp} />
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
}

function ScenariosSummary({ scenarios }: { scenarios: ComparisonScenario[] }) {
  const fuelNames = getUsedFuelNames(scenarios);

  return (
    <details>
      <summary className="text-bold">Summary</summary>
      <ScenarioTable
        title="Totals"
        scenarios={scenarios}
        rows={[
          {
            title: 'Space heating demand (kWh/m²·a)',
            value: (scenario) =>
              scenario.model.normal.spaceHeating.spaceHeatingDemandAnnualEnergyPerArea,
          },
          {
            title: 'CO₂ emissions (kgCO₂/m²·a)',
            value: (scenario) =>
              scenario.model.normal.fuelRequirements.totalCarbonEmissionsAnnualPerArea,
          },
          {
            title: 'Per person energy use (kWh/m²·day)',
            value: (scenario) =>
              scenario.model.normal.fuelRequirements.energyPerPersonPerDayAverage,
          },
        ]}
      />

      <ScenarioTable
        title="Gains and losses (kWh/m²·a)"
        scenarios={scenarios}
        rows={[
          {
            title: 'Internal gains',
            value: (scenario) =>
              scenario.model.normal.spaceHeating.usefulInternalHeatGainEnergyPerArea,
          },
          {
            title: 'Solar gains',
            value: (scenario) =>
              scenario.model.normal.spaceHeating.usefulSolarHeatGainEnergyPerArea,
          },
          {
            title: 'Fabric losses',
            value: (scenario) =>
              scenario.model.normal.spaceHeating.heatLossAnnualEnergyPerArea.fabric,
          },
          {
            title: 'Ventilation losses',
            value: (scenario) =>
              scenario.model.normal.spaceHeating.heatLossAnnualEnergyPerArea.ventilation,
          },
          {
            title: 'Infiltration losses',
            value: (scenario) =>
              scenario.model.normal.spaceHeating.heatLossAnnualEnergyPerArea.infiltration,
          },
        ]}
      />

      <ScenarioTable
        title="Energy demand (kWh/a)"
        dp={0}
        scenarios={scenarios}
        rows={[
          {
            title: 'Appliances',
            value: (scenario) =>
              Math.max(
                scenario.model.normal.appliancesSap.energyAnnual,
                scenario.model.normal.appliancesCookingLoadCollections.appliances
                  .energyDemandAnnual,
              ),
          },
          {
            title: 'Cooking',
            value: (scenario) =>
              Math.max(
                scenario.model.normal.cookingSap.energyAnnual,
                scenario.model.normal.appliancesCookingLoadCollections.cooking
                  .energyDemandAnnual,
              ),
          },
          {
            title: 'Fans and pumps',
            value: (scenario) =>
              scenario.model.normal.fansPumpsElectricKeepHot.annualEnergy,
          },
          {
            title: 'Lighting',
            value: (scenario) => scenario.model.normal.lighting.energyAnnual,
          },
          {
            title: 'Space heating',
            value: (scenario) =>
              scenario.model.normal.spaceHeating.spaceHeatingDemandEnergyAnnual,
          },
          {
            title: 'Water heating',
            value: (scenario) => scenario.model.normal.waterHeating.heatOutputAnnual,
          },
          {
            title: 'Total',
            value: (scenario) => scenario.model.normal.fuelRequirements.totalEnergyAnnual,
          },
        ]}
      />

      <ScenarioTable
        title="Energy demand intensity (kWh/m2/a)"
        dp={1}
        scenarios={scenarios}
        rows={[
          {
            title: 'Appliances',
            value: (scenario) =>
              Math.max(
                scenario.model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual
                  .appliances,
                scenario.model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual
                  .appliancesSap,
              ),
          },
          {
            title: 'Cooking',
            value: (scenario) =>
              Math.max(
                scenario.model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual
                  .cooking,
                scenario.model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual
                  .cookingSap,
              ),
          },
          {
            title: 'Fans and pumps',
            value: (scenario) =>
              scenario.model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual
                .fansPumpsElectricKeepHot,
          },
          {
            title: 'Lighting',
            value: (scenario) =>
              scenario.model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual
                .lighting,
          },
          {
            title: 'Space heating',
            value: (scenario) =>
              scenario.model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual
                .spaceHeating,
          },
          {
            title: 'Water heating',
            value: (scenario) =>
              scenario.model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual
                .waterHeating,
          },
          {
            title: 'Total',
            value: (scenario) =>
              scenario.model.normal.fuelRequirements.totalEnergyAnnualPerArea,
          },
        ]}
      />

      <ScenarioTable
        title="Fuel input (kWh/a)"
        dp={0}
        scenarios={scenarios}
        rows={[
          {
            title: 'Appliances',
            value: (scenario) =>
              Math.max(
                scenario.model.normal.appliancesSap.totalFuelRequirement,
                scenario.model.normal.appliancesCookingLoadCollections.appliances
                  .fuelInputAnnual,
              ),
          },
          {
            title: 'Cooking',
            value: (scenario) =>
              Math.max(
                scenario.model.normal.cookingSap.totalFuelRequirement,
                scenario.model.normal.appliancesCookingLoadCollections.cooking
                  .fuelInputAnnual,
              ),
          },
          {
            title: 'Fans and pumps',
            value: (scenario) =>
              scenario.model.normal.fansPumpsElectricKeepHot.totalFuelDemandAnnual,
          },
          {
            title: 'Lighting',
            value: (scenario) => scenario.model.normal.lighting.totalFuelRequirement,
          },
          {
            title: 'Space heating',
            value: (scenario) =>
              scenario.model.normal.heatingSystemsFuelRequirements
                .spaceHeatingFuelInputAnnual,
          },
          {
            title: 'Water heating ',
            value: (scenario) =>
              scenario.model.normal.heatingSystemsFuelRequirements
                .waterHeatingFuelInputAnnual,
          },
          {
            title: 'Total',
            value: (scenario) => scenario.model.normal.fuelRequirements.totalEnergyAnnual,
          },
        ]}
      />

      <ScenarioTable
        title="Demand by fuel (kWh/a)"
        dp={0}
        scenarios={scenarios}
        rows={fuelNames.map(
          (fuelName) =>
            ({
              title: fuelName,
              value: (scenario) =>
                scenario.model.normal.fuelRequirements.totalsByFuelAnnual.get(fuelName)
                  ?.quantity,
            }) satisfies ScenarioTableRow,
        )}
      />

      <ScenarioTable
        title="CO₂ emissions by fuel (kgCO₂/a)"
        dp={0}
        scenarios={scenarios}
        rows={fuelNames.map(
          (fuelName) =>
            ({
              title: fuelName,
              value: (scenario) =>
                scenario.model.normal.fuelRequirements.totalsByFuelAnnual.get(fuelName)
                  ?.carbonEmissions,
            }) satisfies ScenarioTableRow,
        )}
      />

      <ScenarioTable
        title="Annual cost by fuel (£)"
        dp={0}
        scenarios={scenarios}
        rows={[
          ...fuelNames.map(
            (fuelName) =>
              ({
                title: fuelName,
                value: (scenario) =>
                  scenario.model.normal.fuelRequirements.totalsByFuelAnnual.get(fuelName)
                    ?.cost,
              }) satisfies ScenarioTableRow,
          ),
          {
            title: 'Total cost (£)',
            value: (scenario) => scenario.model.normal.fuelRequirements.totalCostAnnual,
          },
        ]}
      />
    </details>
  );
}

function MeasureSummary({ displayName, measures }: ComparisonScenario) {
  return (
    <details>
      <summary className="text-bold">{displayName}</summary>

      <table className="table">
        <thead>
          <tr>
            <th>Tag</th>
            <th>Name</th>
            <th>Label/location</th>
            <th>Cost</th>
            <th>Carbon</th>
          </tr>
        </thead>
        <tbody>
          {measures.map((measure, idx) => {
            const carbonData = embodiedCarbonFromMeasure(measure);
            return (
              <tr key={idx}>
                <td>{measure.tag}</td>
                <td>{measure.name}</td>
                <td>{measure.location}</td>
                <td className="text-right tabular-nums">
                  £<NumberOutput value={measure.cost_total} />
                </td>
                <td className="text-nowrap">
                  {!carbonData.isComplete ? (
                    'incomplete'
                  ) : carbonData.upperUpfrontCarbon === carbonData.lowerUpfrontCarbon ? (
                    <NumberOutput dp={0} value={carbonData.lowerUpfrontCarbon} />
                  ) : (
                    <>
                      <NumberOutput dp={0} value={carbonData.lowerUpfrontCarbon} />-
                      <NumberOutput dp={0} value={carbonData.upperUpfrontCarbon} />
                    </>
                  )}{' '}
                  kgCO₂e
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </details>
  );
}

function CompleteMeasures({ displayName, measures }: ComparisonScenario) {
  return (
    <details>
      <summary className="text-bold">{displayName}</summary>

      <div className="d-flex flex-v gap-30">
        {measures.map((measure, idx) => (
          <table key={idx} className="table">
            <tbody>
              <tr>
                <th>Measure:</th>
                <td>{measure.name}</td>
              </tr>
              <tr>
                <th>Code:</th>
                <td>{measure.tag}</td>
              </tr>
              <tr>
                <th>Label/location:</th>
                <td>{measure.location}</td>
              </tr>
              <tr>
                <th>Description:</th>
                <td>{measure.description}</td>
              </tr>
              <tr>
                <th>Associated work:</th>
                <td>{measure.associated_work}</td>
              </tr>
              <tr>
                <th>Maintenance:</th>
                <td>{measure.maintenance}</td>
              </tr>
              <tr>
                <th>Special and other considerations:</th>
                <td>{measure.notes}</td>
              </tr>
              <tr>
                <th>Who by:</th>
                <td>{measure.who_by}</td>
              </tr>
              <tr>
                <th>Key risks:</th>
                <td>{measure.key_risks}</td>
              </tr>
              <tr>
                <th>Dirt and disruption:</th>
                <td>{measure.disruption}</td>
              </tr>
              <tr>
                <th>Benefits:</th>
                <td>{measure.benefits}</td>
              </tr>
              <tr>
                <th>Performance target:</th>
                <td>{measure.performance}</td>
              </tr>
              <tr>
                <th>Cost per unit</th>
                <td>
                  {measure.min_cost !== 0 && <>£{measure.min_cost}, plus </>}£
                  {measure.cost} per {measure.cost_units} ×{' '}
                  <NumberOutput value={measure.quantity} /> {measure.cost_units}
                </td>
              </tr>
              <tr>
                <th>Total cost</th>
                <td>
                  £<NumberOutput value={measure.cost_total} />
                </td>
              </tr>
            </tbody>
          </table>
        ))}
      </div>
    </details>
  );
}

export const comparisonModule: UiModule<State, Action, never> = {
  name: 'sandbox',
  component: function Comparison({ state }) {
    const nonBaselineScenarios = Object.entries(state.scenarios).filter(
      ([scenarioId]) => scenarioId !== 'master',
    );
    return (
      <section className="d-flex flex-v gap-15">
        <ScenariosSummary scenarios={Object.values(state.scenarios)} />

        <section>
          <h3>Summary of measures by scenario</h3>
          {nonBaselineScenarios.map(([scenarioId, scenario]) => (
            <MeasureSummary key={scenarioId} {...scenario} />
          ))}
        </section>

        <section>
          <h3>Complete measure info by scenario</h3>
          {nonBaselineScenarios.map(([scenarioId, scenario]) => (
            <CompleteMeasures key={scenarioId} {...scenario} />
          ))}
        </section>
      </section>
    );
  },
  initialState: () => {
    return { scenarios: {} };
  },
  reducer: (state_, action) => {
    switch (action.type) {
      case 'new state':
        return [action.state];
    }
  },
  effector: assertNever,
  shims: {
    extractUpdateAction: ({ project }) => {
      const newState: State = { scenarios: {} };
      for (const [scenarioId, scenario] of Object.entries(project.data)) {
        if (scenario === undefined) {
          return Result.err(new Error('scenario was undefined'));
        }
        const modelRResult = resultSchema(
          z.instanceof(Model),
          z.instanceof(Error),
        ).safeParse(scenario?.model);
        if (!modelRResult.success) {
          return Result.err(
            new Error('scenario.model was not an instance of Result<Model, Error>'),
          );
        }
        if (modelRResult.data.isErr()) {
          return modelRResult.data;
        }
        const model = modelRResult.data.unwrap();
        let measures: GenericMeasure[];
        if (scenarioId === 'master') {
          measures = [];
        } else {
          measures = getScenarioMeasures(scenario);
        }
        newState.scenarios[scenarioId] = {
          displayName: scenario.scenario_name ?? '',
          measures,
          model,
        };
      }
      return Result.ok([{ type: 'new state', state: newState }]);
    },
    mutateLegacyData: () => undefined,
  },
};
