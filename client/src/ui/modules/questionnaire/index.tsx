import { pick } from 'lodash';
import React, { ReactNode, useContext, useState } from 'react';
import { z } from 'zod';
import { projectSchema } from '../../../data-schemas/project';
import { Household, householdSchema } from '../../../data-schemas/scenario/household';
import { assertNever } from '../../../helpers/assert-never';
import { Result } from '../../../helpers/result';
import { UnvalidatedTimePeriod } from '../../../periods/validation';
import { HeatingTable } from '../../input-components/heating-table';
import { TextInput } from '../../input-components/text';
import { Textarea } from '../../input-components/textarea';
import type { Dispatcher, UiModule } from '../../module-management/module-type';
import { nl2br } from '../../output-components/nl2br';
import { HStack, VStack } from '../../stacks';
import {
  MultipleCheckbox,
  MultipleChoice,
  NumberField,
  TextField,
  YesNoField,
} from './components';
import { PriorityPicker } from './priority-picker';

const fieldNames = [
  'interviewDate',
  'surveyorName',
  'householderName',
  'timeSinceMoveLegacy',
  'moveInDate',
  'moveInDateNotes',
  'occupantsUnder5',
  'occupants5to17',
  'occupants18to64',
  'occupants65Over',
  'occupantsNotes',
  'daytimeOccupancy',
  'daytimeOccupancyNotes',
  'lifestyleChange',
  'lifestyleChangeNotes',
  'electricVehicles',
  'electricVehiclesNotes',
  'pets',
  'petsNotes',
  'healthConditions',
  'healthConditionsNotes',
  'numBedrooms',
  'roomUse',
  'priorWork',
  'priorWorkApprovals',
  'buildingMaintenance',
  'electricsCondition',
  'structuralIssues',
  'structuralIssuesNotes',
  'structuralIssuesNotesLegacy',
  'floodingHistory',
  'floodingHistoryNote',
  'dampCondensationMould',
  'dampCondensationMouldLegacy',
  'ventilationSystem',
  'laundryOptions',
  'laundryFrequency',
  'laundryTumbleDrier',
  'laundryTumbleDrierFrequency',
  'laundryNotes',
  'temperatureComfortSummer',
  'temperatureComfortWinter',
  'humidityComfortSummer',
  'humidityComfortWinter',
  'draughtsComfortSummer',
  'draughtsComfortWinter',
  'thermalComfortProblemLocations',
  'thermalComfortProblemLocationsNotes',
  'overheatingNotesLegacy',
  'controlWinter',
  'controlSummer',
  'controlNotes',
  'daylightLevel',
  'daylightProblemLocations',
  'daylightProblemLocationsNotes',
  'noiseProblems',
  'noiseProblemsNotes',
  'roomsUnloved',
  'roomsFavourite',
  'generalContextNotes',
  'heating',
  'heatingControls',
  'heatingOff',
  'heatingOffMonths',
  'heatingThermostatTemp',
  'heatingUnheatedRooms',
  'heatingNotes',
  'heatingRoomHeaters',
  'heatingRoomHeatersNotes',
  'heatingBurnersAirSupply',
  'hotWater',
  'hotWaterCylinder',
  'hotWaterCylinderNotes',
  'hotWaterShowersPerWeek',
  'hotWaterBathsPerWeek',
  'hotWaterUsageNotes',
  'expectedStay',
  'expectedStayNotes',
  'otherWorks',
  'otherWorksDesigns',
  'otherWorksNotes',
  'deadlinesNotes',
  'deadlinesLegacy',
  'priorities',
  'prioritiesLegacy',
  'prioritiesNotes',
  'projectAims',
  'appearanceOutsideFront',
  'appearanceOutsideSideRear',
  'appearanceInternal',
  'appearanceOutsideLegacy',
  'appearanceDetailsToPreserve',
  'appearanceNotes',
  'logisticsPhasing',
  'logisticsDiy',
  'logisticsManagement',
  'logisticsNotes',
  'disruption',
  'disruptionNotes',
  'budget',
  'budgetExact',
  'budgetFinancing',
  'budgetNotes',
] as const;

type KeysOfValue<T, Type> = {
  [K in keyof T]: T[K] extends Type ? K : never;
}[keyof T];

type FieldNames = (typeof fieldNames)[number];
type Questions = Pick<Household, FieldNames>;
type StringKey = KeysOfValue<Questions, string>;
type YesNoKey = KeysOfValue<Questions, boolean | null>;

type State = {
  data: Questions;
  weekend: UnvalidatedTimePeriod[];
  weekday: UnvalidatedTimePeriod[];
};

type Action =
  | { type: 'update'; data: Partial<State['data']> }
  | {
      type: 'update weekend';
      weekend: UnvalidatedTimePeriod[];
    }
  | {
      type: 'update weekday';
      weekday: UnvalidatedTimePeriod[];
    };

const initialState: State = {
  data: pick(householdSchema.parse({}), fieldNames),
  weekend: [
    { start: null, end: null },
    { start: null, end: null },
    { start: null, end: null },
  ],
  weekday: [
    { start: null, end: null },
    { start: null, end: null },
    { start: null, end: null },
  ],
};

type QuestionContextType = {
  state: State;
  dispatch: Dispatcher<Action>;
};
export const QuestionContext = React.createContext<QuestionContextType>({
  state: initialState,
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  dispatch: () => {},
});

function DayMonthYearField({ label, id }: { id: StringKey; label: string }) {
  const {
    state: { data },
    dispatch,
  } = useContext(QuestionContext);

  return (
    <div>
      <label htmlFor={id}>{label}</label>
      <HStack>
        <TextInput
          id={id}
          className="mb-0"
          placeholder="dd/mm/yyyy"
          value={data[id]}
          onChange={(val) => dispatch({ type: 'update', data: { [id]: val } })}
        />
        <button
          className="btn"
          onClick={() => {
            const today = new Date().toLocaleDateString('en-GB');
            dispatch({ type: 'update', data: { [id]: today } });
          }}
        >
          Use today&apos;s date
        </button>
      </HStack>
    </div>
  );
}

function MonthYearField({ label, id }: { id: StringKey; label: string }) {
  const {
    state: { data },
    dispatch,
  } = useContext(QuestionContext);
  return (
    <div>
      <label htmlFor={id}>{label}</label>
      <TextInput
        id={id}
        className="mb-0"
        placeholder="mm/yyyy"
        value={data[id]}
        onChange={(val) => dispatch({ type: 'update', data: { [id]: val } })}
      />
    </div>
  );
}

function NotesField({
  label = 'Notes',
  id,
  prompt,
  hinge,
}: {
  id: StringKey;
  label?: string;
  prompt?: string;
  hinge?: YesNoKey;
}) {
  const {
    state: { data },
    dispatch,
  } = useContext(QuestionContext);
  const [displaying, setDisplaying] = useState<boolean>(
    data[id] !== '' || (hinge === undefined ? true : data[hinge] === true),
  );
  if (displaying === false && hinge !== undefined && data[hinge] === true) {
    setDisplaying(true);
  }

  return displaying === false ? (
    <div>
      <button className="btn" onClick={() => setDisplaying(true)}>
        Add notes
      </button>
    </div>
  ) : (
    <VStack small>
      <label htmlFor={id}>{label}</label>
      <Textarea
        id={id}
        style={{ width: '60ch', height: '5rem' }}
        className="mb-0"
        value={data[id]}
        onChange={(val) => dispatch({ type: 'update', data: { [id]: val } })}
      />
      {prompt !== undefined && <div className="text-italic">{prompt}</div>}
    </VStack>
  );
}

function SurveyorContext({ children }: { children: ReactNode }) {
  return (
    <details style={{ gridColumn: 'span 2' }}>
      <summary>Why?</summary>
      {children}
    </details>
  );
}

function Group({
  section,
  label,
  children,
}: {
  section: string;
  label?: string;
  children: ReactNode;
}) {
  return (
    <div className="d-flex gap-7">
      <div style={{ width: '1.5rem', flexShrink: 0 }}>{section}</div>
      <VStack mid>
        {label !== undefined && <span className="text-bold">{label}</span>}
        {children}
      </VStack>
    </div>
  );
}

function LegacyDisplay({ label, value }: { label: string; value: string }) {
  return (
    <VStack small>
      <span>{label}</span>
      <div style={{ backgroundColor: 'var(--grey-800)' }} className="pa-7">
        {nl2br(value)}
      </div>
    </VStack>
  );
}

function HeatingTimes({ dispatch }: { dispatch: Dispatcher<Action> }) {
  const { state } = useContext(QuestionContext);
  return (
    <div>
      <p>
        Your normal heating hours on a <b>weekday</b> in winter are:
      </p>
      <HeatingTable
        periods={state.weekday}
        onChange={(newPeriods) =>
          dispatch({ type: 'update weekday', weekday: newPeriods })
        }
      />

      <p>
        Your normal heating hours on a <b>weekend</b> in winter are:
      </p>
      <HeatingTable
        periods={state.weekend}
        onChange={(newPeriods) =>
          dispatch({ type: 'update weekend', weekend: newPeriods })
        }
      />
    </div>
  );
}

export const questionnaireModule: UiModule<State, Action, never> = {
  name: 'questionnaire',
  component: function Questionnaire({ state, dispatch }) {
    function update(fieldName: FieldNames) {
      return (val: State['data'][typeof fieldName]) =>
        dispatch({ type: 'update', data: { [fieldName]: val } });
    }

    return (
      <QuestionContext.Provider value={{ state, dispatch }}>
        <div style={{ maxWidth: '40rem' }}>
          <style>label &#123; width: fit-content &#125;</style>

          <VStack mid>
            <DayMonthYearField label="Interview date" id="interviewDate" />

            <TextField
              label="Householder name"
              value={state.data.householderName}
              onChange={update('householderName')}
            />

            <TextField
              label="Surveyor name"
              value={state.data.surveyorName}
              onChange={update('surveyorName')}
            />
          </VStack>

          <h3 className="line-top mb-15">Current context</h3>

          <VStack large>
            <Group section="1.1">
              <MonthYearField label="When did you/will you move in?" id="moveInDate" />
              <NotesField
                prompt="Have they lived there a very long time? Or are they just in the
                process of buying and moving in?"
                id="moveInDateNotes"
              />
              {state.data.timeSinceMoveLegacy !== null && (
                <LegacyDisplay
                  label="Time since move (old format):"
                  value={state.data.timeSinceMoveLegacy}
                />
              )}
            </Group>

            <Group section="1.2" label="How many people live here?">
              <NumberField
                label="Children under 5"
                value={state.data.occupantsUnder5}
                onChange={update('occupantsUnder5')}
              />
              <NumberField
                label="Children 5-17"
                value={state.data.occupants5to17}
                onChange={update('occupants5to17')}
              />
              <NumberField
                label="Adults 18-64"
                value={state.data.occupants18to64}
                onChange={update('occupants18to64')}
              />
              <NumberField
                label="Adults 65 and over"
                value={state.data.occupants65Over}
                onChange={update('occupants65Over')}
              />
              <NotesField
                prompt="Any non-standard patterns of occupation? Occasional lodgers,
                returning adult children etc.?"
                id="occupantsNotes"
              />
            </Group>

            <Group section="1.3">
              <MultipleChoice
                label="Generally is someone at home:"
                choices={['most of the day', 'half the day', 'out all day']}
                value={state.data.daytimeOccupancy}
                onChange={update('daytimeOccupancy')}
              />
              <NotesField id="daytimeOccupancyNotes" />
              <SurveyorContext>
                This can:
                <ul>
                  <li>
                    affect ventilation system performance and heating system control
                    strategies
                  </li>
                  <li>
                    affect how much renewable energy can be used on site as it is
                    generated (e.g. in PV systems)
                  </li>
                  <li>
                    increase the risk of overheating (by increasing internal gains and the
                    hours of the day when people are home that are hottest)
                  </li>
                </ul>
              </SurveyorContext>
            </Group>

            <Group section="1.5">
              <YesNoField
                value={state.data.electricVehicles}
                onChange={update('electricVehicles')}
                label={`Do you have any electric vehicles?`}
              />
              <NotesField
                id="electricVehiclesNotes"
                prompt={`This includes cars, vans and mopeds (not bikes). Collect
                details of model/make, battery size, estimated milage and estimated
                percentage of time charged at home.`}
              />
            </Group>

            <Group section="1.6">
              <YesNoField
                value={state.data.pets}
                onChange={update('pets')}
                label="Do you have any pets?"
              />
              <NotesField
                id="petsNotes"
                prompt={`Note in particular anything that would affect energy use like
                leaving windows open for cats or need for draught-proofed cat flaps.`}
              />
            </Group>

            <Group section="1.A">
              <p className="text-bold">
                Please make clear that that the client does not have to answer this and
                that their answer is kept confidential and will not be included in the
                report.
              </p>
              <YesNoField
                value={state.data.healthConditions}
                onChange={update('healthConditions')}
                label="Do you have any health conditions that could be caused or affected
                by living in a cold or damp home?"
              />
              <NotesField
                id="healthConditionsNotes"
                prompt={`This can go beyond respiratory conditions like asthma or COPD
                and includes heart problems, muscular and joint conditions (e.g.
                arthritis), anxiety and depression.`}
              />
            </Group>

            <Group section="1.7">
              <TextField
                value={state.data.numBedrooms}
                onChange={update('numBedrooms')}
                label="How many bedrooms are there?"
              />
              <NotesField
                id="roomUse"
                label="Anything to note about how you use particular rooms and areas of
                the house?"
              />
            </Group>

            <Group section="1.8">
              <TextField
                value={state.data.priorWork}
                onChange={update('priorWork')}
                label="What building and energy efficiency work has already been done by
                you or previous owners?"
                prompt="This can include DIY work as well as works by professional trades."
              />
              <TextField
                value={state.data.priorWorkApprovals}
                onChange={update('priorWorkApprovals')}
                label="Did you receive building regulations approval for any of this work
                and if so are you able to share the documents and certificates for this?"
              />

              <p>
                Check for: windows and doors, loft insulation, floor insulation, cavity
                wall or insternal wall insulation, heating system and ventilation system
                upgrades, renewable generation and storage. Also note extensions and loft
                conversions etc. Should be able to follow up during site visit to check
                this as the information given here may not be comprehensive - but resident
                should be able to give some context here. Be particularly aware of loft
                conversations that have been done outside building regulations and may not
                meet fire escape and other requirements as any work on these will also
                require fire escape to be regularised.
              </p>
            </Group>

            <Group section="1.9">
              <TextField
                value={state.data.buildingMaintenance}
                onChange={update('buildingMaintenance')}
                label="Do you carry out regular building maintenace? For example clearing
                gutters and servicing boiler annually? Any past issues or problems?"
              />
              <p>
                People&apos;s memories might fail them on this, and their answer might not
                be comprehensive or accurate - but it&apos;s good to help gauge how aware
                they are of maintenace issues.
              </p>
            </Group>

            <Group section="1.10">
              <TextField
                value={state.data.electricsCondition}
                onChange={update('electricsCondition')}
                label="Are you aware of the age and condition of existing electrical
                installations and when any rewiring might have been done?"
              />
              <SurveyorContext>
                <p>
                  People&apos;s memories might fail them on this, and their answer might
                  not be comprehensive or accurate - but if the house has not been rewired
                  in 40 years or alternatively had a full rewire last year, can have big
                  implications for things like heat pumps and PVs.
                </p>
              </SurveyorContext>
            </Group>

            <Group section="1.11">
              <YesNoField
                value={state.data.structuralIssues}
                onChange={update('structuralIssues')}
                label="Are there any past, current or potential structural issues in the building?"
              />
              <NotesField
                id="structuralIssuesNotes"
                prompt={`Note the type, location and severity of any issues. Ask for
                records where relevant of any rectification work done. Ask if any flood
                resistance or resilience measures have been fitted (e.g. flood doors,
                raised services etc.).`}
              />
              {state.data.structuralIssuesNotesLegacy !== '' && (
                <LegacyDisplay
                  label="Notes only visible to surveyor (old format):"
                  value={state.data.structuralIssuesNotesLegacy}
                />
              )}
            </Group>

            <Group section="1.12">
              <YesNoField
                value={state.data.floodingHistory}
                onChange={update('floodingHistory')}
                label="Does the building have a history of flooding?"
              />
              <NotesField
                id="floodingHistoryNote"
                prompt={`Check whether there is any knowledge of past flooding issues -
                even if it's only a basement or cellar. Occasionally places will flood
                even if not shown as a risk on the flood maps (which should be checked on
                the address search page). Note the known frequency and extent/height of
                flooding if available. If possible note the type of flooding - whether
                surface water, rivers/sea etc or if was due to a burst pipe - as these
                have different implications.`}
              />
            </Group>

            <Group section="1.13">
              <NotesField
                id="dampCondensationMould"
                label="Is there any damp, condensation and mould in the building?"
              />

              {state.data.dampCondensationMouldLegacy !== '' && (
                <LegacyDisplay
                  label="Extra notes (old format):"
                  value={state.data.dampCondensationMouldLegacy}
                />
              )}
            </Group>

            <Group section="1.14">
              <TextField
                value={state.data.ventilationSystem}
                onChange={update('ventilationSystem')}
                label="How is the building ventilated?"
              />
              <p>
                This might be a dedicated mechanical system, window opening, wall or floor
                vents or a mix of these. Confirm what, if any, existing systems are
                present and how they are used. Ask about window opening habits if relevant
                and how getting rid of smells and stale air is done.
              </p>
            </Group>

            <Group section="1.15">
              <MultipleCheckbox
                value={state.data.laundryOptions}
                onChange={update('laundryOptions')}
                label="How is laundry dried?"
                choices={[
                  'external line',
                  'internal airing cupbard',
                  'internal racks in living space',
                  'internal heated racks in living space',
                  'on radiators',
                  'electric dryer (standard)',
                  'electric dryer (condensing)',
                ]}
              />
              <MultipleChoice
                label="How often do you generally have clothes drying inside the home?"
                choices={[
                  'most days',
                  'once or twice a week',
                  'most days in winter only',
                  'once or twice a week in winter only',
                  'once a month',
                  'rarely',
                ]}
                value={state.data.laundryFrequency}
                onChange={update('laundryFrequency')}
              />
              <MultipleChoice
                label="Do you have a tumble dryer?"
                choices={['no', 'yes - standard', 'yes - heat pump type']}
                value={state.data.laundryTumbleDrier}
                onChange={update('laundryTumbleDrier')}
              />
              <MultipleChoice
                label="If you use an electric drier, how often is this used?"
                choices={[
                  'most days',
                  'once or twice a week',
                  'most days in winter only',
                  'once or twice a week in winter only',
                  'once a month',
                  'rarely',
                ]}
                value={state.data.laundryTumbleDrierFrequency}
                onChange={update('laundryTumbleDrierFrequency')}
              />
              <NotesField
                id="laundryNotes"
                prompt={`Check whether inside and how often if possible and advise against
                drying on radiators. If a dryer is used add this to the appliances list in
                the model so its energy use is calculated. If suitable, a new form of
                dedicated drying space may form part of recommendations.`}
              />
            </Group>

            <Group section="1.16" label="Thermal comfort">
              <div
                style={{
                  display: 'grid',
                  gridTemplateColumns: 'max-content max-content',
                  gridGap: '30px',
                }}
              >
                <MultipleChoice
                  label="How is the temperature comfort in summer?"
                  choices={[
                    'too cold',
                    'just right',
                    'too hot',
                    "don't know",
                    'not applicable',
                  ]}
                  value={state.data.temperatureComfortSummer}
                  onChange={update('temperatureComfortSummer')}
                />
                <MultipleChoice
                  label="How is the temperature comfort in winter?"
                  choices={[
                    'too cold',
                    'just right',
                    'too hot',
                    "don't know",
                    'not applicable',
                  ]}
                  value={state.data.temperatureComfortWinter}
                  onChange={update('temperatureComfortWinter')}
                />

                <MultipleChoice
                  label="How is the humidity comfort in summer?"
                  choices={[
                    'too dry',
                    'just right',
                    'too stuffy',
                    "don't know",
                    'not applicable',
                  ]}
                  value={state.data.humidityComfortSummer}
                  onChange={update('humidityComfortSummer')}
                />
                <MultipleChoice
                  label="How is the humidity comfort in winter?"
                  choices={[
                    'too dry',
                    'just right',
                    'too stuffy',
                    "don't know",
                    'not applicable',
                  ]}
                  value={state.data.humidityComfortWinter}
                  onChange={update('humidityComfortWinter')}
                />

                <MultipleChoice
                  label="How is your comfort in relation to draughts in the summer?"
                  choices={[
                    'too draughty',
                    'just right',
                    'too still',
                    "don't know",
                    'not applicable',
                  ]}
                  value={state.data.draughtsComfortSummer}
                  onChange={update('draughtsComfortSummer')}
                />
                <MultipleChoice
                  label="How is your comfort in relation to draughts in the winter?"
                  choices={[
                    'too draughty',
                    'just right',
                    'too still',
                    "don't know",
                    'not applicable',
                  ]}
                  value={state.data.draughtsComfortWinter}
                  onChange={update('draughtsComfortWinter')}
                />
              </div>

              <YesNoField
                value={state.data.thermalComfortProblemLocations}
                onChange={update('thermalComfortProblemLocations')}
                label="Are there any problem locations - rooms or places that are often
                too hot or too cold?"
              />
              <NotesField
                id="thermalComfortProblemLocationsNotes"
                hinge="thermalComfortProblemLocations"
              />

              <YesNoField
                value={state.data.controlSummer}
                onChange={update('controlSummer')}
                label="Do you feel like you have control over your home environment in summer?"
              />
              <YesNoField
                value={state.data.controlWinter}
                onChange={update('controlWinter')}
                label="Do you feel like you have control over your home environment in winter?"
              />
              <NotesField id="controlNotes" />

              {state.data.overheatingNotesLegacy !== '' && (
                <LegacyDisplay
                  label="Overheating notes (old format):"
                  value={state.data.overheatingNotesLegacy}
                />
              )}
            </Group>
            <Group section="1.17" label="Daylight levels">
              <MultipleChoice
                label="Is the amount of daylight in the home generally:"
                choices={['too much', 'just right', 'too little', 'varies']}
                value={state.data.daylightLevel}
                onChange={update('daylightLevel')}
              />
              <YesNoField
                value={state.data.daylightProblemLocations}
                onChange={update('daylightProblemLocations')}
                label="Are there any problem locations for daylight?"
              />
              <NotesField
                id="daylightProblemLocationsNotes"
                prompt={`Make a note of anywhere that is too dark or too bright. This
                might influence recommendations on glazing and shading. In particular, may
                affect whether and where any rooflights or new windows added and where
                windows might be blocked up as part of other work.`}
              />
            </Group>

            <Group section="1.18">
              <YesNoField
                value={state.data.noiseProblems}
                onChange={update('noiseProblems')}
                label="Are there any problems with noise from neighbours, between rooms,
                from building services, or from outside?"
              />
              <NotesField
                id="noiseProblemsNotes"
                prompt="In retrofit and energy efficiency work there might be
                opportunities to address noise from outside by improving glazing or
                ventilation systems to improve sound proofing. There might also be the
                opportunity to improve party wall or floor sound-proofing as part of other
                works. Sound insulation between rooms might also be improved - or the use
                of rooms reconsidered."
              />
            </Group>

            <Group section="1.19">
              <NotesField
                id="roomsUnloved"
                label="Are there any rooms that are unloved and/or underused? If so why?"
              />
              <NotesField id="roomsFavourite" label="Is there a favourite room? Why?" />

              <p>
                This question can highlight any issues that have not been picked up above
                to to with use and comfort. It can also indicate rooms that are priorities
                for action - or rooms that should be disturbed as little as possible.
              </p>
            </Group>

            <Group section="1.20">
              <NotesField
                id="generalContextNotes"
                label="Is there anything else in the context of the building we should be
                aware of? e.g. any issues with neighbours or property boundaries etc."
                prompt="This is a broad general question but could include consideration
                of neighbours and raltionship to neighbours property or the public
                highway, relationships with neighbours, or things like access rights and
                trees or other issues not picked up earlier."
              />
            </Group>

            <Group section="1.21" label="Space heating">
              <NotesField id="heating" label="How is the building currently heated?" />
              <NotesField id="heatingControls" label="How do you control this?" />
              <YesNoField
                value={state.data.heatingOff}
                onChange={update('heatingOff')}
                label="Is any central heating system completely switched off in the summer?"
                prompt="This would just affect space heating - there would still be hot
                water for showers and washing."
              />
              <MultipleCheckbox
                value={state.data.heatingOffMonths}
                onChange={update('heatingOffMonths')}
                label="This answer feeds directly into assumptions in the baseline energy model."
                choices={[
                  'March',
                  'April',
                  'May',
                  'June',
                  'July',
                  'August',
                  'September',
                  'October',
                ]}
              />
              <TextField
                value={state.data.heatingThermostatTemp}
                onChange={update('heatingThermostatTemp')}
                label="If there is a room thermostat, what tempature is this normally set to? (°C)"
                prompt="This answer does not feed directly into the model - but should be
                taken into account when considering target heating temperatures."
              />
              <HeatingTimes dispatch={dispatch} />
              <NotesField
                id="heatingUnheatedRooms"
                label="Are any rooms not heated?  (e.g. no radiator or radiator always off)"
                prompt="This answer does not feed directly into the model - but shuld be
                taken into account when considering target heating temperatures."
              />
              <NotesField
                id="heatingNotes"
                prompt="It's important to understand how the home is heated - whether with
                multiple systems or just one - and what the patterns of use are. This can
                help improve the understanding of the baseline condition as well as
                improving any recommendations made about heating but also possible
                renewable systems - e.g. people who are at home all day are more likely to
                benefit financially from solar PV systems without storage."
              />
              <YesNoField
                value={state.data.heatingRoomHeaters}
                onChange={update('heatingRoomHeaters')}
                label="Are there any individual room heaters?"
              />
              <NotesField
                id="heatingRoomHeatersNotes"
                hinge="heatingRoomHeaters"
                label="Which rooms are these in, what type are they, and how often are
                these used?"
              />
              <YesNoField
                value={state.data.heatingBurnersAirSupply}
                onChange={update('heatingBurnersAirSupply')}
                label="As far as you are aware, do any woodburners, open fires, or gas
                fires have a dedicated air supply for combustion? This can be a floor or
                wall vent or a duct which runs to outside or a subfloor space."
              />
              <p>
                Please note that wherever combustion takes place there must be a suitable
                air supply to mitigate carbon monoxide risks. We also strongly advise that
                carbon monoxide alarms are fitted in homes with combustion appliances.
              </p>
              <p>
                This should all be double-checked and noted during the site visit. It may
                be useful to offer advice on room heaters in the report - from a point of
                view of energy efficieny, but also safety and indoor air quality with a
                view to either removing room heaters or replacing them with better
                options. e.g. woodburners and gas fires add to indoor air pollution and
                humidity and have related safety concerns about carbon monoxide and
                combustion; an oil filled radiator may be safer and more cost effiective
                than a fan heater etc.
              </p>
            </Group>

            <Group section="1.22" label="Hot water">
              <NotesField
                id="hotWater"
                label="How is hot water heated in the home?"
                prompt="It's important to understand how the water is heated - whether
                with multiple systems or just one. Remember to ask about electric showers
                and direct point of use water heaters - e..g. kitchen hot water taps -
                that may compliment central systems."
              />
              <YesNoField
                value={state.data.hotWaterCylinder}
                onChange={update('hotWaterCylinder')}
                label="Is there existing hot water storage? (A hot water cylinder)?"
              />
              <NotesField
                id="hotWaterCylinderNotes"
                hinge="hotWaterCylinder"
                label="Have you considered where there might be space to add some?"
                prompt="Explain that this might be necessary if considering moving to a heat pump."
              />
              <NumberField
                label="How many showers are usually taken per week?"
                value={state.data.hotWaterShowersPerWeek}
                onChange={update('hotWaterShowersPerWeek')}
              />
              <NumberField
                label="How many baths are usually taken per week?"
                value={state.data.hotWaterBathsPerWeek}
                onChange={update('hotWaterBathsPerWeek')}
              />
              <NotesField
                id="hotWaterUsageNotes"
                prompt="Knowing the approximate number of showers and baths can help
                calibrate hot water use calculations."
              />
            </Group>
          </VStack>

          <h3 className="line-top mb-15">Aims and plans</h3>

          <VStack large>
            <Group section="2.1">
              <NotesField
                id="expectedStay"
                label="How long do you plan to stay in the property/continue owning it?"
              />
              <NotesField
                id="expectedStayNotes"
                prompt="It's useful to note this as it can affect recommendations - people only planning to stay in the short or medium term may have different priorities to those planning to stay in the long term."
              />
            </Group>

            <Group section="2.2">
              <YesNoField
                value={state.data.lifestyleChange}
                onChange={update('lifestyleChange')}
                label={`Do you forsee any changes in lifestyle, occupancy patterns, or
              household composition?`}
              />
              <NotesField
                id="lifestyleChangeNotes"
                prompt={`This might include more or less working from home, new baby, new
                pet, children moving out, people moving in etc.`}
              />

              <SurveyorContext>
                This can help to inform overall recommendations and also to understand
                patterns in historic energy use.
              </SurveyorContext>
            </Group>

            <Group section="2.3">
              <YesNoField
                value={state.data.otherWorks}
                onChange={update('otherWorks')}
                label="Are you planning other building works or improvements and upgrades to services?"
              />
              <NotesField
                id="otherWorksDesigns"
                label="Do you have existing designs for these or have you comissioned design services and/or engaged a builder?"
              />
              <NotesField id="otherWorksNotes" />
              <p>
                This might include extensions or loft conversions or things they plan to
                do anyway like PV panels or heating system replacements. If they have
                comissioned designs ask to see this information. there may be opportunties
                to engage with the designers or builders here to offer further advice.
              </p>
            </Group>

            <Group section="2.4">
              <NotesField
                id="deadlinesNotes"
                label="Are there any specific deadlines or blockers we should be aware of to help advise you on your retrofit plans? e.g. a deadline to move in, a planned 'round the world' trip etc."
                prompt="It's important to understand if there are any hard deadlines or other programme elements that need to be taken into account. This can affect recommendations - what order things are done in and what is suggested in the first place."
              />
              {state.data.deadlinesLegacy !== null && (
                <LegacyDisplay
                  label="Expected start time (old format):"
                  value={state.data.deadlinesLegacy}
                />
              )}
            </Group>

            <Group section="2.5">
              <PriorityPicker
                state={state.data.priorities}
                onChange={(val) =>
                  dispatch({
                    type: 'update',
                    data: {
                      priorities: val,
                    },
                  })
                }
              />

              {state.data.prioritiesLegacy !== null && (
                <LegacyDisplay
                  label="Priorities (old format):"
                  value={state.data.prioritiesLegacy
                    .map(({ ordinal, title }) => `${ordinal}. ${title}`)
                    .join('\n')}
                />
              )}

              <NotesField id="prioritiesNotes" />
              <p>
                Try to make people pick a &apos;top three&apos; in order rather than just
                saying &apos;all of them&apos;. It will be more useful in decision making
                and forces them to think about it a bit.
              </p>
              <NotesField
                id="projectAims"
                label="Are there any other project aims we should be aware of?"
                prompt="This might include a preference on the type of materials used, a specific performance standard you'd like to aim for, concerns about future maintenance or climate adaptation, and/or requirements related to ethics (e.g no animal products)"
              />
              <p>
                This is about understanding motivations and drivers for the work. Should
                be helpful to ask this in the context of the questions above. If there is
                an original project brief from call with an advisor can be useful to check
                back against this for context.
              </p>
            </Group>

            <Group section="2.6" label="Appearance and features">
              <p>Would you object to changing the appearance of your home:</p>
              <MultipleChoice
                value={state.data.appearanceOutsideFront}
                onChange={update('appearanceOutsideFront')}
                label="Outside at the front/prominent street elevation?"
                choices={['keep the same', 'changing is ok']}
              />
              <MultipleChoice
                value={state.data.appearanceOutsideSideRear}
                onChange={update('appearanceOutsideSideRear')}
                label="Outside at the side/rear?"
                choices={['keep the same', 'changing is ok']}
              />
              {state.data.appearanceOutsideLegacy !== null && (
                <LegacyDisplay
                  label="Appearance outside (old format):"
                  value={state.data.appearanceOutsideLegacy}
                />
              )}
              <MultipleChoice
                value={state.data.appearanceInternal}
                onChange={update('appearanceInternal')}
                label="Inside?"
                choices={['keep the same', 'changing is ok']}
              />
              <NotesField
                id="appearanceDetailsToPreserve"
                label="Are there any features you're keen to preserve?"
                prompt=" e.g. brickwork details, cornices or other internal features, stained glass or original windows etc."
              />
              <NotesField id="appearanceNotes" />
            </Group>

            <Group section="2.7">
              <MultipleChoice
                value={state.data.logisticsPhasing}
                onChange={update('logisticsPhasing')}
                label="Do you hope to do the work..."
                choices={['all in one go', 'in phases over time']}
              />
              <YesNoField
                value={state.data.logisticsDiy}
                onChange={update('logisticsDiy')}
                label="Are you willing to do some of the work yourself? (DIY)"
              />
              <MultipleChoice
                value={state.data.logisticsManagement}
                onChange={update('logisticsManagement')}
                label="How do you plan to manage the project? By this we mean who will manage the project budget and any contracts and also any logistics when it comes to construction work."
                choices={[
                  'fully self-managed',
                  'largely self-managed with some targeted support from professionals and specialists and a main contractor to deal with site logistics',
                  'employ a project manager and main contractor to deal with everything',
                ]}
              />
              <NotesField id="logisticsNotes" />
            </Group>

            <Group section="2.8">
              <MultipleChoice
                value={state.data.disruption}
                onChange={update('disruption')}
                label="How much disruption would you be willing to live with?"
                choices={[
                  'no disruption',
                  'very minimal',
                  'having to redecorate some rooms',
                  'complete strip out and redecoration throughout',
                  'move out for a few days',
                  'move out for a few weeks',
                  'live elsewhere during the works',
                ]}
              />
              <NotesField
                id="disruptionNotes"
                prompt="Note if there are any areas of the building where disruption must be minimised or any vulnerable residents that would need to avoid dust and dirt or other risks.  Retrofit work almost always involves some degree of disruption - from just being without heat for a few hours and a few radiator upgrades to taking a building back to shell at the extreme. Things like floor insulation and internal wall insulation are among the most disruptive measures - though fitting MVHR and new windows etc can also be disruptive."
              />
            </Group>

            <Group section="2.9">
              <MultipleChoice
                value={state.data.budget}
                onChange={update('budget')}
                label="Do you have a budget in mind?"
                choices={[
                  'yes',
                  "will work out a budget after understanding what's possible",
                ]}
              />
              <TextField
                value={state.data.budgetExact}
                onChange={update('budgetExact')}
                label="Exact figure if known:"
              />
              <MultipleCheckbox
                value={state.data.budgetFinancing}
                onChange={update('budgetFinancing')}
                label="How do you plan to pay for the work?"
                choices={['savings', 'grants', 'mortgage borrowing', 'other borrowing']}
              />
              <NotesField id="budgetNotes" />
            </Group>
          </VStack>
        </div>
      </QuestionContext.Provider>
    );
  },
  initialState: () => {
    return initialState;
  },
  reducer: (state, action) => {
    switch (action.type) {
      case 'update': {
        Object.assign(state.data, action.data);
        return [state];
      }
      case 'update weekday': {
        return [{ ...state, weekday: action.weekday }];
      }
      case 'update weekend': {
        return [{ ...state, weekend: action.weekend }];
      }
    }
  },
  effector: assertNever,
  shims: {
    extractUpdateAction: ({ project }) => {
      const baselineScenario = project.data['master'];
      const household = baselineScenario?.household;
      return Result.ok(
        household === undefined
          ? []
          : [
              {
                type: 'update',
                data: pick(household, fieldNames),
              },
              { type: 'update weekday', weekday: household.heatingHoursWeekday },
              { type: 'update weekend', weekend: household.heatingHoursWeekend },
            ],
      );
    },
    mutateLegacyData: ({ project: projectRaw }, _, state) => {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const project = projectRaw as z.input<typeof projectSchema>;

      const data = project.data['master'];
      if (data === undefined) {
        console.error('Could not mutate legacy data as master scenario not found');
        return;
      }

      // When we change the household structure layout this can be junked in favour of
      // a much more simple update of `household` with `state.data`.
      const update: z.input<typeof householdSchema> = {
        interview_date: state.data.interviewDate,
        assessor_name: state.data.surveyorName,
        householder_name: state.data.householderName,
        moveInDate: state.data.moveInDate,
        moveInDateNotes: state.data.moveInDateNotes,
        occupants_under5: state.data.occupantsUnder5,
        occupants_5to17: state.data.occupants5to17,
        occupants_18to65: state.data.occupants18to64,
        occupants_over65: state.data.occupants65Over,
        occupants_note: state.data.occupantsNotes,
        daytimeOccupancy: state.data.daytimeOccupancy,
        daytimeOccupancyNotes: state.data.daytimeOccupancyNotes,
        expected_lifestyle_change: state.data.lifestyleChange,
        expected_lifestyle_change_note: state.data.lifestyleChangeNotes,
        electricVehicles: state.data.electricVehicles,
        electricVehiclesNotes: state.data.electricVehiclesNotes,
        pets: state.data.pets,
        occupants_pets: state.data.petsNotes,
        healthConditions: state.data.healthConditions,
        health: state.data.healthConditionsNotes,
        house_nr_bedrooms: state.data.numBedrooms,
        roomUse: state.data.roomUse,
        previous_works: state.data.priorWork,
        previous_works_approvals: state.data.priorWorkApprovals,
        buildingMaintenance: state.data.buildingMaintenance,
        electricsCondition: state.data.electricsCondition,
        structuralIssues: state.data.structuralIssues,
        structural_issues: state.data.structuralIssuesNotes,
        structural_issues_note: state.data.structuralIssuesNotesLegacy,
        flooding_history: state.data.floodingHistory,
        flooding_note: state.data.floodingHistoryNote,
        damp: state.data.dampCondensationMould,
        damp_note: state.data.dampCondensationMouldLegacy,
        ventilationSystem: state.data.ventilationSystem,
        laundryOptions: state.data.laundryOptions,
        laundryFrequency: state.data.laundryFrequency,
        laundryTumbleDrier: state.data.laundryTumbleDrier,
        laundryTumbleDrierFrequency: state.data.laundryTumbleDrierFrequency,
        laundry: state.data.laundryNotes,
        comfort_temperature_summer: state.data.temperatureComfortSummer,
        comfort_temperature_winter: state.data.temperatureComfortWinter,
        comfort_airquality_summer: state.data.humidityComfortSummer,
        comfort_airquality_winter: state.data.humidityComfortWinter,
        comfort_draughts_summer: state.data.draughtsComfortSummer,
        comfort_draughts_winter: state.data.draughtsComfortWinter,
        thermal_comfort_problems: state.data.thermalComfortProblemLocations,
        thermal_comfort_note: state.data.thermalComfortProblemLocationsNotes,
        overheating_note: state.data.overheatingNotesLegacy,
        controlWinter: state.data.controlWinter,
        controlSummer: state.data.controlSummer,
        controlNotes: state.data.controlNotes,
        daylight: state.data.daylightLevel,
        daylight_problems: state.data.daylightProblemLocations,
        daylight_note: state.data.daylightProblemLocationsNotes,
        noise_problems: state.data.noiseProblems,
        noise_note: state.data.noiseProblemsNotes,
        rooms_unloved: state.data.roomsUnloved,
        rooms_favourite: state.data.roomsFavourite,
        context_and_other_points: state.data.generalContextNotes,
        space_heating_provided: state.data.heating,
        space_heating_controls: state.data.heatingControls,
        heating_off_summer: state.data.heatingOff,
        heating_thermostat: state.data.heatingThermostatTemp,
        heatingHoursWeekday: state.weekday,
        heatingHoursWeekend: state.weekend,
        heating_unheated_habitable: state.data.heatingUnheatedRooms,
        heatingNotes: state.data.heatingNotes,
        heatingRoomHeaters: state.data.heatingRoomHeaters,
        heatingRoomHeatersNotes: state.data.heatingRoomHeatersNotes,
        heatingBurnersAirSupply: state.data.heatingBurnersAirSupply,
        hot_water_provided: state.data.hotWater,
        hotWaterCylinder: state.data.hotWaterCylinder,
        hotWaterCylinderNotes: state.data.hotWaterCylinderNotes,
        hotWaterShowersPerWeek: state.data.hotWaterShowersPerWeek,
        hotWaterBathsPerWeek: state.data.hotWaterBathsPerWeek,
        hotWaterUsageNotes: state.data.hotWaterUsageNotes,
        occupancy_expected: state.data.expectedStay,
        occupancy_expected_note: state.data.expectedStayNotes,
        expected_other_works: state.data.otherWorks,
        expectedOtherWorksDesigns: state.data.otherWorksDesigns,
        expected_other_works_note: state.data.otherWorksNotes,
        expected_start_note: state.data.deadlinesNotes,
        priorities: state.data.priorities,
        priority_qualitative_note: state.data.prioritiesNotes,
        projectAims: state.data.projectAims,
        appearanceOutsideFront: state.data.appearanceOutsideFront,
        appearanceOutsideSideRear: state.data.appearanceOutsideSideRear,
        aesthetics_internal: state.data.appearanceInternal,
        appearanceDetailsToPreserve: state.data.appearanceDetailsToPreserve,
        aesthetics_note: state.data.appearanceNotes,
        logistics_packaging: state.data.logisticsPhasing,
        logistics_diy: state.data.logisticsDiy,
        logisticsManagement: state.data.logisticsManagement,
        logistics_diy_note: state.data.logisticsNotes,
        logistics_disruption: state.data.disruption,
        logistics_disruption_note: state.data.disruptionNotes,
        logistics_budget: state.data.budget,
        budgetExact: state.data.budgetExact,
        budgetFinancing: state.data.budgetFinancing,
        logistics_budget_note: state.data.budgetNotes,
      };
      if (data.household === undefined) {
        data.household = {};
      }
      Object.assign(data.household, update);
    },
  },
};
