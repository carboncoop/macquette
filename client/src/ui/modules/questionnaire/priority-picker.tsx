import { zip } from 'lodash';
import React from 'react';
import { Down, Up, X } from '../../icons';

const options = [
  'reduce carbon emissions',
  'decarbonise heating (stop burning stuff)',
  'reduce fuel bills',
  'improve winter comfort',
  'improve indoor air quality',
  'resolve condensation, damp, and mould issues',
  'general modernisation, maintenance and repairs',
  'improve climate resilience (e.g. vs flooding)',
  'reduce summer overheating',
  'integrate energy efficiency with other building work',
  'future-proofing (e.g. for ageing)',
] as const;

type Option = (typeof options)[number];
type State = Option[];

function swapped(state: State, itemIdx: number, newItemIdx: number) {
  console.log(state, itemIdx, newItemIdx);
  if (
    newItemIdx < 0 ||
    itemIdx < 0 ||
    itemIdx >= state.length ||
    newItemIdx >= state.length
  ) {
    return state;
  }
  // SAFETY: we have checked that the array incedes are in range
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  [state[itemIdx], state[newItemIdx]] = [state[newItemIdx]!, state[itemIdx]!];
  return state;
}

export function PriorityPicker({
  state,
  onChange,
}: {
  state: State;
  onChange: (val: State) => void;
}) {
  return (
    <div>
      <p>
        What are your top 3 priorities in carrying out the work, in order of importance?
        <br />
        <i>(Click to choose; chosen priorities will appear in table below.)</i>
      </p>
      <ul className="list-unstyled mb-15">
        {options.map((option) => (
          <li key={option} className="mb-7">
            <button
              className="btn"
              disabled={state.length > 2 || state.includes(option)}
              onClick={() => {
                onChange([...state, option]);
              }}
            >
              {option}
            </button>
          </li>
        ))}
      </ul>

      <table className="table table--vertical-middle">
        <thead>
          <tr>
            <th>#</th>
            <th>Priority</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {zip(state, [1, 2, 3]).map(([option, priority], idx) => (
            <tr key={idx}>
              <td className="text-tabular-nums">{priority}</td>
              <td style={{ width: '100%' }}>
                {option === undefined ? (
                  <span className="text-muted">(not chosen)</span>
                ) : (
                  option
                )}
              </td>
              <td style={{ width: 0 }}>
                <div style={{ width: 'max-content' }}>
                  <button
                    className="btn ml-7"
                    onClick={() => onChange(swapped(state, idx, idx - 1))}
                    style={{
                      visibility:
                        option === undefined || idx === 0 || idx >= state.length
                          ? 'hidden'
                          : 'visible',
                    }}
                  >
                    <Up />
                  </button>
                  <button
                    className="btn ml-7"
                    onClick={() => onChange(swapped(state, idx, idx + 1))}
                    style={{
                      visibility:
                        option === undefined || idx === 2 || idx >= state.length - 1
                          ? 'hidden'
                          : 'visible',
                    }}
                  >
                    <Down />
                  </button>

                  <button
                    className="btn ml-15"
                    onClick={() => {
                      onChange(state.filter((stateOption) => stateOption !== option));
                    }}
                    disabled={option === undefined}
                  >
                    <X />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}
