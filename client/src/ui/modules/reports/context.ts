import { z } from 'zod';
import { resultSchema } from '../../../data-schemas/helpers/result';
import { Project } from '../../../data-schemas/project';
import { Scenario } from '../../../data-schemas/scenario';
import type { GenericMeasure } from '../../../data-schemas/scenario/measures';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { mean } from '../../../helpers/array-reducers';
import {
  additiveCostOfMeasures,
  getScenarioMeasures,
  totalCostOfMeasures,
} from '../../../measures';
import { Region } from '../../../model/enums/region';
import { ModelError } from '../../../model/error';
import { Model } from '../../../model/model';
import {
  extractHeatingHoursInputFromLegacy,
  HeatingHours,
} from '../../../model/modules/heating-hours';
import { mapWeekly } from '../../../model/modules/mean-internal-temperature/weekly';
import { VERSION } from '../../../version';
import { questionnaireAnswerChoices } from '../../questionnaire-answer-choices';
import { generateReportGraphs } from './graphs';

type DefinitelyScenario = Exclude<Scenario, undefined>;
type Household = Exclude<DefinitelyScenario['household'], undefined>;

function getAddress(hh: Household) {
  if (hh.address.type === 'no data') {
    return '';
  }

  return [
    hh.address.value.line1 ?? '',
    hh.address.value.line2 ?? '',
    hh.address.value.line3 ?? '',
    hh.address.value.postTown ?? '',
    hh.address.value.postcode ?? '',
  ]
    .filter((e) => e !== '')
    .join('\n');
}

function getAvgTemperature(household: Household) {
  const values: number[] = [
    coalesceEmptyString(household.reading_temp1, null) ?? null,
    coalesceEmptyString(household.reading_temp2, null) ?? null,
  ].filter((temp): temp is number => temp !== null);
  if (values.length === 0) {
    return undefined;
  } else {
    return mean(values);
  }
}

function getAvgHumidity(household: Household) {
  const values = [
    coalesceEmptyString(household.reading_humidity1, null) ?? null,
    coalesceEmptyString(household.reading_humidity2, null) ?? null,
  ].filter((temp): temp is number => temp !== null);
  if (values.length === 0) {
    return undefined;
  } else {
    return mean(values);
  }
}

function getCurrentGeneration(model: Model) {
  const annualGeneration = model.normal.currentEnergy.input.generation?.annualEnergy ?? 0;
  const fractionUsedOnsite =
    model.normal.currentEnergy.input.generation?.fractionUsedOnsite ?? 0;

  if (annualGeneration === 0) {
    return false;
  }

  return {
    kwh: annualGeneration,
    used_onsite: fractionUsedOnsite * 100,
  };
}

function getUsedFuelNames(scenarioData: ScenarioData[]): Set<string> {
  const fuelNames = new Set<string>();

  for (const { scenario } of scenarioData) {
    for (const fuel of Object.values(scenario.fuel_totals ?? [])) {
      if (fuel.quantity !== 0) {
        fuelNames.add(fuel.name);
      }
    }
  }

  return fuelNames;
}

function getUsedFuels(usedFuelNames: Set<string>, model: Model) {
  return [...usedFuelNames].map((name) => {
    const fuel = model.normal.fuels.fuels[name];
    if (fuel === undefined) {
      throw new Error(`Fuel ${name} doesn't exist`);
    }

    return {
      name,
      is_electricity: fuel.category === 'electricity' || fuel.category === 'generation',
      co2factor: fuel.carbonEmissionsFactor,
      standingcharge: fuel.standingCharge / 100,
      fuelcost: fuel.unitPrice / 100,
    };
  });
}

function getAnyScenarioHasBiomass(usedFuelNames: Set<string>): boolean {
  for (let name of usedFuelNames) {
    name = name.toLowerCase();
    // Dual fuel appliances can usually take logs or coal - so this is a 'maybe'.
    if (name.includes('wood') || name.includes('dual fuel')) {
      return true;
    }
  }

  return false;
}

function getAnyScenarioHasGeneration(scenarioData: ScenarioData[]): boolean {
  for (const { scenario } of scenarioData) {
    const generationTotal =
      coalesceEmptyString(scenario.fuel_totals?.['generation']?.quantity, 0) ?? 0;
    if (generationTotal !== 0) {
      return true;
    }
  }
  return false;
}

function getAnyScenarioHasNonElectricity(
  usedFuels: ReturnType<typeof getUsedFuels>,
): boolean {
  return usedFuels.some((fuel) => !fuel.is_electricity);
}

function extractNumberFromScenarioId(scenarioId: string): number {
  if (scenarioId === 'master') {
    return 0;
  } else {
    const number = scenarioId.match(/(\d+)/g);
    if (number === null) {
      throw new Error(`Could not extract number from scenario ID ${scenarioId}`);
    }
    return parseInt(number[0] ?? '', 10);
  }
}

function lookupInTable<T extends string, U>(
  table: Record<T, U>,
  key: T | undefined | null,
) {
  if (key === undefined || key === null) {
    return undefined;
  }
  return table[key];
}

function extractHeatlossData(model: Model) {
  return {
    floorwk: Math.round(model.normal.fabric.heatLossTotals.floor),
    windowswk: Math.round(model.normal.fabric.heatLossTotals.windowLike),
    wallswk: Math.round(
      model.normal.fabric.heatLossTotals.externalWall +
        model.normal.fabric.heatLossTotals.partyWall,
    ),
    roofwk: Math.round(model.normal.fabric.heatLossTotals.roof),
    ventilationwk: Math.round(model.normal.ventilation.heatLossAverage),
    infiltrationwk: Math.round(model.normal.infiltration.heatLossAverage),
    thermalbridgewk: Math.round(model.normal.fabric.thermalBridgingHeatLoss),
    totalwk: Math.round(
      model.normal.fabric.heatLossTotals.floor +
        model.normal.fabric.heatLossTotals.windowLike +
        model.normal.fabric.heatLossTotals.externalWall +
        model.normal.fabric.heatLossTotals.partyWall +
        model.normal.fabric.heatLossTotals.roof +
        model.normal.ventilation.heatLossAverage +
        model.normal.infiltration.heatLossAverage +
        model.normal.fabric.thermalBridgingHeatLoss,
    ),
  };
}

function roundTo10(val: number): number {
  return Math.round(val / 10) * 10;
}

function extractContextData(scenarioData: ScenarioData[]) {
  const data = scenarioData[0];
  if (data === undefined) {
    throw new Error('Baseline scenario does not exist');
  }
  const { scenario: baseline, model } = data;
  const hh = baseline.household;
  if (hh === undefined) {
    throw new Error('Cannot generate report: no household data to use');
  }
  const usedFuelNames = getUsedFuelNames(scenarioData);

  // We construct an ad-hoc model module for this, because the model does not usually touch household data
  const householdHeatingHours = new HeatingHours(
    extractHeatingHoursInputFromLegacy({
      space_heating: {
        heatingHours: {
          weekday: baseline?.household?.heatingHoursWeekday ?? [],
          weekend: baseline?.household?.heatingHoursWeekend ?? [],
        },
      },
    }),
    {
      modelBehaviourFlags: model.normal.modelBehaviourFlags,
    },
  );

  return {
    front: {
      name: hh.householderName,
      address: getAddress(hh),
      local_authority:
        hh.localAuthority.type === 'no data' ? null : hh.localAuthority.value,
      survey_date: hh.surveyDate,
      report_date: hh.report.date,
      report_version: hh.report.version,
      report_reference: hh.report.reference,
      assessor_name: hh.surveyorName,
      signoff_name: hh.report.signoff,
    },
    aims: {
      future: {
        occupancy_actual: hh.timeSinceMoveLegacy ?? undefined,
        occupancy_planned: hh.expectedStay,
        occupancy_notes: hh.expectedStayNotes,
        lifestyle_change: hh.lifestyleChange === true,
        lifestyle_change_notes: hh.lifestyleChangeNotes,
        other_works: hh.otherWorks === true,
        other_works_notes: hh.otherWorksNotes,
        works_start: hh.deadlinesLegacy,
        works_start_notes: hh.deadlinesNotes,
      },
      priorities:
        hh.prioritiesLegacy !== null
          ? hh.prioritiesLegacy.slice(0, 3).map((elem) => elem.title)
          : [],
      qual_criteria: hh.prioritiesNotes,
      aesthetics: {
        internal: hh.appearanceInternal === 'changing is ok',
        external: hh.appearanceOutsideLegacy === 'changing is ok',
        note: hh.appearanceNotes,
      },
      logistics: {
        packaging: hh.logisticsPhasing,
        can_diy: hh.logisticsDiy === true,
        diy_note: hh.logisticsNotes,
        disruption: hh.disruption,
        disruption_comment: hh.disruptionNotes,
        has_budget: hh.budget === 'yes',
        budget_comment: hh.budgetNotes,
        brief: hh.commentary.brief,
      },
    },
    now: {
      climate_region:
        baseline.region === undefined
          ? Region.fromIndex0(0).name
          : new Region(baseline.region).name,
      occupancy: baseline.occupancy,
      heating: {
        hours_on_weekday: householdHeatingHours.weekday.totalHoursOn,
        hours_on_weekend: householdHeatingHours.weekend.totalHoursOn,
      },
      home_type: hh.propertyType,
      num_bedrooms: hh.numBedrooms,
      floor_area: baseline.TFA,
      construction: {
        floors: hh.construct_note_floors,
        walls: hh.construct_note_walls,
        roof: hh.construct_note_roof,
        openings: hh.construct_note_openings,
        drainage: hh.construct_note_drainage,
        ventiliation: hh.construct_note_ventiliation,
        ingress: hh.construct_note_ingress,
      },
      previous_works: hh.priorWork,
      structural: {
        you_said: hh.structuralIssuesNotes,
        we_noted: hh.structuralIssuesNotesLegacy,
      },
      damp: {
        you_said: hh.dampCondensationMould,
        we_noted: hh.dampCondensationMouldLegacy,
      },
      space_heating: hh.heating,
      space_heating_controls: hh.heatingControls,
      hot_water: hh.hotWater,
      utility: {
        electricity: hh.hasMainsElectricity,
        gas: hh.hasMainsGas,
        water: hh.hasMainsWater,
        sewer: hh.hasMainsSewer,
      },
      ventilation: {
        system_name: baseline.ventilation?.ventilation_name,
        ventilation_suggestion: lookupInTable(
          questionnaireAnswerChoices.VENTILATION_SUGGESTION,
          hh.ventilation_suggestion,
        ),
        avg_humidity: getAvgHumidity(hh),
        adequate_paths: hh.ventilation_adequate_paths === 'YES',
        purge_vents: hh.ventilation_purge_vents === 'YES',
        gaps: hh.ventilation_gaps === 'YES',
        note: hh.ventilation_note,
        fuel_burner: hh.fuel_burner,
        fuel_burner_note: hh.fuel_burner_note,
      },
      laundry_habits: hh.laundryNotes,
      radon_chance: lookupInTable(questionnaireAnswerChoices.RADON_RISK, hh.radonRisk),
      experience: {
        thermal: {
          temperature_winter: ((level) => {
            switch (level) {
              case 'just right':
                return 'MID';
              case 'too hot':
                return 'HIGH';
              case 'too cold':
                return 'LOW';
              case "don't know":
                return 'DK';
              default:
                return level;
            }
          })(hh.temperatureComfortWinter),
          temperature_summer: ((level) => {
            switch (level) {
              case 'just right':
                return 'MID';
              case 'too hot':
                return 'HIGH';
              case 'too cold':
                return 'LOW';
              case "don't know":
                return 'DK';
              default:
                return level;
            }
          })(hh.temperatureComfortSummer),
          airquality_winter: ((level) => {
            switch (level) {
              case 'just right':
                return 'MID';
              case 'too stuffy':
                return 'HIGH';
              case 'too dry':
                return 'LOW';
              case "don't know":
                return 'DK';
              default:
                return level;
            }
          })(hh.humidityComfortWinter),
          airquality_summer: ((level) => {
            switch (level) {
              case 'just right':
                return 'MID';
              case 'too stuffy':
                return 'HIGH';
              case 'too dry':
                return 'LOW';
              case "don't know":
                return 'DK';
              default:
                return level;
            }
          })(hh.humidityComfortSummer),
          draughts_winter: ((level) => {
            switch (level) {
              case 'just right':
                return 'MID';
              case 'too still':
                return 'HIGH';
              case 'too draughty':
                return 'LOW';
              case "don't know":
                return 'DK';
              default:
                return level;
            }
          })(hh.draughtsComfortWinter),
          draughts_summer: ((level) => {
            switch (level) {
              case 'just right':
                return 'MID';
              case 'too still':
                return 'HIGH';
              case 'too draughty':
                return 'LOW';
              case "don't know":
                return 'DK';
              default:
                return level;
            }
          })(hh.draughtsComfortSummer),
          avg_temp: getAvgTemperature(hh),
          problems: hh.thermalComfortProblemLocations,
          note: hh.thermalComfortProblemLocationsNotes,
        },
        daylight: {
          amount: ((level) => {
            switch (level) {
              case 'just right':
                return 'MID';
              case 'too much':
                return 'HIGH';
              case 'too little':
                return 'LOW';
              case 'varies':
                return 'VAR';
              case null:
                return level;
            }
          })(hh.daylightLevel),
          problems: hh.daylightProblemLocations === true,
          note: hh.daylightProblemLocationsNotes,
        },
        noise: {
          problems: hh.noiseProblems === true,
          note: hh.noiseProblemsNotes,
        },
        rooms: {
          favourite: hh.roomsFavourite,
          unloved: hh.roomsUnloved,
        },
      },
      historic: {
        when_built:
          hh.builtExact !== ''
            ? hh.builtExact
            : lookupInTable(questionnaireAnswerChoices.HISTORIC_AGE_BAND, hh.builtBand),
        conservation: hh.inConservationArea === true,
        listed:
          hh.listedStatus === 'NO'
            ? false
            : lookupInTable(questionnaireAnswerChoices.HISTORIC_LISTED, hh.listedStatus),
        comments: hh.listedStatusNotes,
      },
      theworldisburning: {
        flooding_history: hh.floodingHistory === true,
        flooding_rivers_sea: lookupInTable(
          questionnaireAnswerChoices.FLOODING_RISK,
          hh.floodingRiversAndSea,
        ),
        flooding_surface_water: lookupInTable(
          questionnaireAnswerChoices.FLOODING_RISK,
          hh.floodingSurfaceWater,
        ),
        flooding_comments: hh.floodingHistoryNote,
        overheating_comments: hh.overheatingNotesLegacy,
      },
      context_and_other_points: hh.generalContextNotes,
      generation: getCurrentGeneration(model),
    },
    used_fuels: getUsedFuels(usedFuelNames, model),
    any_scenario_has_generation: getAnyScenarioHasGeneration(scenarioData),
    any_scenario_has_biomass: getAnyScenarioHasBiomass(usedFuelNames),
    any_scenario_has_non_electricity: getAnyScenarioHasNonElectricity(
      getUsedFuels(usedFuelNames, model),
    ),
    commentary: {
      brief: hh.commentary.brief,
      context: hh.commentary.context,
      decisions: hh.commentary.decisions,
    },
    scenarios: {
      list: scenarioData
        .filter(({ id }) => id !== 'master')
        .map(({ id, num, scenario }) => ({
          id,
          num,
          name: scenario.scenario_name,
          description: scenario.scenario_description,
        })),
    },
    // new - serverside generation only
    // rename to 'scenarios' when deployed
    scenario_data: scenarioData.map(
      ({ id, num, createdFromNum, scenario, measures, model }) => {
        return {
          id,
          num,
          name: scenario.scenario_name,
          description: scenario.scenario_description,
          created_from: { num: createdFromNum },
          measures,
          measures_cost: roundTo10(totalCostOfMeasures(measures)),
          measures_additive_cost: roundTo10(additiveCostOfMeasures(id, scenarioData)),
          heating_load: {
            temp: model.normal.heatLoss.internalDesignTemperature ?? 0,
            temp_low: model.normal.geography.externalDesignTemperature ?? 0,
            peak_heat: (model.normal.heatLoss.peakHeatLoad ?? 0) / 1000,
            peak_heat_m2: model.normal.heatLoss.peakHeatLoadPerArea ?? 0,
          },
          target_temp: scenario.temperature?.target ?? 0,
          co2: {
            total: scenario.annualco2,
            per_person: (scenario.annualco2 ?? 0) / model.normal.occupancy.occupancy,
          },
          heatloss: extractHeatlossData(model),
          heating_hours_on: mapWeekly(
            model.normal.heatingHours,
            (heatingPattern) => heatingPattern.totalHoursOn,
          ),
        };
      },
    ),
  };
}

type ScenarioData = {
  id: string;
  scenario: DefinitelyScenario;
  model: Model;
  num: number;
  createdFromNum: number;
  createdFromName: string | null;
  measures: GenericMeasure[];
};

function extractModel(value: unknown): Model | null {
  try {
    return (
      resultSchema(
        z.instanceof(Model),
        z.union([z.instanceof(ModelError), z.instanceof(z.ZodError)]),
      )
        .optional()
        .parse(value)
        ?.mapErr(() => null)
        ?.coalesce() ?? null
    );
  } catch (_err) {
    return null;
  }
}

function extractScenarioData(project: Project, scenarioIds: string[]): ScenarioData[] {
  return scenarioIds.map((id) => {
    const scenario = project.data[id];
    if (scenario === undefined) {
      throw new Error(`couldn't access data for ${id}`);
    }
    const model = extractModel(scenario.model);
    if (model === null) {
      throw new Error(`no model for ${id}`);
    }
    const num = extractNumberFromScenarioId(id);
    const createdFromName = scenario.created_from ?? null;
    const createdFromNum =
      num === 0 ? -1 : extractNumberFromScenarioId(createdFromName ?? '');
    const measures = getScenarioMeasures(scenario);

    return {
      id,
      num,
      scenario,
      model,
      createdFromName,
      createdFromNum,
      measures,
    };
  });
}

export function generatePayload(
  project: Project,
  scenarioIds: string[],
  options: { isSample: boolean },
) {
  const scenarioData = extractScenarioData(project, scenarioIds);
  const context = extractContextData(scenarioData);
  const graphs = generateReportGraphs(scenarioData);
  return {
    context: {
      options: { is_sample: options.isSample },
      ...context,
    },
    graphs,
    reproducibility_data: { codebase_version: VERSION, project },
  };
}
