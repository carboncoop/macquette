import React, { useRef, useState } from 'react';

import { z } from 'zod';
import { HTTPClient } from '../../../api/http';
import { Project, projectSchema } from '../../../data-schemas/project';
import { assertNever } from '../../../helpers/assert-never';
import { Result } from '../../../helpers/result';
import { CheckboxInput } from '../../input-components/checkbox';
import { CheckboxGroup } from '../../input-components/checkbox-group';
import { TextInput } from '../../input-components/text';
import type { UiModule } from '../../module-management/module-type';
import { Spinner } from '../../output-components/spinner';
import { generatePayload } from './context';

type State = {
  project: Project | null;
  scenarios: { name: string; id: string }[];
  reportDate: string;
  reportVersion: string;
  reportReference: string;
  signedOffBy: string;
};
type Action =
  | { type: 'external update'; state: State }
  | {
      type: 'ui update';
      newState: Partial<
        Pick<State, 'reportDate' | 'reportVersion' | 'signedOffBy' | 'reportReference'>
      >;
    };

export const reportsModule: UiModule<State, Action, never> = {
  name: 'sandbox',
  component: function Reports({ state, dispatch }) {
    const [fetching, setFetching] = useState(false);
    const [showingPreview, setShowingPreview] = useState(false);
    const [selectedScenarios, setSelectedScenarios] = useState<string[]>(
      state.scenarios.map((scenario) => scenario.id),
    );
    const [isSample, setIsSample] = useState(false);
    const iframeRef = useRef<null | HTMLIFrameElement>(null);

    function generatePreview() {
      const api = new HTTPClient();

      if (state.project === null) {
        alert("Can't generate report");
        console.error('Project is null');
        return;
      }

      setFetching(true);
      api
        .generateReportPreview(
          state.project.id,
          generatePayload(state.project, selectedScenarios, { isSample }),
        )
        .then((html) => {
          setShowingPreview(true);
          const iframe = iframeRef.current?.contentDocument?.querySelector('body');
          if (iframe !== null && iframe !== undefined) {
            iframe.innerHTML = html;
          } else {
            console.error('iframe not visible');
          }
        })
        .catch(console.error)
        .finally(() => setFetching(false));
    }

    function generatePDF() {
      const api = new HTTPClient();

      if (state.project === null) {
        alert("Can't generate report");
        console.error('Project is null');
        return;
      }

      setFetching(true);
      api
        .generateReport(
          state.project.id,
          generatePayload(state.project, selectedScenarios, { isSample }),
        )
        .then((pdf) => {
          const downloadLink = document.createElement('a');
          downloadLink.href = window.URL.createObjectURL(pdf);
          downloadLink.download = `report_${state.project?.name ?? ''}.pdf`;
          downloadLink.click();
        })
        .catch(console.error)
        .finally(() => setFetching(false));
    }

    return (
      <section>
        <label htmlFor="reportDate">Report date:</label>
        <div>
          <TextInput
            value={state.reportDate}
            onChange={(reportDate) =>
              dispatch({ type: 'ui update', newState: { reportDate } })
            }
            placeholder="dd/mm/yyyy"
            id="reportDate"
          />
        </div>

        <label htmlFor="reportVersion">Report version:</label>
        <div>
          <TextInput
            value={state.reportVersion}
            onChange={(reportVersion) =>
              dispatch({ type: 'ui update', newState: { reportVersion } })
            }
            id="reportVersion"
          />
        </div>

        <label htmlFor="reportReference">Reference:</label>
        <div>
          <TextInput
            value={state.reportReference}
            onChange={(reportReference) =>
              dispatch({ type: 'ui update', newState: { reportReference } })
            }
            placeholder="dd/mm/yyyy"
            id="reportReference"
          />
        </div>

        <label htmlFor="reportSignoff">Signed off by:</label>
        <div>
          <TextInput
            value={state.signedOffBy}
            onChange={(signedOffBy) =>
              dispatch({ type: 'ui update', newState: { signedOffBy } })
            }
            id="reportSignoff"
          />
        </div>

        <div className="mb-15">
          <p>Show these scenarios in the report:</p>

          <CheckboxGroup
            options={state.scenarios.map((scenario) => ({
              value: scenario.id,
              display: scenario.name,
            }))}
            value={selectedScenarios}
            onChange={(scenarios) => setSelectedScenarios(scenarios)}
          />
        </div>

        <details className="mb-15">
          <summary>Show advanced options</summary>

          <div className="mb-15">
            <label htmlFor="sample">
              <CheckboxInput
                id="sample"
                className="mr-7 big-checkbox"
                value={isSample}
                onChange={(val) => setIsSample(val)}
              />
              Generate with &apos;sample&apos; background
            </label>
          </div>
        </details>

        <div className="mb-30 d-flex gap-7">
          <button className="btn" disabled={fetching} onClick={generatePreview}>
            Preview
          </button>
          <button className="btn" disabled={fetching} onClick={generatePDF}>
            Get PDF
          </button>
          <Spinner visibility={fetching} />
        </div>

        <div style={{ display: showingPreview ? 'block' : 'none' }}>
          <span className="small-caps">Report preview</span>
          <div className="alert mb-0">
            This is useful to check text, but some aspects will not appear correctly when
            compared to the generated PDF.
          </div>
          <iframe
            title="Report preview"
            style={{ width: '100%', height: '90vh', border: '2px solid black' }}
            ref={iframeRef}
          ></iframe>
        </div>
      </section>
    );
  },
  initialState: (): State => {
    return {
      project: null,
      scenarios: [],
      reportDate: '',
      reportVersion: '',
      reportReference: '',
      signedOffBy: '',
    };
  },
  reducer: (state, action) => {
    switch (action.type) {
      case 'external update':
        return [action.state];

      case 'ui update': {
        return [{ ...state, ...action.newState }];
      }
    }
  },
  effector: assertNever,
  shims: {
    extractUpdateAction: ({ project }) => {
      const scenarios = Object.entries(project.data).map(([id, data]) => ({
        id,
        name: data?.scenario_name ?? '',
      }));
      const baseline = project.data['master'];
      const reportDate = baseline?.household?.report.date ?? '';
      const reportVersion = baseline?.household?.report.version ?? '';
      const reportReference = baseline?.household?.report.reference ?? '';
      const signedOffBy = baseline?.household?.report.signoff ?? '';
      return Result.ok([
        {
          type: 'external update',
          state: {
            project,
            scenarios,
            reportDate,
            reportVersion,
            reportReference,
            signedOffBy,
          },
        },
      ]);
    },
    mutateLegacyData: ({ project: projectRaw }, _, state) => {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const project = projectRaw as z.input<typeof projectSchema>;

      const baseline = project.data['master'];
      if (baseline === undefined) {
        console.error('Could not mutate legacy data as master scenario not found');
        return;
      }

      const update = {
        report_date: state.reportDate,
        report_version: state.reportVersion,
        report_signoff: state.signedOffBy,
        report_reference: state.reportReference,
      };

      if (baseline.household === undefined) {
        baseline.household = {};
      }
      Object.assign(baseline.household, update);
    },
  },
};
