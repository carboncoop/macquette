import { mapValues, range } from 'lodash';

import { Scenario } from '../../../data-schemas/scenario';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { sum } from '../../../helpers/array-reducers';
import { Tree, TreeNode } from '../../../helpers/tree';
import { embodiedCarbonFromMeasures } from '../../../measures';
import { Model } from '../../../model/model';
import { Fuel } from '../../../model/modules/fuels';

// @note These types are coupled to server-side types in Python, and no static
// analysis or automated tests check that they line up. If you change them you
// must manually test that graph generation still works.
type Line = {
  value: number;
  label: string;
};

type ShadedArea = {
  interval: [number, number];
  label?: string;
};

export type BarChart = {
  type: 'bar';
  stacked?: boolean;
  units: string;
  numCategories: number;
  bins: { label: string; data: number[] }[];
  categoryLabels?: string[];
  categoryColours?: string[];
  lines?: Line[];
  areas?: ShadedArea[];
};

export type LineGraph = {
  type: 'line';
  xAxis: { units: string };
  yAxis: { units: string };
  rows: {
    label: string;
    data: number[][];
  }[];
};

function scenarioName(scenarioId: string, idx: number) {
  return scenarioId === 'master' ? 'Baseline' : `Scenario ${idx}`;
}

function heatBalance(scenarios: ScenarioData[]): BarChart {
  return {
    type: 'bar',
    stacked: true,
    units: 'kWh per m² per year',
    numCategories: 5,
    bins: scenarios.map(({ model, id }, idx) => ({
      label: scenarioName(id, idx),
      data: [
        model.normal.spaceHeating.usefulSolarHeatGainEnergyPerArea,
        model.normal.spaceHeating.usefulInternalHeatGainEnergyPerArea,
        model.normal.spaceHeating.spaceHeatingDemandAnnualEnergyPerArea,
        -model.normal.spaceHeating.heatLossAnnualEnergyPerArea.fabric,
        -(
          model.normal.spaceHeating.heatLossAnnualEnergyPerArea.infiltration +
          model.normal.spaceHeating.heatLossAnnualEnergyPerArea.ventilation
        ),
      ],
    })),
    categoryLabels: [
      'Solar gains',
      'Internal gains',
      'Space heating requirement',
      'Fabric losses',
      'Ventilation and infiltration losses',
    ],
    categoryColours: ['#e89d25', '#f5bd76', '#4286f4', '#68ab20', '#b5d490'],
  };
}

function spaceHeatingDemand(scenarios: ScenarioData[]): BarChart {
  return {
    type: 'bar',
    units: 'kWh per m² per year',
    numCategories: 2,
    bins: scenarios.map(({ model, id }, idx) => ({
      label: scenarioName(id, idx),
      data: [
        model.standardisedHeating.spaceHeating.spaceHeatingDemandAnnualEnergyPerArea,
        model.normal.spaceHeating.spaceHeatingDemandAnnualEnergyPerArea,
      ],
    })),
    categoryLabels: [
      'With standardised heating pattern and temperature',
      'With your heating pattern and temperature',
    ],
    categoryColours: ['#9cbdfc', '#4286f4'],
    lines: [{ value: 120, label: 'UK average' }],
    areas: [{ label: 'Target', interval: [20, 70] }],
  };
}

function peakHeatingLoad(scenarios: ScenarioData[]): BarChart {
  return {
    type: 'bar',
    units: 'kW',
    numCategories: 1,
    bins: scenarios.map(({ model, id }, idx) => ({
      label: scenarioName(id, idx),
      data: [(model.normal.heatLoss.peakHeatLoad ?? 0) / 1000],
    })),
    areas: [
      { interval: [3, 6], label: 'Small' },
      { interval: [6, 10], label: 'Medium' },
      { interval: [10, 15], label: 'Large' },
      { interval: [15, 21], label: 'Very large' },
    ],
  };
}

function fuelUse(scenarios: ScenarioData[]): BarChart {
  const baselineModel = scenarios[0]?.model;

  let billsData = [0, 0, 0, 0];
  if (baselineModel !== undefined) {
    const summedByCategory: Record<Fuel['category'], number> = {
      electricity: 0,
      generation: 0,
      gas: 0,
      oil: 0,
      'solid fuel': 0,
    };
    for (const currentFuel of Object.values(baselineModel.normal.currentEnergy.fuels)) {
      summedByCategory[currentFuel.fuel.category] += currentFuel.annualUse;
    }

    billsData = [
      // Add consumption from generation as electricity
      summedByCategory['electricity'] +
        (baselineModel.normal.currentEnergy.generation?.annualEnergyOnSite ?? 0),
      summedByCategory['gas'],
      summedByCategory['oil'],
      summedByCategory['solid fuel'],
    ];
  }

  return {
    type: 'bar',
    stacked: true,
    units: 'kWh per year',
    numCategories: 4,
    bins: [
      {
        label: 'Bills data',
        data: billsData,
      },
      ...scenarios.map(({ model, id }, idx) => ({
        label: scenarioName(id, idx),
        data: [
          model.normal.fuelRequirements.fuelInputByFuelCategoryAnnual['electricity'],
          model.normal.fuelRequirements.fuelInputByFuelCategoryAnnual['gas'],
          model.normal.fuelRequirements.fuelInputByFuelCategoryAnnual['oil'],
          model.normal.fuelRequirements.fuelInputByFuelCategoryAnnual['solid fuel'],
        ],
      })),
    ],
    categoryLabels: ['Electricity', 'Gas', 'Oil', 'Solid fuels'],
  };
}

export function energyUseIntensity(scenarios: ScenarioData[]): BarChart {
  const baselineModel = scenarios[0]?.model;

  const generation =
    (baselineModel?.normal.currentEnergy.generation?.annualEnergyOnSite ?? 0) /
    (baselineModel?.normal.floors.totalFloorArea ?? 1);
  const billsConsumption =
    (baselineModel?.normal.currentEnergy.annualEnergyFromFuels ?? 0) /
    (baselineModel?.normal.floors.totalFloorArea ?? 1);

  return {
    type: 'bar',
    stacked: true,
    units: 'kWh per m² per year',
    numCategories: 8,
    bins: [
      {
        label: 'Bills data',
        data: [0, 0, 0, 0, 0, 0, billsConsumption, generation],
      },
      ...scenarios.flatMap(({ model, id }, idx) => ({
        label: scenarioName(id, idx),
        data: [
          model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual.waterHeating,
          model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual.spaceHeating,
          model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual.cooking +
            model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual.cookingSap,
          model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual.appliances +
            model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual.appliancesSap,
          model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual.lighting,
          model.normal.fuelRequirements.fuelInputByModulePerAreaAnnual
            .fansPumpsElectricKeepHot,
          0,
          0,
        ],
      })),
    ],
    categoryLabels: [
      'Water Heating',
      'Space Heating',
      'Cooking',
      'Appliances',
      'Lighting',
      'Fans and Pumps',
      'Bills data',
      'Assumed consumption',
    ],
    areas: [
      { interval: [0, 35], label: 'New build' },
      { interval: [35, 60], label: 'Retrofit' },
    ],
  };
}

function operationalAnnualCarbonByUse(scenarios: ScenarioData[]): BarChart {
  const baselineModel = scenarios[0]?.model;

  const billsConsumption = baselineModel?.normal.currentEnergy.annualCarbonEmissions ?? 0;

  return {
    type: 'bar',
    stacked: true,
    units: 'kgCO₂e/year',
    numCategories: 7,
    bins: [
      {
        label: 'Bills data',
        data: [0, 0, 0, 0, 0, 0, billsConsumption],
      },
      ...scenarios.flatMap(({ model, id }, idx) => ({
        label: scenarioName(id, idx),
        data: [
          model.normal.fuelRequirements.carbonEmissionsByModuleAnnual.waterHeating,
          model.normal.fuelRequirements.carbonEmissionsByModuleAnnual.spaceHeating,
          model.normal.fuelRequirements.carbonEmissionsByModuleAnnual.cooking,
          model.normal.fuelRequirements.carbonEmissionsByModuleAnnual.appliances,
          model.normal.fuelRequirements.carbonEmissionsByModuleAnnual.lighting,
          model.normal.fuelRequirements.carbonEmissionsByModuleAnnual
            .fansPumpsElectricKeepHot,
          0,
        ],
      })),
    ],
    categoryLabels: [
      'Water Heating',
      'Space Heating',
      'Cooking',
      'Appliances',
      'Lighting',
      'Fans and Pumps',
      'Bills data',
    ],
  };
}

function embodiedCarbon(scenarios: ScenarioData[]): BarChart {
  return {
    type: 'bar',
    stacked: true,
    units: 'kgCO₂e',
    numCategories: 4,
    bins: [
      ...scenarios.slice(1).flatMap(({ scenario, id }, idx) => {
        const data = embodiedCarbonFromMeasures(scenario);
        return [
          {
            label: `${scenarioName(id, idx + 1)} upfront`,
            data: [
              0,
              0,
              data.lowerUpfrontCarbon,
              data.upperUpfrontCarbon - data.lowerUpfrontCarbon,
            ],
          },
          {
            label: `${scenarioName(id, idx + 1)} biogenic`,
            data: [
              data.lowerBiogenicCarbon,
              data.upperBiogenicCarbon - data.lowerBiogenicCarbon,
              0,
              0,
            ],
          },
        ];
      }),
    ],
    categoryLabels: [
      'Biogenic/stored carbon (base)',
      'Biogenic/stored carbon (high)',
      'Upfront carbon (base)',
      'Upfront carbon (high)',
    ],
    categoryColours: [
      selectColourPair(1)[0],
      selectColourPair(1)[1],
      selectColourPair(0)[0],
      selectColourPair(0)[1],
    ],
  };
}

const yearsToShow = 20;

function cumulativeCo2(scenarios: ScenarioData[]): LineGraph {
  const embodiedCarbonNodes = scenarios.map(({ id, scenario }) => {
    const { isComplete, upperUpfrontCarbon, lowerUpfrontCarbon } =
      embodiedCarbonFromMeasures(scenario);

    return new TreeNode(id, {
      createdFrom: scenario.created_from ?? null,
      isComplete,
      upperUpfrontCarbon,
      lowerUpfrontCarbon,
    });
  });
  const completeEmbodiedCarbon = embodiedCarbonNodes.every(
    (node) => node.data.isComplete,
  );
  const embodiedCarbonTree = new Tree(embodiedCarbonNodes);
  for (const node of embodiedCarbonTree.nodes) {
    if (node.data.createdFrom !== null) {
      embodiedCarbonTree.link(node.name, node.data.createdFrom);
    }
  }

  return {
    type: 'line',
    xAxis: { units: 'year' },
    yAxis: { units: 'kgCO₂e' },
    rows: scenarios.map(({ id, model }, idx) => {
      const startYear = new Date().getFullYear() + 1;
      const operationalCarbon = model.normal.carbon.cumulativeEmissionsByYear(
        startYear,
        startYear + yearsToShow,
      );

      const embodiedNodes = embodiedCarbonTree.lineage(id);
      const upperCarbon = sum(embodiedNodes.map((node) => node.data.upperUpfrontCarbon));
      const lowerCarbon = sum(embodiedNodes.map((node) => node.data.lowerUpfrontCarbon));
      const embodiedCarbonAvg = completeEmbodiedCarbon
        ? (lowerCarbon + upperCarbon) / 2
        : 0;
      const embodiedCarbonVariation = completeEmbodiedCarbon
        ? embodiedCarbonAvg - lowerCarbon
        : 0;

      return {
        label: scenarioName(id, idx),

        // The x axis starts at the next full year. i.e. 1 Jan 2025. The y value for this
        // entry is 0. The value for the beginning of the year after that, 2026, is the
        // total carbon emitted in the previous year.
        data: [
          [startYear, embodiedCarbonAvg, embodiedCarbonVariation],
          ...range(1, yearsToShow + 1).map((yearOffset) => [
            // SAFETY: we use one source of truth, yearsToShow, to make sure that our array
            // lengths line up.
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            operationalCarbon[yearOffset]!.year,
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            operationalCarbon[yearOffset - 1]!.cumulativeEmissions + embodiedCarbonAvg,
            embodiedCarbonVariation,
          ]),
        ],
      };
    }),
  };
}

// Dark, light pairs
const COLOUR_PAIRS: [string, string][] = [
  ['#4286f4', '#9cbdfc'],
  ['#f6a607', '#fce780'],
  ['#18563e', '#408567'],
  ['#66469e', '#9371bd'],
  ['#83332f', '#ec674f'],
];

function selectColourPair(idx: number): [string, string] {
  const pair = COLOUR_PAIRS[idx % COLOUR_PAIRS.length];
  if (pair !== undefined) {
    return pair;
  } else {
    throw Error('unreachable because of modulo');
  }
}

export type EnergyCostsGraphInput = {
  modelModules: {
    currentEnergy: {
      generation: { annualCostSaved: number } | null;
      fuels: Record<string, { annualUse: number }>;
    };
    fuels: {
      fuels: Record<string, { standingCharge: number; unitPrice: number }>;
    };
  };

  // Array rather than Record because order matters
  scenarios: Array<
    [
      string,
      {
        generationCostSaving: number;
        annualEnergyUseByFuel: Record<string, number>;
      },
    ]
  >;
};

function energyCostsGraphInput(scenariosIn: ScenarioData[]): EnergyCostsGraphInput {
  const baselineModel = scenariosIn[0]?.model ?? {
    normal: {
      fuels: { fuels: {} },
      currentEnergy: { generation: null, fuels: {} },
    },
  };
  const scenarios = scenariosIn.map(
    ({ scenario, id }): EnergyCostsGraphInput['scenarios'][number] => {
      const { fuel_totals } = scenario;
      const generationCostSaving = -(
        coalesceEmptyString(fuel_totals?.['generation']?.annualcost, 0) ?? 0
      );
      const energyUseByFuel = mapValues(
        fuel_totals ?? {},
        ({ quantity }) => quantity ?? 0,
      );
      return [id, { generationCostSaving, annualEnergyUseByFuel: energyUseByFuel }];
    },
  );
  return {
    modelModules: baselineModel.normal,
    scenarios,
  };
}

export function energyCosts({
  modelModules: { fuels, currentEnergy },
  scenarios,
}: EnergyCostsGraphInput): BarChart {
  const fuelNamesFromCurrentEnergy = Object.entries(currentEnergy.fuels ?? {})
    .filter(([, fuel]) => fuel.annualUse > 0)
    .map(([fuelName]) => fuelName);
  const fuelNamesFromScenarios = scenarios.flatMap(([, scenario]) =>
    Object.entries(scenario.annualEnergyUseByFuel)
      .filter(([, annualUse]) => annualUse !== 0)
      .map(([fuelName]) => fuelName),
  );
  const usedFuelNames = new Set(
    [...fuelNamesFromCurrentEnergy, ...fuelNamesFromScenarios].filter(
      // We handle generation specially later on
      (name) => name !== 'generation',
    ),
  );
  type FuelCostEntry = { name: string; colour: string } & (
    | {
        source: 'UNIT_COST';
        unitCost: number;
      }
    | {
        source: 'STANDING_CHARGE';
        standingCharge: number;
      }
  );

  const costEntries: FuelCostEntry[] = [...usedFuelNames].flatMap((name, idx) => {
    const standingCharge = fuels.fuels[name]?.standingCharge ?? 0;
    const unitCost = fuels.fuels[name]?.unitPrice ?? 0;
    const colours = selectColourPair(idx);
    if (standingCharge > 0) {
      return [
        { name, source: 'STANDING_CHARGE', standingCharge, colour: colours[0] },
        { name, source: 'UNIT_COST', unitCost, colour: colours[1] },
      ];
    } else {
      return [{ name, source: 'UNIT_COST', unitCost, colour: colours[0] }];
    }
  });

  const actualGenerationSavings = currentEnergy?.generation?.annualCostSaved ?? 0;
  const graph: BarChart = {
    type: 'bar',
    stacked: true,
    units: '£ per year',
    numCategories: costEntries.length + 1,
    bins: [
      {
        label: 'Bills data',
        data: [
          ...costEntries.map((costEntry) => {
            const amountUsed = currentEnergy?.fuels[costEntry.name]?.annualUse ?? 0;
            if (amountUsed === 0) {
              return 0;
            }

            if (costEntry.source === 'UNIT_COST') {
              return (amountUsed * costEntry.unitCost) / 100;
            } else {
              return costEntry.standingCharge;
            }
          }),
          actualGenerationSavings,
        ],
      },
      ...scenarios.map(([scenarioId, scenarioGraphData], idx) => ({
        label: scenarioName(scenarioId, idx),
        data: [
          ...costEntries.map((costEntry) => {
            const amountUsed =
              scenarioGraphData.annualEnergyUseByFuel[costEntry.name] ?? 0;
            if (amountUsed === 0) {
              return 0;
            }

            if (costEntry.source === 'UNIT_COST') {
              let totalCost = (amountUsed * costEntry.unitCost) / 100;
              if (costEntry.name === 'Standard Tariff') {
                totalCost -= scenarioGraphData.generationCostSaving;
                if (totalCost < 0) {
                  totalCost = 0;
                }
              }
              return totalCost;
            } else {
              return costEntry.standingCharge;
            }
          }),
          scenarioGraphData.generationCostSaving,
        ],
      })),
    ],
    categoryLabels: [
      ...costEntries.map(({ name, source }) => {
        if (name === 'Standard Tariff') {
          name = 'Electricity';
        }
        return `${name} ${source === 'UNIT_COST' ? 'use' : 'standing charge'}`;
      }),
      'Assumed saving from generation',
    ],
    categoryColours: [...costEntries.map(({ colour }) => colour), '#bbbbbb'],
  };
  return removeEmptyCategories(removeEmptyBins(removeNaNs('energyCosts', graph)));
}

// Remove NaNs to work around the model sometimes outputting NaNs.
// NaNs get serialized to null, which trips up the server-side type checker - it's
// expecting only numbers.
// Instead of wrapping every call in the above with NaN checks, just do it in one go,
// it results in cleaner code.
function removeNaNs(name: string, chart: BarChart): BarChart {
  const outBins = chart.bins.map((bin) => ({
    ...bin,
    data: bin.data.map((val) => {
      if (Number.isNaN(val)) {
        return 0;
      } else {
        return val;
      }
    }),
  }));
  return {
    ...chart,
    bins: outBins,
  };
}

function removeEmptyCategories(chart: BarChart): BarChart {
  const emptyCategoryIndexes = new Set<number>();
  for (let categoryIdx = 0; categoryIdx < chart.numCategories; categoryIdx++) {
    const categoryValues = chart.bins.map((bin) => bin.data[categoryIdx]);
    if (categoryValues.every((value) => value === 0)) {
      emptyCategoryIndexes.add(categoryIdx);
    }
  }
  function removeArrayIndices<T>(array: T[], indicesToRemove: Set<number>): T[] {
    return array.flatMap((val, index) => (indicesToRemove.has(index) ? [] : [val]));
  }
  return {
    ...chart,
    numCategories: chart.numCategories - emptyCategoryIndexes.size,
    bins: chart.bins.map((bin) => ({
      ...bin,
      data: removeArrayIndices(bin.data, emptyCategoryIndexes),
    })),
    ...(chart.categoryColours === undefined
      ? {}
      : {
          categoryColours: removeArrayIndices(
            chart.categoryColours,
            emptyCategoryIndexes,
          ),
        }),
    ...(chart.categoryLabels === undefined
      ? {}
      : {
          categoryLabels: removeArrayIndices(chart.categoryLabels, emptyCategoryIndexes),
        }),
  };
}

function removeEmptyBins(chart: BarChart): BarChart {
  return {
    ...chart,
    bins: chart.bins.flatMap((bin) => {
      if (bin.data.every((value) => value === 0)) {
        return [];
      } else {
        return [bin];
      }
    }),
  };
}

type ScenarioData = {
  scenario: Exclude<Scenario, undefined>;
  model: Model;
  id: string;
};

export function generateReportGraphs(
  scenarios: ScenarioData[],
): Record<string, BarChart | LineGraph> {
  const bars = mapValues(
    {
      heatBalance: heatBalance(scenarios),
      spaceHeatingDemand: spaceHeatingDemand(scenarios),
      peakHeatingLoad: peakHeatingLoad(scenarios),
      fuelUse: fuelUse(scenarios),
      energyUseIntensity: energyUseIntensity(scenarios),
      operationalAnnualCarbonByUse: operationalAnnualCarbonByUse(scenarios),
      energyCosts: energyCosts(energyCostsGraphInput(scenarios)),
    },
    (graph, name) => removeEmptyCategories(removeEmptyBins(removeNaNs(name, graph))),
  );
  return {
    ...bars,
    cumulativeCo2: cumulativeCo2(scenarios),
    embodiedCarbon: embodiedCarbon(scenarios),
  };
}
