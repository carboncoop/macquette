import { z } from 'zod';

import type { projectSchema } from '../../../data-schemas/project';
import { Tree, TreeNode } from '../../../helpers/tree';

type InputScenario = z.input<typeof projectSchema>['data'][string];

export function deleteScenario(
  scenarios: z.input<typeof projectSchema>['data'],
  scenarioId: string,
): z.input<typeof projectSchema>['data'] {
  const nodes = Object.entries(scenarios).map(([name, data]) => new TreeNode(name, data));
  const tree = new Tree<InputScenario>(nodes);

  for (const [name, data] of Object.entries(scenarios)) {
    if (typeof data?.created_from === 'string') {
      tree.link(name, data.created_from);
    }
  }

  tree.removeNodeAndPromoteChildren(scenarioId);

  for (const [idx, node] of tree.nodes.entries()) {
    if (idx === 0) {
      node.name = 'master';
    } else {
      node.name = `scenario${idx}`;
    }
  }

  for (const node of tree.nodes) {
    const parent = tree.findParentNode(node);
    if (node.data !== undefined && parent !== null) {
      node.data.created_from = parent.name;
    }
  }

  return tree.linearise();
}
