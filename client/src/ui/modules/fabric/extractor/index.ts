import { Project } from '../../../../data-schemas/project';
import { Scenario } from '../../../../data-schemas/scenario';
import { Result } from '../../../../helpers/result';
import { AppContext } from '../../../module-management/app-context';
import type { Action } from './../reducer';
import type { State } from './../state';
import { getFloors } from './floors';
import { getOpenings } from './openings';
import { getWalls } from './walls';

export const thermalMassParameterTable: {
  value: null | 100 | 250 | 450;
  label: State['thermalMassParameter'];
}[] = [
  { value: 100, label: 'low' },
  { value: 250, label: 'medium' },
  { value: 450, label: 'high' },
];

function thermalMassParameter(scenario: Scenario): State['thermalMassParameter'] {
  const { fabric } = scenario ?? {};
  if (fabric?.global_TMP !== true) {
    return 'no override';
  } else {
    return (
      thermalMassParameterTable.find(({ value }) => fabric.global_TMP_value === value)
        ?.label ?? 'no override'
    );
  }
}

function thermalBridgingOverride(scenario: Scenario): State['thermalBridgingOverride'] {
  const fabric = scenario?.fabric ?? {};
  const measures = scenario?.measures ?? {};

  if (!('thermal_bridging_override' in fabric)) {
    // unknown because the flag wasn't saved, need to do detective work
    if (
      ('thermal_bridging_yvalue' in fabric && fabric.thermal_bridging_yvalue !== 0.15) ||
      'thermal_bridging' in measures
    ) {
      fabric.thermal_bridging_override = true;
    } else {
      fabric.thermal_bridging_override = false;
    }
  }

  if (fabric?.thermal_bridging_override === true) {
    return {
      value: fabric?.thermal_bridging_yvalue ?? null,
      reason: measures?.thermal_bridging?.measure.description ?? '',
    };
  } else {
    return null;
  }
}

function maxId(scenario: Scenario) {
  const { fabric } = scenario ?? {};
  const allIds = [
    fabric?.elements?.map((element) => element.id),
    Object.keys(fabric?.measures ?? {}).map((key) => parseInt(key)),
  ]
    .flat(1)
    .filter((id): id is number => id !== undefined);
  allIds.sort((a, b) => a - b);
  return allIds.slice(-1)[0] ?? 0;
}

function extractBulkMeasures(
  fabric: Exclude<Scenario, undefined>['fabric'],
): State['bulkMeasures'] {
  return Object.entries(fabric?.measures ?? {})
    .filter(([, v]) => 'original_elements' in v)
    .map(([k, v]) => ({
      id: parseInt(k, 10),
      appliesTo: Object.values(
        v['original_elements'] !== undefined ? v.original_elements : {},
      ).map((element) => element.id),
    }));
}

function getBulkMeasuresByScenario(
  project: Project,
): Record<string, State['bulkMeasures']> {
  return Object.fromEntries(
    Object.entries(project.data).map(([scenarioId, scenario]) => [
      scenarioId,
      extractBulkMeasures(scenario?.fabric),
    ]),
  );
}

export function extractUpdateAction({
  project,
  currentScenario,
  currentModel,
  scenarioId,
}: AppContext): Result<Action[], never> {
  if (scenarioId === null) {
    throw new Error('scenarioId was null');
  }

  const bulkMeasuresByScenario = getBulkMeasuresByScenario(project);
  const bulkMeasures = bulkMeasuresByScenario[scenarioId];
  if (bulkMeasures === undefined) {
    throw new Error('scenarioId was not in bulkMeasuresByScenario');
  }

  const model = currentModel.mapErr(() => null).coalesce();

  return Result.ok([
    {
      type: 'external data update',
      state: {
        maxId: maxId(currentScenario),
        currentScenarioIsBaseline: scenarioId === 'master',
        thermalMassParameter: thermalMassParameter(currentScenario),
        walls: getWalls(currentScenario, model, bulkMeasures, project),
        floors: getFloors(currentScenario, model, bulkMeasures, project),
        openings: getOpenings(currentScenario, model, bulkMeasures, project),
        thermalBridgingOverride: thermalBridgingOverride(currentScenario),
        deletedElement: null,
        bulkMeasures,
        locked: currentScenario?.locked ?? false,
        model: currentModel.mapErr(() => null).coalesce(),
      },
    },
  ]);
}
