import { Project } from '../../../../data-schemas/project';
import { Scenario } from '../../../../data-schemas/scenario';
import {
  Hatch as LegacyHatch,
  Opening as LegacyOpening,
  isHatch,
  isOpening,
} from '../../../../data-schemas/scenario/fabric';
import { coalesceEmptyString } from '../../../../data-schemas/scenario/value-schemas';
import { Result } from '../../../../helpers/result';
import { calcMeasureQtyAndCost } from '../../../../measures';
import { Orientation } from '../../../../model/enums/orientation';
import { Overshading } from '../../../../model/enums/overshading';
import { Model } from '../../../../model/model';
import {
  Hatch as HatchModel,
  WindowLike as WindowLikeModel,
} from '../../../../model/modules/fabric/element-types';
import type { Opening, State } from './../state';
import { calcAreaSpec, getOriginalElement } from './helpers';

function extractOpeningModelElement(
  model: Model | null,
  elementId: string | number,
): Result<HatchModel | WindowLikeModel, Error> {
  const parsedElementId =
    typeof elementId === 'number' ? elementId : parseFloat(elementId);
  if (model === null) {
    return Result.err(new Error('No model to extract fabric model element from'));
  }
  const element = model.normal.fabric.getElementById(parsedElementId);
  if (element === null) {
    return Result.err(
      new Error(
        `Could not find a fabric model element with the provided ID: ${elementId}`,
      ),
    );
  }
  if (!(element instanceof WindowLikeModel) && !(element instanceof HatchModel)) {
    return Result.err(
      new Error(`Fabric model element with ID ${elementId} was not an opening`),
    );
  }
  return Result.ok(element);
}

function extractOpeningElementFromLegacy(
  legacyOpening: LegacyOpening | LegacyHatch,
): Opening {
  return {
    id: legacyOpening.id,
    type: 'element',
    elementType: 'opening',
    libraryItem:
      legacyOpening.type === 'hatch'
        ? {
            tag: legacyOpening.lib,
            name: legacyOpening.name,
            description: legacyOpening.description,
            source: legacyOpening.source,
            uValue: legacyOpening.uvalue,
            kValue: legacyOpening.kvalue,
            type: 'hatch',
            g: null,
            gL: null,
            ff: null,
          }
        : {
            tag: legacyOpening.lib,
            name: legacyOpening.name,
            description: legacyOpening.description,
            source: legacyOpening.source,
            uValue: legacyOpening.uvalue,
            kValue: legacyOpening.kvalue,
            type: legacyOpening.type,
            g: coalesceEmptyString(legacyOpening.g, 0),
            gL: coalesceEmptyString(legacyOpening.gL, 0),
            ff: coalesceEmptyString(legacyOpening.ff, 0),
          },
    inputs:
      legacyOpening.type === 'hatch'
        ? {
            location: legacyOpening.location,
            area: calcAreaSpec(
              legacyOpening.areaInputs,
              legacyOpening.area,
              legacyOpening.l,
              legacyOpening.h,
            ),
            orientation: null,
            overshading: null,
            subtractFrom: legacyOpening.subtractfrom,
          }
        : {
            location: legacyOpening.location,
            area: calcAreaSpec(
              legacyOpening.areaInputs,
              legacyOpening.area,
              legacyOpening.l,
              legacyOpening.h,
            ),
            orientation:
              Orientation.optionalFromIndex0(
                coalesceEmptyString(legacyOpening.orientation, -1),
              )?.name ?? null,
            overshading:
              Overshading.optionalFromIndex0(
                coalesceEmptyString(legacyOpening.overshading, -1),
              )?.name ?? null,
            subtractFrom: legacyOpening.subtractfrom,
          },
    modelElement: null,
    revertTo: null,
  };
}

function extractOpeningFromLegacy(
  legacyOpening: LegacyOpening | LegacyHatch,
  fabric: Exclude<Scenario, undefined>['fabric'],
  bulkMeasures: State['bulkMeasures'],
): Opening {
  function findMeasureIdx(id: string | number): string | number | null {
    const measuresList = fabric?.measures ?? {};

    const bulkMeasure = bulkMeasures.find((measure) => measure.appliesTo.includes(id));
    if (bulkMeasure !== undefined) {
      return bulkMeasure.id;
    }

    const measure = measuresList[id];
    if (measure !== undefined && measure.original_elements === undefined) {
      return id;
    }

    return null;
  }

  const measureIdx = findMeasureIdx(legacyOpening.id);
  if (measureIdx === null) {
    return extractOpeningElementFromLegacy(legacyOpening);
  }

  // We only use measure data for stuff that is constant across
  // different elements, i.e. not inputs or calculations
  const measureData = fabric?.measures?.[measureIdx]?.measure;
  if (measureData === undefined) {
    throw new Error(`unreachable (bad array access ${measureIdx})`);
  } else if (!('type' in measureData)) {
    throw new Error(`no type in measure data (array index ${measureIdx})`);
  } else if (
    measureData.type !== 'hatch' &&
    measureData.type !== 'door' &&
    measureData.type !== 'window' &&
    measureData.type !== 'roof light'
  ) {
    throw new Error(`Measure data for ${legacyOpening.type} is ${measureData.type})`);
  }

  const [costQuantity, costTotal] = calcMeasureQtyAndCost({
    costUnits: 'sqm',
    area: coalesceEmptyString(legacyOpening.area, 0),
    costPerUnit: measureData.cost,
    baseCost: measureData.min_cost,
    isExternalWallInsulation: false,
  });

  const nonMeasureData = extractOpeningElementFromLegacy(legacyOpening);
  return {
    ...nonMeasureData,
    type: 'measure',
    elementType: 'opening',
    libraryItem: {
      ...nonMeasureData.libraryItem,
      source: legacyOpening.source !== '' ? legacyOpening.source : measureData.source,
      associated_work: measureData.associated_work,
      benefits: measureData.benefits,
      cost: measureData.cost,
      cost_units: measureData.cost_units,
      description: measureData.description,
      disruption: measureData.disruption,
      key_risks: measureData.key_risks,
      min_cost: measureData.min_cost,
      maintenance: measureData.maintenance,
      lifetimeYears: measureData.lifetimeYears,
      notes: measureData.notes,
      performance: measureData.performance,
      who_by: measureData.who_by,
      carbonType: 'high/low',
      carbonHighBaseUpfront: measureData.carbonHighBaseUpfront,
      carbonHighBaseBiogenic: measureData.carbonHighBaseBiogenic,
      carbonHighPerUnitUpfront: measureData.carbonHighPerUnitUpfront,
      carbonHighPerUnitBiogenic: measureData.carbonHighPerUnitBiogenic,
      carbonLowBaseUpfront: measureData.carbonLowBaseUpfront,
      carbonLowBaseBiogenic: measureData.carbonLowBaseBiogenic,
      carbonLowPerUnitUpfront: measureData.carbonLowPerUnitUpfront,
      carbonLowPerUnitBiogenic: measureData.carbonLowPerUnitBiogenic,
      carbonSource: measureData.carbonSource,
    },
    outputs: {
      costQuantity,
      costTotal,
    },
  };
}

export function getOpenings(
  currentScenario: Scenario,
  model: Model | null,
  bulkMeasures: State['bulkMeasures'],
  project: Project,
): Opening[] {
  return (
    currentScenario?.fabric?.elements
      ?.filter(
        (element): element is LegacyOpening | LegacyHatch =>
          isOpening(element) || isHatch(element),
      )
      .map((legacyOpening) => {
        const opening = extractOpeningFromLegacy(
          legacyOpening,
          currentScenario.fabric,
          bulkMeasures,
        );

        opening.modelElement = extractOpeningModelElement(model, legacyOpening.id)
          .mapErr(() => null)
          .coalesce();

        if (opening.type === 'measure') {
          const element = getOriginalElement(
            opening.id,
            project.data,
            currentScenario?.created_from,
          );
          if (
            element !== null &&
            (element.type === 'roof light' ||
              element.type === 'window' ||
              element.type === 'door' ||
              element.type === 'hatch')
          ) {
            opening.revertTo = extractOpeningElementFromLegacy(element);
          }
        }
        return opening;
      }) ?? []
  );
}
