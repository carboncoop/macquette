import { cloneDeep } from 'lodash';
import { Project } from '../../../../data-schemas/project';
import { Scenario } from '../../../../data-schemas/scenario';
import { Floor as LegacyFloor, isFloor } from '../../../../data-schemas/scenario/fabric';
import {
  FloorType,
  PerFloorTypeSpec,
} from '../../../../data-schemas/scenario/fabric/floor-u-value';
import { coalesceEmptyString } from '../../../../data-schemas/scenario/value-schemas';
import { Result } from '../../../../helpers/result';
import { calcMeasureQtyAndCost } from '../../../../measures';
import { Model } from '../../../../model/model';
import { Floor as FloorModel } from '../../../../model/modules/fabric/element-types';
import * as fuvcState from '../components/u-value-calculator/state';
import type { Floor, State } from './../state';
import { getOriginalElement } from './helpers';

function extractFloorModelElement(
  model: Model | null,
  elementId: string | number,
): Result<FloorModel, Error> {
  const parsedElementId =
    typeof elementId === 'number' ? elementId : parseFloat(elementId);
  if (model === null) {
    return Result.err(new Error('No model to extract fabric model element from'));
  }
  const element = model.normal.fabric.getElementById(parsedElementId);
  if (element === null) {
    return Result.err(
      new Error(
        `Could not find a fabric model element with the provided ID: ${elementId}`,
      ),
    );
  }
  if (!(element instanceof FloorModel)) {
    return Result.err(
      new Error(`Fabric model element with ID ${elementId} was not a floor`),
    );
  }
  return Result.ok(element);
}

function extractFloorElementFromLegacy(legacyFloor: LegacyFloor): Floor {
  let selectedFloorType: FloorType;
  let perFloorTypeSpec: PerFloorTypeSpec;
  if (
    legacyFloor.perFloorTypeSpec === undefined ||
    legacyFloor.selectedFloorType === undefined
  ) {
    perFloorTypeSpec = cloneDeep(fuvcState.initialPerFloorTypeSpec);
    selectedFloorType = 'custom';
    perFloorTypeSpec.custom.uValue = coalesceEmptyString(legacyFloor.uvalue, null);
  } else {
    perFloorTypeSpec = legacyFloor.perFloorTypeSpec;
    selectedFloorType = legacyFloor.selectedFloorType;
  }

  return {
    id: legacyFloor.id,
    type: 'element',
    elementType: 'floor',
    libraryItem: {
      tag: legacyFloor.lib,
      name: legacyFloor.name,
      description: legacyFloor.description,
      type: legacyFloor.libraryFloorType,
      source: legacyFloor.source,
      uValue: legacyFloor.uvalue,
      kValue: legacyFloor.kvalue,
    },
    inputs: {
      location: legacyFloor.location,
      area: coalesceEmptyString(legacyFloor.area, null),
      exposedPerimeter: coalesceEmptyString(legacyFloor.perimeter, null) ?? null,
      selectedFloorType: selectedFloorType,
      perFloorTypeSpec: perFloorTypeSpec,
    },
    modelElement: null,
    revertTo: null,
  };
}

function extractFloorFromLegacy(
  legacyFloor: LegacyFloor,
  fabric: Exclude<Scenario, undefined>['fabric'],
  bulkMeasures: State['bulkMeasures'],
): Floor {
  function findMeasureIdx(id: string | number): string | number | null {
    const measuresList = fabric?.measures ?? {};

    const bulkMeasure = bulkMeasures.find((measure) => measure.appliesTo.includes(id));
    if (bulkMeasure !== undefined) {
      return bulkMeasure.id;
    }

    const measure = measuresList[id];
    if (measure !== undefined && measure.original_elements === undefined) {
      return id;
    }

    return null;
  }

  const measureIdx = findMeasureIdx(legacyFloor.id);
  if (measureIdx === null) {
    return extractFloorElementFromLegacy(legacyFloor);
  }

  // We only use measure data for stuff that is constant across
  // different elements, i.e. not inputs or calculations
  const measureData = fabric?.measures?.[measureIdx]?.measure;
  if (measureData === undefined) {
    throw new Error(`unreachable (bad array access ${measureIdx})`);
  } else if (!('type' in measureData)) {
    throw new Error(`no type in measure data (array index ${measureIdx})`);
  } else if (measureData.type !== 'floor') {
    throw new Error(`Measure data for ${legacyFloor.type} is ${measureData.type})`);
  }

  const [costQuantity, costTotal] = calcMeasureQtyAndCost({
    area: coalesceEmptyString(legacyFloor.area, 0),
    perimeter: coalesceEmptyString(legacyFloor.perimeter, 0) ?? 0,
    costUnits: measureData.cost_units,
    costPerUnit: measureData.cost,
    baseCost: measureData.min_cost,
    isExternalWallInsulation: false,
  });

  const nonMeasureData = extractFloorElementFromLegacy(legacyFloor);
  return {
    ...nonMeasureData,
    type: 'measure',
    elementType: 'floor',
    libraryItem: {
      ...nonMeasureData.libraryItem,
      source: legacyFloor.source !== '' ? legacyFloor.source : measureData.source,
      associated_work: measureData.associated_work,
      benefits: measureData.benefits,
      cost: measureData.cost,
      cost_units: measureData.cost_units,
      description: measureData.description,
      disruption: measureData.disruption,
      key_risks: measureData.key_risks,
      min_cost: measureData.min_cost,
      maintenance: measureData.maintenance,
      lifetimeYears: measureData.lifetimeYears,
      notes: measureData.notes,
      performance: measureData.performance,
      who_by: measureData.who_by,
      carbonType: 'high/low',
      carbonHighBaseUpfront: measureData.carbonHighBaseUpfront,
      carbonHighBaseBiogenic: measureData.carbonHighBaseBiogenic,
      carbonHighPerUnitUpfront: measureData.carbonHighPerUnitUpfront,
      carbonHighPerUnitBiogenic: measureData.carbonHighPerUnitBiogenic,
      carbonLowBaseUpfront: measureData.carbonLowBaseUpfront,
      carbonLowBaseBiogenic: measureData.carbonLowBaseBiogenic,
      carbonLowPerUnitUpfront: measureData.carbonLowPerUnitUpfront,
      carbonLowPerUnitBiogenic: measureData.carbonLowPerUnitBiogenic,
      carbonSource: measureData.carbonSource,
    },
    outputs: {
      costQuantity,
      costTotal,
    },
  };
}

export function getFloors(
  currentScenario: Scenario,
  model: Model | null,
  bulkMeasures: State['bulkMeasures'],
  project: Project,
): Floor[] {
  return (
    currentScenario?.fabric?.elements?.filter(isFloor).map((legacyFloor) => {
      const floor = extractFloorFromLegacy(
        legacyFloor,
        currentScenario.fabric,
        bulkMeasures,
      );

      floor.modelElement = extractFloorModelElement(model, floor.id)
        .mapErr(() => null)
        .coalesce();

      if (floor.type === 'measure') {
        const element = getOriginalElement(
          floor.id,
          project.data,
          currentScenario?.created_from,
        );
        if (element !== null && element.type === 'floor') {
          floor.revertTo = extractFloorElementFromLegacy(element);
        }
      }
      return floor;
    }) ?? []
  );
}
