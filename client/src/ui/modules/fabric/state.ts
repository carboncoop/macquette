import {
  Floor as LibraryFloor,
  FloorMeasure as LibraryFloorMeasure,
} from '../../../data-schemas/libraries/fabric-floors';
import {
  Opening as LibraryOpening,
  OpeningMeasure as LibraryOpeningMeasure,
} from '../../../data-schemas/libraries/fabric-openings';
import {
  WallLike as LibraryWallLike,
  WallLikeMeasure as LibraryWallLikeMeasure,
} from '../../../data-schemas/libraries/fabric-walls';
import {
  FloorType,
  PerFloorTypeSpec,
} from '../../../data-schemas/scenario/fabric/floor-u-value';
import { OrientationName } from '../../../model/enums/orientation';
import { OvershadingName } from '../../../model/enums/overshading';
import { Model } from '../../../model/model';
import {
  Floor as FloorModel,
  Hatch as HatchModel,
  WallLike as WallLikeModel,
  WindowLike as WindowLikeModel,
} from '../../../model/modules/fabric/element-types';

type AreaSpecSpecific = {
  area: number | null;
};
type AreaSpecDimensions = {
  area: number | null;
  length: number | null;
  height: number | null;
};
export type AreaSpec = {
  type: 'specific' | 'dimensions';
  specific: AreaSpecSpecific;
  dimensions: AreaSpecDimensions;
};

type MeasureOutputs = {
  costQuantity: number;
  costTotal: number;
};

type Id = string | number;

export type WallType = LibraryWallLike['type'];
type AppliedWallBase = {
  id: Id;
  elementType: 'wall';
  inputs: {
    location: string;
    area: AreaSpec;
  };
  modelElement: WallLikeModel | null;
  revertTo: WallLike | null;
};
export type AppliedWallElement = AppliedWallBase & {
  type: 'element';
  libraryItem: LibraryWallLike;
};
export type AppliedWallMeasure = AppliedWallBase & {
  type: 'measure';
  libraryItem: LibraryWallLikeMeasure;
  outputs: MeasureOutputs;
};
export type WallLike = AppliedWallElement | AppliedWallMeasure;

type AppliedFloorBase = {
  id: Id;
  elementType: 'floor';
  inputs: {
    location: string;
    area: number | null;
    exposedPerimeter: number | null;
    selectedFloorType: FloorType;
    perFloorTypeSpec: PerFloorTypeSpec;
  };
  modelElement: FloorModel | null;
  revertTo: Floor | null;
};
type AppliedFloorElement = AppliedFloorBase & {
  type: 'element';
  libraryItem: LibraryFloor;
};
export type AppliedFloorMeasure = AppliedFloorBase & {
  type: 'measure';
  libraryItem: LibraryFloorMeasure;
  outputs: MeasureOutputs;
};
export type Floor = AppliedFloorElement | AppliedFloorMeasure;

export type OpeningType = LibraryOpening['type'];
type AppliedOpeningBase = {
  id: Id;
  elementType: 'opening';
  inputs: {
    location: string;
    area: AreaSpec;
    orientation: OrientationName | null;
    overshading: OvershadingName | null;
    subtractFrom: Id | null;
  };
  modelElement: HatchModel | WindowLikeModel | null;
  revertTo: Opening | null;
};
type AppliedOpeningElement = AppliedOpeningBase & {
  type: 'element';
  libraryItem: LibraryOpening;
};
export type AppliedOpeningMeasure = AppliedOpeningBase & {
  type: 'measure';
  libraryItem: LibraryOpeningMeasure;
  outputs: MeasureOutputs;
};
export type Opening = AppliedOpeningElement | AppliedOpeningMeasure;

type FabricModal =
  | {
      type: 'add wall' | 'select wall bulk measure';
      elementType: WallType;
    }
  | {
      type: 'replace wall' | 'apply wall measure';
      elementType: WallType;
      id: Id;
    }
  | {
      type: 'select wall bulk measure elements';
      measure: LibraryWallLikeMeasure;
    }
  | {
      type: 'add floor' | 'select floor bulk measure';
    }
  | {
      type: 'replace floor' | 'apply floor measure';
      id: Id;
    }
  | {
      type: 'select floor bulk measure elements';
      measure: LibraryFloorMeasure;
    }
  | {
      type: 'add opening' | 'select opening bulk measure';
      elementType: OpeningType;
    }
  | {
      type: 'replace opening' | 'apply opening measure';
      elementType: OpeningType;
      id: Id;
    }
  | {
      type: 'select opening bulk measure elements';
      measure: LibraryOpeningMeasure;
    }
  | null;

export type State = {
  thermalMassParameter: 'no override' | 'low' | 'medium' | 'high';
  walls: WallLike[];
  floors: Floor[];
  openings: Opening[];
  bulkMeasures: { id: Id; appliesTo: Id[] }[];
  thermalBridgingOverride: null | {
    value: number | null;
    reason: string;
  };

  model: Model | null;
  maxId: number;
  justInserted: string | number | null;
  currentScenarioIsBaseline: boolean;
  modal: FabricModal;
  locked: boolean;
};

export function initialState(): State {
  return {
    thermalMassParameter: 'no override',
    walls: [],
    floors: [],
    openings: [],
    bulkMeasures: [],
    thermalBridgingOverride: null,
    model: null,
    maxId: 0,
    justInserted: null,
    currentScenarioIsBaseline: false,
    modal: null,
    locked: false,
  };
}
