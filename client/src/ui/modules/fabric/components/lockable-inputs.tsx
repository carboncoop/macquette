import React from 'react';

import {
  Button as GlobalButton,
  ButtonProps as GlobalButtonProps,
} from '../../../input-components/button';
import { NumberInput, NumberInputProps } from '../../../input-components/number';
import type { RadioGroupProps } from '../../../input-components/radio-group';
import { RadioGroup } from '../../../input-components/radio-group';
import { TextInput, TextInputProps } from '../../../input-components/text';

type PageContextInterface = {
  locked: boolean;
};

export const PageContext = React.createContext<PageContextInterface>({
  locked: false,
});

export function MyNumericInput(props: Omit<NumberInputProps, 'readOnly' | 'style'>) {
  const { locked } = React.useContext(PageContext);

  return (
    <NumberInput
      {...props}
      readOnly={locked}
      style={{ width: '50px', marginBottom: 0 }}
    />
  );
}

export function MyTextInput(props: Omit<TextInputProps, 'readOnly' | 'style'>) {
  const { locked } = React.useContext(PageContext);

  return <TextInput {...props} readOnly={locked} style={{ marginBottom: 0 }} />;
}

export function MyRadioGroup<T extends string>(
  props: Omit<RadioGroupProps<T>, 'disabled'>,
) {
  const { locked } = React.useContext(PageContext);

  return <RadioGroup {...props} disabled={locked} />;
}

export function Button(passthroughProps: GlobalButtonProps) {
  const { locked } = React.useContext(PageContext);
  return <GlobalButton disabled={locked} {...passthroughProps} />;
}
