import React from 'react';

import { FormGrid } from '../../../input-components/forms';
import { NumberInput } from '../../../input-components/number';
import { TextInput } from '../../../input-components/text';
import { NumberOutput } from '../../../output-components/numeric';
import { Action } from '../reducer';
import type { State } from './../state';

type Dispatcher = (action: Action) => void;
type SubProps = {
  state: State;
  dispatch: Dispatcher;
};

export function ThermalBridging({ state, dispatch }: SubProps) {
  return (
    <section className="line-top mb-45">
      <h3 className="mt-0 mb-15">Thermal Bridging</h3>

      <p>
        This is measure of heat lost due to non-repeating linear thermal bridges. In
        retrofit it is generally not possible to eliminate these - especially at floor
        junctions for example - but we can improve things at windows, doors, roof eaves,
        etc. This value is left to assessor judgement.
      </p>

      <div className="d-flex align-items-center my-15">
        <div>
          Total External Area
          <br />
          <span className="text-bigger text-bold">
            <NumberOutput
              value={state.model?.normal.fabric.externalArea}
              dp={2}
              unit="m²"
            />
          </span>
        </div>
        <div className="mx-15" style={{ fontSize: '2rem' }}>
          ×
        </div>
        <div>
          Thermal bridging heat loss factor (y)
          <br />
          <span className="text-bigger text-bold">
            <NumberOutput
              value={state.model?.normal.fabric.thermalBridgingAverageConductivity}
              dp={2}
              unit="W/m²·K"
            />
          </span>
        </div>
        <div className="mx-15" style={{ fontSize: '2rem' }}>
          =
        </div>
        <div>
          Thermal bridging heat loss
          <br />
          <span className="text-bigger text-bold">
            <NumberOutput
              value={state.model?.normal.fabric.thermalBridgingHeatLoss}
              dp={2}
              unit="W/K"
            />
          </span>
        </div>
      </div>

      {state.thermalBridgingOverride !== null ? (
        <div>
          <FormGrid>
            <label htmlFor="heatLossFactor">Heat loss factor:</label>
            <NumberInput
              id="heatLossFactor"
              value={state.thermalBridgingOverride.value}
              onChange={(value) =>
                dispatch({
                  type: 'fabric/set thermal bridging override',
                  value: { value, reason: state.thermalBridgingOverride?.reason ?? '' },
                })
              }
              style={{ width: '60px' }}
              unit="W/m²·K"
            />

            <label htmlFor="reasonForChange">Reason for change:</label>
            <TextInput
              id="reasonForChange"
              value={state.thermalBridgingOverride.reason}
              onChange={(reason) =>
                dispatch({
                  type: 'fabric/set thermal bridging override',
                  value: { value: state.thermalBridgingOverride?.value ?? 0, reason },
                })
              }
              style={{ width: '20rem' }}
            />

            <p>Example thermal bridging values:</p>
            <table className="table">
              <tbody>
                <tr>
                  <td>0.15 W/m²K</td>
                  <td>Existing condition, minimal works to address thermal bridging</td>
                </tr>
                <tr>
                  <td>0.125 W/m²K</td>
                  <td>
                    Some works to address bridging, such as window reveal insulation
                  </td>
                </tr>
                <tr>
                  <td>0.1 W/m²K</td>
                  <td>EWI and IWI which considers thermal bridging</td>
                </tr>
                <tr>
                  <td>0.08 W/m²K</td>
                  <td>Full EWI on a simple house</td>
                </tr>
              </tbody>
            </table>
          </FormGrid>

          <button
            className="btn"
            onClick={() =>
              dispatch({
                type: 'fabric/set thermal bridging override',
                value: null,
              })
            }
          >
            Remove override
          </button>
        </div>
      ) : (
        <button
          className="btn"
          onClick={() =>
            dispatch({
              type: 'fabric/set thermal bridging override',
              value: { value: null, reason: '' },
            })
          }
        >
          Override heat loss factor
        </button>
      )}
    </section>
  );
}
