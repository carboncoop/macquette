import type { Floor, Opening, WallLike } from '../state';

type MeasureStatus = 'new measure' | 'previous measure' | 'none';

export const colourForStatus: Record<MeasureStatus, string> = {
  'new measure': '--pale-green',
  'previous measure': '--blue-400',
  none: '--grey-600',
};

export function measureStatus(wall: WallLike | Floor | Opening): MeasureStatus {
  if (wall.type === 'measure') {
    return 'new measure';
  } else if (wall.libraryItem.tag.includes('-M')) {
    return 'previous measure';
  } else {
    return 'none';
  }
}
