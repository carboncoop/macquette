import React from 'react';

import { sum } from '../../../../helpers/array-reducers';
import { NumberOutput } from '../../../output-components/numeric';
import type { State } from './../state';

type SubProps = {
  state: State;
};

export function Totals({ state }: SubProps) {
  return (
    <section className="line-top mb-45">
      <h3 className="mt-0 mb-15">Totals</h3>

      <div className="d-flex align-items-center my-15">
        <div>
          Total fabric heat loss
          <br />
          <span className="text-bigger text-bold">
            <NumberOutput value={state.model?.normal.fabric.heatLoss} dp={1} unit="W/K" />
            <br />
          </span>
        </div>
        <div className="mx-15" style={{ fontSize: '2rem' }}>
          +
        </div>
        <div>
          Ventilation heat loss
          <br />
          <span className="text-bigger text-bold">
            <NumberOutput
              value={state.model?.normal.heatLoss.averageVentilationInfiltrationHeatLoss}
              dp={1}
              unit="W/K"
            />
          </span>
        </div>
        <div className="mx-15" style={{ fontSize: '2rem' }}>
          =
        </div>
        <div>
          Total heat loss
          <br />
          <span className="text-bigger text-bold">
            <NumberOutput
              value={
                state.model === null
                  ? null
                  : state.model.normal.fabric.heatLoss +
                    state.model.normal.heatLoss.averageVentilationInfiltrationHeatLoss
              }
              dp={1}
              unit="W/K"
            />
            <br />
          </span>
        </div>
      </div>

      <table>
        <tbody>
          <tr>
            <td className="pr-15 py-7">Total fabric thermal capacity:</td>
            <td>
              <span className="text-bigger text-bold">
                <NumberOutput
                  value={
                    state.model === null
                      ? null
                      : sum(
                          state.model.normal.fabric.flatElements.map(
                            (e) => e.thermalCapacity,
                          ),
                        )
                  }
                  dp={0}
                  unit="kJ/K·m3"
                />
              </span>
            </td>
          </tr>
          <tr>
            <td className="pr-15 py-7">Annual solar gains:</td>
            <td>
              <span className="text-bigger text-bold">
                <NumberOutput
                  value={state.model?.normal.fabric.heatGainSolarAnnual}
                  dp={0}
                  unit="W"
                />{' '}
                (
                <NumberOutput
                  value={
                    state.model === null
                      ? null
                      : (state.model.normal.fabric.heatGainSolarAnnual * 365 * 24) / 1000
                  }
                  dp={0}
                  unit="kWh"
                />
                )
              </span>
            </td>
          </tr>
        </tbody>
      </table>
    </section>
  );
}
