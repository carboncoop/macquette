import React from 'react';

import { sum } from '../../../../helpers/array-reducers';
import { DeleteIcon, HammerIcon, PlusIcon, UndoIcon } from '../../../icons';
import { NumberOutput } from '../../../output-components/numeric';
import type { Action } from '../reducer';
import type { State, WallLike } from '../state';
import { AreaInput } from './area-input';
import { Button, MyTextInput } from './lockable-inputs';
import { colourForStatus, measureStatus } from './measure-status';
import { showModal } from './modals';

type Dispatcher = (action: Action) => void;
type SubProps = {
  state: State;
  dispatch: Dispatcher;
};

type WallProps = SubProps & {
  wall: WallLike;
  secondDimension: 'width' | 'height';
};

function WallRow({ state, dispatch, wall, secondDimension }: WallProps) {
  const status = measureStatus(wall);

  const isDeleted =
    (wall.inputs.area.type === 'dimensions' &&
      wall.inputs.area.dimensions.length === 0 &&
      wall.inputs.area.dimensions.height === 0) ||
    (wall.inputs.area.type === 'specific' && wall.inputs.area.specific.area === 0);

  return (
    <div
      className="card"
      style={{
        borderLeft: `10px solid var(${colourForStatus[status]})`,
      }}
    >
      <div className="d-flex justify-content-between align-items-center mb-15">
        <div>
          <span>
            {isDeleted && <span className="label label-important mr-7">Deleted</span>}
            <b>{wall.libraryItem.name}</b>
          </span>
          <div className="d-flex">
            <span>{wall.libraryItem.tag}</span>
            <span className="ml-15">U value: {wall.libraryItem.uValue} W/K·m²</span>
            <span className="ml-15">k value: {wall.libraryItem.kValue} kJ/K·m²</span>
            {wall.type === 'measure' && (
              <span className="ml-15">
                Measure cost: £
                <NumberOutput value={wall.outputs.costTotal} dp={0} />
              </span>
            )}
          </div>
        </div>

        <div>
          {state.currentScenarioIsBaseline ? (
            <Button
              title="Delete"
              icon={DeleteIcon}
              className="ml-15"
              onClick={() =>
                dispatch({
                  type: 'fabric/delete',
                  id: wall.id,
                })
              }
            />
          ) : wall.type === 'measure' && wall.revertTo !== null ? (
            <Button
              title="Revert"
              icon={UndoIcon}
              className="ml-15"
              onClick={() =>
                dispatch({
                  type: 'fabric/revert measure',
                  id: wall.id,
                })
              }
            />
          ) : null}

          {state.currentScenarioIsBaseline ? (
            <Button
              title="Replace"
              className="ml-15"
              onClick={showModal(dispatch, {
                type: 'replace wall',
                elementType: wall.libraryItem.type,
                id: wall.id,
              })}
            />
          ) : (
            <Button
              title={wall.type === 'measure' ? 'Replace measure' : 'Apply measure'}
              icon={HammerIcon}
              className="ml-15"
              onClick={showModal(dispatch, {
                type: 'apply wall measure',
                elementType: wall.libraryItem.type,
                id: wall.id,
              })}
            />
          )}
        </div>
      </div>
      <div className="d-flex flex-wrap">
        <div className="mr-30">
          <label className="d-i small-caps" htmlFor={`e${wall.id}-location`}>
            Location
          </label>
          <br />
          <MyTextInput
            id={`e${wall.id}-location`}
            value={wall.inputs.location}
            onChange={(location) =>
              dispatch({
                type: 'fabric/merge wall input',
                id: wall.id,
                value: { location },
              })
            }
            className={wall.id === state.justInserted ? 'just-inserted' : ''}
          />
        </div>

        <AreaInput
          area={wall.inputs.area}
          dimension2={secondDimension === 'height' ? 'Height' : 'Width'}
          onChange={(area) =>
            dispatch({
              type: 'fabric/merge wall input',
              id: wall.id,
              value: { area },
            })
          }
        />

        <span style={{ fontSize: '1.5rem', margin: '0 0.2rem' }}>
          <br />-
        </span>
        <div>
          <span className="small-caps">Windows</span> <br />
          <NumberOutput value={wall.modelElement?.deductibleArea ?? 0} unit="m²" />
        </div>

        <span style={{ fontSize: '1.5rem', margin: '0 0.2rem' }}>
          <br />=
        </span>

        <div>
          <span className="small-caps">Net area</span> <br />
          <NumberOutput value={wall.modelElement?.netArea ?? 0} unit="m²" />
        </div>

        <div className="ml-30">
          <span className="small-caps">Heat loss</span> <br />
          <NumberOutput value={wall.modelElement?.heatLoss ?? 0} unit="W/K" />
        </div>
      </div>
    </div>
  );
}

function Totals({ walls }: { walls: WallLike[] }) {
  if (walls.length === 0) {
    return null;
  }

  return (
    <table className="mb-15 table" style={{ width: 'auto' }}>
      <tbody>
        <tr>
          <th className="text-left">Total net area</th>
          <td>
            <NumberOutput
              value={sum(walls.map((wall) => wall.modelElement?.netArea ?? 0))}
              unit="m²"
            />
          </td>
        </tr>
        <tr>
          <th className="text-left">Total heat loss</th>
          <td>
            <NumberOutput
              value={sum(walls.map((wall) => wall.modelElement?.heatLoss ?? 0))}
              unit="W/K"
            />
          </td>
        </tr>
      </tbody>
    </table>
  );
}

export function Walls({ state, dispatch }: SubProps) {
  const walls = state.walls.filter((wall) => wall.libraryItem.type === 'external wall');
  return (
    <section className="line-top mb-45">
      <h3 className="mt-0 mb-15">Walls</h3>

      <div className="rounded-cards mb-15">
        {walls.map((wall) => (
          <WallRow
            wall={wall}
            key={wall.id}
            state={state}
            dispatch={dispatch}
            secondDimension={'height'}
          />
        ))}
      </div>

      <Totals walls={walls} />

      <Button
        title="New wall"
        icon={PlusIcon}
        onClick={showModal(dispatch, {
          type: 'add wall',
          elementType: 'external wall',
        })}
      />

      {state.currentScenarioIsBaseline || (
        <Button
          title="Apply bulk measure"
          icon={HammerIcon}
          onClick={showModal(dispatch, {
            type: 'select wall bulk measure',
            elementType: 'external wall',
          })}
          className={'ml-15'}
          disabled={walls.length === 0}
        />
      )}
    </section>
  );
}

export function PartyWalls({ state, dispatch }: SubProps) {
  const walls = state.walls.filter((wall) => wall.libraryItem.type === 'party wall');
  return (
    <section className="line-top mb-45">
      <h3 className="mt-0 mb-15">Party walls</h3>

      <div className="rounded-cards mb-15">
        {walls.map((wall) => (
          <WallRow
            wall={wall}
            key={wall.id}
            state={state}
            dispatch={dispatch}
            secondDimension={'height'}
          />
        ))}
      </div>

      <Totals walls={walls} />

      <Button
        title="New wall"
        icon={PlusIcon}
        onClick={showModal(dispatch, {
          type: 'add wall',
          elementType: 'party wall',
        })}
      />

      {state.currentScenarioIsBaseline || (
        <Button
          title="Apply bulk measure"
          icon={HammerIcon}
          onClick={showModal(dispatch, {
            type: 'select wall bulk measure',
            elementType: 'party wall',
          })}
          className={'ml-15'}
          disabled={walls.length === 0}
        />
      )}
    </section>
  );
}

export function RoofsAndLofts({ state, dispatch }: SubProps) {
  const walls = state.walls.filter(
    (wall) => wall.libraryItem.type === 'roof' || wall.libraryItem.type === 'loft',
  );
  return (
    <section className="line-top mb-45">
      <h3 className="mt-0 mb-15">Roofs and lofts</h3>
      <p className="mb-0">
        When inputting roofs and lofts, only add that area which forms part of the thermal
        envelope of the building:
      </p>
      <ul className="mb-15">
        <li>
          Don&apos;t add a roof where it sits above a loft which is insulated at ceiling
          level.
        </li>
        <li>
          Don&apos;t add a loft where it sits below a roof which is insulated at rafter
          level.
        </li>
        <li>
          If the thermal envelope changes in a scenario (e.g. loft insulation is added and
          so the roof is no longer part of the envelope), then set the area of the element
          that is no longer part of the thermal envelope to 0, and add a new element to
          represent the new envelope, and then apply a measure to it.
        </li>
      </ul>

      <div className="rounded-cards mb-15">
        {walls.map((wall) => (
          <WallRow
            wall={wall}
            key={wall.id}
            state={state}
            dispatch={dispatch}
            secondDimension={'width'}
          />
        ))}
      </div>

      <Totals walls={walls} />

      <div style={{ display: 'flex', flexDirection: 'column', gap: '7px' }}>
        <div>
          <Button
            title="New loft"
            icon={PlusIcon}
            onClick={showModal(dispatch, {
              type: 'add wall',
              elementType: 'loft',
            })}
          />

          {state.currentScenarioIsBaseline || (
            <Button
              title="Apply loft bulk measure"
              icon={HammerIcon}
              onClick={showModal(dispatch, {
                type: 'select wall bulk measure',
                elementType: 'loft',
              })}
              className={'ml-15'}
              disabled={walls.length === 0}
            />
          )}
        </div>
        <div>
          <Button
            title="New roof"
            icon={PlusIcon}
            onClick={showModal(dispatch, {
              type: 'add wall',
              elementType: 'roof',
            })}
          />

          {state.currentScenarioIsBaseline || (
            <Button
              title="Apply roof bulk measure"
              icon={HammerIcon}
              onClick={showModal(dispatch, {
                type: 'select wall bulk measure',
                elementType: 'roof',
              })}
              className={'ml-15'}
              disabled={walls.length === 0}
            />
          )}
        </div>
      </div>
    </section>
  );
}
