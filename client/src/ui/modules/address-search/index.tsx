import { pick } from 'lodash';
import React, { ReactElement, ReactNode } from 'react';
import { z } from 'zod';
import type { ResolvedAddress } from '../../../data-schemas/address';
import {
  getWithOriginValue,
  hasUserData,
  withDatabaseData,
  WithOrigin,
  withUserData,
} from '../../../data-schemas/helpers/with-origin';
import { projectSchema } from '../../../data-schemas/project';
import { Household, householdSchema } from '../../../data-schemas/scenario/household';
import { assertNever } from '../../../helpers/assert-never';
import { Result } from '../../../helpers/result';
import { frostAttackRisk, FrostAttackRisk } from '../../../model/datasets/frost-attack';
import { hasHighOverheatingRisk } from '../../../model/datasets/overheating';
import type { RegionName } from '../../../model/enums/region';
import { Region } from '../../../model/enums/region';
import { FormGrid, InfoTooltip } from '../../input-components/forms';
import { NumberInput } from '../../input-components/number';
import { Select } from '../../input-components/select';
import { TextInput } from '../../input-components/text';
import { Textarea } from '../../input-components/textarea';
import type { UiModule } from '../../module-management/module-type';
import { NumberOutput } from '../../output-components/numeric';
import { VStack } from '../../stacks';
import { LookupSection } from './lookup';

const fieldNames = [
  'address',
  'areaOfOutstandingNaturalBeauty',
  'exposureZone',
  'floodingGroundwater',
  'floodingReservoirs',
  'floodingRiversAndSea',
  'floodingSurfaceWater',
  'frostAttackRisk',
  'landRegistryLink',
  'latLong',
  'localAuthority',
  'localPlanningAuthority',
  'locationDensity',
  'lowerLayerSuperOutputArea',
  'overheatingRisk',
  'propertyType',
  'propertyTypeNotes',
  'radonRisk',
  'subsidence2030',
  'subsidence2050',
  'soilHeaveRisk',
  'tenure',
  'tenureNotes',
  'uniquePropertyReferenceNumber',
  'planningNotes',
  'builtBand',
  'builtExact',
  'listedStatus',
  'listedStatusNotes',
  'inConservationArea',
  'treeNotes',
  'hasMainsElectricity',
  'hasMainsGas',
  'hasMainsWater',
  'hasMainsSewer',
  'electricityDNO',
  'electricityLooped',
  'electricityMPAN',
  'electricitySmartMeter',
  'gasTransporter',
  'gasMPRN',
  'gasSmartMeter',
  'externalNoisePollution',
  'externalAirPollution',
  'smokeControlZone',
  'heatNetworkNotes',
  'heatNetworkInZone',
  'siteOfSpecialScientificInterest',
  'worldHeritageSite',
  'nearAncientMonument',
] as const;
type FieldNames = (typeof fieldNames)[number];

type Address = {
  line1: string;
  line2: string;
  line3: string;
  postTown: string;
  postcode: string;
  country: string;
};

type State = {
  // We only use the first external update
  initialUpdateComplete: boolean;
  externalDesignTemperature: WithOrigin<number, null>;
  region: WithOrigin<RegionName, null>;
  elevation: WithOrigin<number, null>;
  addressAlreadyResolved: boolean;
} & Pick<Household, FieldNames>;

type ExternalDataAction = { type: 'external data update'; state: Partial<State> };

type Action =
  | { type: 'use resolved address'; data: ResolvedAddress }
  | { type: 'merge data'; state: Partial<State> }
  | ExternalDataAction;

type Dispatcher = (action: Action) => void;

function WithOriginInput<T, U>({
  data,
  labelText,
  emptyText,
  displayComponent,
  inputComponent,
  hintComponent = () => <></>,
  inputId,
  onChange,
  initialValue,
}: {
  data: WithOrigin<T, U>;
  labelText: string;
  emptyText: string;
  displayComponent: (data: T) => ReactElement;
  inputComponent: (box: WithOrigin<T, U>, data: T | U) => ReactElement;
  hintComponent?: () => ReactElement;
  inputId: string;
  onChange: (value: WithOrigin<T, U>) => void;
  initialValue: T | U;
}) {
  return (
    <>
      {hasUserData(data) ? (
        <label htmlFor={inputId}>{labelText}:</label>
      ) : (
        <>{labelText}:</>
      )}

      {data.type === 'no data' && (
        <div>
          {emptyText}
          <button
            className="btn ml-7"
            onClick={() => onChange(withUserData<T, U>(data, initialValue))}
          >
            Enter manually
          </button>
        </div>
      )}
      {data.type === 'user provided' && (
        <div>
          {inputComponent(data, data.value)}
          {hintComponent()}
        </div>
      )}
      {data.type === 'from database' && (
        <div>
          {displayComponent(data.value)}
          <button
            className="btn ml-7"
            onClick={() => onChange(withUserData<T, U>(data, data.value))}
          >
            Override
          </button>
          {hintComponent()}
        </div>
      )}
      {data.type === 'overridden' && (
        <div>
          {inputComponent(data, data.value)}
          <button
            className="btn ml-7"
            onClick={() => onChange({ type: 'from database', value: data.dbValue })}
          >
            Remove override
          </button>
          {hintComponent()}
        </div>
      )}
    </>
  );
}

function opt<T>(val: T, legacy = false): { value: T; display: T; legacy: boolean } {
  return { value: val, display: val, legacy: legacy };
}

function AddressSearch({ state, dispatch }: { state: State; dispatch: Dispatcher }) {
  function dispatchMerge(state: Partial<State>) {
    dispatch({
      type: 'merge data',
      state,
    });
  }
  const overheatingRiskRegion =
    state.address.type !== 'no data'
      ? getOverheatingRiskRegionName(
          Region.fromPostcode(state.address.value.postcode).unwrap().name,
        )
      : 'unknown';

  return (
    <section>
      <div className="text-callout mb-15">
        <p>
          This is a preliminary desktop assessment of the property. It is intended to
          provide an initial understanding of the building and its setting. It should be
          reviewed before conducting the questionnaire with the householder
          client/resident, before visiting site, and before constructing suggested
          retrofit scenarios or retrofit plan. This element of the process is aligned with
          the Requirement for a Context Assessment in BS40104, Clause 5.
        </p>
      </div>

      <LookupSection
        onResolve={(address) => dispatch({ type: 'use resolved address', data: address })}
        initiallyResolved={state.addressAlreadyResolved}
      />

      <h3 className="line-top mt-0">Address data</h3>

      <FormGrid>
        <WithOriginInput
          data={state.address}
          labelText="Address"
          emptyText="Enter a postcode for automatic lookup"
          displayComponent={(address) => (
            <>
              {address.line1}
              <br />
              {address.line2 !== '' && (
                <>
                  {address.line2}
                  <br />
                </>
              )}
              {address.line3 !== '' && (
                <>
                  {address.line3}
                  <br />
                </>
              )}
              {address.postTown}
              <br />
              {address.postcode}
              <br />
              {address.country}
            </>
          )}
          initialValue={{
            line1: '',
            line2: '',
            line3: '',
            postTown: '',
            postcode: '',
            country: '',
          }}
          onChange={(val) => dispatchMerge({ address: val })}
          inputComponent={(box, address) => {
            function updateLine(newLine: Partial<Address>): Address {
              return {
                ...address,
                ...newLine,
              };
            }
            function dispatchLineChange(newLine: Partial<Address>) {
              dispatchMerge({
                address: withUserData(box, updateLine(newLine)),
              });
            }
            return (
              <>
                <TextInput
                  id="address_1"
                  className="mb-7"
                  value={address.line1}
                  onChange={(val) => {
                    dispatchLineChange({ line1: val });
                  }}
                />
                <br />
                <TextInput
                  id="line_2"
                  className="mb-7"
                  value={address.line2}
                  onChange={(val) => {
                    dispatchLineChange({ line2: val });
                  }}
                />
                <br />
                <TextInput
                  id="line_3"
                  className="mb-7"
                  value={address.line3}
                  onChange={(val) => {
                    dispatchLineChange({ line3: val });
                  }}
                />
                <br />
                <TextInput
                  id="post_town"
                  placeholder="Town/City"
                  className="mb-7"
                  style={{ width: '18ch' }}
                  value={address.postTown}
                  onChange={(val) => {
                    dispatchLineChange({ postTown: val });
                  }}
                />
                <br />
                <TextInput
                  id="postcode"
                  placeholder="Postcode"
                  className="mb-7"
                  style={{ width: '10ch' }}
                  value={address.postcode}
                  onChange={(val) => {
                    dispatchLineChange({ postcode: val });
                  }}
                />
                <br />
                <TextInput
                  id="country"
                  placeholder="Country"
                  className="mb-7"
                  value={address.country}
                  onChange={(val) => {
                    dispatchLineChange({ country: val });
                  }}
                />
                <br />
              </>
            );
          }}
          inputId="line_1"
        />

        {state.address.type !== 'no data' && (
          <>
            <span>Links:</span>
            <span>
              <a
                href={`https://www.google.co.uk/maps/search/${[
                  state.address.value.line1,
                  state.address.value.line2,
                  state.address.value.line3,
                  state.address.value.postTown,
                  state.address.value.postcode,
                ].join(',')}/`}
                target="_blank"
                rel="noreferrer noopener"
              >
                Google Maps
              </a>
              <br />
              <a
                href={`https://find-energy-certificate.service.gov.uk/find-a-certificate/search-by-postcode?postcode=${state.address.value.postcode}`}
                target="_blank"
                rel="noreferrer noopener"
              >
                List of EPCs for postcode
              </a>
            </span>
          </>
        )}

        <WithOriginInput
          data={state.uniquePropertyReferenceNumber}
          labelText="UPRN"
          emptyText="Enter a postcode for automatic lookup"
          displayComponent={(uprn) => <>{uprn}</>}
          initialValue={''}
          onChange={(val) => dispatchMerge({ uniquePropertyReferenceNumber: val })}
          inputComponent={(_box, uprn) => (
            <TextInput
              id="uprn"
              value={uprn}
              onChange={(val) =>
                dispatchMerge({
                  uniquePropertyReferenceNumber: withUserData(
                    state.uniquePropertyReferenceNumber,
                    val,
                  ),
                })
              }
              style={{ width: '14ch' }}
            />
          )}
          inputId="uprn"
        />

        <WithOriginInput
          data={state.localAuthority}
          labelText="Local authority"
          emptyText="Enter a postcode for automatic lookup"
          displayComponent={(localAuthority) => <>{localAuthority}</>}
          initialValue={''}
          onChange={(val) => dispatchMerge({ localAuthority: val })}
          inputComponent={(_box, localAuthority) => (
            <TextInput
              id="localAuthority"
              value={localAuthority}
              onChange={(val) =>
                dispatchMerge({
                  localAuthority: withUserData(state.localAuthority, val),
                })
              }
            />
          )}
          inputId="localAuthority"
        />

        <WithOriginInput
          data={state.lowerLayerSuperOutputArea}
          labelText="LSOA"
          emptyText="Enter a postcode for automatic lookup"
          displayComponent={(lsoa) => <>{lsoa}</>}
          initialValue={''}
          onChange={(val) => dispatchMerge({ lowerLayerSuperOutputArea: val })}
          inputComponent={(_box, lsoa) => (
            <TextInput
              id="lsoa"
              value={lsoa}
              onChange={(val) =>
                dispatchMerge({
                  lowerLayerSuperOutputArea: withUserData(
                    state.lowerLayerSuperOutputArea,
                    val,
                  ),
                })
              }
            />
          )}
          inputId="lsoa"
        />

        <label htmlFor="propertyType">Property type:</label>
        <div>
          <Select
            className="input--auto-width"
            id="propertyType"
            options={[
              opt('mid-terrace' as const),
              opt('semi-detached' as const),
              opt('end terrace' as const),
              opt('detached' as const),
              opt('flat (conversion)' as const),
              opt('flat (purpose built)' as const),
            ]}
            value={state.propertyType}
            onChange={(val) => dispatchMerge({ propertyType: val })}
          />
        </div>

        <label htmlFor="propertyTypeNotes">Short description of property:</label>
        <div>
          <TextInput
            id="propertyTypeNotes"
            value={state.propertyTypeNotes}
            onChange={(val) => dispatchMerge({ propertyTypeNotes: val })}
          />
        </div>
      </FormGrid>

      <h3>Ownership</h3>

      <div className="text-callout mb-15">
        <p>
          It&apos;s important to establish the ownership of the property as this may
          affect works that can be done and permissions required. This should be asked of
          the client before a survey is commissioned. If there are any concerns about
          ownership or about property boundaries or easements etc it is possible to look
          up the title deeds for the property for a small fee via the land registry in
          England and Wales or the Land Register of Scotland. Title plans will often need
          to be obtained to check property boundaries and for any easements when designs
          are developed for construction or planning applications are prepared. Provide
          comments below if there is anything a surveyor should be aware of.
        </p>

        <p>
          Most properties will be{' '}
          <a
            href="https://www.gov.uk/search-property-information-land-registry"
            rel="noreferrer"
            target="_blank"
          >
            listed on the land registry
          </a>{' '}
          - though some may not appear if they haven&apos;t changed hands in a long time.
          The link or relevant reference here can help locate that information.
        </p>
      </div>

      <FormGrid>
        <label htmlFor="tenure">Tenure:</label>
        <div>
          <Select
            className="input--auto-width"
            id="tenure"
            options={[
              opt('owner-occupied' as const, true),
              opt('keep the same' as const, true),
              opt('owner-occupied, with mortgage' as const),
              opt('owner-occupied, no mortgage' as const),
              opt('holiday let' as const),
              opt('private rent' as const),
              opt('social or affordable rent' as const),
              opt('co-op' as const),
              opt('shared ownership' as const),
            ]}
            value={state.tenure}
            onChange={(val) => dispatchMerge({ tenure: val })}
          />
        </div>

        <label htmlFor="propertyTypeNotes">Notes:</label>
        <div>
          <TextInput
            id="propertyTypeNotes"
            value={state.tenureNotes}
            onChange={(val) => dispatchMerge({ tenureNotes: val })}
          />
        </div>

        <label htmlFor="landRegistryLink">Land registry reference link:</label>
        <div>
          <TextInput
            id="landRegistryLink"
            value={state.landRegistryLink}
            onChange={(val) => dispatchMerge({ landRegistryLink: val })}
          />
        </div>
      </FormGrid>

      <h3>Climate and Environment</h3>
      <div className="text-callout mb-15">
        <p>
          The current and future climate for a property - its exposure to wind driven
          rain, overheating risk, potnetial subsidence and flood risk - can all have a
          bearing on retrofit recommendations.
        </p>
      </div>

      <FormGrid>
        <WithOriginInput
          data={state.region}
          labelText="SAP climate region"
          emptyText="Enter a postcode for automatic lookup"
          displayComponent={(region) => <>{region}</>}
          initialValue={null}
          onChange={(val) => dispatchMerge({ region: val })}
          inputComponent={(box, region) => (
            <Select
              id="region"
              className="input--auto-width"
              options={Region.all.map((region) => ({
                value: region.name,
                display: region.name,
              }))}
              value={region}
              onChange={(val) =>
                dispatchMerge({
                  region: withUserData(box, val),
                })
              }
            />
          )}
          inputId="region"
        />

        <WithOriginInput
          data={state.elevation}
          labelText="Elevation"
          emptyText="Enter a postcode for automatic lookup"
          displayComponent={(elevation) => <>{elevation}m</>}
          initialValue={null}
          onChange={(val) => dispatchMerge({ elevation: val })}
          inputComponent={(_box, elevation) => (
            <NumberInput
              id="elevation"
              value={elevation}
              unit="m"
              onChange={(val) =>
                dispatchMerge({
                  elevation: withUserData(state.elevation, val),
                })
              }
            />
          )}
          inputId="elevation"
        />

        <WithOriginInput
          data={state.externalDesignTemperature}
          labelText="External design temperature"
          emptyText="Enter a postcode for automatic lookup"
          displayComponent={(externalDesignTemperature) => (
            <>
              <NumberOutput value={externalDesignTemperature} unit="°C" />
              <InfoTooltip>
                Worked out using 99.6th pecentile dry bulb temperatures in CIBSE A, taking
                account of elevation changes
              </InfoTooltip>
            </>
          )}
          initialValue={null}
          onChange={(val) => dispatchMerge({ externalDesignTemperature: val })}
          inputComponent={(_box, externalDesignTemperature) => (
            <NumberInput
              id="externalDesignTemperature"
              value={externalDesignTemperature}
              unit="°C"
              onChange={(val) =>
                dispatchMerge({
                  externalDesignTemperature: withUserData(
                    state.externalDesignTemperature,
                    val,
                  ),
                })
              }
            />
          )}
          inputId="externalDesignTemperature"
        />

        <WithOriginInput<FrostAttackRisk, null>
          data={state.frostAttackRisk}
          labelText="Frost attack risk"
          emptyText="Enter a postcode for automatic lookup"
          displayComponent={(risk) => <>{risk}</>}
          initialValue={null}
          onChange={(val) => dispatchMerge({ frostAttackRisk: val })}
          inputComponent={(box, frostAttackRisk) => (
            <>
              <Select
                className="input--auto-width"
                id="frost-attack"
                options={[
                  {
                    value: 'high risk',
                    display: 'high risk of severe exposure to frost attack',
                  },
                  {
                    value: 'within a postcode with high risk',
                    display:
                      'within a postcode with high risk of severe exposure to frost attack',
                  },
                  {
                    value: 'not at high risk',
                    display: 'not at risk of severe exposure to frost attack',
                  },
                ]}
                value={frostAttackRisk}
                onChange={(val) =>
                  dispatchMerge({
                    frostAttackRisk: withUserData(box, val),
                  })
                }
              />
              <Hint>
                <p>
                  You can find{' '}
                  <a
                    rel="noreferrer"
                    target="_blank"
                    href="https://nhbc-standards.co.uk/2019/6-superstructure-excluding-roofs/6-1-external-masonry-walls/6-1-6-exposure/"
                  >
                    severe frost attack risk map here
                  </a>
                  .
                </p>
              </Hint>
            </>
          )}
          inputId="frost-attack"
        />

        {state.overheatingRisk !== null &&
          state.overheatingRisk !== 'outside England' && (
            <>
              <span>Overheating risk:</span>
              <span>
                {state.overheatingRisk}{' '}
                <InfoTooltip>
                  This is based on the postcodes listed in the Approved Document for Part
                  O of the Building Regulations in England as having an increased risk of
                  overheating, mainly due to the urban heat island effect. This does not
                  mean that homes outside this area are not at risk of overheating. It is
                  not applicable in Scotland.
                </InfoTooltip>
              </span>
            </>
          )}

        <span>Overheating risk region:</span>
        <span>
          {overheatingRiskRegion}
          <InfoTooltip>
            The risk of overheating is greater the further south and east you go in the
            UK.
          </InfoTooltip>
        </span>

        <label htmlFor="density">Location type:</label>
        <div>
          <Select
            className="input--auto-width"
            id="density"
            options={[
              { value: 'rural', display: 'rural' },
              {
                value: 'suburban',
                display:
                  'low rise urban / suburban (town or village situations with other buildings well spaced)',
              },
              {
                value: 'urban',
                display: 'dense urban (mostly closely spaced buildings of >3 storeys)',
              },
            ]}
            value={state.locationDensity}
            onChange={(val) => dispatchMerge({ locationDensity: val })}
          />
        </div>

        <label htmlFor="exposure">Exposure zone:</label>
        <div>
          <Select
            className="input--auto-width"
            id="exposure"
            options={[
              { value: 'very severe', display: 'very severe' },
              { value: 'severe', display: 'severe' },
              { value: 'moderate', display: 'moderate' },
              { value: 'sheltered', display: 'sheltered' },
            ]}
            value={state.exposureZone}
            onChange={(val) => dispatchMerge({ exposureZone: val })}
          />
          <Hint>
            You can find{' '}
            <a
              rel="noreferrer"
              target="_blank"
              href="https://www.arcgis.com/apps/dashboards/c49fb51fc54c4c569dd111162c48a083"
            >
              an exposure map here
            </a>
            . This map is based on the data in{' '}
            <i>
              BS 8104:1992 Code of Practice for assessing exposure of walls to wind-driven
              rain
            </i>
            . This is now quite old and the climate is changing with heavier rainfall and
            more rainfall generally. Use caution sin considering how this will affect a
            building.
          </Hint>
        </div>
      </FormGrid>

      <h3>Ground disturbance</h3>

      <div className="text-callout mb-15">
        <p>
          As the climate changes the risk of disturbance to the ground increases and so
          the risk of structural damage to buildings. This particularly affects buildings
          on heavy clay soils. Further
          <a
            href="https://www.bgs.ac.uk/news/maps-show-the-real-threat-of-climate-related-subsidence-to-british-homes-and-properties/"
            rel="noreferrer"
            target="_blank"
          >
            information and guidance is available from BGS
          </a>
          .
        </p>
        <p>
          ShrinkSwell data can be found using{' '}
          <a
            href="https://www.arcgis.com/apps/dashboards/b00f049dc2f7498991e7f00207e42eef"
            rel="noreferrer"
            target="_blank"
          >
            this map
          </a>
          .
        </p>
      </div>

      <FormGrid>
        <label htmlFor="subsidence2030">
          GeoClimateGB ShrinkSwell 2030 Average Probability:
        </label>
        <div>
          <Select
            className="input--auto-width"
            id="subsidence2030"
            options={[
              opt('probable' as const),
              opt('possible' as const),
              opt('improbable' as const),
            ]}
            value={state.subsidence2030}
            onChange={(subsidence2030) => dispatchMerge({ subsidence2030 })}
          />
        </div>

        <label htmlFor="subsidence2050">
          GeoClimateGB ShrinkSwell 2050 Average Probability:
        </label>
        <div>
          <Select
            className="input--auto-width"
            id="subsidence2050"
            options={[
              opt('probable' as const),
              opt('possible' as const),
              opt('improbable' as const),
            ]}
            value={state.subsidence2050}
            onChange={(subsidence2050) => dispatchMerge({ subsidence2050 })}
          />
        </div>

        <label htmlFor="soilHeaveRisk">Future soil heave risk:</label>
        <div>
          <Select
            className="input--auto-width"
            id="soilHeaveRisk"
            options={[
              { value: '1', display: '1 (low risk)' },
              { value: '2', display: '2' },
              { value: '3', display: '3' },
              { value: '4', display: '4' },
              { value: '5', display: '5 (high risk)' },
            ]}
            value={state.soilHeaveRisk}
            onChange={(val) => dispatchMerge({ soilHeaveRisk: val })}
          />
          <Hint>
            Data available at the National Trust{' '}
            <a
              rel="noreferrer"
              target="_blank"
              href="https://experience.arcgis.com/experience/0295557a52b5446595fc4ba6a97161bb/page/Page/"
            >
              climate risks map
            </a>
            .
          </Hint>
        </div>
      </FormGrid>

      <h3>Risk of flooding</h3>

      <div className="text-callout mb-15">
        This data can be found using the{' '}
        <a
          href="https://flood-warning-information.service.gov.uk/long-term-flood-risk/map"
          rel="noreferrer"
          target="_blank"
        >
          uk.gov flood risk map
        </a>
        .
      </div>

      <FormGrid>
        <span>Surface water:</span>
        <div>
          <Select
            id="surface_water"
            options={[
              { value: 'HIGH', display: 'high risk' },
              { value: 'MED', display: 'medium risk' },
              { value: 'LOW', display: 'low risk' },
              { value: 'VLOW', display: 'very low risk' },
            ]}
            value={state.floodingSurfaceWater}
            onChange={(val) =>
              dispatchMerge({
                floodingSurfaceWater: val,
              })
            }
          />
        </div>

        <span>Rivers and sea:</span>
        <div>
          <Select
            id="rivers_and_sea"
            options={[
              { value: 'HIGH', display: 'high risk' },
              { value: 'MED', display: 'medium risk' },
              { value: 'LOW', display: 'low risk' },
              { value: 'VLOW', display: 'very low risk' },
            ]}
            value={state.floodingRiversAndSea}
            onChange={(val) => dispatchMerge({ floodingRiversAndSea: val })}
          />
        </div>

        <span>Reservoirs:</span>
        <span>
          <Select
            id="reservoirs"
            className="input--auto-width"
            options={[
              {
                value: 'WITHIN',
                display: 'there is a risk of flooding from reservoirs',
              },
              {
                value: 'OUTWITH',
                display: 'unlikely in this area',
              },
            ]}
            value={state.floodingReservoirs}
            onChange={(val) => dispatchMerge({ floodingReservoirs: val })}
          />
        </span>

        <label htmlFor="groundwater">Groundwater:</label>
        <span>
          <Select
            id="groundwater"
            options={[
              {
                value: 'UNLIKELY',
                display: 'unlikely in this area',
              },
              {
                value: 'POSSIBLE',
                display: 'possible',
              },
            ]}
            value={state.floodingGroundwater}
            onChange={(val) => dispatchMerge({ floodingGroundwater: val })}
          />
        </span>
      </FormGrid>

      <h3>Radon</h3>

      <div className="text-callout mb-15">
        <p>
          Radon is a naturally occurring radioactive gas that is a known cancer risk.
          Levels in homes vary due to differing geology and in an individual home can
          depend on floor construction and levels and types of ventilation. Where a home
          has an existing high level of radon, or it could be made unacceptably high by
          making the home more air-tight, it could affect recommendations for retrofit and
          priorities for householders and clients.
        </p>

        <p>
          This data can be found using the{' '}
          <a
            href="https://www.ukradon.org/information/ukmaps"
            rel="noreferrer"
            target="_blank"
          >
            UK Radon map
          </a>
          .
        </p>
      </div>

      <FormGrid>
        <label htmlFor="radon">Radon risk:</label>
        <span>
          <Select
            id="radon"
            className="input--auto-width"
            options={[
              { value: 'LOW', display: 'less than 1%' },
              { value: '1-3', display: '1-3%' },
              { value: '3-5', display: '3-5%' },
              { value: '5-10', display: '5-10%' },
              { value: '10-30', display: '10-30%' },
              { value: '30', display: 'greater than 30%' },
            ]}
            value={state.radonRisk}
            onChange={(val) => dispatchMerge({ radonRisk: val })}
          />{' '}
          chance of a high level of radon
        </span>
      </FormGrid>

      <h3>Local air quality and noise pollution</h3>

      <FormGrid>
        <label htmlFor="smokeControlZone">Within a smoke control zone:</label>
        <div>
          <Select
            id="smokeControlZone"
            className="input--auto-width"
            options={[
              { display: 'yes', value: 'yes' },
              { display: 'no', value: 'no' },
              { display: 'unknown', value: 'unknown' },
            ]}
            onChange={(val) => {
              dispatchMerge({ smokeControlZone: yesNoToBoolean(val) });
            }}
            value={booleanToYesNo(state.smokeControlZone)}
          />
          <Hint>
            Many homes in the UK, especially in urban areas, are in smoke control zones.
            In these areas it is not permitted to release smoke from a chimney and only
            approved fuel burning appliances or smokeless fuels may be used. See{' '}
            <a
              href="https://uk-air.defra.gov.uk/data/sca/"
              rel="noreferrer"
              target="_blank"
            >
              DEFRA smoke control zone map
            </a>
            .
          </Hint>
        </div>

        <label htmlFor="externalAirPollution">External air pollution:</label>
        <div>
          <Select
            id="externalAirPollution"
            className="input--auto-width"
            options={[
              { display: 'exceeds WHO limits', value: 'yes' },
              { display: 'does not exceed WHO limits', value: 'no' },
              { display: 'unknown', value: 'unknown' },
            ]}
            onChange={(val) => {
              dispatchMerge({ externalAirPollution: yesNoToBoolean(val) });
            }}
            value={booleanToYesNo(state.externalAirPollution)}
          />
          <Hint>
            If the local environment is heavily polluted this may affect choices for
            ventilation systems and materials finishes. Search on{' '}
            <a href="https://addresspollution.org/" rel="noreferrer" target="_blank">
              addresspollution.org
            </a>
            .
          </Hint>
        </div>

        <label htmlFor="externalNoisePollution">Close to a major source of noise?</label>
        <div>
          <TextInput
            id="externalNoisePollution"
            value={state.externalNoisePollution}
            onChange={(val) => dispatchMerge({ externalNoisePollution: val })}
          />
          <Hint>
            e.g. a major road, railway or airport. If the local environment is noisy this
            may limit the ability of residents to open windows for ventilation and cooling
            and so this should be taken into account in the assessment and design of
            ventilation systems.
          </Hint>
        </div>
      </FormGrid>

      <h3>Planning and statutory considerations</h3>

      <FormGrid>
        <WithOriginInput
          data={state.localPlanningAuthority}
          labelText="Local planning authority"
          emptyText="Enter a postcode for automatic lookup"
          displayComponent={(localPlanningAuthority) => <>{localPlanningAuthority}</>}
          initialValue={''}
          onChange={(val) => dispatchMerge({ localPlanningAuthority: val })}
          inputComponent={(_box, localPlanningAuthority) => (
            <TextInput
              id="localPlanningAuthority"
              value={localPlanningAuthority}
              onChange={(val) =>
                dispatchMerge({
                  localPlanningAuthority: withUserData(state.localPlanningAuthority, val),
                })
              }
            />
          )}
          hintComponent={() => (
            <Hint>
              Overide this if the planning authority is different to the local
              authority/council. For example, the planning authority for National Parks is
              usually the National Park Authority rather than the local authority. You can
              check the planning authority at{' '}
              <a
                rel="noreferrer"
                target="_blank"
                href="https://www.planningportal.co.uk/find-your-local-planning-authority"
              >
                the Planning Portal
              </a>
              .
            </Hint>
          )}
          inputId="localPlanningAuthority"
        />

        <label htmlFor="aonb">
          <abbr title="Area of Outstanding Natural Beauty">AONB</abbr>:
        </label>
        <span>
          <Select
            id="aonb"
            className="input--auto-width"
            options={[{ value: 'none', display: '(none)' }]}
            sections={[
              {
                name: 'England',
                options: [
                  { value: 'Arnside and Silverdale', display: 'Arnside and Silverdale' },
                  { value: 'Blackdown Hills', display: 'Blackdown Hills' },
                  { value: 'Cannock Chase', display: 'Cannock Chase' },
                  { value: 'Chichester Harbour', display: 'Chichester Harbour' },
                  { value: 'Chilterns', display: 'Chilterns' },
                  { value: 'Cornwall', display: 'Cornwall' },
                  { value: 'Cotswolds', display: 'Cotswolds' },
                  {
                    value: 'Cranborne Chase and West Wiltshire Downs',
                    display: 'Cranborne Chase and West Wiltshire Downs',
                  },
                  { value: 'Dedham Vale', display: 'Dedham Vale' },
                  { value: 'Dorset', display: 'Dorset' },
                  { value: 'East Devon', display: 'East Devon' },
                  { value: 'Forest of Bowland', display: 'Forest of Bowland' },
                  { value: 'High Weald', display: 'High Weald' },
                  { value: 'Howardian Hills', display: 'Howardian Hills' },
                  { value: 'Isle of Wight', display: 'Isle of Wight' },
                  { value: 'Isles of Scilly', display: 'Isles of Scilly' },
                  { value: 'Kent Downs', display: 'Kent Downs' },
                  { value: 'Lincolnshire Wolds', display: 'Lincolnshire Wolds' },
                  { value: 'Malvern Hills', display: 'Malvern Hills' },
                  { value: 'Mendip Hills', display: 'Mendip Hills' },
                  { value: 'Nidderdale', display: 'Nidderdale' },
                  { value: 'Norfolk Coast', display: 'Norfolk Coast' },
                  { value: 'North Devon Coast', display: 'North Devon Coast' },
                  { value: 'North Pennines', display: 'North Pennines' },
                  { value: 'Northumberland Coast', display: 'Northumberland Coast' },
                  { value: 'North Wessex Downs', display: 'North Wessex Downs' },
                  { value: 'Quantock Hills', display: 'Quantock Hills' },
                  { value: 'Shropshire Hills', display: 'Shropshire Hills' },
                  { value: 'Solway Coast', display: 'Solway Coast' },
                  { value: 'South Devon', display: 'South Devon' },
                  {
                    value: 'Suffold & Essex Coast & Heaths',
                    display: 'Suffold & Essex Coast & Heaths',
                  },
                  { value: 'Surrey Hills', display: 'Surrey Hills' },
                  { value: 'Tamar Valley', display: 'Tamar Valley' },
                ],
              },
              {
                name: 'Cymru / Wales',
                options: [
                  { value: 'Ynys Môn', display: 'Ynys Môn / Anglesey' },
                  {
                    value: 'Bryniau Clwyd a Dyffryn Dyfrdwy',
                    display:
                      'Bryniau Clwyd a Dyffryn Dyfrdwy / Clwydian Range and Dee Valley',
                  },
                  { value: 'Gŵyr', display: 'Gŵyr / Gower' },
                  { value: 'Llŷn', display: 'Llŷn' },
                  { value: 'Dyffryn Gwy', display: 'Dyffryn Gwy / Wye Valley' },
                  {
                    value: 'Wye Valley',
                    display: 'Wye Valley (partly in England)',
                  },
                ],
              },
              {
                name: 'Northern Ireland',
                options: [
                  { value: 'Antrim Coast and Glens', display: 'Antrim Coast and Glens' },
                  { value: 'Binevenagh', display: 'Binevenagh' },
                  { value: 'Causeway Coast', display: 'Causeway Coast' },
                  { value: 'Lagan Valley', display: 'Lagan Valley' },
                  { value: 'Mourne Mountains', display: 'Mourne Mountains' },
                  { value: 'Ring of Gullion', display: 'Ring of Gullion' },
                  { value: 'Sperrins', display: 'Sperrins' },
                  { value: 'Strangford and Lecale', display: 'Strangford and Lecale' },
                ],
              },
            ]}
            value={state.areaOfOutstandingNaturalBeauty}
            onChange={(val) => dispatchMerge({ areaOfOutstandingNaturalBeauty: val })}
          />
          <Hint>
            If a property is in an AONB there may be planning restrictions and policies
            similar to those in place in national parks which will need to be taken into
            account in planning. Find map at{' '}
            <a
              rel="noreferrer"
              target="_blank"
              href="https://magic.defra.gov.uk/magicmap.aspx"
            >
              DEFRA
            </a>
            .
          </Hint>
        </span>

        <label htmlFor="planningNotes">Notes:</label>
        <div>
          <Textarea
            id="planningNotes"
            value={state.planningNotes}
            onChange={(planningNotes) => dispatchMerge({ planningNotes })}
            style={{ width: '30em', height: '3lh' }}
          />
          <Hint>
            Include links to any relevant planning applications. These can usually be
            accessed via the local planning authority website or{' '}
            <a
              rel="noreferrer"
              target="_blank"
              href="https://www.planningportal.co.uk/find-your-local-planning-authority"
            >
              Planning Portal
            </a>
            .
          </Hint>
        </div>

        <label htmlFor="builtBand">Date built:</label>
        <div>
          <Select
            id="builtBand"
            options={[
              { value: 'before-1900', display: 'Before 1900' },
              { value: '1900-1929', display: '1900 - 1929' },
              { value: '1930-1949', display: '1930 - 1949' },
              { value: '1950-1966', display: '1950 - 1966' },
              { value: '1967-1975', display: '1967 - 1975' },
              { value: '1976-1982', display: '1976 - 1982' },
              { value: '1983-1990', display: '1983 - 1990' },
              { value: '1991-1995', display: '1991 - 1995' },
              { value: '1996-2002', display: '1996 - 2002' },
              { value: '2003-2006', display: '2003 - 2006' },
              { value: '2007-2011', display: '2007 - 2011' },
              { value: '2012-onwards', display: '2012 onwards' },
            ]}
            value={state.builtBand}
            onChange={(builtBand) => dispatchMerge({ builtBand })}
          />
        </div>

        <label htmlFor="builtExact">Exact date:</label>
        <div>
          <TextInput
            id="builtExact"
            value={state.builtExact}
            onChange={(builtExact) => dispatchMerge({ builtExact })}
          />
          <Hint>Leave empty if not known</Hint>
        </div>
      </FormGrid>

      <VStack small>
        <VStack xsmall>
          <label className="mb-0" htmlFor="inConservationArea">
            Is the property in a conservation area?
          </label>
          <span>
            <Select
              id="inConservationArea"
              className="input--auto-width"
              options={[
                { display: 'yes', value: 'yes' },
                { display: 'no', value: 'no' },
                { display: 'unknown', value: 'unknown' },
              ]}
              onChange={(val) => {
                dispatchMerge({ inConservationArea: yesNoToBoolean(val) });
              }}
              value={booleanToYesNo(state.inConservationArea)}
            />
          </span>
        </VStack>

        <VStack xsmall>
          <label className="mb-0" htmlFor="listedStatus">
            Is the property listed?
          </label>
          <span>
            <Select
              id="listedStatus"
              options={[
                { value: 'NO', display: 'no' },
                { value: 'I', display: 'grade I' },
                { value: 'II', display: 'grade II' },
                { value: 'II*', display: 'grade II*' },
                { value: 'LOCAL', display: 'local listing' },
              ]}
              value={state.listedStatus}
              onChange={(listedStatus) => dispatchMerge({ listedStatus })}
            />
          </span>
        </VStack>

        {state.listedStatus !== 'NO' && (
          <>
            <label htmlFor="listedStatusNotes">
              Link to listing and further details:
            </label>
            <div>
              <TextInput
                style={{ width: '20rem' }}
                id="listedStatusNotes"
                value={state.listedStatusNotes}
                onChange={(listedStatusNotes) => dispatchMerge({ listedStatusNotes })}
              />
              <Hint>
                Listings can be looked up at{' '}
                <a
                  href="https://historicengland.org.uk/listing/the-list/"
                  rel="noreferrer"
                  target="_blank"
                >
                  Historic England
                </a>
                ,{' '}
                <a
                  href="https://cadw.gov.wales/advice-support/cof-cymru/search-cadw-records"
                  rel="noreferrer"
                  target="_blank"
                >
                  CADW (in Wales)
                </a>{' '}
                and{' '}
                <a
                  href="https://www.historicenvironment.scot/advice-and-support/listing-scheduling-and-designations/listed-buildings/search-for-a-listed-building/"
                  rel="noreferrer"
                  target="_blank"
                >
                  Historic Environment Scotland
                </a>
                .
              </Hint>
            </div>
          </>
        )}

        <VStack xsmall>
          <label className="mb-0" htmlFor="nearAncientMonument">
            Is the property within the curtilage of a scheduled ancient monument?
          </label>
          <span>
            <Select
              id="nearAncientMonument"
              className="input--auto-width"
              options={[
                { display: 'yes', value: 'yes' },
                { display: 'no', value: 'no' },
                { display: 'unknown', value: 'unknown' },
              ]}
              onChange={(val) => {
                dispatchMerge({ nearAncientMonument: yesNoToBoolean(val) });
              }}
              value={booleanToYesNo(state.nearAncientMonument)}
            />
          </span>
        </VStack>

        <VStack xsmall>
          <label className="mb-0" htmlFor="worldHeritageSite">
            Is the property within the boundary or the buffer zone of a World Heritage
            Site?
          </label>
          <span>
            <Select
              id="worldHeritageSite"
              className="input--auto-width"
              options={[
                { display: 'yes, within a World Heritage Site', value: 'within whs' },
                { display: 'yes, within buffer zone', value: 'within buffer zone' },
                { display: 'no', value: 'no' },
              ]}
              onChange={(worldHeritageSite) => dispatchMerge({ worldHeritageSite })}
              value={state.worldHeritageSite}
            />
          </span>
        </VStack>

        <VStack xsmall>
          <label className="mb-0" htmlFor="treeNotes">
            Are there any trees nearby that may be covered by Tree Preservation Orders
            (TPO) or within a conservation area?
          </label>
          <div className="mb-7">
            <Textarea
              className="mb-0"
              id="treeNotes"
              value={state.treeNotes}
              onChange={(treeNotes) => dispatchMerge({ treeNotes })}
              style={{ width: '30em', height: '3lh' }}
            />
            <Hint>
              Tree preservation orders curtail the cutting down, topping, lopping,
              uprooting, wilful damage or wilful destruction of trees without the local
              planning authority’s written consent. Cutting roots is also a prohibited
              activity and requires the authority’s consent. The presence of a TPO can
              therefore affect some choices of work.
            </Hint>
          </div>
        </VStack>

        <VStack xsmall>
          <label className="mb-0" htmlFor="siteOfSpecialScientificInterest">
            Is the property within or adjacent to a historic park or Site of Special
            Scientifc Interest?
          </label>
          <div>
            <Textarea
              className="mb-0"
              id="siteOfSpecialScientificInterest"
              value={state.siteOfSpecialScientificInterest}
              onChange={(siteOfSpecialScientificInterest) =>
                dispatchMerge({ siteOfSpecialScientificInterest })
              }
              style={{ width: '30em', height: '3lh' }}
            />
            <Hint>
              You can look up landscape and biodiversity desginations on{' '}
              <a
                href="https://magic.defra.gov.uk/MagicMap.aspx"
                rel="noreferrer"
                target="_blank"
              >
                this DEFRA map
              </a>
              .
            </Hint>
          </div>
        </VStack>
      </VStack>

      <h3>Utilities</h3>

      <b>Electricity</b>

      <FormGrid>
        <label htmlFor="hasMainsElectricity">Mains electricity connection?</label>
        <div>
          <Select
            id="hasMainsElectricity"
            className="input--auto-width"
            options={[
              { display: 'yes', value: 'yes' },
              { display: 'no', value: 'no' },
              { display: 'unknown', value: 'unknown' },
            ]}
            onChange={(val) => {
              dispatchMerge({ hasMainsElectricity: yesNoToBoolean(val) });
            }}
            value={booleanToYesNo(state.hasMainsElectricity)}
          />
        </div>

        <label htmlFor="electricityDNO">Distribution Network Operator (DNO):</label>
        <div>
          <TextInput
            id="electricityDNO"
            value={state.electricityDNO}
            onChange={(electricityDNO) => dispatchMerge({ electricityDNO })}
          />
          <Hint>
            Can be checked at{' '}
            <a
              rel="noreferrer"
              target="_blank"
              href="https://www.energynetworks.org/customers/find-my-network-operator"
            >
              ENA
            </a>
            .
          </Hint>
        </div>

        <label htmlFor="electricityLooped">Supply potentially looped?</label>
        <div>
          <Select
            id="electricityLooped"
            className="input--auto-width"
            options={[
              { display: 'yes', value: 'yes' },
              { display: 'no', value: 'no' },
              { display: 'unknown', value: 'unknown' },
            ]}
            onChange={(val) => {
              dispatchMerge({ electricityLooped: yesNoToBoolean(val) });
            }}
            value={booleanToYesNo(state.electricityLooped)}
          />
          <Hint>
            Some homes, espeically older terraces and semi-detached homes, have a shared
            mains electricity supply between several homes. This reduces the capacity of
            the supply and can become a problem when adding heatpumps/ EVs/PVs. Most DNOs
            offer &lsquo;unlooping&rsquo;, but this is sometimes a charged-for service, so
            it can add to retrofit project budgets.
          </Hint>
        </div>

        <label htmlFor="electricityMPAN">
          Meter <abbr title="Meter Point Access Number">MPAN</abbr>:
        </label>
        <div>
          <TextInput
            id="electricityMPAN"
            value={state.electricityMPAN}
            onChange={(electricityMPAN) => dispatchMerge({ electricityMPAN })}
          />
        </div>

        <label htmlFor="electricitySmartMeter">Working electricity smart meter?</label>
        <div>
          <Select
            id="electricitySmartMeter"
            className="input--auto-width"
            options={[
              { display: 'yes', value: 'yes' },
              { display: 'no', value: 'no' },
              { display: 'unknown', value: 'unknown' },
            ]}
            onChange={(val) => {
              dispatchMerge({ electricitySmartMeter: yesNoToBoolean(val) });
            }}
            value={booleanToYesNo(state.electricitySmartMeter)}
          />
          <Hint>
            Can be checked at{' '}
            <a
              rel="noreferrer"
              target="_blank"
              href="https://smartmetercheck.citizensadvice.org.uk/"
            >
              Citizens Advice
            </a>
            .
          </Hint>
        </div>
      </FormGrid>

      <b>Gas</b>

      <FormGrid>
        <label htmlFor="hasMainsGas">Mains gas connection?</label>
        <div>
          <Select
            id="hasMainsGas"
            className="input--auto-width"
            options={[
              { display: 'yes', value: 'yes' },
              { display: 'no', value: 'no' },
              { display: 'unknown', value: 'unknown' },
            ]}
            onChange={(val) => {
              dispatchMerge({ hasMainsGas: yesNoToBoolean(val) });
            }}
            value={booleanToYesNo(state.hasMainsGas)}
          />
        </div>

        {state.hasMainsGas === true && (
          <>
            <label htmlFor="gasTransporter">Gas Transporter (Network):</label>
            <div>
              <TextInput
                id="gasTransporter"
                value={state.gasTransporter}
                onChange={(gasTransporter) => dispatchMerge({ gasTransporter })}
              />
              <Hint>
                Find this and MPRN at{' '}
                <a rel="noreferrer" target="_blank" href="https://findmysupplier.energy/">
                  findmysupplier.energy
                </a>
                .
              </Hint>
            </div>

            <label htmlFor="gasMPRN">MPRN:</label>
            <div>
              <TextInput
                id="gasMPRN"
                value={state.gasMPRN}
                onChange={(gasMPRN) => dispatchMerge({ gasMPRN })}
              />
            </div>

            <label htmlFor="gasSmartMeter">Working gas smart meter:</label>
            <div>
              <Select
                id="gasSmartMeter"
                className="input--auto-width"
                options={[
                  { display: 'yes', value: 'yes' },
                  { display: 'no', value: 'no' },
                  { display: 'unknown', value: 'unknown' },
                ]}
                onChange={(val) => {
                  dispatchMerge({ gasSmartMeter: yesNoToBoolean(val) });
                }}
                value={booleanToYesNo(state.gasSmartMeter)}
              />
              <Hint>
                Can be checked at{' '}
                <a
                  rel="noreferrer"
                  target="_blank"
                  href="https://smartmetercheck.citizensadvice.org.uk/"
                >
                  Citizens Advice
                </a>
                .
              </Hint>
            </div>
          </>
        )}
      </FormGrid>

      <b>Heat Networks</b>

      <div className="text-callout mb-15">
        <p>
          Planned and operational heat networks can be{' '}
          <a
            href="https://data.barbour-abi.com/smart-map/repd/desnz/"
            rel="noreferrer"
            target="_blank"
          >
            found on this map
          </a>
          . However, this may not be comprehensive and other local information may be
          available in your area (e.g.
          <a href="https://apps.london.gov.uk/heatmap/" rel="noreferrer" target="_blank">
            in London
          </a>
          ). Heat networks that run on renewables, heat pumps or waste energy can be a
          good solution in densely built up areas or flats where individual heat pumps or
          other low-carbon solutions for heating would be difficult, provided they are
          well designed and managed to minimise losses and be fair in charging. (Gas and
          biomass powered heat networks are not low carbon.)
        </p>
      </div>

      <FormGrid>
        <label htmlFor="heatNetworkInZone">In a planned heat network zone?</label>
        <div>
          <Select
            id="heatNetworkInZone"
            className="input--auto-width"
            options={[
              { display: 'yes', value: 'yes' },
              { display: 'no', value: 'no' },
              { display: 'unknown', value: 'unknown' },
            ]}
            onChange={(val) => {
              dispatchMerge({ heatNetworkInZone: yesNoToBoolean(val) });
            }}
            value={booleanToYesNo(state.heatNetworkInZone)}
          />
        </div>

        <label htmlFor="heatNetworkNotes">Notes and links to further details:</label>
        <div>
          <TextInput
            id="heatNetworkNotes"
            value={state.heatNetworkNotes}
            onChange={(heatNetworkNotes) => dispatchMerge({ heatNetworkNotes })}
          />
        </div>
      </FormGrid>

      <b>Water and sewage</b>

      <FormGrid>
        <label htmlFor="hasMainsWater">Mains water:</label>
        <div>
          <Select
            id="hasMainsWater"
            className="input--auto-width"
            options={[
              { display: 'yes', value: 'yes' },
              { display: 'no', value: 'no' },
              { display: 'unknown', value: 'unknown' },
            ]}
            onChange={(val) => {
              dispatchMerge({ hasMainsWater: yesNoToBoolean(val) });
            }}
            value={booleanToYesNo(state.hasMainsWater)}
          />
        </div>
        <label htmlFor="hasMainsSewer">Mains sewage:</label>
        <div>
          <Select
            id="hasMainsSewer"
            className="input--auto-width"
            options={[
              { display: 'yes', value: 'yes' },
              { display: 'no', value: 'no' },
              { display: 'unknown', value: 'unknown' },
            ]}
            onChange={(val) => {
              dispatchMerge({ hasMainsSewer: yesNoToBoolean(val) });
            }}
            value={booleanToYesNo(state.hasMainsSewer)}
          />
        </div>{' '}
      </FormGrid>
    </section>
  );
}

function Hint({ children }: { children: ReactNode }) {
  return <small style={{ display: 'block', width: '40em' }}>{children}</small>;
}

function yesNoToBoolean(val: 'yes' | 'no' | 'unknown') {
  switch (val) {
    case 'yes':
      return true;
    case 'no':
      return false;
    case 'unknown':
      return null;
  }
}

function booleanToYesNo(val: boolean | null) {
  switch (val) {
    case true:
      return 'yes';
    case false:
      return 'no';
    case null:
      return 'unknown';
  }
}

function getOverheatingRiskRegionName(regionName: RegionName | null) {
  switch (regionName) {
    case 'East Anglia':
    case 'South East England':
    case 'Southern England':
    case 'Thames':
      return 'London and South East';
    case 'Borders Scotland / Borders England':
    case 'East Pennines':
    case 'East Scotland':
    case 'Highland':
    case 'North East England':
    case 'North East Scotland':
    case 'North West England / South West Scotland':
    case 'Northern Ireland':
    case 'Orkney':
    case 'Shetland':
    case 'West Scotland':
    case 'Western Isles':
      return 'Northern England, Scotland or Northern Ireland';
    case 'Midlands':
    case 'Severn Wales / Severn England':
    case 'South West England':
    case 'Wales':
    case 'West Pennines Wales / West Pennines England':
      return 'Rest of England and Wales';
    case 'UK average':
    case null:
      return 'unknown';
  }
}

export const addressSearchModule: UiModule<State, Action, never> = {
  name: 'address search',
  component: AddressSearch,
  initialState: (): State => {
    return {
      initialUpdateComplete: false,
      externalDesignTemperature: { type: 'no data' },
      region: { type: 'no data' },
      elevation: { type: 'no data' },
      addressAlreadyResolved: false,
      ...pick(householdSchema.parse({}), fieldNames),
    };
  },
  effector: assertNever,
  reducer: (state: State, action: Action): [State] => {
    switch (action.type) {
      case 'use resolved address': {
        const { address, localAuthority, uprn, lsoa, elevation, coordinates } =
          action.data;
        return [
          {
            ...state,
            address: withDatabaseData(state.address, address),
            lowerLayerSuperOutputArea:
              lsoa !== null
                ? withDatabaseData(state.lowerLayerSuperOutputArea, lsoa)
                : state.lowerLayerSuperOutputArea,
            elevation:
              elevation !== null
                ? withDatabaseData(
                    state.elevation.type === 'user provided' &&
                      state.elevation.value !== null &&
                      state.elevation.value === 0
                      ? { type: 'no data' }
                      : state.elevation,
                    elevation,
                  )
                : state.elevation,
            uniquePropertyReferenceNumber: withDatabaseData(
              state.uniquePropertyReferenceNumber,
              uprn,
            ),
            localAuthority: withDatabaseData(state.localAuthority, localAuthority),
            localPlanningAuthority: withDatabaseData(
              state.localAuthority,
              localAuthority,
            ),
            region: withDatabaseData(
              state.region.type === 'user provided' &&
                state.region.value !== null &&
                state.region.value === 'UK average'
                ? { type: 'no data' }
                : state.region,
              Region.fromPostcode(address.postcode).unwrap().name,
            ),
            overheatingRisk:
              address.country === 'England'
                ? hasHighOverheatingRisk(address.postcode)
                  ? 'high risk'
                  : 'moderate risk'
                : 'outside England',
            frostAttackRisk: withDatabaseData<FrostAttackRisk, null>(
              state.frostAttackRisk,
              frostAttackRisk(address.postcode),
            ),
            latLong: coordinates,
          },
        ];
      }
      case 'merge data': {
        return [
          {
            ...state,
            ...action.state,
          },
        ];
      }
      case 'external data update': {
        if (state.initialUpdateComplete) {
          return [state];
        } else {
          return [
            {
              ...state,
              ...action.state,
              initialUpdateComplete: true,
            },
          ];
        }
      }
    }
  },
  shims: {
    extractUpdateAction: ({ project, currentModel }) => {
      const baselineScenario = project.data['master'];
      const household = baselineScenario?.household;
      if (baselineScenario === undefined || household === undefined) {
        return Result.ok([]);
      }

      const model = currentModel.mapErr(() => null).coalesce();

      const region: State['region'] =
        baselineScenario.region_full ??
        (baselineScenario.region !== undefined
          ? { type: 'user provided', value: baselineScenario.region }
          : { type: 'no data' });

      let externalDesignTemperature: State['externalDesignTemperature'];

      if (
        baselineScenario.externalDesignTemperatureOverride === undefined ||
        baselineScenario.externalDesignTemperatureOverride.enabled === false
      ) {
        externalDesignTemperature =
          model === null
            ? { type: 'no data' as const }
            : {
                type: 'from database' as const,
                value: model.normal.geography.cibseExternalDesignTemperature,
              };
      } else {
        externalDesignTemperature =
          model === null
            ? {
                type: 'user provided' as const,
                value: baselineScenario.externalDesignTemperatureOverride.value,
              }
            : {
                type: 'overridden' as const,
                value: baselineScenario.externalDesignTemperatureOverride.value,
                dbValue: model.normal.geography.cibseExternalDesignTemperature,
              };
      }

      const action: ExternalDataAction = {
        type: 'external data update',
        state: {
          externalDesignTemperature,
          // These first two should be moved out to the data schema at some point
          region,
          elevation:
            baselineScenario.altitude_full ??
            (baselineScenario.altitude !== undefined && baselineScenario.altitude !== ''
              ? { type: 'user provided', value: baselineScenario.altitude }
              : { type: 'no data' }),
          ...pick(household, fieldNames),
        },
      };

      if (household?.addressSearch.lookedUp === true) {
        action.state.addressAlreadyResolved = true;
      }

      return Result.ok([action]);
    },

    mutateLegacyData: (
      { project: projectRaw }: { project: unknown },
      _context: unknown,
      state: State,
    ) => {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const project = projectRaw as z.input<typeof projectSchema>;

      const baseline = project.data['master'];
      if (baseline === undefined) {
        throw new Error('No baseline scenario');
      }

      const household = baseline.household ?? {};

      delete household.address_1;
      delete household.address_2;
      delete household.address_3;
      delete household.address_town;
      delete household.address_postcode;
      delete household.address_country;
      delete household.address_la;
      delete household.address_lsoa;

      if (household.looked_up !== true) {
        household.looked_up =
          state.address.type === 'from database' || state.address.type === 'overridden';
      }

      household.address_full = state.address;
      household.address_la_full = state.localAuthority;
      household.address_lsoa_full = state.lowerLayerSuperOutputArea;
      household.local_planning_authority = state.localPlanningAuthority;
      household.house_type = state.propertyType;
      household.house_type_note = state.propertyTypeNotes;
      household.location_density = state.locationDensity ?? undefined;
      household.exposure = state.exposureZone ?? undefined;
      household.frostAttackRisk = state.frostAttackRisk;
      household.tenure = state.tenure;
      household.tenure_note = state.tenureNotes;
      household.landRegistryLink = state.landRegistryLink;
      household.overheatingRisk = state.overheatingRisk;
      household.latLong = state.latLong;
      household.flooding_rivers_sea = state.floodingRiversAndSea ?? undefined;
      household.flooding_surface_water = state.floodingSurfaceWater ?? undefined;
      household.flooding_reservoirs = state.floodingReservoirs ?? undefined;
      household.flooding_groundwater = state.floodingGroundwater ?? undefined;
      household.radon_risk = state.radonRisk ?? undefined;
      household.uniquePropertyReferenceNumber = state.uniquePropertyReferenceNumber;
      household.areaOfOutstandingNaturalBeauty = state.areaOfOutstandingNaturalBeauty;
      household.subsidence2030 = state.subsidence2030;
      household.subsidence2050 = state.subsidence2050;
      household.soilHeaveRisk = state.soilHeaveRisk;
      household.planningNotes = state.planningNotes;
      household.historic_age_band = state.builtBand;
      household.historic_age_precise = state.builtExact;
      household.historic_listed = state.listedStatus;
      household.historic_note = state.listedStatusNotes;
      household.historic_conserved = state.inConservationArea;
      household.treeNotes = state.treeNotes;
      household.mains_electricity = state.hasMainsElectricity;
      household.mains_gas = state.hasMainsGas;
      household.mains_water = state.hasMainsWater;
      household.mains_sewer = state.hasMainsSewer;
      household.electricityDNO = state.electricityDNO;
      household.electricityLooped = state.electricityLooped;
      household.electricityMPAN = state.electricityMPAN;
      household.electricitySmartMeter = state.electricitySmartMeter;
      household.gasTransporter = state.gasTransporter;
      household.gasMPRN = state.gasMPRN;
      household.gasSmartMeter = state.gasSmartMeter;

      for (const scenario of Object.values(project.data)) {
        if (scenario === undefined) {
          continue;
        }

        scenario.altitude = getWithOriginValue(state.elevation) ?? undefined;
        scenario.altitude_full = state.elevation;

        scenario.region = getWithOriginValue(state.region) ?? undefined;
        scenario.region_full = state.region;

        if (hasUserData(state.externalDesignTemperature)) {
          scenario.externalDesignTemperatureOverride = {
            enabled: true,
            value: state.externalDesignTemperature.value,
          };
        } else {
          scenario.externalDesignTemperatureOverride = {
            enabled: false,
            value: null,
          };
        }

        scenario.household = household;
      }
    },
  },
};
