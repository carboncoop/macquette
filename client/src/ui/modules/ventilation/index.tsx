import React from 'react';
import { assertNever } from '../../../helpers/assert-never';

import { z } from 'zod';
import { VentilationSystem } from '../../../data-schemas/libraries/ventilation-system';
import { projectSchema } from '../../../data-schemas/project';
import { Result } from '../../../helpers/result';
import { LockableContext } from '../../input-components/lockable-context';
import { AppContext } from '../../module-management/app-context';
import type { UiModule } from '../../module-management/module-type';
import {
  ClothesAction,
  ClothesDryingFacilities,
  ClothesState,
  clothesExtractor,
  clothesMutator,
  clothesReducer,
  initialClothesState,
} from './clothes-drying-facilities';
import {
  ExtractVents,
  ExtractVentsAction,
  ExtractVentsState,
  extractVentsExtractor,
  extractVentsMutator,
  extractVentsReducer,
  initialExtractVentsState,
} from './extract-vents';
import {
  StructuralInfiltration,
  StructuralInfiltrationAction,
  StructuralInfiltrationState,
  initialStructuralInfiltrationState,
  structuralInfiltrationExtractor,
  structuralInfiltrationMutator,
  structuralInfiltrationReducer,
} from './structural-infiltration';
import {
  VentilationSystemAction,
  VentilationSystem as VentilationSystemComponent,
  VentilationSystemState,
  initialVentilationSystemState,
  ventilationSystemExtractor,
  ventilationSystemMutator,
  ventilationSystemReducer,
} from './ventilation-system';
import {
  VentsFlues,
  VentsFluesAction,
  VentsFluesState,
  initialVentsFluesState,
  ventsFluesExtractor,
  ventsFluesMutator,
  ventsFluesReducer,
} from './vents-and-flues';
type State = {
  isBaseline: boolean;
  isLocked: boolean;
  ventilationSystemType: VentilationSystem['ventilation_type'];
  ventilationSystem: VentilationSystemState;
  clothes: ClothesState;
  ventsFlues: VentsFluesState;
  structuralInfiltration: StructuralInfiltrationState;
  extractVents: ExtractVentsState;
};
type Action =
  | { type: 'external update'; state: Partial<State> }
  | VentilationSystemAction
  | ClothesAction
  | VentsFluesAction
  | StructuralInfiltrationAction
  | ExtractVentsAction;

export const ventilationModule: UiModule<State, Action, never> = {
  name: 'ventilation',
  component: function Ventilation({ state, dispatch }) {
    return (
      <LockableContext.Provider value={{ locked: state.isLocked }}>
        <section className="line-top mb-45">
          <h3 className="ma-0 mb-15">Ventilation</h3>

          <VentilationSystemComponent
            state={state.ventilationSystem}
            dispatch={dispatch}
            isBaseline={state.isBaseline}
          />

          {['IE', 'PS'].includes(state.ventilationSystemType) && (
            <ExtractVents
              state={state.extractVents}
              dispatch={dispatch}
              isBaseline={state.isBaseline}
            />
          )}
        </section>

        <section className="line-top mb-45">
          <h3 className="ma-0 mb-15">Infiltration</h3>

          <StructuralInfiltration
            state={state.structuralInfiltration}
            dispatch={dispatch}
            isBaseline={state.isBaseline}
          />
          <VentsFlues
            state={state.ventsFlues}
            dispatch={dispatch}
            isBaseline={state.isBaseline}
          />
        </section>

        <ClothesDryingFacilities
          state={state.clothes}
          dispatch={dispatch}
          isBaseline={state.isBaseline}
        />
      </LockableContext.Provider>
    );
  },
  initialState: () => {
    return {
      isBaseline: true,
      isLocked: false,
      ventilationSystemType: 'NV',
      ventilationSystem: initialVentilationSystemState,
      clothes: initialClothesState,
      ventsFlues: initialVentsFluesState,
      structuralInfiltration: initialStructuralInfiltrationState,
      extractVents: initialExtractVentsState,
    };
  },
  reducer: (state, action) => {
    switch (action.type) {
      case 'external update':
        return [{ ...state, ...action.state }];

      case 'clothes/external update':
      case 'clothes/show modal':
      case 'clothes/add':
      case 'clothes/delete':
        state.clothes = clothesReducer(state.clothes, action);
        return [state];

      case 'vf/external update':
      case 'vf/show modal':
      case 'vf/add':
      case 'vf/modify':
      case 'vf/apply measure':
      case 'vf/delete':
        state.ventsFlues = ventsFluesReducer(state.ventsFlues, action);
        return [state];

      case 'ev/external update':
      case 'ev/show modal':
      case 'ev/add':
      case 'ev/modify':
      case 'ev/apply measure':
      case 'ev/delete':
        state.extractVents = extractVentsReducer(state.extractVents, action);
        return [state];

      case 'si/external update':
      case 'si/show modal':
      case 'si/modify':
      case 'si/apply measure':
        state.structuralInfiltration = structuralInfiltrationReducer(
          state.structuralInfiltration,
          action,
        );
        return [state];

      case 'sys/external update':
      case 'sys/show modal':
      case 'sys/replace':
      case 'sys/modify':
      case 'sys/apply measure':
        state.ventilationSystem = ventilationSystemReducer(
          state.ventilationSystem,
          action,
        );
        return [state];
    }
  },
  effector: assertNever,
  shims: {
    extractUpdateAction: ({ scenarioId, currentScenario, currentModel }: AppContext) => {
      return Result.ok([
        {
          type: 'external update',
          state: {
            isBaseline: scenarioId === 'master',
            isLocked: currentScenario?.locked ?? false,
            ventilationSystemType: currentScenario?.ventilation?.ventilation_type ?? 'NV',
          },
        },
        ...ventilationSystemExtractor(
          currentScenario,
          currentModel.mapErr(() => null).coalesce(),
        ),
        ...clothesExtractor(currentScenario),
        ...ventsFluesExtractor(currentScenario),
        ...extractVentsExtractor(currentScenario),
        ...structuralInfiltrationExtractor(
          currentScenario,
          currentModel.mapErr(() => null).coalesce(),
        ),
      ]);
    },
    mutateLegacyData(
      { project: projectRaw }: { project: unknown },
      { scenarioId }: { scenarioId: string | null },
      state: State,
    ) {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const project = projectRaw as z.input<typeof projectSchema>;
      if (scenarioId === null) {
        throw new Error("can't mutate nonexistent scenario");
      }
      const scenario = project.data[scenarioId];
      if (scenario === undefined) {
        throw new Error("can't mutate nonexistent scenario");
      }

      ventilationSystemMutator(scenario, state.ventilationSystem);
      clothesMutator(scenario, state.clothes);
      ventsFluesMutator(scenario, state.ventsFlues);
      extractVentsMutator(scenario, state.extractVents);
      structuralInfiltrationMutator(scenario, state.structuralInfiltration);
    },
  },
};
