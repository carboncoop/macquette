import React from 'react';

import { max, omit, sortBy } from 'lodash';
import { z } from 'zod';
import type {
  ExtractVent,
  ExtractVentMeasure,
} from '../../../data-schemas/libraries/extract-ventilation-points';
import { projectSchema } from '../../../data-schemas/project';
import { Scenario } from '../../../data-schemas/scenario';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { DeleteIcon, EditIcon, HammerIcon, PlusIcon, X } from '../../icons';
import { Button } from '../../input-components/button';
import {
  SelectExtractVent,
  SelectExtractVentMeasure,
} from '../../input-components/library-picker/extract-vents';
import { NumberInput } from '../../input-components/number';
import { TextInput } from '../../input-components/text';
import { NumberOutput } from '../../output-components/numeric';
import { colourForStatus } from '../fabric/components/measure-status';

export type ExtractVentsAction =
  | { type: 'ev/external update'; state: Pick<ExtractVentsState, 'items'> }
  | { type: 'ev/show modal'; modal: ExtractVentsState['modal'] }
  | {
      type: 'ev/add';
      item: ExtractVent;
    }
  | {
      type: 'ev/modify';
      id: number;
      update: Partial<Item['inputs']>;
    }
  | {
      type: 'ev/apply measure';
      ids: number[] | null;
      measure: ExtractVentMeasure;
    }
  | { type: 'ev/delete'; id: number };

export type ExtractVentsState = {
  modal:
    | {
        type: 'picker';
      }
    | {
        type: 'measure picker';
        replaces: number | null;
      }
    | null;
  items: Item[];
};

export const initialExtractVentsState: ExtractVentsState = { modal: null, items: [] };

type BaseItem = {
  type: 'item';
  id: number;
  inputs: {
    location: string;
    overrideVentilationRate: boolean;
    ventilationRate: number | null;
  };
  libraryItem: ExtractVent;
};
type MeasureItem = {
  type: 'measure';
  id: number;
  inputs: {
    location: string;
    overrideVentilationRate: boolean;
    ventilationRate: number | null;
  };
  libraryItem: ExtractVentMeasure;
};
type Item = BaseItem | MeasureItem;

export function extractVentsReducer(
  state: ExtractVentsState,
  action: ExtractVentsAction,
): ExtractVentsState {
  switch (action.type) {
    case 'ev/external update':
      return { ...state, ...action.state };
    case 'ev/show modal':
      return { modal: action.modal, items: state.items };
    case 'ev/add': {
      const nextId = (max(state.items.map((item) => item.id)) ?? 0) + 1;
      state.items.push({
        type: 'item',
        id: nextId,
        inputs: {
          location: '',
          overrideVentilationRate: false,
          ventilationRate: null,
        },
        libraryItem: action.item,
      });
      state.modal = null;
      return state;
    }
    case 'ev/modify': {
      for (const item of state.items) {
        if (item.id !== action.id) {
          continue;
        }
        item.inputs = {
          ...item.inputs,
          ...action.update,
        };
      }
      return state;
    }
    case 'ev/apply measure': {
      if (action.ids !== null) {
        const ids = action.ids;
        return {
          items: state.items.map((item) =>
            ids.includes(item.id)
              ? {
                  type: 'measure',
                  id: item.id,
                  inputs: {
                    location: item.inputs.location,
                    overrideVentilationRate: false,
                    ventilationRate: null,
                  },
                  libraryItem: action.measure,
                }
              : item,
          ),
          modal: null,
        };
      } else {
        const nextId = (max(state.items.map((item) => item.id)) ?? 0) + 1;
        state.items.push({
          type: 'measure',
          id: nextId,
          inputs: {
            location: '',
            overrideVentilationRate: false,
            ventilationRate: action.measure.ventilation_rate,
          },
          libraryItem: action.measure,
        });
        state.modal = null;
      }
      return state;
    }
    case 'ev/delete':
      return {
        modal: state.modal,
        items: state.items.filter((facility) => facility.id !== action.id),
      };
  }
}

export function extractVentsMutator(
  scenario: Exclude<z.input<typeof projectSchema>['data'][string], undefined>,
  state: ExtractVentsState,
): void {
  scenario.ventilation = scenario.ventilation ?? {};
  scenario.ventilation.EVP = state.items.map((item) => ({
    id: item.id,
    tag: item.libraryItem.tag,
    name: item.libraryItem.name,
    source: item.libraryItem.source,
    overrideVentilationRate: item.inputs.overrideVentilationRate,
    ventilation_rate: item.inputs.overrideVentilationRate
      ? (item.inputs.ventilationRate ?? '')
      : item.libraryItem.ventilation_rate,
    location: item.inputs.location,
    libraryItem: item.libraryItem,
  }));

  scenario.measures = scenario.measures ?? {};
  scenario.measures.ventilation = scenario.measures.ventilation ?? {};
  scenario.measures.ventilation.extract_ventilation_points = Object.fromEntries(
    state.items
      .filter(
        (item): item is Extract<Item, { type: 'measure' }> => item.type === 'measure',
      )
      .map((item) => [
        item.id,
        {
          measure: {
            id: item.id,
            location: item.inputs.location,
            ...item.libraryItem,
            quantity: 1,
            cost_total: item.libraryItem.cost,
          },
        },
      ]),
  );
}

export function extractVentsExtractor(scenario: Scenario): ExtractVentsAction[] {
  const items: Item[] = [];
  const measures = scenario?.measures?.ventilation?.extract_ventilation_points ?? {};

  for (const item of scenario?.ventilation?.EVP ?? []) {
    if (item.id === '') {
      throw new Error('id was not a number');
    }

    if (item.id in measures) {
      // SAFETY: presence checked in the if statement
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const measure = measures[item.id]!.measure;
      items.push({
        type: 'measure' as const,
        id: item.id,
        inputs: {
          location: item.location,
          overrideVentilationRate: item.overrideVentilationRate,
          ventilationRate: coalesceEmptyString(item.ventilation_rate, 0),
        },
        libraryItem: {
          ...omit(measure, ['ventilation_rate', 'cost_total', 'quantity']),
          ventilation_rate: coalesceEmptyString(measure.ventilation_rate, 0),
        },
      });
    } else {
      items.push({
        type: 'item' as const,
        id: item.id,
        inputs: {
          location: item.location,
          overrideVentilationRate: item.overrideVentilationRate,
          ventilationRate: coalesceEmptyString(item.ventilation_rate, 0),
        },
        libraryItem: item.libraryItem ?? {
          tag: item.tag,
          name: item.name,
          source: item.source,
          ventilation_rate: coalesceEmptyString(item.ventilation_rate, 0) ?? 0,
        },
      });
    }
  }

  return [
    {
      type: 'ev/external update',
      state: { items: sortBy(items, ['id']) },
    },
  ];
}

export function ExtractVents({
  state,
  dispatch,
  isBaseline,
}: {
  state: ExtractVentsState;
  dispatch: (a: ExtractVentsAction) => void;
  isBaseline: boolean;
}) {
  return (
    <section className="mb-45">
      <h4 className="mb-15">
        Extract ventilation points: intermittent fans and passive vents
      </h4>
      {state.items.length > 0 && (
        <table className="table mt-15 table--vertical-middle" style={{ width: 'auto' }}>
          <thead>
            <tr style={{ backgroundColor: 'var(--grey-800)' }}>
              <th>Tag</th>
              <th>Name</th>
              <th>Location</th>
              <th className="text-nowrap">Ventilation rate</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {state.items.map(({ id, type, inputs, libraryItem }) => (
              <tr key={id}>
                <td className="text-nowrap">
                  {libraryItem.tag}
                  {type === 'measure' && (
                    <span
                      className="circle ml-7"
                      title="Measure applied"
                      style={{
                        backgroundColor: `var(${colourForStatus['new measure']})`,
                      }}
                    />
                  )}
                </td>
                <td>{libraryItem.name}</td>
                <td>
                  <TextInput
                    value={inputs.location}
                    onChange={(location) =>
                      dispatch({ type: 'ev/modify', id, update: { location } })
                    }
                    className="mb-0"
                  />
                </td>
                <td className="text-right text-tabular-nums">
                  {inputs.overrideVentilationRate ? (
                    <span className="d-flex gap-7 justify-content-end">
                      <NumberInput
                        className="mb-0"
                        value={inputs.ventilationRate}
                        style={{ width: '2rem' }}
                        onChange={(ventilationRate) =>
                          dispatch({ type: 'ev/modify', id, update: { ventilationRate } })
                        }
                        unit="m³/h"
                      />
                      <Button
                        icon={X}
                        onClick={() =>
                          dispatch({
                            type: 'ev/modify',
                            id,
                            update: { overrideVentilationRate: false },
                          })
                        }
                      />
                    </span>
                  ) : (
                    <span className="d-flex gap-7 justify-content-end">
                      <NumberOutput value={libraryItem.ventilation_rate} unit="m³/h" />
                      <Button
                        icon={EditIcon}
                        onClick={() =>
                          dispatch({
                            type: 'ev/modify',
                            id,
                            update: { overrideVentilationRate: true },
                          })
                        }
                      />
                    </span>
                  )}
                </td>
                <td className="text-nowrap">
                  <div className="d-flex gap-7 justify-content-end">
                    {!isBaseline && (
                      <Button
                        icon={HammerIcon}
                        title={type === 'measure' ? 'Replace measure' : 'Apply measure'}
                        onClick={() =>
                          dispatch({
                            type: 'ev/show modal',
                            modal: { type: 'measure picker', replaces: id },
                          })
                        }
                      />
                    )}
                    <Button
                      icon={DeleteIcon}
                      onClick={() => dispatch({ type: 'ev/delete', id })}
                    />
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      )}

      {state.modal?.type === 'picker' && (
        <SelectExtractVent
          onSelect={(item) => dispatch({ type: 'ev/add', item })}
          onClose={() => dispatch({ type: 'ev/show modal', modal: null })}
        />
      )}

      {state.modal?.type === 'measure picker' && (
        <SelectExtractVentMeasure
          onSelect={(measure) => {
            if (state.modal?.type !== 'measure picker') {
              return;
            }
            dispatch({
              type: 'ev/apply measure',
              ids: state.modal.replaces === null ? null : [state.modal.replaces],
              measure,
            });
          }}
          onClose={() => dispatch({ type: 'ev/show modal', modal: null })}
        />
      )}

      {isBaseline ? (
        <Button
          title="New extract vent"
          icon={PlusIcon}
          onClick={() => dispatch({ type: 'ev/show modal', modal: { type: 'picker' } })}
        />
      ) : (
        <Button
          title="New extract vent measure"
          icon={HammerIcon}
          onClick={() =>
            dispatch({
              type: 'ev/show modal',
              modal: { type: 'measure picker', replaces: null },
            })
          }
        />
      )}
    </section>
  );
}
