import React, { Dispatch } from 'react';
import { z } from 'zod';
import { Scenario, scenarioSchema } from '../../../data-schemas/scenario';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';
import { UnvalidatedTimePeriod } from '../../../periods/validation';
import { UndoIcon } from '../../icons';
import { Button } from '../../input-components/button';
import { CheckboxInput } from '../../input-components/checkbox';
import { FormGrid, LabelWithInfo } from '../../input-components/forms';
import { HeatingTable } from '../../input-components/heating-table';
import { NumberInput } from '../../input-components/number';
import type { Action as ParentAction } from './top';

export type Action =
  | {
      type: 'shd/external update';
      state: State;
    }
  | {
      type: 'shd/set living area';
      value: number | null;
    }
  | {
      type: 'shd/set target temperature';
      value: number | null;
    }
  | {
      type: 'shd/set heating off summer';
      value: boolean;
    }
  | {
      type: 'shd/update heating pattern';
      part: 'weekday' | 'weekend';
      pattern: UnvalidatedTimePeriod[];
    }
  | { type: 'shd/reset heating pattern to household'; part: 'weekday' | 'weekend' };

export function isSpaceHeatingDemandAction(action: ParentAction): action is Action {
  return action.type.startsWith('shd/');
}

export type State = {
  livingArea: number | null;
  targetTemperature: number | null;
  heatingOffSummer: boolean;
  heatingHours: {
    weekday: UnvalidatedTimePeriod[];
    weekend: UnvalidatedTimePeriod[];
  };
  householdHeatingHours: {
    weekday: UnvalidatedTimePeriod[];
    weekend: UnvalidatedTimePeriod[];
  };
};

export const initialState: State = {
  livingArea: null,
  targetTemperature: null,
  heatingOffSummer: false,
  heatingHours: {
    weekday: [], // Initial three empty periods are populated by the extractor
    weekend: [],
  },
  householdHeatingHours: {
    weekday: [{ start: null, end: null }],
    weekend: [{ start: null, end: null }],
  },
};

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'shd/external update':
      return action.state;
    case 'shd/set living area':
      return { ...state, livingArea: action.value };
    case 'shd/set target temperature':
      return { ...state, targetTemperature: action.value };
    case 'shd/set heating off summer':
      return { ...state, heatingOffSummer: action.value };
    case 'shd/update heating pattern':
      return {
        ...state,
        heatingHours: { ...state.heatingHours, [action.part]: action.pattern },
      };
    case 'shd/reset heating pattern to household':
      return {
        ...state,
        heatingHours: {
          ...state.heatingHours,
          [action.part]: state.householdHeatingHours[action.part],
        },
      };
  }
}

export function SpaceHeatingDemand({
  state,
  dispatch,
}: {
  state: State;
  dispatch: Dispatch<Action>;
}) {
  return (
    <section className="line-top mb-45">
      <h3 className="ma-0 mb-15">Space heating demand</h3>

      <FormGrid>
        <LabelWithInfo
          htmlFor="living-area"
          infoText={
            "The 'Living Area' is the floor area of the lounge or living room or largest reception room in the home, including any rooms not separated from this by doors. It does not, however, extend over more than one storey, even when stairs enter the living area directly"
          }
        >
          Living area
        </LabelWithInfo>
        <NumberInput
          name="living-area"
          value={state.livingArea}
          unit="m²"
          onChange={(value) => dispatch({ type: 'shd/set living area', value })}
        />
        <LabelWithInfo
          htmlFor="Target temperature"
          infoText={
            "The target temperature will usually be based on the householder's normal thermostat setting. If the thermostat is in the living room, its setting would be the demand temperature. If it is outside the living room (e.g. hall), add 3ºC to the thermostat setting to estimate the temperature achieved in the living room."
          }
        >
          Target temperature
        </LabelWithInfo>
        <NumberInput
          name="target-temperature"
          value={state.targetTemperature}
          unit="°C"
          onChange={(value) => dispatch({ type: 'shd/set target temperature', value })}
        />

        <LabelWithInfo
          htmlFor="heating-off-summer"
          infoText="Summer is defined from June to September, inclusive"
        >
          Heating off for the whole summer
        </LabelWithInfo>
        <CheckboxInput
          name="heating-off-summer"
          value={state.heatingOffSummer}
          onChange={(value) => dispatch({ type: 'shd/set heating off summer', value })}
        />
      </FormGrid>
      <h4>Heating hours</h4>
      {(['weekday', 'weekend'] as const).map((part) => (
        <section key={part} className="mb-15 mr-15 d-ib">
          <HeatingTable
            label={weekPartDisplay(part)}
            periods={state.heatingHours[part]}
            onChange={(newPeriods) =>
              dispatch({
                type: 'shd/update heating pattern',
                part,
                pattern: newPeriods,
              })
            }
          />
          <Button
            icon={UndoIcon}
            title="Reset to household questionnaire"
            onClick={() =>
              dispatch({
                type: 'shd/reset heating pattern to household',
                part,
              })
            }
          />
        </section>
      ))}
    </section>
  );
}

export function extractor(currentScenario: Scenario): Action[] {
  let livingArea: number | null =
    coalesceEmptyString(currentScenario?.temperature?.living_area, null) ?? null;
  if (Number.isNaN(livingArea)) livingArea = 0;
  const targetTemperature: number | null =
    coalesceEmptyString(currentScenario?.temperature?.target, null) ?? null;
  const heatingOffSummer: boolean =
    currentScenario?.space_heating?.heating_off_summer ?? false;
  const householdHeatingHours = {
    weekday: currentScenario?.household?.heatingHoursWeekday ?? [
      { start: null, end: null },
      { start: null, end: null },
      { start: null, end: null },
    ],
    weekend: currentScenario?.household?.heatingHoursWeekend ?? [
      { start: null, end: null },
      { start: null, end: null },
      { start: null, end: null },
    ],
  };
  const heatingHours =
    currentScenario?.space_heating?.heatingHours ?? householdHeatingHours;
  return [
    {
      type: 'shd/external update',
      state: {
        livingArea,
        targetTemperature,
        heatingOffSummer,
        heatingHours,
        householdHeatingHours,
      },
    },
  ];
}

export function mutator(
  scenario: Exclude<z.input<typeof scenarioSchema>, undefined>,
  state: State,
) {
  if (scenario.temperature === undefined) scenario.temperature = {};
  scenario.temperature.living_area = state.livingArea;
  scenario.temperature.target = state.targetTemperature;
  if (scenario.space_heating === undefined) scenario.space_heating = {};
  scenario.space_heating.heating_off_summer = state.heatingOffSummer;
  scenario.space_heating.heatingHours = state.heatingHours;
}

export function weekPartDisplay(part: 'weekday' | 'weekend'): string {
  switch (part) {
    case 'weekday':
      return 'Weekday';
    case 'weekend':
      return 'Weekend';
  }
}
