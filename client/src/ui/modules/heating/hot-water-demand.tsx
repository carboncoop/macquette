import React from 'react';

import { max, omit } from 'lodash';
import { z } from 'zod';
import {
  WaterUsageItem,
  WaterUsageMeasure,
} from '../../../data-schemas/libraries/water-usage';
import { Scenario, scenarioSchema } from '../../../data-schemas/scenario';
import { DeepPartial, safeMerge } from '../../../helpers/safe-merge';
import { Model } from '../../../model/model';
import { DeleteIcon, EditIcon, HammerIcon, PlusIcon, X } from '../../icons';
import { Button } from '../../input-components/button';
import { CheckboxInput } from '../../input-components/checkbox';
import { FormGrid } from '../../input-components/forms';
import {
  SelectWaterUsageItem,
  SelectWaterUsageMeasure,
} from '../../input-components/library-picker/water-usage';
import { NumberInput } from '../../input-components/number';
import { NumberOutput } from '../../output-components/numeric';
import { colourForStatus } from '../fabric/components/measure-status';
import type { Action as ParentAction } from './top';

export type Action =
  | {
      type: 'hwd/external update';
      state: Pick<State, 'lowWaterUseDesign' | 'energyUse' | 'items'>;
    }
  | {
      type: 'hwd/modify';
      update: DeepPartial<Pick<State, 'lowWaterUseDesign' | 'energyUse'>>;
    }
  | { type: 'hwd/show modal'; modal: State['modal'] }
  | {
      type: 'hwd/add';
      itemType: 'measure';
      libraryItem: WaterUsageMeasure;
    }
  | {
      type: 'hwd/add';
      itemType: 'item';
      libraryItem: WaterUsageItem;
    }
  | { type: 'hwd/delete'; id: number };

export function isHotWaterDemandAction(action: ParentAction): action is Action {
  return action.type.startsWith('hwd/');
}

export type State = {
  modal: 'picker' | 'measure picker' | null;
  lowWaterUseDesign: boolean;
  energyUse: { type: 'from sap' } | { type: 'override'; value: number | null };
  items: Item[];
};

export const initialState: State = {
  modal: null,
  lowWaterUseDesign: false,
  energyUse: { type: 'from sap' },
  items: [],
};

export type Item =
  | {
      type: 'item';
      id: number;
      libraryItem: WaterUsageItem;
    }
  | {
      type: 'measure';
      id: number;
      libraryItem: WaterUsageMeasure;
    };

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case 'hwd/external update':
      return { ...state, ...action.state };
    case 'hwd/modify':
      return safeMerge(state, action.update);
    case 'hwd/show modal':
      return { ...state, modal: action.modal };
    case 'hwd/add': {
      const nextId = (max(state.items.map((item) => item.id)) ?? 0) + 1;
      if (action.itemType === 'measure') {
        state.items.push({
          type: 'measure',
          id: nextId,
          libraryItem: action.libraryItem,
        });
      } else {
        state.items.push({
          type: 'item',
          id: nextId,
          libraryItem: action.libraryItem,
        });
      }
      state.modal = null;
      return state;
    }
    case 'hwd/delete':
      return {
        ...state,
        items: state.items.filter((item) => item.id !== action.id),
      };
  }
}

export function mutator(
  scenario: Exclude<z.input<typeof scenarioSchema>, undefined>,
  state: State,
): void {
  scenario.water_heating ??= {};
  scenario.water_heating.low_water_use_design = state.lowWaterUseDesign;

  if (state.energyUse.type === 'from sap') {
    scenario.water_heating.override_annual_energy_content = false;
    delete scenario.water_heating.annual_energy_content;
  } else {
    scenario.water_heating.override_annual_energy_content = true;
    scenario.water_heating.annual_energy_content = state.energyUse.value ?? undefined;
  }

  scenario.water_heating.water_usage = state.items.map((item) => ({
    id: item.id,
    tag: item.libraryItem.tag,
    name: item.libraryItem.name,
    source: item.libraryItem.source,
  }));

  scenario.measures ??= {};
  scenario.measures.water_heating ??= {};
  scenario.measures.water_heating.water_usage = Object.fromEntries(
    state.items
      .filter(
        (item): item is Extract<Item, { type: 'measure' }> => item.type === 'measure',
      )
      .map((item) => [
        item.id,
        {
          measure: {
            ...item.libraryItem,
            cost_total: item.libraryItem.cost,
            quantity: 1,
          },
        },
      ]),
  );
}

export function extractor(scenario: Scenario): Action[] {
  const items: Item[] = [];

  const waterUsage = scenario?.water_heating?.water_usage ?? [];
  const waterUsageMeasures = scenario?.measures?.water_heating?.water_usage ?? {};

  for (const item of waterUsage) {
    if (item.id in waterUsageMeasures) {
      // SAFETY: presence checked in the if statement
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const measure = waterUsageMeasures[item.id]!;
      items.push({ type: 'measure', id: item.id, libraryItem: measure.measure });
    } else {
      items.push({ type: 'item', id: item.id, libraryItem: omit(item, ['id']) });
    }
  }

  return [
    {
      type: 'hwd/external update',
      state: {
        lowWaterUseDesign: scenario?.water_heating?.low_water_use_design ?? false,
        energyUse:
          scenario?.water_heating?.override_annual_energy_content === true
            ? {
                type: 'override',
                value: scenario?.water_heating?.annual_energy_content ?? null,
              }
            : { type: 'from sap' },
        items,
      },
    },
  ];
}

export function HotWaterDemand({
  state,
  model,
  dispatch,
  isBaseline,
}: {
  state: State;
  model: Model | null;
  dispatch: (a: Action) => void;
  isBaseline: boolean;
}) {
  return (
    <section className="line-top mb-45">
      <h3 className="ma-0 mb-15">Hot water demand</h3>

      <FormGrid>
        <label htmlFor="lowWaterUseDesign">
          All water use is not more than 125 litres/person/day
        </label>
        <CheckboxInput
          id="lowWaterUseDesign"
          value={state.lowWaterUseDesign}
          onChange={(lowWaterUseDesign) =>
            dispatch({
              type: 'hwd/modify',
              update: { lowWaterUseDesign },
            })
          }
        />

        {state.energyUse.type === 'override' ? (
          <>
            <label htmlFor="energyUse">Water heating annual energy use</label>
            <span className="d-flex gap-7">
              <NumberInput
                id="energyUse"
                className="mb-0"
                unit="kWh"
                value={state.energyUse.value}
                style={{ width: '2rem' }}
                onChange={(value) =>
                  dispatch({
                    type: 'hwd/modify',
                    update: { energyUse: { value } },
                  })
                }
              />
              <Button
                icon={X}
                title={`Use SAP value (${
                  model !== null
                    ? Math.round(model.normal.waterCommon.hotWaterEnergyContentAnnualSap)
                    : '-'
                }kWh) instead`}
                onClick={() =>
                  dispatch({
                    type: 'hwd/modify',
                    update: { energyUse: { type: 'from sap' } },
                  })
                }
              />
            </span>
          </>
        ) : (
          <>
            <span>Water heating annual energy use</span>
            <span className="d-flex gap-7">
              <NumberOutput
                value={model?.normal.waterCommon.hotWaterEnergyContentAnnualSap}
                dp={0}
                unit="kWh"
              />
              <Button
                icon={EditIcon}
                title="Override"
                onClick={() =>
                  dispatch({
                    type: 'hwd/modify',
                    update: { energyUse: { type: 'override' } },
                  })
                }
              />
            </span>
          </>
        )}

        <span>
          Annual average hot water usage V<sub>d,average</sub>
        </span>
        <span>
          <NumberOutput
            value={model?.normal.waterCommon.dailyHotWaterUsageLitresMeanAnnual}
            unit="litres/day"
          />
        </span>
      </FormGrid>

      <div>
        <h4>Water efficiency</h4>

        {state.items.length > 0 && (
          <table className="table mt-15 table--vertical-middle" style={{ width: 'auto' }}>
            <thead>
              <tr style={{ backgroundColor: 'var(--grey-800)' }}>
                <th>Tag</th>
                <th>Name</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {state.items.map(({ id, type, libraryItem }) => (
                <tr key={id}>
                  <td>
                    {libraryItem.tag}
                    {type === 'measure' && (
                      <span
                        className="circle ml-7"
                        title="Measure applied"
                        style={{
                          backgroundColor: `var(${colourForStatus['new measure']})`,
                        }}
                      />
                    )}
                  </td>
                  <td>{libraryItem.name}</td>
                  <td>
                    <Button
                      title="Delete"
                      icon={DeleteIcon}
                      onClick={() => dispatch({ type: 'hwd/delete', id })}
                    />
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        )}

        {state.modal === 'picker' && (
          <SelectWaterUsageItem
            onSelect={(libraryItem) =>
              dispatch({ type: 'hwd/add', itemType: 'item', libraryItem })
            }
            onClose={() => dispatch({ type: 'hwd/show modal', modal: null })}
          />
        )}

        {state.modal === 'measure picker' && (
          <SelectWaterUsageMeasure
            onSelect={(libraryItem) =>
              dispatch({ type: 'hwd/add', itemType: 'measure', libraryItem })
            }
            onClose={() => dispatch({ type: 'hwd/show modal', modal: null })}
          />
        )}

        {isBaseline ? (
          <Button
            title="New pre-existing water efficiency measure"
            icon={PlusIcon}
            onClick={() => dispatch({ type: 'hwd/show modal', modal: 'picker' })}
          />
        ) : (
          <Button
            title="New water efficiency measure"
            icon={HammerIcon}
            onClick={() =>
              dispatch({
                type: 'hwd/show modal',
                modal: 'measure picker',
              })
            }
          />
        )}
      </div>
    </section>
  );
}
