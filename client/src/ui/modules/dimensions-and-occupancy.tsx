import React from 'react';
import { z } from 'zod';
import { projectSchema } from '../../data-schemas/project';
import { coalesceEmptyString } from '../../data-schemas/scenario/value-schemas';
import { Result } from '../../helpers/result';
import { Model } from '../../model/model';
import { EditIcon, PlusIcon, X } from '../icons';
import { Button } from '../input-components/button';
import { InfoTooltip } from '../input-components/forms';
import { LockableContext } from '../input-components/lockable-context';
import { NumberInput } from '../input-components/number';
import { TextInput } from '../input-components/text';
import { UiModule } from '../module-management/module-type';
import { NumberOutput } from '../output-components/numeric';

export type LoadedState = {
  isLocked: boolean;
  model: Model | null;
  scenarioName: string;
  floors: Array<{
    name: string;
    area: number | null;
    height: number | null;
  }>;
  occupancyOverride: {
    enabled: boolean;
    value: number | null;
  };
  volumeReductionOverride: {
    enabled: boolean;
    value: number | null;
  };
};
export type State = 'loading' | LoadedState;
export type Action =
  | {
      type: 'external data update';
      newState: LoadedState;
    }
  | {
      type: 'set scenario name';
      scenarioName: string;
    }
  | {
      type: 'set floor data';
      index: number;
      floorData: Partial<LoadedState['floors'][number]>;
    }
  | { type: 'delete floor'; index: number }
  | { type: 'add floor' }
  | { type: 'override occupancy'; enabled: boolean }
  | { type: 'set occupancy override'; value: number | null }
  | { type: 'override volume reduction'; enabled: boolean }
  | { type: 'set volume reduction override'; value: number | null };

export const dimensionsAndOccupancy: UiModule<State, Action, never> = {
  name: 'dimensions and occupancy',
  effector: () => {
    throw new Error('unreachable');
  },
  initialState: () => 'loading',
  reducer: (state, action) => {
    if (action.type === 'external data update') {
      return [action.newState];
    }
    if (state === 'loading') {
      return [state];
    }
    switch (action.type) {
      case 'set scenario name':
        return [{ ...state, scenarioName: action.scenarioName }];
      case 'set floor data': {
        const floor = state.floors[action.index];
        if (floor === undefined) {
          return [state];
        }
        return [
          {
            ...state,
            floors: [
              ...state.floors.slice(0, action.index),
              { ...floor, ...action.floorData },
              ...state.floors.slice(action.index + 1),
            ],
          },
        ];
      }
      case 'delete floor': {
        return [
          {
            ...state,
            floors: [
              ...state.floors.slice(0, action.index),
              ...state.floors.slice(action.index + 1),
            ],
          },
        ];
      }
      case 'add floor': {
        const name =
          state.floors.length === 0
            ? 'Ground Floor'
            : `${state.floors.length}${ordinalNumberSuffix(state.floors.length)} Floor`;
        return [
          {
            ...state,
            floors: [
              ...state.floors,
              {
                name,
                area: 0,
                height: 0,
              },
            ],
          },
        ];
      }
      case 'override occupancy':
        return [
          {
            ...state,
            occupancyOverride: { ...state.occupancyOverride, enabled: action.enabled },
          },
        ];
      case 'set occupancy override':
        return [
          {
            ...state,
            occupancyOverride: { ...state.occupancyOverride, value: action.value },
          },
        ];
      case 'override volume reduction':
        return [
          {
            ...state,
            volumeReductionOverride: {
              ...state.volumeReductionOverride,
              enabled: action.enabled,
            },
          },
        ];
      case 'set volume reduction override':
        return [
          {
            ...state,
            volumeReductionOverride: {
              ...state.volumeReductionOverride,
              value: action.value,
            },
          },
        ];
    }
  },
  component: function DimensionsAndOccupancy({ state, dispatch }) {
    if (state === 'loading') {
      return <>Loading...</>;
    }
    const model = state.model;
    return (
      <LockableContext.Provider value={{ locked: state.isLocked }}>
        <section className="line-top mb-45">
          <label htmlFor="scenario-name">
            <h3>Scenario name</h3>
          </label>
          <p>
            <TextInput
              id="scenario-name"
              value={state.scenarioName}
              onChange={(value) =>
                dispatch({ type: 'set scenario name', scenarioName: value })
              }
            />
          </p>
        </section>
        <section className="mb-45">
          <h3>Dimensions</h3>
          {state.floors.length > 0 && (
            <table
              className="table mt-15 mb-0 table--vertical-middle"
              style={{ width: 'auto' }}
            >
              <thead style={{ backgroundColor: 'var(--grey-800)' }}>
                <tr>
                  <th>Name</th>
                  <th>Area</th>
                  <th>Storey height</th>
                  <th>Volume</th>
                  <td></td>
                </tr>
              </thead>
              <tbody>
                {state.floors.map(({ name, area, height }, idx) => (
                  <tr key={idx}>
                    <td>
                      <TextInput
                        value={name}
                        onChange={(value) =>
                          dispatch({
                            type: 'set floor data',
                            index: idx,
                            floorData: { name: value },
                          })
                        }
                      />
                    </td>
                    <td>
                      <NumberInput
                        value={area}
                        unit="m²"
                        onChange={(value) =>
                          dispatch({
                            type: 'set floor data',
                            index: idx,
                            floorData: { area: value },
                          })
                        }
                      />
                      <span className="ml-15">×</span>
                    </td>
                    <td>
                      <NumberInput
                        value={height}
                        unit="m"
                        onChange={(value) =>
                          dispatch({
                            type: 'set floor data',
                            index: idx,
                            floorData: { height: value },
                          })
                        }
                      />
                      <span className="ml-15">=</span>
                    </td>
                    <td>
                      <NumberOutput
                        value={model?.normal.floors.floors[idx]?.volume}
                        unit="m³"
                        dp={2}
                      />
                    </td>
                    <td>
                      <Button
                        onClick={() => dispatch({ type: 'delete floor', index: idx })}
                        title="Delete"
                      />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          )}
          <Button
            icon={PlusIcon}
            onClick={() => dispatch({ type: 'add floor' })}
            title="Add new floor"
          />
        </section>
        <section>
          <h4>Area and volume</h4>
          <div className="form-grid__root">
            <span>Total floor area</span>
            <span>
              <NumberOutput
                value={model?.normal.floors.totalFloorArea}
                unit="m²"
                dp={2}
              />
            </span>
            <span>
              Form factor
              <InfoTooltip>
                Form factor is the ratio between heat loss surface area and internal floor
                area. A form factor of 2 or less is ideal in terms of fabric efficiency,
                and a form factor above 5 will be harder to make very energy efficient.
              </InfoTooltip>
            </span>
            <span>
              <NumberOutput value={model?.normal.fabric.formFactor} dp={1} />
            </span>
            {(model?.normal.modelBehaviourFlags.floors.applyVolumeReduction ?? false) && (
              <>
                <span>Volume reduction for internal walls and floors</span>
                <span>
                  {state.volumeReductionOverride.enabled ? (
                    <>
                      <NumberInput
                        value={
                          state.volumeReductionOverride.value === null
                            ? null
                            : state.volumeReductionOverride.value * 100
                        }
                        unit="%"
                        onChange={(value) => {
                          dispatch({
                            type: 'set volume reduction override',
                            value: value !== null ? value / 100 : null,
                          });
                        }}
                      />
                      <Button
                        className="ml-15"
                        icon={X}
                        title="Reset to default"
                        onClick={() =>
                          dispatch({
                            type: 'override volume reduction',
                            enabled: false,
                          })
                        }
                      />
                    </>
                  ) : (
                    <>
                      <NumberOutput
                        value={
                          model === null
                            ? null
                            : model.normal.floors.volumeReduction * 100
                        }
                        unit="%"
                        dp={2}
                      />
                      <Button
                        className="ml-15"
                        icon={EditIcon}
                        title="Override"
                        onClick={() =>
                          dispatch({
                            type: 'override volume reduction',
                            enabled: true,
                          })
                        }
                      />
                    </>
                  )}
                </span>
                <span>Gross volume</span>
                <span>
                  <NumberOutput
                    value={model?.normal.floors.grossVolume}
                    unit="m³"
                    dp={2}
                  />
                </span>
              </>
            )}
            <span>Total volume</span>
            <span>
              <NumberOutput value={model?.normal.floors.totalVolume} unit="m³" dp={2} />
            </span>
          </div>
        </section>
        <section>
          <h3>Occupancy</h3>
          <div className="form-grid__root">
            <span>Occupancy</span>
            <span>
              {state.occupancyOverride.enabled ? (
                <>
                  <NumberInput
                    value={state.occupancyOverride.value}
                    onChange={(value) => {
                      dispatch({ type: 'set occupancy override', value });
                    }}
                  />
                  <Button
                    className="ml-15"
                    icon={X}
                    title="Reset to SAP value"
                    onClick={() =>
                      dispatch({
                        type: 'override occupancy',
                        enabled: false,
                      })
                    }
                  />
                </>
              ) : (
                <>
                  <NumberOutput
                    value={model?.normal.occupancy.sapOccupancy}
                    unit={
                      model?.normal.occupancy.sapOccupancy === 1 ? 'person' : 'people'
                    }
                    dp={2}
                  />
                  <Button
                    className="ml-15"
                    icon={EditIcon}
                    title="Override"
                    onClick={() =>
                      dispatch({
                        type: 'override occupancy',
                        enabled: true,
                      })
                    }
                  />
                </>
              )}
            </span>
          </div>
        </section>
      </LockableContext.Provider>
    );
  },
  shims: {
    extractUpdateAction: (context) => {
      const model = context.currentModel
        .mapErr((err) => {
          console.error(err);
          return null;
        })
        .coalesce();
      const scenarioName = context.currentScenario?.scenario_name ?? '';
      const floors: LoadedState['floors'] = (context.currentScenario?.floors ?? []).map(
        ({ name, area, height }) => ({
          name,
          area: coalesceEmptyString(area, 0),
          height: coalesceEmptyString(height, 0),
        }),
      );
      return Result.ok([
        {
          type: 'external data update',
          newState: {
            isLocked: context.currentScenario?.locked ?? false,
            model,
            scenarioName,
            floors,
            occupancyOverride: {
              enabled: context.currentScenario?.use_custom_occupancy ?? false,
              value:
                coalesceEmptyString(context.currentScenario?.custom_occupancy, null) ??
                null,
            },
            volumeReductionOverride: context.currentScenario?.volumeReductionOverride ?? {
              enabled: false,
              value: null,
            },
          },
        },
      ]);
    },
    mutateLegacyData: ({ project: projectRaw }, { scenarioId }, state) => {
      if (state === 'loading') return;
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const project = projectRaw as z.input<typeof projectSchema>;
      if (scenarioId === null) {
        console.error('scenarioId was null');
        return;
      }
      const currentScenario = project.data[scenarioId];
      if (currentScenario === undefined) {
        console.error('currentScenario was undefined');
        return;
      }
      currentScenario.scenario_name = state.scenarioName;
      currentScenario.floors = state.floors;
      currentScenario.use_custom_occupancy = state.occupancyOverride.enabled;
      currentScenario.custom_occupancy = state.occupancyOverride.value;
      currentScenario.volumeReductionOverride = state.volumeReductionOverride;
    },
  },
};

export function ordinalNumberSuffix(n: number): string {
  if (n >= 11 && n <= 13) {
    return 'th';
  }
  const decimal = n.toString(10);
  if (decimal.includes('.')) return 'th';
  switch (decimal[decimal.length - 1]) {
    case '1':
      return 'st';
    case '2':
      return 'nd';
    case '3':
      return 'rd';
    default:
      return 'th';
  }
}
