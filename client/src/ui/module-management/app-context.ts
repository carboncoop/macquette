import { cloneDeep, isEqual, isEqualWith, omit } from 'lodash';
import { z } from 'zod';
import { resultSchema } from '../../data-schemas/helpers/result';
import { Project, projectSchema } from '../../data-schemas/project';
import { Scenario } from '../../data-schemas/scenario';
import { cache } from '../../helpers/cache-decorators';
import { Result } from '../../helpers/result';
import { Model } from '../../model/model';
import { Externals, externals, getCurrentRoute } from '../../shims/typed-globals';
import { Route } from '../../ui/routes';

/** Inspect various pieces of global state and return them bundled up */
export class AppContext {
  @cache get route(): Route {
    return getCurrentRoute();
  }
  @cache get project(): Project {
    return projectSchema.parse(externals().project);
  }
  get scenarioId(): string | null {
    return this.route.type === 'with scenario' ? this.route.scenarioId : null;
  }
  get currentScenario(): Exclude<Scenario, undefined> {
    const currentScenario =
      this.scenarioId === null
        ? this.project.data['master']
        : this.project.data[this.scenarioId];
    if (currentScenario === undefined) {
      throw new Error('Current scenario not found in project');
    }
    return currentScenario;
  }
  @cache get currentModel(): Result<Model, Error> {
    // We parse this here rather than in the scenario schema because:
    // 1. It would cause cyclical imports
    // 2. If currentScenario.model doesn't fit this shape (which could happen due
    //    due to an unexpected error), we still need to be able to run the model.
    const parsed = resultSchema(z.instanceof(Model), z.instanceof(Error)).safeParse(
      this.currentScenario.model,
    );
    return parsed.success
      ? parsed.data
      : Result.err(new Error('model was not parseable'));
  }
  get appName(): string {
    return externals().appName;
  }
  get userId(): string {
    return externals().userId;
  }
}

export function getAppContext(): AppContext {
  return new AppContext();
}

export function applyDataMutator(
  mutator: (toMutate: Pick<Externals, 'project'>, context: AppContext) => void,
  source: { moduleName: string; instanceKey: string | null },
) {
  const origProject = cloneDeep(externals().project);
  mutator(externals(), getAppContext());

  const origMetadata = omit(origProject, 'data');
  const newMetadata = omit(externals().project, 'data');

  if (
    !isEqualWith(
      origProject['data'],
      externals().project['data'],
      (a: unknown, b: unknown) => {
        // Because we attach the Model object to the scenario object, we need to make sure
        // we don't recursively check every lazily-evaluated model output here
        if (a instanceof Model || b instanceof Model) {
          return true;
        } else {
          return undefined;
        }
      },
    )
  ) {
    // (UN)SAFETY: we are relying on the external interface having a certain API that we
    // can't check in advance.
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    externals().update({ dataChanged: true, source });
  } else if (!isEqual(origMetadata, newMetadata)) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    externals().update({ dataChanged: false, source });
  }
}
