export type Target = {
  label: string;
  value: number;
};

export const spaceHeatingDemand: Target[] = [
  { label: 'Min target', value: 20 },
  { label: 'Max target', value: 70 },
  { label: 'UK average', value: 120 },
];

export const energyUsePerPerson: Target[] = [
  { label: '70% heating saving', value: 8.6 },
  { label: 'UK average', value: 19.6 },
];

export const peakHeatLoad: Target[] = [
  { label: 'Small', value: 3 },
  { label: 'Medium', value: 6 },
  { label: 'Large', value: 10 },
  { label: 'Very large', value: 15 },
];

export const energyUseIntensity: Target[] = [
  { label: 'New build max', value: 35 },
  { label: 'Retrofit max', value: 60 },
];
