import { parsePostcode } from '../../helpers/postcodes';

export type FrostAttackRisk =
  | 'high risk'
  | 'within a postcode with high risk'
  | 'not at high risk';

export function frostAttackRisk(postcode: string): FrostAttackRisk {
  const [outcode] = parsePostcode(postcode);

  for (const pattern of highRiskPostcodes) {
    if (pattern.exec(outcode) !== null) {
      return 'high risk';
    }
  }

  for (const pattern of maybeHighRiskPostcodes) {
    if (pattern.exec(outcode) !== null) {
      return 'within a postcode with high risk';
    }
  }

  return 'not at high risk';
}

// Taken from NHBC Standards 2019:
// https://nhbc-standards.co.uk/2019/6-superstructure-excluding-roofs/6-1-external-masonry-walls/6-1-6-exposure/
const highRiskPostcodes = [
  /^BB(4|8|9|10|11)$/,
  /^BD24$/,
  /^BL(0|8)$/,
  /^CA9$/,
  /^CF(44|45|46|47|48)$/,
  /^DG(4|13)$/,
  /^FK(18|19|20|21)$/,
  /^HX7$/,
  /^KA(17|18)$/,
  /^LA10$/,
  /^LD(4|5|6)$/,
  /^LL(23|24|25)$/,
  /^ML10$/,
  /^NP(2|3)$/,
  /^OL(1|2|3|4|5|11|12|13|14|15|16)$/,
  /^PA36$/,
  /^PH(30|31)$/,
  /^SA(20|40)$/,
  /^SK13$/,
  /^SY(18|19|25)$/,
];

const maybeHighRiskPostcodes = [
  /^AA(3|5)$/,
  /^BB(1|2|3|5|6|7|12)$/,
  /^BD(13|15|20|21|22|23)$/,
  /^BL(1|2|7|9)$/,
  /^CA(5|6|7|8|10|11|12|13|16|17|19|20|22|23)$/,
  /^CF(8|37|39|40|41|42|43)$/,
  /^CH7$/,
  /^DD(8|9)$/,
  /^DE(4|6)$/,
  /^DG(1|2|3|6|7|8|10|11|12|14)$/,
  /^DH8$/,
  /^DL(8|11|12|13)$/,
  /^EH(14|23|26|27|28|43|44|45|46|47|48|55)$/,
  /^FK(1|8|11|12|13|14|15|16|17|47)$/,
  /^G(62|63|64|65|72|74|75|76|77|81|82|83|84)$/,
  /^HD(3|4|7|8)$/,
  /^HG3$/,
  /^HR(2|3|5)$/,
  /^HX(2|4|6|7)$/,
  /^IV(1|3|4|6|7|12|13|14|15|16|17|18|19|22|23|24|25|26|27|28|40|54)$/,
  /^KA(1|3|4|5|6|16|19|26)$/,
  /^KW(3|5|6|7|8|9|10|11|12|13|14)$/,
  /^KY13$/,
  /^LA(2|6|8|9|12|20|21|22|23)$/,
  /^LD(1|2|3|7|8)$/,
  /^LL(11|15|16|20|21|22|26|27|28|32|33|40|41|54|55|57)$/,
  /^M24$/,
  /^ML(1|2|3|6|7|8|9|11|12)$/,
  /^NE(19|46|47|48|49|66|71)$/,
  /^NP(1|4|5|6|7|8|44)$/,
  /^OL(6|7|8|9|10)$/,
  /^PA(23|24|25|26|27|32|33|34|35|37|38|40|41)$/,
  /^PH(1|2|3|4|5|6|7|8|9|10|11|15|16|17|18|19|20|21)$/,
  /^PH(22|23|25|26|32|33|34|35|36|37|38|39|40|41)$/,
  /^S(6|10|11|30)$/,
  /^SA(9|10|11|13|19|32|33|39|44|48)$/,
  /^SK(6|10|11|12|14|15|16|17)$/,
  /^ST(10|13)$/,
  /^SY(10|16|17|20|21|22|23|24)$/,
  /^TD(1|2|5|8|11|71)$/,
  /^TS9$/,
  /^YO(21|18|21|22)$/,
];
