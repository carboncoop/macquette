import { z } from 'zod';
import { MeanInternalTemperatureInput } from '.';
import { scenarioSchema } from '../../../data-schemas/scenario';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';

export function extractMeanInternalTemperatureInputFromLegacy(
  scenario: z.infer<typeof scenarioSchema>,
): MeanInternalTemperatureInput {
  return {
    targetTemperature: scenario?.temperature?.target ?? 21,
    livingArea:
      coalesceEmptyString(scenario?.temperature?.living_area, undefined) ?? null,
  };
}
