import { sum } from '../../../helpers/array-reducers';
import { cache, cacheMonth } from '../../../helpers/cache-decorators';
import { externalTemperature } from '../../datasets';
import { Month } from '../../enums/month';
import { Region } from '../../enums/region';
import { ModelBehaviourFlags } from '../behaviour-version';
import { Fabric } from '../fabric';
import { Floors } from '../floors';
import { HeatGain } from '../heat-gain';
import { timeConstant, utilisationFactor } from '../heat-gain-utilisation-factor';
import { HeatLoss } from '../heat-loss';
import { HeatingPattern } from '../heating-hours';
import { HeatingSystems as HeatingSystemsModule } from '../heating-systems';
import { HeatingSystemSpaceParameters } from '../heating-systems/space-heating';
import { Mode } from '../mode';
import { Weekly, averageWeekly, mapWeekly } from './weekly';

export type MeanInternalTemperatureInput = {
  targetTemperature: number;
  livingArea: number | null;
};

export type MeanInternalTemperatureDependencies = {
  floors: Pick<Floors, 'totalFloorArea'>;
  fabric: Pick<Fabric, 'thermalMassParameter'>;
  heatGain: Pick<HeatGain, 'heatGain' | 'heatGainSolar'>;
  heatLoss: Pick<HeatLoss, 'heatLossMonthly' | 'heatLossParameter'>;
  region: Region;
  heatingSystems: Pick<HeatingSystemsModule, 'spaceHeatingSystems'>;
  heatingHours: Weekly<Pick<HeatingPattern, 'hoursOffPattern'>>;
  mode: Mode;
  modelBehaviourFlags: Pick<ModelBehaviourFlags, 'meanInternalTemperature'>;
};

export type MeanInternalTemperatureFlags =
  MeanInternalTemperatureDependencies['modelBehaviourFlags']['meanInternalTemperature'];

type HeatingSystems =
  | { main: null; secondMain: null }
  | {
      main: HeatingSystemSpaceParameters;
      secondMain: null;
    }
  | {
      main: HeatingSystemSpaceParameters;
      secondMain: HeatingSystemSpaceParameters;
    };

function constructHeatingSystems(
  input: MeanInternalTemperatureInput,
  dependencies: MeanInternalTemperatureDependencies,
): HeatingSystems {
  const filteredHeatingSystems = dependencies.heatingSystems.spaceHeatingSystems.filter(
    // fraction <= 0 is used to signal a system which has been removed from a
    // non-baseline scenario
    (system) => system.fraction > 0,
  );
  const mains = filteredHeatingSystems.filter((system) => system.type === 'main');
  const secondMains = filteredHeatingSystems.filter(
    (system) =>
      system.type === 'second main (whole house)' ||
      system.type === 'second main (part of house)',
  );

  // Find "first main" heating system
  if (mains.length > 1) {
    console.warn('Multiple "first main" heating systems specified. Using the first one.');
  }
  const main: HeatingSystemSpaceParameters | null = mains[0] ?? null;

  // Find "second main" heating system
  if (secondMains.length > 1) {
    console.warn(
      'Multiple "second main" heating systems specified. Using the first one.',
    );
  }
  const secondMain: HeatingSystemSpaceParameters | null = secondMains[0] ?? null;

  // Validate
  if (main === null) {
    if (secondMain !== null) {
      console.warn(
        'Specified "second main" heating system but no "first main". Ignoring both.',
      );
      return { main: null, secondMain: null };
    } else {
      return { main, secondMain };
    }
  } else {
    return { main, secondMain };
  }
}

export class MeanInternalTemperature {
  public heatingSystems: HeatingSystems;

  constructor(
    private input: MeanInternalTemperatureInput,
    private dependencies: MeanInternalTemperatureDependencies,
  ) {
    this.heatingSystems = constructHeatingSystems(input, dependencies);
    if (dependencies.mode === 'standardised heating (pre-2025)') {
      const standardisedInput: MeanInternalTemperatureInput = {
        ...this.input,
        targetTemperature: 21,
      };
      const standardisedDependencies: MeanInternalTemperatureDependencies = {
        ...this.dependencies,
        heatingHours: {
          weekday: { hoursOffPattern: [7, 8] },
          weekend: { hoursOffPattern: [8] },
        },
        mode: 'normal',
      };
      const standardisedSelf = new MeanInternalTemperature(
        standardisedInput,
        standardisedDependencies,
      );
      return standardisedSelf;
    }
  }

  get flags() {
    return this.dependencies.modelBehaviourFlags.meanInternalTemperature;
  }

  get livingAreaFraction(): number {
    if (this.input.livingArea === null) return 1;
    else return this.input.livingArea / this.dependencies.floors.totalFloorArea;
  }

  get mainHeatingResponsiveness(): number {
    if (this.heatingSystems.main === null) {
      // SAP 10.2 Table 4a, page 163
      return 1;
    }
    return weightBySystemFractions(
      this.heatingSystems.main,
      this.heatingSystems.secondMain,
      this.heatingSystems.main.responsiveness,
      this.heatingSystems.secondMain?.responsiveness ?? null,
      { normalise: true },
    );
  }

  // SAP: (202)
  get fractionOfSpaceHeatingFromMainSystems(): number {
    return (
      (this.heatingSystems.main?.fraction ?? 0) +
      (this.heatingSystems.secondMain?.fraction ?? 0)
    );
  }

  // SAP: (203)
  get fractionOfMainHeatingFromSecondMainSystem(): number {
    if (this.heatingSystems.main === null || this.heatingSystems.secondMain === null) {
      return 0;
    } else {
      return (
        this.heatingSystems.secondMain.fraction /
        (this.heatingSystems.main.fraction + this.heatingSystems.secondMain.fraction)
      );
    }
  }

  private heatGain(month: Month): number {
    if (this.dependencies.modelBehaviourFlags.meanInternalTemperature.includeSolarGains) {
      return this.dependencies.heatGain.heatGain(month);
    } else {
      return (
        this.dependencies.heatGain.heatGain(month) -
        this.dependencies.heatGain.heatGainSolar(month)
      );
    }
  }

  @cacheMonth
  wholeDwellingCalculators(month: Month): Weekly<WholeDwellingCalculator> {
    return mapWeekly(
      this.dependencies.heatingHours,
      (heatingPattern) =>
        new WholeDwellingCalculator(
          {
            heatingSystems: this.heatingSystems,
            targetTemperature: this.input.targetTemperature,
            livingAreaFraction: this.livingAreaFraction,
            externalTemperature: externalTemperature(this.dependencies.region, month),
            thermalMassParameter: this.dependencies.fabric.thermalMassParameter,
            heatLossParameter: this.dependencies.heatLoss.heatLossParameter(month),
            heatLoss: this.dependencies.heatLoss.heatLossMonthly(month),
            heatGain: this.heatGain(month),
            mainHeatingResponsiveness: this.mainHeatingResponsiveness,
            hoursOffPattern: heatingPattern.hoursOffPattern,
            fractionOfMainHeatingFromSecondMainSystem:
              this.fractionOfMainHeatingFromSecondMainSystem,
          },
          this.flags,
        ),
    );
  }

  initialUtilisationFactorLivingArea(month: Month): number {
    return this.wholeDwellingCalculators(month).weekday.livingAreaCalculator
      .utilisationFactor;
  }

  meanInternalTemperatureLivingArea(month: Month): number {
    return averageWeekly(
      mapWeekly(
        this.wholeDwellingCalculators(month),
        ({ livingAreaCalculator }) => livingAreaCalculator.meanInternalTemperature,
      ),
    );
  }

  initialUtilisationFactorRestOfDwelling(month: Month): number {
    if (
      !this.flags.whenNoHeatingSystems.correctRestOfDwellingUtilisationFactor &&
      this.heatingSystems.main === null
    ) {
      return 0;
    }
    return this.wholeDwellingCalculators(month).weekday.restOfDwellingCalculator
      .utilisationFactor;
  }

  temperatureWhenHeatedRestOfDwelling(month: Month): number {
    return this.wholeDwellingCalculators(month).weekday.restOfDwellingCalculator
      .temperatureWhenHeated;
  }

  meanInternalTemperatureRestOfDwelling(month: Month): number {
    return averageWeekly(
      mapWeekly(
        this.wholeDwellingCalculators(month),
        (calculator) => calculator.meanInternalTemperatureRestOfDwelling,
      ),
    );
  }

  meanInternalTemperatureOverallUnadjusted(month: Month): number {
    return averageWeekly(
      mapWeekly(
        this.wholeDwellingCalculators(month),
        (calculator) => calculator.meanInternalTemperatureOverall,
      ),
    );
  }

  get temperatureAdjustment(): number {
    if (this.heatingSystems.main === null) {
      if (this.flags.whenNoHeatingSystems.useSAPTemperatureAdjustment) {
        // SAP 10.2 Table 4e, page 171
        return 0.3;
      } else {
        // Legacy model behaviour
        return 0;
      }
    }
    if (this.heatingSystems.secondMain?.type === 'second main (whole house)') {
      return this.heatingSystems.main.temperatureAdjustment;
    } else {
      return weightBySystemFractions(
        this.heatingSystems.main,
        this.heatingSystems.secondMain,
        this.heatingSystems.main.temperatureAdjustment,
        this.heatingSystems.secondMain?.temperatureAdjustment ?? null,
        {
          normalise: this.flags.normaliseHeatingFractionsForTemperatureAdjustment,
        },
      );
    }
  }

  @cacheMonth
  meanInternalTemperatureOverall(month: Month): number {
    switch (this.dependencies.mode) {
      case 'standardised heating':
        return 20;
      case 'standardised heating (pre-2025)':
      case 'regulated':
      case 'normal':
        return (
          this.meanInternalTemperatureOverallUnadjusted(month) +
          this.temperatureAdjustment
        );
    }
  }

  /** The final, overall utilisation factor */
  @cacheMonth
  utilisationFactor(month: Month): number {
    return utilisationFactor(
      {
        thermalMassParameter: this.dependencies.fabric.thermalMassParameter,
        heatLossParameter: this.dependencies.heatLoss.heatLossParameter(month),
        heatGain: this.heatGain(month),
        heatLoss: this.dependencies.heatLoss.heatLossMonthly(month),
        temperatureWhenHeated: this.meanInternalTemperatureOverall(month),
        externalTemperature: externalTemperature(this.dependencies.region, month),
      },
      this.flags.utilisationFactor,
    );
  }

  /* eslint-disable
     @typescript-eslint/no-explicit-any,
     @typescript-eslint/no-unsafe-assignment,
     @typescript-eslint/no-unsafe-member-access,
    */
  mutateLegacyData(scenario: any): void {
    if (scenario === undefined) return;
    scenario.HLP = Month.all.map((m) => this.dependencies.heatLoss.heatLossParameter(m));
    scenario.external_temperature = Month.all.map((m) =>
      externalTemperature(this.dependencies.region, m),
    );
    scenario.internal_temperature = Month.all.map((m) =>
      this.meanInternalTemperatureOverall(m),
    );
    scenario.mean_internal_temperature = {
      ...scenario.mean_internal_temperature,
      fLA: this.livingAreaFraction,
      m_i_t_living_area: Month.all.map((m) => this.meanInternalTemperatureLivingArea(m)),
      m_i_t_whole_dwelling: Month.all.map((m) =>
        this.meanInternalTemperatureOverallUnadjusted(m),
      ),
      m_i_t_whole_dwelling_adjusted: Month.all.map((m) =>
        this.meanInternalTemperatureOverall(m),
      ),
      m_i_t_rest_of_dwelling: Month.all.map((m) =>
        this.meanInternalTemperatureRestOfDwelling(m),
      ),
      t_heating_periods_rest_of_dwelling: Month.all.map((m) =>
        this.temperatureWhenHeatedRestOfDwelling(m),
      ),
      u_factor_living_area: Month.all.map((m) =>
        this.initialUtilisationFactorLivingArea(m),
      ),
      u_factor_rest_of_dwelling: Month.all.map((m) =>
        this.initialUtilisationFactorRestOfDwelling(m),
      ),
    };
    scenario.temperature = {
      ...scenario.temperature,
      living_area: this.livingAreaFraction * this.dependencies.floors.totalFloorArea,
      responsiveness: this.mainHeatingResponsiveness,
      temperature_adjustment: this.temperatureAdjustment,
    };
  }
  /* eslint-enable */
}

type SharedParams = {
  externalTemperature: number;
  thermalMassParameter: number;
  heatLossParameter: number;
  heatLoss: number;
  heatGain: number;
  mainHeatingResponsiveness: number;
  hoursOffPattern: number[];
};

export type WholeDwellingCalculatorInput = {
  heatingSystems: HeatingSystems;
  targetTemperature: number;
  livingAreaFraction: number;

  // Redundant parameter but does not depend on month, and we have one
  // WholeDwellingCalculater per month * {weekday,weekend}
  fractionOfMainHeatingFromSecondMainSystem: number;
} & SharedParams;

/** A class which encapsulates temperature calculations for the living area and the rest
 * of the dwelling, and computing their weighted average */
export class WholeDwellingCalculator {
  constructor(
    private input: WholeDwellingCalculatorInput,
    private flags: MeanInternalTemperatureFlags,
  ) {}

  @cache
  get livingAreaCalculator(): TemperatureCalculator {
    return new TemperatureCalculator(
      {
        temperatureWhenHeated: {
          type: 'living area' as const,
          value: this.input.targetTemperature,
        },
        ...this.input,
      },
      this.flags,
    );
  }

  get mainHeatingSystemControlType(): HeatingSystemSpaceParameters['controlType'] {
    if (this.input.heatingSystems.main === null) {
      // SAP 10.2 Table 4e, page 171
      return 2;
    } else {
      return this.input.heatingSystems.main.controlType;
    }
  }

  @cache
  get restOfDwellingCalculator(): TemperatureCalculator {
    let restOfDwellingControlType: HeatingSystemSpaceParameters['controlType'];
    if (this.input.heatingSystems.secondMain === null) {
      restOfDwellingControlType = this.mainHeatingSystemControlType;
    } else {
      if (
        this.input.heatingSystems.secondMain.type === 'second main (whole house)' &&
        this.input.heatingSystems.secondMain.controlType !==
          this.input.heatingSystems.main.controlType
      ) {
        console.warn(
          'second main heating system heats whole house, but has different control type to first main heating system',
        );
        // This is undefined behaviour according to SAP, but we proceed matching the legacy model
        restOfDwellingControlType = this.input.heatingSystems.main.controlType;
      } else {
        restOfDwellingControlType = this.input.heatingSystems.secondMain.controlType;
      }
    }
    return new TemperatureCalculator(
      {
        temperatureWhenHeated: {
          type: 'rest of dwelling' as const,
          livingAreaValue: this.input.targetTemperature,
          heatingSystemControlType: restOfDwellingControlType,
        },
        ...this.input,
      },
      this.flags,
    );
  }

  // SAP: 1 - (91)
  get restDwellingFraction(): number {
    return 1 - this.input.livingAreaFraction;
  }

  @cache
  get restOfDwellingWithMainSystemControls(): TemperatureCalculator {
    return new TemperatureCalculator(
      {
        temperatureWhenHeated: {
          type: 'rest of dwelling' as const,
          livingAreaValue: this.input.targetTemperature,
          heatingSystemControlType: this.mainHeatingSystemControlType,
        },
        ...this.input,
      },
      this.flags,
    );
  }

  get meanInternalTemperatureRestOfDwelling(): number {
    if (
      !this.flags.whenNoHeatingSystems.correctRestOfDwellingTemperature &&
      this.input.heatingSystems.main === null
    ) {
      return this.livingAreaCalculator.meanInternalTemperature;
    }
    if (
      this.input.heatingSystems.secondMain === null ||
      this.input.heatingSystems.secondMain.type !== 'second main (part of house)'
    ) {
      return this.restOfDwellingCalculator.meanInternalTemperature;
    }
    /* There are a number of problems with the legacy version of this calculation, which
     * are documented inline. We control which version of the calculation to apply with a
     * single flag.
     */
    if (this.flags.fixRestOfDwellingTemperatureCalculation) {
      /* SAP disagrees with itself on how to perform the following check.
       *
       * Table 9c says "If the fraction of the dwelling heated by main system 2, (203), is
       * greater than the rest of house area, 1 - (91), ..." (SAP 10.2 p185), but (203) is
       * actually "Fraction of main heating from main system 2" (SAP 10.2 p142).
       *
       * The text refers to "fraction of the dwelling" (i.e. the non-normalised value),
       * but (203) refers to "fraction of main heating" (i.e. the normalised value).
       *
       * The legacy model goes by the textual description in Table 9c, but we think the
       * value in (203) is the true intention, because the rest of the calculation uses
       * (203) rather than the non-normalised fraction.
       */
      if (
        this.input.fractionOfMainHeatingFromSecondMainSystem > this.restDwellingFraction
      ) {
        return this.restOfDwellingCalculator.meanInternalTemperature;
      }
      /* The weight calculated here is as SAP specifies. */
      const weight =
        this.input.fractionOfMainHeatingFromSecondMainSystem / this.restDwellingFraction;
      const weightedAverage =
        weight * this.restOfDwellingCalculator.meanInternalTemperature +
        (1 - weight) * this.restOfDwellingWithMainSystemControls.meanInternalTemperature;
      return weightedAverage;
    } else {
      /* Use the textual description in Table 9c, i.e. compare the second main system's
       * contribution to *all* heating, not just main heating. */
      if (this.input.heatingSystems.secondMain.fraction > this.restDwellingFraction) {
        return this.restOfDwellingCalculator.meanInternalTemperature;
      }
      /* The legacy model's weighting equations here are simply incorrect due to errors
       * when rearranging the formulae from SAP, and thinking that the result must be
       * divided by 2. Details can be found in the golden master code in the tests. */
      const weightStandard =
        (1 - this.input.fractionOfMainHeatingFromSecondMainSystem) /
        this.restDwellingFraction;
      const weightWithMainSystemControls =
        (1 -
          this.input.fractionOfMainHeatingFromSecondMainSystem -
          this.input.livingAreaFraction) /
        this.restDwellingFraction;
      const weightedAverage =
        (weightStandard * this.restOfDwellingCalculator.meanInternalTemperature +
          weightWithMainSystemControls *
            this.restOfDwellingWithMainSystemControls.meanInternalTemperature) /
        2;
      return weightedAverage;
    }
  }

  get meanInternalTemperatureOverall(): number {
    return (
      this.livingAreaCalculator.meanInternalTemperature * this.input.livingAreaFraction +
      this.meanInternalTemperatureRestOfDwelling * (1 - this.input.livingAreaFraction)
    );
  }
}

type TemperatureCalculatorParams = {
  temperatureWhenHeated:
    | { type: 'living area'; value: number }
    | {
        type: 'rest of dwelling';
        livingAreaValue: number;
        heatingSystemControlType: null | 1 | 2 | 3;
      };
} & SharedParams;

/** A class for computing the temperature reduction, given the location within the
 * dwelling (living area or rest of dwelling), heating hours pattern, and all other
 * parameters. */
class TemperatureCalculator {
  constructor(
    private input: TemperatureCalculatorParams,
    private flags: MeanInternalTemperatureFlags,
  ) {}

  @cache
  get temperatureWhenHeated(): number {
    const params = this.input.temperatureWhenHeated;
    switch (params.type) {
      case 'living area':
        return params.value;
      case 'rest of dwelling': {
        const clampedHeatLossParameter = Math.min(6.0, this.input.heatLossParameter);
        switch (params.heatingSystemControlType) {
          case null:
            return params.livingAreaValue;
          case 1:
            return params.livingAreaValue - 0.5 * clampedHeatLossParameter;
          case 2:
          case 3:
            return (
              params.livingAreaValue -
              clampedHeatLossParameter +
              clampedHeatLossParameter ** 2 / 12
            );
        }
      }
    }
  }

  @cache
  get utilisationFactor(): number {
    return utilisationFactor(
      {
        heatLossParameter: this.input.heatLossParameter,
        thermalMassParameter: this.input.thermalMassParameter,
        heatLoss: this.input.heatLoss,
        heatGain: this.input.heatGain,
        temperatureWhenHeated: this.temperatureWhenHeated,
        externalTemperature: this.input.externalTemperature,
      },
      this.flags.utilisationFactor,
    );
  }

  // u_1 + u_2 + ...
  get meanTemperatureReduction(): number {
    const timeConstantHours = 4 + 0.25 * timeConstant(this.input);
    return sum(
      this.input.hoursOffPattern.map((hoursOff) => {
        if (Number.isNaN(hoursOff)) return 0; // Legacy compatibility
        if (this.input.heatLoss === 0) {
          if (this.input.heatGain === 0) return 0;
          else return -Infinity;
        }
        const internalTemperatureWithoutHeating =
          (1 - this.input.mainHeatingResponsiveness) * (this.temperatureWhenHeated - 2) +
          this.input.mainHeatingResponsiveness *
            (this.input.externalTemperature +
              (this.utilisationFactor * this.input.heatGain) / this.input.heatLoss);
        if (hoursOff <= timeConstantHours) {
          return (
            (0.5 *
              hoursOff ** 2 *
              (this.temperatureWhenHeated - internalTemperatureWithoutHeating)) /
            (24 * timeConstantHours)
          );
        } else {
          return (
            ((this.temperatureWhenHeated - internalTemperatureWithoutHeating) *
              (hoursOff - 0.5 * timeConstantHours)) /
            24
          );
        }
      }),
    );
  }

  get meanInternalTemperature(): number {
    return this.temperatureWhenHeated - this.meanTemperatureReduction;
  }
}

// In this module, we often have to compute averages of results weighted by the fractions
// of heating contribution by the two main systems. We are interested in fractions of the
// systems' contribution to *main heating*, whereas the heating systems are expressed as
// contribution to all heating. This means we have to normalise these fractions so that
// they sum to 1. This is the purpose of SAP cell 203. Unfortunately, the legacy code
// forgets to do this normalisation in various places, so we provide a flag to toggle the
// legacy behaviour.
function weightBySystemFractions(
  main: HeatingSystemSpaceParameters,
  secondaryMain: HeatingSystemSpaceParameters | null,
  mainValue: number,
  secondaryValue: number | null,
  opts: { normalise: boolean },
) {
  if (!opts.normalise) {
    if (secondaryMain === null) return mainValue;
    else {
      return (
        main.fraction * mainValue + (secondaryMain?.fraction ?? 0) * (secondaryValue ?? 0)
      );
    }
  } else {
    return (
      (main.fraction * mainValue +
        (secondaryMain?.fraction ?? 0) * (secondaryValue ?? 0)) /
      (main.fraction + (secondaryMain?.fraction ?? 0))
    );
  }
}
