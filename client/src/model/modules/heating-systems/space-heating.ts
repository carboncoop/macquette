import type { LegacyHeatingSystem } from '.';
import { coalesceEmptyString } from '../../../data-schemas/scenario/value-schemas';

export type HeatingSystemSpaceParameters = {
  type:
    | 'main'
    | 'second main (part of house)'
    | 'second main (whole house)'
    | 'supplementary';
  responsiveness: number;
  fraction: number;
  // Control type corresponds to SAP Table 9, used to calculate temperature reduction in
  // rest of dwelling; null means skip the reduction and set rest of dwelling temperature
  // to living area temperature
  controlType: null | 1 | 2 | 3;
  temperatureAdjustment: number; // SAP Table 4e
};

export function extractHeatingSystemSpaceParameters(
  legacySystem: LegacyHeatingSystem,
): HeatingSystemSpaceParameters | null {
  if (
    !(
      legacySystem.provides === 'heating_and_water' || legacySystem.provides === 'heating'
    )
  ) {
    return null;
  }
  let type: HeatingSystemSpaceParameters['type'];
  switch (legacySystem.main_space_heating_system) {
    case undefined:
    case 'secondaryHS':
      type = 'supplementary';
      break;
    case 'mainHS1':
      type = 'main';
      break;
    case 'mainHS2_whole_house':
      type = 'second main (whole house)';
      break;
    case 'mainHS2_part_of_the_house':
      type = 'second main (part of house)';
      break;
  }
  let controlType: HeatingSystemSpaceParameters['controlType'];
  switch (legacySystem.heating_controls) {
    case 0:
    case undefined:
      controlType = null;
      break;
    default:
      controlType = legacySystem.heating_controls;
  }
  return {
    type,
    fraction: coalesceEmptyString(legacySystem.fraction_space, 0) ?? 0,
    responsiveness: coalesceEmptyString(legacySystem.responsiveness, 0) ?? 0,
    controlType: controlType,
    temperatureAdjustment:
      coalesceEmptyString(legacySystem.temperature_adjustment, 0) ?? 0,
  };
}
