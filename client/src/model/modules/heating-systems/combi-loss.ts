import type { LegacyHeatingSystem } from '.';
import { TypeOf, t } from '../../../data-schemas/visitable-types';

export const combiLossParameters = t.nullable(
  t.discriminatedUnion('type', [
    t.struct({
      type: t.literal('instantaneous'),
      keepHotFacility: t.nullable(
        t.struct({
          controlledByTimeClock: t.boolean(),
        }),
      ),
    }),
    t.struct({
      type: t.literal('storage'),
      capacity: t.union([t.literal(null), t.literal('>= 55 litres'), t.number()]),
    }),
  ]),
);
export type CombiLossParameters = TypeOf<typeof combiLossParameters>;

export function extractCombiLossParametersFromLegacy(
  legacySystem: LegacyHeatingSystem,
  Vc: number | null,
): CombiLossParameters {
  let combiLoss: CombiLossParameters;
  if (legacySystem.instantaneous_water_heating ?? false) {
    // "Instantaneous" here means at point of use
    combiLoss = null;
  } else {
    switch (legacySystem.combi_loss) {
      case '0':
      case 0:
      case undefined:
        // Category was "Combi boilers" but `combi_loss` was 0. This
        // does not make sense, but occurs in some library items as a
        // hack for skipping the combi loss calcs.
        combiLoss = null;
        break;
      case 'Storage combi boiler  55 litres':
        // 'Storage combi boiler  55 litres' is a value produced by the old PHP backend
        // applying HTML sanitisation to the original string 'Storage combi boiler < 55
        // litres'. It occurs in some very old assessments and breaks the legacy model, so
        // we skip the calcs.
        combiLoss = null;
        break;
      case 'Instantaneous, without keep hot-facility':
        combiLoss = {
          type: 'instantaneous',
          keepHotFacility: null,
        };
        break;
      case 'Instantaneous, with keep-hot facility controlled by time clock':
        combiLoss = {
          type: 'instantaneous',
          keepHotFacility: { controlledByTimeClock: true },
        };
        break;
      case 'Instantaneous, with keep-hot facility not controlled by time clock':
        combiLoss = {
          type: 'instantaneous',
          keepHotFacility: { controlledByTimeClock: false },
        };
        break;
      case 'Storage combi boiler < 55 litres':
        combiLoss = {
          type: 'storage',
          capacity: Vc ?? null,
        };
        break;
      case 'Storage combi boiler >= 55 litres':
        combiLoss = {
          type: 'storage',
          capacity: '>= 55 litres',
        };
        break;
    }
  }
  return combiLoss;
}
