import { sum } from '../../helpers/array-reducers';
import { cacheMonth } from '../../helpers/cache-decorators';
import { Month } from '../enums/month';
import { Fabric } from './fabric';
import { Floors } from './floors';
import { Geography } from './geography';
import { Infiltration } from './ventilation-infiltration/infiltration';
import { Ventilation } from './ventilation-infiltration/ventilation';

export type HeatLossDependencies = {
  geography: Pick<Geography, 'externalDesignTemperature'>;
  floors: Pick<Floors, 'totalFloorArea'>;
  ventilation: Pick<Ventilation, 'heatLossAverage' | 'heatLossMonthly'>;
  infiltration: Pick<Infiltration, 'heatLossAverage' | 'heatLossMonthly'>;
  fabric: Pick<Fabric, 'heatLoss'>;
};

export type HeatLossBySource = {
  fabric: number;
  ventilation: number;
  infiltration: number;
};

export class HeatLoss {
  constructor(
    _input: null,
    private dependencies: HeatLossDependencies,
  ) {}

  // aka Heat Transfer Coefficient, W/K
  @cacheMonth
  heatLossMonthly(month: Month): number {
    return sum(Object.values(this.heatLossBySourceMonthly(month)));
  }

  @cacheMonth
  heatLossBySourceMonthly(month: Month): HeatLossBySource {
    return {
      fabric: this.dependencies.fabric.heatLoss,
      ventilation: this.dependencies.ventilation.heatLossMonthly(month),
      infiltration: this.dependencies.infiltration.heatLossMonthly(month),
    };
  }

  sapVentilationHeatLossMonthly(month: Month): number {
    return (
      this.dependencies.ventilation.heatLossMonthly(month) +
      this.dependencies.infiltration.heatLossMonthly(month)
    );
  }

  get averageVentilationInfiltrationHeatLoss(): number {
    return (
      this.dependencies.infiltration.heatLossAverage +
      this.dependencies.ventilation.heatLossAverage
    );
  }

  get internalDesignTemperature(): number {
    // We use 20 degrees here as an average value. MCS calcs specify different
    // minimums per type of room but we can't do that.
    return 20;
  }

  get totalAverageHeatLoss(): number {
    return (
      this.dependencies.fabric.heatLoss + this.averageVentilationInfiltrationHeatLoss
    );
  }

  /** Peak heat load in W */
  get peakHeatLoad(): number {
    return (
      this.totalAverageHeatLoss *
      (this.internalDesignTemperature -
        this.dependencies.geography.externalDesignTemperature)
    );
  }

  /** Peak heat load, W/m² */
  get peakHeatLoadPerArea(): number {
    return this.peakHeatLoad / this.dependencies.floors.totalFloorArea;
  }

  heatLossParameter(month: Month): number {
    return this.heatLossMonthly(month) / this.dependencies.floors.totalFloorArea;
  }

  /* eslint-disable
       @typescript-eslint/no-explicit-any,
       @typescript-eslint/no-unsafe-member-access,
    */
  mutateLegacyData(data: any) {
    data.totalWK = this.totalAverageHeatLoss;
    data.totalWK_monthly = Month.all.map((m): number => this.heatLossMonthly(m));
    data.ventilation.SAP_ventilation_WK = Month.all.map((m): number =>
      this.sapVentilationHeatLossMonthly(m),
    );
    data.ventilation.average_WK = this.averageVentilationInfiltrationHeatLoss;
  }
  /* eslint-enable */
}
