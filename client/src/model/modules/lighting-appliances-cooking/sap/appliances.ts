import { Scenario } from '../../../../data-schemas/scenario';
import { sum } from '../../../../helpers/array-reducers';
import { cache } from '../../../../helpers/cache-decorators';
import { isTruthy } from '../../../../helpers/is-truthy';
import { Month } from '../../../enums/month';
import { ModelBehaviourFlags } from '../../behaviour-version';
import type { FuelDemandByFuel, FuelDemandParameters } from '../../fuel-requirements';
import { FuelName, Fuels, FuelsDict } from '../../fuels';
import { extractFuelFractionsFromLegacy } from '../../fuels/extract-fuel-fractions-from-legacy';
import { validateFuelNames } from '../../fuels/validate-fuel-fractions';

export type AppliancesSAPInput = {
  enabled: boolean;
  energyEfficient: boolean;
  fuelFractions: FuelsDict<number>;
};

export function extractAppliancesSAPInputFromLegacy(
  scenario: Scenario,
): AppliancesSAPInput {
  const enabled = scenario?.LAC_calculation_type !== 'carboncoop_SAPlighting';
  const { LAC } = scenario ?? {};
  return {
    enabled,
    energyEfficient: LAC?.energy_efficient_appliances ?? false,
    fuelFractions: extractFuelFractionsFromLegacy(LAC?.fuels_appliances),
  };
}

export type AppliancesSAPDependencies = {
  floors: {
    totalFloorArea: number;
  };
  occupancy: {
    occupancy: number;
  };
  fuels: {
    names: string[];
  };
  modelBehaviourFlags: Pick<ModelBehaviourFlags, 'appliancesSap'>;
};

export class AppliancesSAP {
  constructor(
    public input: AppliancesSAPInput,
    private dependencies: AppliancesSAPDependencies,
  ) {
    validateFuelNames(input.fuelFractions, dependencies.fuels, this.constructor.name);
  }

  @cache
  get initialEnergyAnnual(): number {
    if (!this.input.enabled) return 0;
    return (
      207.8 *
      Math.pow(
        this.dependencies.floors.totalFloorArea * this.dependencies.occupancy.occupancy,
        0.4714,
      )
    );
  }

  energyMonthly(month: Month): number {
    const adjustment = 1 + 0.157 * Math.cos((2 * Math.PI * (month.index1 - 1.78)) / 12);
    const weight = month.days / 365;
    let reducedEnergyFactor: number;
    if (
      this.dependencies.modelBehaviourFlags.appliancesSap
        .applyEfficiencyReductionForEnergyMonthly
    ) {
      reducedEnergyFactor = this.input.energyEfficient ? 0.9 : 1.0;
    } else {
      reducedEnergyFactor = 1.0;
    }
    return this.initialEnergyAnnual * reducedEnergyFactor * adjustment * weight;
  }

  get energyAnnual(): number {
    let reducedEnergyFactor: number;
    if (
      this.dependencies.modelBehaviourFlags.appliancesSap
        .applyEfficiencyReductionForEnergyMonthly
    ) {
      reducedEnergyFactor = 1.0;
    } else {
      reducedEnergyFactor = this.input.energyEfficient ? 0.9 : 1.0;
    }
    return reducedEnergyFactor * sum(Month.all.map((m) => this.energyMonthly(m)));
  }

  heatGain(month: Month): number {
    const reducedGainFactor = this.input.energyEfficient ? 0.67 : 1.0;
    return ((this.energyMonthly(month) * 1000) / (24 * month.days)) * reducedGainFactor;
  }

  get heatGainAnnual(): number {
    return sum(Month.all.map((m) => this.heatGain(m)));
  }

  get fuelDemandByFuelAnnual(): FuelDemandByFuel {
    if (!this.input.enabled) return new Map();
    let fuelFractionsEntries = Object.entries(this.input.fuelFractions);
    if (fuelFractionsEntries.length === 0) {
      fuelFractionsEntries = [[Fuels.STANDARD_TARIFF, 1]];
    }
    return new Map(
      fuelFractionsEntries.map(
        ([fuelName, fraction]): [FuelName, FuelDemandParameters] => [
          fuelName,
          {
            energyDemand: this.energyAnnual * fraction,
            fuelInput: this.energyAnnual * fraction,
            relativeFraction: fraction,
          },
        ],
      ),
    );
  }

  get totalFuelRequirement(): number {
    return sum(
      Array.from(this.fuelDemandByFuelAnnual.values()).map(({ fuelInput }) => fuelInput),
    );
  }

  /* eslint-disable
       @typescript-eslint/no-explicit-any,
       @typescript-eslint/no-unsafe-argument,
       @typescript-eslint/no-unsafe-assignment,
       @typescript-eslint/no-unsafe-member-access,
    */
  mutateLegacyData(data: any) {
    data.LAC = data.LAC ?? {};
    data.LAC.EA = this.energyAnnual;
    data.LAC.energy_efficient_appliances = this.input.energyEfficient;

    if (!isTruthy(data.LAC.fuels_appliances)) {
      data.LAC.fuels_appliances = [{ fuel: Fuels.STANDARD_TARIFF, fraction: 1 }];
    }

    if (this.energyAnnual > 0) {
      data.gains_W['Appliances'] = Month.all.map((m) => this.heatGain(m));
      data.energy_requirements = data.energy_requirements ?? {};
      data.energy_requirements.appliances = {
        name: 'Appliances',
        quantity: this.energyAnnual,
        monthly: Month.all.map((m) => this.energyMonthly(m)),
      };
      const legacyFuelDemandArray = Array.from(this.fuelDemandByFuelAnnual).map(
        ([fuelName, { energyDemand, fuelInput, relativeFraction }]) => ({
          fuel: fuelName,
          demand: energyDemand,
          fuel_input: fuelInput,
          fraction: relativeFraction,
        }),
      );
      data.LAC.fuels_appliances = legacyFuelDemandArray;
      data.fuel_requirements.appliances.list = legacyFuelDemandArray;
      data.fuel_requirements.appliances.quantity = this.totalFuelRequirement;
    }
  }
  /* eslint-enable */
}
