import { Scenario } from '../../data-schemas/scenario';
import { sum } from '../../helpers/array-reducers';
import { cache } from '../../helpers/cache-decorators';
import { positiveMod } from '../../helpers/positive-mod';
import { periodLength, TimePeriod } from '../../periods/core';
import { UnvalidatedTimePeriod, validatePattern } from '../../periods/validation';
import { ModelError } from '../error';
import { ModelBehaviourFlags } from './behaviour-version';
import { mapWeekly, Weekly } from './mean-internal-temperature/weekly';

type DeepPick<T, Path> = Path extends [infer S, ...infer RestS]
  ? S extends keyof T
    ? { [K in S]: DeepPick<T[S], RestS> }
    : never
  : T;

export type HeatingHoursDependencies = {
  modelBehaviourFlags: DeepPick<ModelBehaviourFlags, ['heatingHours']>;
};
export type HeatingPatternInput = {
  pattern: Array<TimePeriod>;
  unvalidatedPattern: Array<UnvalidatedTimePeriod>; // for legacy only
};
export type HeatingHoursInput = {
  weekday: HeatingPatternInput;
  weekend: HeatingPatternInput;
};

export function extractHeatingHoursInputFromLegacy(
  scenario: Scenario,
): HeatingHoursInput {
  let unvalidatedPatterns: Weekly<UnvalidatedTimePeriod[]>;
  if (scenario?.space_heating?.heatingHours === undefined) {
    // use household questionnaire data
    unvalidatedPatterns = {
      weekday: scenario?.household?.heatingHoursWeekday ?? [],
      weekend: scenario?.household?.heatingHoursWeekend ?? [],
    };
  } else {
    // passthrough app state
    unvalidatedPatterns = {
      weekday: scenario?.space_heating?.heatingHours?.weekday ?? [],
      weekend: scenario?.space_heating?.heatingHours?.weekend ?? [],
    };
  }
  return mapWeekly(unvalidatedPatterns, (unvalidated) => {
    return {
      pattern: validatePattern(unvalidated)
        .map(([_, vr]) => vr)
        .filter((vr) => vr.isOk())
        .map((vr) => vr.unwrap()),
      unvalidatedPattern: unvalidated,
    };
  });
}

export class HeatingHours {
  constructor(
    private input: HeatingHoursInput,
    private dependencies: HeatingHoursDependencies,
  ) {}

  @cache
  get weekday(): HeatingPattern {
    return new HeatingPattern(
      this.input.weekday,
      this.dependencies.modelBehaviourFlags.heatingHours,
    );
  }

  @cache
  get weekend(): HeatingPattern {
    return new HeatingPattern(
      this.input.weekend,
      this.dependencies.modelBehaviourFlags.heatingHours,
    );
  }
}

export class HeatingPattern {
  constructor(
    private input: HeatingPatternInput,
    private flags: ModelBehaviourFlags['heatingHours'],
  ) {}

  @cache
  get sorted(): HeatingPatternInput['pattern'] {
    return [...this.input.pattern].sort(
      ({ start: startA }, { start: startB }) => startA - startB,
    );
  }

  get totalHoursOn(): number {
    return sum(this.input.pattern.map((p) => periodLength(p))) / 60;
  }

  get hoursOffPattern(): number[] {
    if (!this.flags.fixHoursOffCalculation) {
      return legacyHoursOff(this.input.unvalidatedPattern);
    }
    if (sum(this.sorted.map(periodLength)) === 0) {
      return [24];
    }
    const offPattern: number[] = [];
    // SAFETY: caught by sum check above
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    let curTime = this.sorted[this.input.pattern.length - 1]!.end;
    for (const period of this.sorted) {
      if (curTime !== period.start) {
        offPattern.push(positiveMod(period.start - curTime, 24 * 60) / 60);
      }
      curTime = period.end;
    }
    return offPattern;
  }
}

/* eslint-disable eqeqeq */
function legacyHoursOff(pattern: Array<UnvalidatedTimePeriod>) {
  if (pattern.length > 3) {
    throw new ModelError('Legacy hours off supports max 3 periods', { pattern: pattern });
  }
  const pat = pattern.map(({ start, end }) => ({
    on: {
      h: start === null ? undefined : Math.floor(start / 60),
      m: start === null ? undefined : start % 60,
    },
    off: {
      h: end === null ? undefined : Math.floor(end / 60),
      m: end === null ? undefined : end % 60,
    },
  }));
  function get_hours_off_one_period(on: Date, off: Date) {
    // heating is on before midnight and off after midnight
    if (on > off) {
      return Math.abs(off.getTime() - on.getTime()) / 36e5;
    } else {
      on.setDate(on.getDate() + 1);
      return Math.abs(on.getTime() - off.getTime()) / 36e5;
    }
  }
  function get_hours_two_periods(on_1: Date, off_1: Date, on_2: Date, off_2: Date) {
    const hours_off = [];
    hours_off.push((on_2.getTime() - off_1.getTime()) / 36e5);
    hours_off.push(get_hours_off_one_period(on_1, off_2));
    return hours_off;
  }
  function get_hours_three_periods(
    on_1: Date,
    off_1: Date,
    on_2: Date,
    off_2: Date,
    on_3: Date,
    off_3: Date,
  ) {
    const hours_off = [];
    hours_off.push((on_2.getTime() - off_1.getTime()) / 36e5);
    hours_off.push((on_3.getTime() - off_2.getTime()) / 36e5);
    hours_off.push(get_hours_off_one_period(on_1, off_3));
    return hours_off;
  }
  let hours_off = [];
  if (
    pat[2]?.off.h != undefined &&
    pat[2]?.off.m != undefined &&
    (pat[2]?.on.h != pat[2]?.off.h || pat[2]?.on.m != pat[2]?.off.m)
  ) {
    const on_1 = new Date(2000, 1, 1, pat[0]?.on.h, pat[0]?.on.m, 0, 0);
    const off_1 = new Date(2000, 1, 1, pat[0]?.off.h, pat[0]?.off.m, 0, 0);
    const on_2 = new Date(2000, 1, 1, pat[1]?.on.h, pat[1]?.on.m, 0, 0);
    const off_2 = new Date(2000, 1, 1, pat[1]?.off.h, pat[1]?.off.m, 0, 0);
    const on_3 = new Date(2000, 1, 1, pat[2]?.on.h, pat[2]?.on.m, 0, 0);
    const off_3 = new Date(2000, 1, 1, pat[2]?.off.h, pat[2]?.off.m, 0, 0);
    hours_off = get_hours_three_periods(on_1, off_1, on_2, off_2, on_3, off_3);
  } else if (
    pat[1]?.off.h != undefined &&
    pat[1]?.off.m != undefined &&
    (pat[1]?.on.h != pat[1]?.off.h || pat[1]?.on.m != pat[1]?.off.m)
  ) {
    const on_1 = new Date(2000, 1, 1, pat[0]?.on.h, pat[0]?.on.m, 0, 0);
    const off_1 = new Date(2000, 1, 1, pat[0]?.off.h, pat[0]?.off.m, 0, 0);
    const on_2 = new Date(2000, 1, 1, pat[1]?.on.h, pat[1]?.on.m, 0, 0);
    const off_2 = new Date(2000, 1, 1, pat[1]?.off.h, pat[1]?.off.m, 0, 0);
    hours_off = get_hours_two_periods(on_1, off_1, on_2, off_2);
  } else if (pat[0]?.off.h != pat[0]?.on.h || pat[0]?.off.m != pat[0]?.on.m) {
    const off = new Date(2000, 1, 1, pat[0]?.off.h, pat[0]?.off.m, 0, 0);
    const on = new Date(2000, 1, 1, pat[0]?.on.h, pat[0]?.on.m, 0, 0);
    hours_off.push(get_hours_off_one_period(on, off));
  } else {
    hours_off.push(0);
  }
  return hours_off;
}
/* eslint-enable */
