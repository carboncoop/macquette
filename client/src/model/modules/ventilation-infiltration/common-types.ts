export type VentilationPoint = {
  ventilationRate: number; // m³/hour
};
