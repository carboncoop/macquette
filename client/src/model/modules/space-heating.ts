import { mapValues } from 'lodash';
import { Scenario } from '../../data-schemas/scenario';
import { sum } from '../../helpers/array-reducers';
import { cache, cacheMonth } from '../../helpers/cache-decorators';
import { externalTemperature } from '../datasets';
import { Month } from '../enums/month';
import { Region } from '../enums/region';
import { ModelBehaviourFlags } from './behaviour-version';
import { Fabric } from './fabric';
import { Floors } from './floors';
import { HeatGain } from './heat-gain';
import {
  UtilisationFactorParams,
  utilisationFactor,
} from './heat-gain-utilisation-factor';
import { HeatLoss, HeatLossBySource } from './heat-loss';
import { MeanInternalTemperature } from './mean-internal-temperature';

export type SpaceHeatingInput = {
  heatingOffInSummer: boolean;
};
export function extractSpaceHeatingInputFromLegacy(
  scenario: Scenario,
): SpaceHeatingInput {
  return { heatingOffInSummer: scenario?.space_heating?.heating_off_summer ?? true };
}

export type SpaceHeatingDependencies = {
  meanInternalTemperature: Pick<
    MeanInternalTemperature,
    'meanInternalTemperatureOverall'
  >;
  fabric: Pick<Fabric, 'thermalMassParameter'>;
  heatGain: Pick<HeatGain, 'heatGainInternal' | 'heatGainSolar' | 'heatGain'>;
  region: Region;
  heatLoss: Pick<
    HeatLoss,
    'heatLossMonthly' | 'heatLossParameter' | 'heatLossBySourceMonthly'
  >;
  floors: Pick<Floors, 'totalFloorArea'>;
  modelBehaviourFlags: Pick<ModelBehaviourFlags, 'spaceHeating'>;
};

export class SpaceHeating {
  constructor(
    private input: SpaceHeatingInput,
    private dependencies: SpaceHeatingDependencies,
    private signalEdgeCase: () => void = () => undefined,
  ) {}

  externalTemperature(month: Month): number {
    return externalTemperature(this.dependencies.region, month);
  }

  @cacheMonth
  heatGainUtilisationFactor(month: Month) {
    if (month.season === 'summer' && this.input.heatingOffInSummer) return 0;
    const params: UtilisationFactorParams = {
      thermalMassParameter: this.dependencies.fabric.thermalMassParameter,
      heatLossParameter: this.dependencies.heatLoss.heatLossParameter(month),
      heatGain:
        this.dependencies.heatGain.heatGainInternal(month) +
        this.dependencies.heatGain.heatGainSolar(month),
      heatLoss: this.dependencies.heatLoss.heatLossMonthly(month),
      temperatureWhenHeated:
        this.dependencies.meanInternalTemperature.meanInternalTemperatureOverall(month),
      externalTemperature: this.externalTemperature(month),
    };
    return utilisationFactor(
      params,
      {
        ...this.dependencies.modelBehaviourFlags.spaceHeating.utilisationFactor,
        etaIs1IfGammaIsLessThanOrEqualTo0: true,
      },
      this.signalEdgeCase,
    );
  }

  usefulInternalHeatGain(month: Month) {
    return (
      this.heatGainUtilisationFactor(month) *
      this.dependencies.heatGain.heatGainInternal(month)
    );
  }

  usefulSolarHeatGain(month: Month) {
    return (
      this.heatGainUtilisationFactor(month) *
      this.dependencies.heatGain.heatGainSolar(month)
    );
  }

  usefulHeatGain(month: Month) {
    return this.usefulInternalHeatGain(month) + this.usefulSolarHeatGain(month);
  }

  // kWh/m²
  get usefulInternalHeatGainEnergyPerArea() {
    const gainEnergyAnnualKWh =
      (sum(Month.all.map((m) => this.usefulInternalHeatGain(m) * m.days)) * 24) / 1000;
    return gainEnergyAnnualKWh / this.dependencies.floors.totalFloorArea;
  }

  // kWh/m²
  get usefulSolarHeatGainEnergyPerArea() {
    const gainEnergyAnnualKWh =
      (sum(Month.all.map((m) => this.usefulSolarHeatGain(m) * m.days)) * 24) / 1000;
    return gainEnergyAnnualKWh / this.dependencies.floors.totalFloorArea;
  }

  // kWh/m²
  @cache
  get heatLossAnnualEnergyPerArea(): HeatLossBySource {
    const heatLossPowerBySource = this.heatLossPowerBySource.bind(this);
    const totalFloorArea = this.dependencies.floors.totalFloorArea;
    function computeLossPerAreaForSource(source: keyof HeatLossBySource): number {
      const lossEnergyAnnualForSource =
        (sum(
          Month.all.map((month) => heatLossPowerBySource(month)[source] * month.days),
        ) *
          24) /
        (1000 * totalFloorArea);
      return lossEnergyAnnualForSource;
    }
    return {
      fabric: computeLossPerAreaForSource('fabric'),
      ventilation: computeLossPerAreaForSource('ventilation'),
      infiltration: computeLossPerAreaForSource('infiltration'),
    };
  }

  /** Positive means warmer inside than out */
  temperatureDifference(month: Month) {
    const out =
      this.dependencies.meanInternalTemperature.meanInternalTemperatureOverall(month) -
      this.externalTemperature(month);
    if (
      this.dependencies.modelBehaviourFlags.spaceHeating
        .clampNegativeTemperatureDifference
    ) {
      return Math.max(0, out);
    } else {
      return out;
    }
  }

  /** Watts */
  @cacheMonth
  heatLossPowerBySource(month: Month): HeatLossBySource {
    if (month.season === 'summer' && this.input.heatingOffInSummer) {
      return { fabric: 0, ventilation: 0, infiltration: 0 };
    } else {
      return mapValues(
        this.dependencies.heatLoss.heatLossBySourceMonthly(month),
        (loss) => loss * this.temperatureDifference(month),
      );
    }
  }

  /** Watts */
  heatLossPower(month: Month) {
    return sum(Object.values(this.heatLossPowerBySource(month)));
  }

  /* Watts */
  spaceHeatingDemand(month: Month) {
    return Math.max(0, this.heatLossPower(month) - this.usefulHeatGain(month));
  }

  // kWh
  spaceHeatingDemandEnergy(month: Month) {
    return (this.spaceHeatingDemand(month) * month.days * 24) / 1000;
  }

  // kWh
  get spaceHeatingDemandEnergyAnnual() {
    return sum(Month.all.map((m) => this.spaceHeatingDemandEnergy(m)));
  }

  // kWh/m²
  get spaceHeatingDemandAnnualEnergyPerArea() {
    return this.spaceHeatingDemandEnergyAnnual / this.dependencies.floors.totalFloorArea;
  }

  /** @deprecated This method reproduces a broken legacy calculation */
  spaceCoolingDemand(month: Month) {
    if (
      this.dependencies.modelBehaviourFlags.spaceHeating.skipLegacyCoolingCalculation ??
      true
    ) {
      return 0;
    }
    const unclampedCoolingDemand = this.usefulHeatGain(month) - this.heatLossPower(month);
    if (Number.isNaN(unclampedCoolingDemand)) {
      this.signalEdgeCase();
      return 0;
    } else {
      return Math.max(0, unclampedCoolingDemand);
    }
  }

  // kWh
  /** @deprecated */
  spaceCoolingDemandEnergy(month: Month) {
    return (this.spaceCoolingDemand(month) * month.days * 24) / 1000;
  }

  // kWh
  /** @deprecated */
  get spaceCoolingDemandEnergyAnnual() {
    return sum(Month.all.map((m) => this.spaceCoolingDemandEnergy(m)));
  }

  // kWh/m²
  /** @deprecated */
  get spaceCoolingDemandAnnualEnergyPerArea() {
    return this.spaceCoolingDemandEnergyAnnual / this.dependencies.floors.totalFloorArea;
  }

  /* eslint-disable @typescript-eslint/no-explicit-any,
      @typescript-eslint/no-unsafe-assignment,
      @typescript-eslint/no-unsafe-member-access */
  mutateLegacyData(data: any) {
    if (data === undefined) return;
    data.space_heating = data.space_heating ?? {};
    data.space_heating.delta_T = Month.all.map((m) => this.temperatureDifference(m));
    data.space_heating.total_losses = Month.all.map((m) => this.heatLossPower(m));
    data.space_heating.total_gains = Month.all.map((m) =>
      this.dependencies.heatGain.heatGain(m),
    );
    data.space_heating.utilisation_factor = Month.all.map((m) =>
      this.heatGainUtilisationFactor(m),
    );
    data.space_heating.useful_gains = Month.all.map((m) => this.usefulHeatGain(m));
    data.annual_useful_gains_kWh_m2 = {
      Solar: this.usefulSolarHeatGainEnergyPerArea,
      Internal: this.usefulInternalHeatGainEnergyPerArea,
    };
    data.annual_losses_kWh_m2 = this.heatLossAnnualEnergyPerArea;
    data.space_heating.heat_demand = Month.all.map((m) => this.spaceHeatingDemand(m));
    data.space_heating.cooling_demand = Month.all.map((m) => this.spaceCoolingDemand(m));
    data.space_heating.heat_demand_kwh = Month.all.map((m) =>
      this.spaceHeatingDemandEnergy(m),
    );
    data.space_heating.cooling_demand_kwh = Month.all.map((m) =>
      this.spaceCoolingDemandEnergy(m),
    );
    data.space_heating.annual_heating_demand = this.spaceHeatingDemandEnergyAnnual;
    data.space_heating.annual_cooling_demand = this.spaceCoolingDemandEnergyAnnual;
    data.space_heating.annual_heating_demand_m2 =
      this.spaceHeatingDemandAnnualEnergyPerArea;
    data.space_heating.annual_cooling_demand_m2 =
      this.spaceCoolingDemandAnnualEnergyPerArea;
    data.energy_requirements = data.energy_requirements ?? {};
    data.energy_requirements.space_heating = {
      name: 'Space Heating',
      quantity: this.spaceHeatingDemandEnergyAnnual,
      monthly: Month.all.map((m) => this.spaceHeatingDemandEnergy(m)),
    };
    data.space_heating_demand_m2 =
      this.spaceHeatingDemandAnnualEnergyPerArea +
      this.spaceCoolingDemandAnnualEnergyPerArea;
  }
  /* eslint-enable */
}
