export type UtilisationFactorFlags = {
  etaIs1IfGammaIsLessThanOrEqualTo0: boolean;
  preserveGammaPrecision: boolean;
};

export type UtilisationFactorParams = {
  thermalMassParameter: number;
  heatLossParameter: number;
  heatLoss: number;
  heatGain: number;
  temperatureWhenHeated: number;
  externalTemperature: number;
};

export function timeConstant(params: {
  thermalMassParameter: number;
  heatLossParameter: number;
}) {
  return params.thermalMassParameter / (3.6 * params.heatLossParameter);
}

export function utilisationFactor(
  input: UtilisationFactorParams,
  flags: UtilisationFactorFlags,
  signalEdgeCase: () => void = () => undefined,
): number {
  const timeConstant_ = timeConstant(input);
  const a = 1 + timeConstant_ / 15;
  if (!Number.isFinite(timeConstant_)) {
    signalEdgeCase();
  }
  const heatLossRateWatts =
    input.heatLoss * (input.temperatureWhenHeated - input.externalTemperature);
  let gamma = input.heatGain / heatLossRateWatts;
  // SAP says that if heatLossRateWatts = 0, then we should replace gamma with 10^6 to
  // avoid a division by zero. The problem with this is that the limit as the loss rate
  // approaches 0 is different depending on whether it approaches 0 from the negative side
  // or the positive side. Taking limits in various directions leads to the following logic:
  if (!Number.isFinite(gamma)) {
    signalEdgeCase();
    if (gamma === Infinity) {
      // assuming a > 0, then eta -> 0 as gamma -> +infinity
      return 0;
    } else {
      // could be 0/0, or x/-0, or some other weirdness; bail out and return a noop
      // utilisation factor
      return 1;
    }
  }

  // If gamma is close to 1, the general case formula is either undefined due to a
  // division by 0 or erratic due to FP instability. SAP says to solve this by rounding
  // gamma to 8 decimal places and completing the function at gamma = 1 with its limit.
  //
  // Completing with the limit is a useful approach, but rounding to 8 decimal places is
  // a poor way of dealing with the FP instability. Instead, we replace the function
  // with its Taylor series when gamma is close (in floating point terms) to 1.
  //
  // We have a flag to replicate the legacy behaviour
  if (!(flags?.preserveGammaPrecision ?? true)) {
    gamma = Math.round(gamma * 10e7) / 10e7;
  }
  if (gamma <= 0) {
    if (!(flags.etaIs1IfGammaIsLessThanOrEqualTo0 ?? true)) {
      return 0;
    } else {
      return 1;
    }
  }
  if (1 - 1e-21 < gamma && gamma < 1 + 1e-21) {
    return a / (a + 1) - (a * (gamma - 1)) / (2 * (a + 1)); // First two terms of the Taylor series
  } else {
    const eta = (1 - gamma ** a) / (1 - gamma ** (a + 1));
    if (!Number.isFinite(eta)) {
      signalEdgeCase();
    }
    return eta;
  }
}
