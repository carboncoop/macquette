import { FuelName, Fuels, FuelsDict } from '../fuels';

export function validateFuelNames(
  toValidate: FuelsDict<unknown> | FuelName[],
  fuels: Pick<Fuels, 'names'>,
  moduleName: string,
) {
  const toValidate_ = Array.isArray(toValidate) ? toValidate : Object.keys(toValidate);
  for (const fuelName of toValidate_) {
    if (fuelName === Fuels.GENERATION) {
      // This is a possibility because we treat generation as a "fuel" with negative
      // quantities, which are applied as offsets against the standard tariff in later
      // fuel requirements modules. It does not make sense for earlier modules to require
      // fuel input from generation.

      // Some legacy assessments erroneously specify "generation" as the fuel source for
      // such things as PV diverter heating systems, so this is a warning.
      console.warn(
        'module which specifies fuel requirements specified input from generation',
      );
    }
    if (!fuels.names.includes(fuelName)) {
      console.error('got an unknown fuel name', moduleName, fuelName);
    }
  }
}
