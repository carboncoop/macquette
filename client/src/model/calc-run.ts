import { isIndexable } from '../helpers/is-indexable';
import { Model } from './model';

export function calcRun(legacyDataInput: unknown) {
  let legacyDataOutput: Record<string, unknown>;
  if (isIndexable(legacyDataInput)) {
    legacyDataOutput = legacyDataInput;
  } else {
    legacyDataOutput = {};
  }
  const modelR = Model.fromLegacy(legacyDataOutput);
  legacyDataOutput['model'] = modelR;
  if (!modelR.isOk()) {
    console.error(modelR.coalesce());
  } else {
    modelR.coalesce().mutateLegacyData(legacyDataOutput);
  }
  return legacyDataOutput;
}
