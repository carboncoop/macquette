import { z } from 'zod';

import { libraryItemCommonSchema, makeLibrarySchema } from './common';

const item = z
  .object({
    units: z.string(),
    category: z.enum([
      'Computing',
      'Cooking',
      'Food storage',
      'Laundry',
      'Miscelanea',
      'Other kitchen / cleaning',
      'TV',
    ]),
    frequency: z.number(),
    efficiency: z.number(),
    norm_demand: z.number(),
    type_of_fuel: z.enum(['Electricity', 'Gas', 'Oil', 'Solid fuel']),
    reference_quantity: z.number(),
    utilisation_factor: z.number(),
  })
  .merge(libraryItemCommonSchema);

export const appliancesAndCooking = makeLibrarySchema('appliances_and_cooking', item);
