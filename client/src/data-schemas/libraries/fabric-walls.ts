import { z } from 'zod';

import type { Library } from '.';
import {
  highLowCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const common = z
  .object({
    description: z.string(),
    uValue: z.number(),
    kValue: z.number(),
  })
  .merge(libraryItemCommonSchema);

const wallLike = common.extend({
  type: z.enum(['external wall', 'party wall', 'loft', 'roof']),
});

const wallMeasure = wallLike
  .merge(measureCommonSchema)
  .merge(highLowCarbonAccountingSchema)
  .extend({
    isExternalWallInsulation: z.boolean(),
    cost_units: z.literal('sqm' as const),
  });

export const fabricWalls = makeLibrarySchema('walls', wallLike);
export const fabricWallMeasures = makeLibrarySchema('wall_measures', wallMeasure);

export type WallLike = z.infer<typeof wallLike>;
export type WallLikeMeasure = z.infer<typeof wallMeasure>;

export type FabricWallsLibrary = z.infer<typeof fabricWalls>;
export function isFabricWallsLibrary(library: Library): library is FabricWallsLibrary {
  return library.type === 'walls';
}

export type FabricWallMeasuresLibrary = z.infer<typeof fabricWallMeasures>;
export function isFabricWallMeasuresLibrary(
  library: Library,
): library is FabricWallMeasuresLibrary {
  return library.type === 'wall_measures';
}
