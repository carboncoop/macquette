import { z } from 'zod';
import type { Library } from '.';
import {
  basicCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const measure = z
  .object({
    SELECT: z.enum([
      'All accesible piperwok insulated',
      'First 1m from cylinder insulated',
      'Fully insulated primary pipework',
    ]),
  })
  .merge(libraryItemCommonSchema)
  .merge(measureCommonSchema)
  .merge(basicCarbonAccountingSchema);

export type PipeworkInsulationMeasure = z.infer<typeof measure>;

export const pipeworkInsulation = makeLibrarySchema('pipework_insulation', measure);

export type PipeworkInsulationMeasuresLibrary = z.infer<typeof pipeworkInsulation>;
export function isPipeworkInsulationMeasuresLibrary(
  library: Library,
): library is PipeworkInsulationMeasuresLibrary {
  return library.type === 'pipework_insulation';
}
