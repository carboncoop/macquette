import { z } from 'zod';

import type { Library } from '.';
import { nullableStringyFloat } from '../scenario/value-schemas';
import {
  basicCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const item = z
  .object({
    ventilation_type: z.enum(['NV', 'IE', 'MEV', 'PS', 'MVHR', 'MV', 'DEV']),
    specific_fan_power: nullableStringyFloat.transform((val) =>
      val === '' ? null : val,
    ),
    system_air_change_rate: nullableStringyFloat.transform((val) =>
      val === '' ? null : val,
    ),
    balanced_heat_recovery_efficiency: nullableStringyFloat.transform((val) =>
      val === '' ? null : val,
    ),
  })
  .merge(libraryItemCommonSchema);

const measure = item
  .merge(measureCommonSchema)
  .merge(basicCarbonAccountingSchema)
  .extend({
    cost_units: z.union([z.literal('unit' as const), z.literal('sqm' as const)]),
  });

export type VentilationSystem = z.infer<typeof item>;
export type VentilationSystemMeasure = z.infer<typeof measure>;

export const ventilationSystems = makeLibrarySchema('ventilation_systems', item);
export type VentilationSystemsLibrary = z.infer<typeof ventilationSystems>;
export function isVentilationSystemsLibrary(
  library: Library,
): library is VentilationSystemsLibrary {
  return library.type === 'ventilation_systems';
}

export const ventilationSystemsMeasures = makeLibrarySchema(
  'ventilation_systems_measures',
  measure,
);
export type VentilationSystemMeasuresLibrary = z.infer<typeof ventilationSystemsMeasures>;
export function isVentilationSystemMeasuresLibrary(
  library: Library,
): library is VentilationSystemMeasuresLibrary {
  return library.type === 'ventilation_systems_measures';
}
