import { z } from 'zod';
import type { Library } from '.';
import {
  basicCarbonAccountingSchema,
  libraryItemCommonSchema,
  makeLibrarySchema,
  measureCommonSchema,
} from './common';

const measure = z
  .object({
    control_type: z.enum([
      'Cylinder thermostat, water heating not separately timed',
      'Cylinder thermostat, water heating separately timed',
    ]),
  })
  .merge(libraryItemCommonSchema)
  .merge(measureCommonSchema)
  .merge(basicCarbonAccountingSchema);

export type HotWaterControlTypeMeasure = z.infer<typeof measure>;

export const hotWaterControlTypeMeasures = makeLibrarySchema(
  'hot_water_control_type',
  measure,
);

export type HotWaterControlTypeMeasuresLibrary = z.infer<
  typeof hotWaterControlTypeMeasures
>;
export function isHotWaterControlTypeMeasuresLibrary(
  library: Library,
): library is HotWaterControlTypeMeasuresLibrary {
  return library.type === 'hot_water_control_type';
}
