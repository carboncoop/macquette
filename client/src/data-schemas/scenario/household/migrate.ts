import { TimeOfDay } from '../../../periods/core';
import { pad2, parseTime } from '../../../periods/strings';
import { coalesceEmptyString } from '../value-schemas';

import { HouseholdLegacy } from './legacy';

function reconstituteLegacyTime(
  hour: string | number | undefined,
  minute: string | number | undefined,
): TimeOfDay | null {
  if (hour === undefined && minute === undefined) {
    return null;
  }

  let output = '';
  if (typeof hour === 'string') {
    output += hour;
  } else if (typeof hour === 'number') {
    output += pad2(hour);
  }
  output += ':';
  if (typeof minute === 'string') {
    output += minute;
  } else if (typeof minute === 'number') {
    output += pad2(minute);
  }

  const [time] = parseTime(output);
  return time;
}

function attemptParseInt(val: string | undefined) {
  const attempt = parseInt(val ?? '', 10);
  if (Number.isNaN(attempt)) {
    return val;
  } else {
    return attempt;
  }
}

function makeLegacyPriorities(
  priority_airquality: string | undefined,
  priority_carbon: string | undefined,
  priority_comfort: string | undefined,
  priority_health: string | undefined,
  priority_modernisation: string | undefined,
  priority_money: string | undefined,
) {
  const priorities = [
    { title: 'Save carbon', ordinal: attemptParseInt(priority_carbon) },
    { title: 'Save money', ordinal: attemptParseInt(priority_money) },
    { title: 'Improve comfort', ordinal: attemptParseInt(priority_comfort) },
    {
      title: 'Improve indoor air quality',
      ordinal: attemptParseInt(priority_airquality),
    },
    { title: 'General modernisation', ordinal: attemptParseInt(priority_modernisation) },
    { title: 'Improve health', ordinal: attemptParseInt(priority_health) },
  ].filter(
    (priority): priority is { title: string; ordinal: string | number } =>
      priority.ordinal !== undefined,
  );

  const result = priorities.sort(function (a, b) {
    if (typeof a.ordinal === 'number' && typeof b.ordinal === 'number') {
      return a.ordinal - b.ordinal;
    } else if (typeof a.ordinal === 'number') {
      return -1;
    } else if (typeof b.ordinal === 'number') {
      return 1;
    } else {
      return a.ordinal.localeCompare(b.ordinal);
    }
  });
  if (result.length === 0) {
    return null;
  } else {
    return result;
  }
}

export function migrateLegacyToV1({
  address_1,
  address_2,
  address_3,
  address_town,
  address_postcode,
  address_country,
  address_full,
  address_la,
  address_la_full,
  address_lsoa,
  address_lsoa_full,
  local_planning_authority,
  house_type,
  house_type_note,
  tenure,
  tenure_note,
  landRegistryLink,
  overheatingRisk,
  subsidence2030,
  subsidence2050,
  subsidence2070,
  soilHeaveRisk,
  areaOfOutstandingNaturalBeauty,
  planningNotes,
  latLong,
  location_density,
  exposure,
  frostAttackRisk,
  flooding_rivers_sea,
  flooding_surface_water,
  flooding_reservoirs,
  flooding_groundwater,
  radon_risk,
  uniquePropertyReferenceNumber,
  historic_age_band,
  historic_age_precise,
  historic_listed,
  historic_note,
  historic_conserved,
  treeNotes,
  mains_electricity,
  mains_gas,
  mains_water,
  mains_sewer,
  electricityDNO,
  electricityLooped,
  electricityMPAN,
  electricitySmartMeter,
  gasTransporter,
  gasMPRN,
  gasSmartMeter,
  householder_name,
  assessor_name,
  assessment_date,
  interview_date,
  occupancy_actual,
  moveInDate,
  moveInDateNotes,
  occupants_under5,
  occupants_5to17,
  occupants_18to65,
  occupants_over65,
  occupants_note,
  daytimeOccupancy,
  daytimeOccupancyNotes,
  expected_lifestyle_change,
  expected_lifestyle_change_note,
  electricVehicles,
  electricVehiclesNotes,
  pets,
  occupants_pets,
  healthConditions,
  health,
  house_nr_bedrooms,
  roomUse,
  previous_works,
  previous_works_approvals,
  buildingMaintenance,
  electricsCondition,
  structuralIssues,
  structural_issues,
  structural_issues_note,
  flooding_history,
  flooding_note,
  damp,
  damp_note,
  ventilationSystem,
  laundryOptions,
  laundryFrequency,
  laundryTumbleDrier,
  laundryTumbleDrierFrequency,
  laundry,
  comfort_temperature_summer,
  comfort_temperature_winter,
  comfort_airquality_summer,
  comfort_airquality_winter,
  comfort_draughts_summer,
  comfort_draughts_winter,
  thermal_comfort_note,
  thermal_comfort_problems,
  overheating_note,
  controlSummer,
  controlWinter,
  controlNotes,
  daylight,
  daylight_note,
  daylight_problems,
  noise_problems,
  noise_note,
  rooms_unloved,
  rooms_favourite,
  context_and_other_points,
  space_heating_provided,
  space_heating_controls,
  heating_off_summer,
  heating_thermostat,
  heatingOffMonths,
  heating_weekday_on1_hours,
  heating_weekday_on1_mins,
  heating_weekday_off1_hours,
  heating_weekday_off1_mins,
  heating_weekday_on2_hours,
  heating_weekday_on2_mins,
  heating_weekday_off2_hours,
  heating_weekday_off2_mins,
  heating_weekday_on3_hours,
  heating_weekday_on3_mins,
  heating_weekday_off3_hours,
  heating_weekday_off3_mins,
  heating_weekend_on1_hours,
  heating_weekend_on1_mins,
  heating_weekend_off1_hours,
  heating_weekend_off1_mins,
  heating_weekend_on2_hours,
  heating_weekend_on2_mins,
  heating_weekend_off2_hours,
  heating_weekend_off2_mins,
  heating_weekend_on3_hours,
  heating_weekend_on3_mins,
  heating_weekend_off3_hours,
  heating_weekend_off3_mins,
  heatingHoursWeekday,
  heatingHoursWeekend,
  heating_unheated_habitable,
  heatingNotes,
  heatingRoomHeaters,
  heatingRoomHeatersNotes,
  heatingBurnersAirSupply,
  hot_water_provided,
  hotWaterCylinder,
  hotWaterCylinderNotes,
  hotWaterShowersPerWeek,
  hotWaterBathsPerWeek,
  hotWaterUsageNotes,
  occupancy_expected,
  occupancy_expected_note,
  expected_other_works,
  expected_other_works_note,
  expectedOtherWorksDesigns,
  expected_start,
  expected_start_note,
  priorities,
  priority_airquality,
  priority_carbon,
  priority_comfort,
  priority_health,
  priority_modernisation,
  priority_money,
  priority_qualitative_note,
  projectAims,
  aesthetics_internal,
  aesthetics_external,
  aesthetics_note,
  appearanceOutsideFront,
  appearanceOutsideSideRear,
  appearanceDetailsToPreserve,
  logistics_packaging,
  logistics_diy,
  logistics_diy_note,
  logisticsManagement,
  logistics_disruption,
  logistics_disruption_note,
  logistics_budget,
  logistics_budget_note,
  budgetExact,
  budgetFinancing,
  externalNoisePollution,
  externalAirPollution,
  smokeControlZone,
  heatNetworkNotes,
  heatNetworkInZone,
  siteOfSpecialScientificInterest,
  worldHeritageSite,
  nearAncientMonument,
  report_date,
  report_signoff,
  report_version,
  report_reference,
  commentary_brief,
  commentary_context,
  commentary_decisions,
  looked_up,
  ...rest
}: HouseholdLegacy) {
  const r = reconstituteLegacyTime;
  if (heatingHoursWeekday === undefined) {
    heatingHoursWeekday = [
      {
        start: r(heating_weekday_on1_hours, heating_weekday_on1_mins),
        end: r(heating_weekday_off1_hours, heating_weekday_off1_mins),
      },
      {
        start: r(heating_weekday_on2_hours, heating_weekday_on2_mins),
        end: r(heating_weekday_off2_hours, heating_weekday_off2_mins),
      },
      {
        start: r(heating_weekday_on3_hours, heating_weekday_on3_mins),
        end: r(heating_weekday_off3_hours, heating_weekday_off3_mins),
      },
    ];
  }
  if (heatingHoursWeekend === undefined) {
    heatingHoursWeekend = [
      {
        start: r(heating_weekend_on1_hours, heating_weekend_on1_mins),
        end: r(heating_weekend_off1_hours, heating_weekend_off1_mins),
      },
      {
        start: r(heating_weekend_on2_hours, heating_weekend_on2_mins),
        end: r(heating_weekend_off2_hours, heating_weekend_off2_mins),
      },
      {
        start: r(heating_weekend_on3_hours, heating_weekend_on3_mins),
        end: r(heating_weekend_off3_hours, heating_weekend_off3_mins),
      },
    ];
  }

  const prioritiesLegacy = makeLegacyPriorities(
    priority_airquality,
    priority_carbon,
    priority_comfort,
    priority_health,
    priority_modernisation,
    priority_money,
  );

  const legacyAddress = {
    line1: address_1 ?? '',
    line2: address_2 ?? '',
    line3: address_3 ?? '',
    postTown: address_town ?? '',
    postcode: address_postcode ?? '',
    country: address_country ?? '',
  };
  const legacyAddressEmpty = Object.values(legacyAddress).every((val) => val === '');
  const address =
    address_full ??
    (legacyAddressEmpty === true
      ? { type: 'no data' }
      : { type: 'user provided', value: legacyAddress });

  return {
    householderName: householder_name ?? '',
    address,
    uniquePropertyReferenceNumber: uniquePropertyReferenceNumber ?? {
      type: 'no data',
    },
    localAuthority:
      address_la_full ??
      (address_la !== undefined
        ? { type: 'user provided', value: address_la }
        : { type: 'no data' }),
    lowerLayerSuperOutputArea:
      address_lsoa_full ??
      (address_lsoa !== undefined
        ? { type: 'user provided', value: address_lsoa }
        : { type: 'no data' }),
    localPlanningAuthority: local_planning_authority ?? { type: 'no data' },
    propertyType: house_type ?? null,
    propertyTypeNotes: house_type_note ?? '',
    tenure: tenure ?? null,
    tenureNotes: tenure_note ?? '',
    landRegistryLink: landRegistryLink ?? '',
    overheatingRisk: overheatingRisk ?? null,
    subsidence2030: subsidence2030 ?? null,
    subsidence2050: subsidence2050 ?? null,
    subsidence2070Legacy: subsidence2070 ?? null,
    soilHeaveRisk: soilHeaveRisk ?? null,
    latLong: latLong ?? null,
    locationDensity: location_density ?? null,
    exposureZone: exposure ?? null,
    frostAttackRisk: frostAttackRisk ?? { type: 'no data' },
    floodingRiversAndSea: coalesceEmptyString(flooding_rivers_sea, null) ?? null,
    floodingSurfaceWater: coalesceEmptyString(flooding_surface_water, null) ?? null,
    floodingReservoirs: coalesceEmptyString(flooding_reservoirs, null) ?? null,
    floodingGroundwater: coalesceEmptyString(flooding_groundwater, null) ?? null,
    radonRisk: radon_risk ?? null,
    areaOfOutstandingNaturalBeauty: areaOfOutstandingNaturalBeauty ?? null,
    planningNotes: planningNotes ?? '',
    builtBand: historic_age_band ?? null,
    builtExact: historic_age_precise ?? '',
    listedStatus: historic_listed ?? null,
    listedStatusNotes: historic_note ?? '',
    inConservationArea: historic_conserved ?? null,
    treeNotes: treeNotes ?? '',
    hasMainsElectricity: mains_electricity ?? null,
    electricityDNO: electricityDNO ?? '',
    electricityLooped: electricityLooped ?? null,
    electricityMPAN: electricityMPAN ?? '',
    electricitySmartMeter: electricitySmartMeter ?? null,
    hasMainsGas: mains_gas ?? null,
    gasTransporter: gasTransporter ?? '',
    gasMPRN: gasMPRN ?? '',
    gasSmartMeter: gasSmartMeter ?? null,
    hasMainsWater: mains_water ?? null,
    hasMainsSewer: mains_sewer ?? null,
    interviewDate: interview_date ?? '',
    surveyDate: assessment_date ?? '',
    surveyorName: assessor_name ?? '',
    timeSinceMoveLegacy: occupancy_actual ?? null,
    moveInDate: moveInDate ?? '',
    moveInDateNotes: moveInDateNotes ?? '',
    occupantsUnder5: coalesceEmptyString(occupants_under5, null) ?? null,
    occupants5to17: coalesceEmptyString(occupants_5to17, null) ?? null,
    occupants18to64: coalesceEmptyString(occupants_18to65, null) ?? null,
    occupants65Over: coalesceEmptyString(occupants_over65, null) ?? null,
    occupantsNotes: occupants_note ?? '',
    daytimeOccupancy: daytimeOccupancy ?? null,
    daytimeOccupancyNotes: daytimeOccupancyNotes ?? '',
    lifestyleChange: expected_lifestyle_change ?? null,
    lifestyleChangeNotes: expected_lifestyle_change_note ?? '',
    electricVehicles: electricVehicles ?? null,
    electricVehiclesNotes: electricVehiclesNotes ?? '',
    pets: pets ?? null,
    petsNotes: occupants_pets ?? '',
    healthConditions: healthConditions ?? null,
    healthConditionsNotes: health ?? '',
    numBedrooms: house_nr_bedrooms ?? '',
    roomUse: roomUse ?? '',
    priorWork: previous_works ?? '',
    priorWorkApprovals: previous_works_approvals ?? '',
    buildingMaintenance: buildingMaintenance ?? '',
    electricsCondition: electricsCondition ?? '',
    structuralIssues: structuralIssues ?? null,
    structuralIssuesNotes: structural_issues ?? '',
    structuralIssuesNotesLegacy: structural_issues_note ?? '',
    floodingHistory: flooding_history ?? null,
    floodingHistoryNote: flooding_note ?? '',
    dampCondensationMould: damp ?? '',
    dampCondensationMouldLegacy: damp_note ?? '',
    ventilationSystem: ventilationSystem ?? '',
    laundryOptions: laundryOptions ?? [],
    laundryFrequency: laundryFrequency ?? null,
    laundryTumbleDrier: laundryTumbleDrier ?? null,
    laundryTumbleDrierFrequency: laundryTumbleDrierFrequency ?? null,
    laundryNotes: laundry ?? '',
    temperatureComfortSummer: comfort_temperature_summer ?? null,
    temperatureComfortWinter: comfort_temperature_winter ?? null,
    humidityComfortSummer: comfort_airquality_summer ?? null,
    humidityComfortWinter: comfort_airquality_winter ?? null,
    draughtsComfortSummer: comfort_draughts_summer ?? null,
    draughtsComfortWinter: comfort_draughts_winter ?? null,
    thermalComfortProblemLocations: thermal_comfort_problems ?? null,
    thermalComfortProblemLocationsNotes: thermal_comfort_note ?? '',
    overheatingNotesLegacy: overheating_note ?? '',
    controlWinter: controlWinter ?? null,
    controlSummer: controlSummer ?? null,
    controlNotes: controlNotes ?? '',
    daylightLevel: daylight ?? null,
    daylightProblemLocations: daylight_problems ?? null,
    daylightProblemLocationsNotes: daylight_note ?? '',
    noiseProblems: noise_problems ?? null,
    noiseProblemsNotes: noise_note ?? '',
    roomsUnloved: rooms_unloved ?? '',
    roomsFavourite: rooms_favourite ?? '',
    generalContextNotes: context_and_other_points ?? '',
    heating: space_heating_provided ?? '',
    heatingControls: space_heating_controls ?? '',
    heatingOff: heating_off_summer ?? null,
    heatingOffMonths: heatingOffMonths ?? [],
    heatingThermostatTemp: heating_thermostat ?? '',
    heatingHoursWeekday,
    heatingHoursWeekend,
    heatingUnheatedRooms: heating_unheated_habitable ?? '',
    heatingNotes: heatingNotes ?? '',
    heatingRoomHeaters: heatingRoomHeaters ?? null,
    heatingRoomHeatersNotes: heatingRoomHeatersNotes ?? '',
    heatingBurnersAirSupply: heatingBurnersAirSupply ?? null,
    hotWater: hot_water_provided ?? '',
    hotWaterCylinder: hotWaterCylinder ?? null,
    hotWaterCylinderNotes: hotWaterCylinderNotes ?? '',
    hotWaterShowersPerWeek: hotWaterShowersPerWeek ?? null,
    hotWaterBathsPerWeek: hotWaterBathsPerWeek ?? null,
    hotWaterUsageNotes: hotWaterUsageNotes ?? '',
    expectedStay: occupancy_expected ?? '',
    expectedStayNotes: occupancy_expected_note ?? '',
    otherWorks: expected_other_works ?? null,
    otherWorksDesigns: expectedOtherWorksDesigns ?? '',
    otherWorksNotes: expected_other_works_note ?? '',
    deadlinesNotes: expected_start_note ?? '',
    deadlinesLegacy: expected_start ?? null,
    priorities: priorities ?? [],
    prioritiesLegacy: prioritiesLegacy,
    prioritiesNotes: priority_qualitative_note ?? '',
    projectAims: projectAims ?? '',
    appearanceOutsideFront: appearanceOutsideFront ?? null,
    appearanceOutsideSideRear: appearanceOutsideSideRear ?? null,
    appearanceInternal: aesthetics_internal ?? null,
    appearanceOutsideLegacy: aesthetics_external ?? null,
    appearanceDetailsToPreserve: appearanceDetailsToPreserve ?? '',
    appearanceNotes: aesthetics_note ?? '',
    logisticsPhasing: logistics_packaging ?? null,
    logisticsDiy: logistics_diy ?? null,
    logisticsManagement: logisticsManagement ?? null,
    logisticsNotes: logistics_diy_note ?? '',
    disruption: logistics_disruption ?? null,
    disruptionNotes: logistics_disruption_note ?? '',
    budget: logistics_budget ?? null,
    budgetExact: budgetExact ?? '',
    budgetFinancing: budgetFinancing ?? [],
    budgetNotes: logistics_budget_note ?? '',
    externalNoisePollution: externalNoisePollution ?? '',
    externalAirPollution: externalAirPollution ?? null,
    smokeControlZone: smokeControlZone ?? null,
    heatNetworkNotes: heatNetworkNotes ?? '',
    heatNetworkInZone: heatNetworkInZone ?? null,
    siteOfSpecialScientificInterest: siteOfSpecialScientificInterest ?? '',
    worldHeritageSite: worldHeritageSite ?? null,
    nearAncientMonument: nearAncientMonument ?? null,
    commentary: {
      brief: commentary_brief ?? '',
      context: commentary_context ?? '',
      decisions: commentary_decisions ?? '',
    },
    addressSearch: {
      lookedUp: looked_up ?? false,
    },
    report: {
      date: report_date ?? '',
      signoff: report_signoff ?? '',
      version: report_version ?? '',
      reference: report_reference ?? '',
    },
    ...rest,
  };
}
