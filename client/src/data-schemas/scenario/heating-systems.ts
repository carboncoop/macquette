import { z } from 'zod';

import { legacyBoolean, stringyFloatSchema } from './value-schemas';

export const heatingSystems = z.array(
  z
    .object({
      id: z.union([z.number(), z.string()]),
      provides: z.enum(['water', 'heating_and_water', 'heating']),
      fraction_water_heating: stringyFloatSchema,
      instantaneous_water_heating: legacyBoolean,
      primary_circuit_loss: z.enum(['Yes', 'No']),
      combi_loss: z.union([
        z.literal(0),
        z.enum([
          '0',
          'Instantaneous, without keep hot-facility',
          'Instantaneous, with keep-hot facility controlled by time clock',
          'Instantaneous, with keep-hot facility not controlled by time clock',
          'Storage combi boiler >= 55 litres',
          'Storage combi boiler < 55 litres',
          // Buggy value that occurs in very old assessments due to an HTML
          // sanitisation bug in the old PHP backend.
          'Storage combi boiler  55 litres',
        ]),
      ]),
      category: z.enum([
        'Combi boilers',
        'System boilers',
        'Heat pumps',
        'Room heaters',
        'Warm air systems',
        'Hot water only',
      ]),
      sfp: z.union([z.literal('undefined'), z.literal(null), stringyFloatSchema]),
      central_heating_pump_inside: legacyBoolean,
      central_heating_pump: stringyFloatSchema,
      main_space_heating_system: z.enum([
        'mainHS1',
        'mainHS2_whole_house',
        'mainHS2_part_of_the_house',
        'secondaryHS',
      ]),
      fraction_space: stringyFloatSchema,
      responsiveness: stringyFloatSchema,
      heating_controls: z.union([
        z.literal(0),
        z.literal(1),
        z.literal(2),
        z.literal(3),
        z.literal('2').transform(() => 2 as const),
        z.literal('3').transform(() => 3 as const),
      ]),
      temperature_adjustment: stringyFloatSchema,
      fans_and_supply_pumps: z
        .union([stringyFloatSchema, z.nan(), z.literal(null), z.literal('null')])
        .transform((v) => (typeof v === 'number' && !Number.isNaN(v) ? v : null)),
      summer_efficiency: stringyFloatSchema,
      winter_efficiency: stringyFloatSchema,
    })
    .partial()
    .extend({ fuel: z.string() }),
);
