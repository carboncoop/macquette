import { z } from 'zod';

import {
  nullableStringyFloat,
  stringyFloatSchema,
  stringyIntegerSchema,
} from './value-schemas';

const optionalString = z.string().optional().default('');
const numberOrDefaultZero = stringyFloatSchema
  .optional()
  .nullable()
  .transform((val) => (val === '' ? 0 : (val ?? 0)));

const noCarbonAccountingSchema = z.object({
  carbonType: z.literal('none').optional().default('none'),
});

const basicCarbonAccountingSchema = z.object({
  carbonType: z.literal('basic'),
  carbonBaseUpfront: z.number().nullable().optional().default(null),
  carbonBaseBiogenic: z.number().nullable().optional().default(null),
  carbonPerUnitUpfront: z.number().nullable().optional().default(null),
  carbonPerUnitBiogenic: z.number().nullable().optional().default(null),
  carbonSource: z.string().optional().default(''),
});

const highLowCarbonAccountingSchema = z.object({
  carbonType: z.literal('high/low'),
  carbonHighBaseUpfront: z.number().nullable().optional().default(null),
  carbonHighBaseBiogenic: z.number().nullable().optional().default(null),
  carbonHighPerUnitUpfront: z.number().nullable().optional().default(null),
  carbonHighPerUnitBiogenic: z.number().nullable().optional().default(null),
  carbonLowBaseUpfront: z.number().nullable().optional().default(null),
  carbonLowBaseBiogenic: z.number().nullable().optional().default(null),
  carbonLowPerUnitUpfront: z.number().nullable().optional().default(null),
  carbonLowPerUnitBiogenic: z.number().nullable().optional().default(null),
  carbonSource: z.string().optional().default(''),
});

const anyAccountingSchema = z.union([
  noCarbonAccountingSchema,
  basicCarbonAccountingSchema,
  highLowCarbonAccountingSchema,
]);

const measureSchema = z.object({
  associated_work: optionalString,
  benefits: optionalString,
  cost: numberOrDefaultZero,
  cost_total: numberOrDefaultZero,
  cost_units: optionalString,
  min_cost: numberOrDefaultZero,
  lifetimeYears: z
    .number()
    .optional()
    .nullable()
    .transform((val) => val ?? null),
  description: optionalString,
  source: optionalString,
  disruption: optionalString,
  key_risks: optionalString,
  location: optionalString,
  maintenance: optionalString,
  name: z.string(),
  tag: z.string(),
  notes: optionalString,
  performance: optionalString,
  quantity: numberOrDefaultZero,
  who_by: optionalString,
});

const genericMeasure = z.intersection(anyAccountingSchema, measureSchema);

export type GenericMeasure = z.infer<typeof genericMeasure>;

const measureInMeasure = z.object({ measure: genericMeasure });
const measuresRecord = z.record(z.string(), measureInMeasure);

export const measures = z
  .object({
    ventilation: z
      .object({
        extract_ventilation_points: z.record(
          z.string(),
          z.object({
            measure: measureSchema
              .and(basicCarbonAccountingSchema.omit({ carbonType: true }))
              .and(
                z.object({
                  ventilation_rate: stringyIntegerSchema,
                  carbonType: z.literal('basic').optional().default('basic'),
                }),
              ),
          }),
        ),
        intentional_vents_and_flues_measures: z.record(
          z.string(),
          z.object({
            measure: measureSchema
              .and(basicCarbonAccountingSchema.omit({ carbonType: true }))
              .and(
                z.object({
                  ventilation_rate: stringyIntegerSchema,
                  carbonType: z.literal('basic').optional().default('basic'),
                }),
              ),
            original_elements: z.record(z.string(), z.unknown()).optional(),
          }),
        ),
        draught_proofing_measures: z.object({
          measure: measureSchema
            .and(basicCarbonAccountingSchema.omit({ carbonType: true }))
            .and(
              z.object({
                q50: stringyFloatSchema,
                carbonType: z.literal('basic').optional().default('basic'),
              }),
            ),
        }),
        ventilation_systems_measures: z.object({
          measure: measureSchema
            .and(basicCarbonAccountingSchema.omit({ carbonType: true }))
            .and(
              z.object({
                ventilation_type: z.enum(['NV', 'IE', 'MEV', 'PS', 'MVHR', 'MV', 'DEV']),
                specific_fan_power: nullableStringyFloat.transform((val) =>
                  val === '' ? null : val,
                ),
                system_air_change_rate: nullableStringyFloat.transform((val) =>
                  val === '' ? null : val,
                ),
                balanced_heat_recovery_efficiency: nullableStringyFloat.transform(
                  (val) => (val === '' ? null : val),
                ),
                carbonType: z.literal('basic').optional().default('basic'),
              }),
            ),
        }),
        clothes_drying_facilities: z.record(
          z.string(),
          z.object({
            measure: measureSchema
              .and(basicCarbonAccountingSchema.omit({ carbonType: true }))
              .and(
                z.object({
                  id: z.number(),
                  carbonType: z.literal('basic').optional().default('basic'),
                }),
              ),
          }),
        ),
      })
      .partial(),
    water_heating: z
      .object({
        water_usage: z.record(
          z.string(),
          z.object({
            measure: measureSchema
              .and(basicCarbonAccountingSchema.omit({ carbonType: true }))
              .and(
                z.object({
                  carbonType: z.literal('basic').optional().default('basic'),
                }),
              ),
          }),
        ),
        storage_type_measures: z.object({
          measure: measureSchema
            .and(basicCarbonAccountingSchema.omit({ carbonType: true }))
            .and(
              z.object({
                carbonType: z.literal('basic').optional().default('basic'),
                category: z.enum([
                  'Cylinders with inmersion',
                  'Indirectly heated cylinders',
                ]),
                declared_loss_factor_known: z.boolean(),
                manufacturer_loss_factor: z
                  .union([z.boolean(), stringyFloatSchema])
                  .optional(),
                temperature_factor_a: z
                  .union([
                    stringyFloatSchema,
                    z.literal('undefined').transform(() => '' as const),
                  ])
                  .optional(),
                storage_volume: z
                  .union([
                    stringyFloatSchema,
                    z.literal('undefined').transform(() => '' as const),
                  ])
                  .optional(),
                loss_factor_b: z
                  .union([
                    stringyFloatSchema,
                    z.literal('undefined').transform(() => '' as const),
                  ])
                  .optional(),
                volume_factor_b: z
                  .union([
                    stringyFloatSchema,
                    z.literal('undefined').transform(() => '' as const),
                  ])
                  .optional(),
                temperature_factor_b: z
                  .union([
                    stringyFloatSchema,
                    z.literal('undefined').transform(() => '' as const),
                  ])
                  .optional(),
              }),
            ),
        }),
        pipework_insulation: z.object({
          measure: measureSchema
            .and(basicCarbonAccountingSchema.omit({ carbonType: true }))
            .and(
              z.object({
                carbonType: z.literal('basic').optional().default('basic'),
                pipework_insulation: z.enum([
                  'All accesible piperwok insulated',
                  'First 1m from cylinder insulated',
                  'Fully insulated primary pipework',
                ]),
              }),
            ),
        }),
        hot_water_control_type: z.object({
          measure: measureSchema
            .and(basicCarbonAccountingSchema.omit({ carbonType: true }))
            .and(
              z.object({
                carbonType: z.literal('basic').optional().default('basic'),
                control_type: z.enum([
                  'Cylinder thermostat, water heating not separately timed',
                  'Cylinder thermostat, water heating separately timed',
                ]),
              }),
            ),
        }),
      })
      .partial(),
    LAC: z.object({ lighting: measureInMeasure }).partial(),
    PV_generation: z.object({
      measure: measureSchema
        .omit({ cost_units: true })
        .and(highLowCarbonAccountingSchema.omit({ carbonType: true }))
        .and(
          z.object({
            kWp: stringyFloatSchema,
            cost_units: z.enum(['unit', 'kWp']),
            carbonType: z.literal('high/low').optional().default('high/low'),
          }),
        ),
    }),
    space_heating_control_type: measuresRecord,
    heating_systems: measuresRecord,
    thermal_bridging: z.object({
      measure: z.object({
        value: stringyFloatSchema.nullable(),
        description: z.string(),
      }),
    }),
  })
  .partial();
