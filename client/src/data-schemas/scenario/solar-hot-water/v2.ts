import { z } from 'zod';
import { assertNever } from '../../../helpers/assert-never';
import { Orientation } from '../../../model/enums/orientation';
import { Overshading } from '../../../model/enums/overshading';
import { t } from '../../visitable-types';
import { makeZodSchema } from '../../visitable-types/zod';
import { SolarHotWaterV1 } from './v1';

export const shwV2 = z.object({
  version: z.literal(2),
  input: makeZodSchema(
    t.nullable(
      t.struct({
        pump: t.enum(['PV', 'electric'], { default_: 'electric' }),
        dedicatedSolarStorageVolume: t.number(),
        combinedCylinderVolume: t.number(),
        collector: t.struct({
          apertureArea: t.number(),
          inclination: t.number(),
          orientation: t.enum([...Orientation.names]),
          overshading: t.enum([...Overshading.names]),
          parameters: t.discriminatedUnion('source', [
            t.struct({
              source: t.literal('test certificate'),
              zeroLossEfficiency: t.number(),
              linearHeatLossCoefficient: t.number(),
              secondOrderHeatLossCoefficient: t.number(),
            }),
            t.struct({
              source: t.literal('estimate'),
              collectorType: t.enum(
                ['evacuated tube', 'flat plate, glazed', 'unglazed'],
                {
                  default_: 'unglazed',
                },
              ),
              apertureAreaType: t.enum(['gross', 'exact'], {
                default_: 'gross',
              }),
            }),
          ]),
        }),
      }),
    ),
  ),
});

export type SolarHotWaterV2 = z.infer<typeof shwV2>;

export function migrateV1ToV2(v1: SolarHotWaterV1): SolarHotWaterV2 {
  let parameters:
    | Exclude<SolarHotWaterV2['input'], null>['collector']['parameters']
    | null = null;
  switch (v1.input.collector.parameterSource) {
    case 'estimate': {
      parameters = {
        source: 'estimate',
        collectorType: v1.input.collector.estimate.collectorType ?? 'unglazed',
        apertureAreaType: v1.input.collector.estimate.apertureAreaType ?? 'gross',
      };
      break;
    }
    case 'test certificate':
    case null: {
      parameters = {
        source: 'test certificate',
        zeroLossEfficiency: v1.input.collector.testCertificate.zeroLossEfficiency ?? 0,
        linearHeatLossCoefficient:
          v1.input.collector.testCertificate.linearHeatLossCoefficient ?? 0,
        secondOrderHeatLossCoefficient:
          v1.input.collector.testCertificate.secondOrderHeatLossCoefficient ?? 0,
      };
      break;
    }
    default:
      assertNever(v1.input.collector.parameterSource);
  }
  const input = {
    pump: v1.pump ?? 'electric',
    combinedCylinderVolume: v1.input.combinedCylinderVolume ?? 0,
    dedicatedSolarStorageVolume: v1.input.dedicatedSolarStorageVolume ?? 0,
    collector: {
      apertureArea: v1.input.collector.apertureArea ?? 0,
      inclination: v1.input.collector.inclination ?? 0,
      orientation: v1.input.collector.orientation ?? 'East/West',
      overshading: v1.input.collector.overshading ?? '<20%',
      parameters,
    },
  };
  return {
    version: 2,
    input,
  };
}
