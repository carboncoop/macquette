import { z } from 'zod';
import { dateSchema } from './helpers/date';

export const organisationResponse = z.object({
  id: z.string(),
  name: z.string(),
  assessments: z.number(),
  members: z.array(
    z.object({
      id: z.string(),
      name: z.string(),
      email: z.string(),
      lastLogin: z.union([z.literal('never'), dateSchema]),
      isAdmin: z.boolean(),
      isLibrarian: z.boolean(),
    }),
  ),
  permissions: z.object({
    canEditMembersAndRoles: z.boolean(),
    canEditLibraries: z.boolean(),
    canShareLibraries: z.boolean(),
  }),
});

export const listOrganisationResponse = z.array(organisationResponse);
export type Organisation = z.output<typeof organisationResponse>;
