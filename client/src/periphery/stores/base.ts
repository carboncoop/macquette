export class Store {
  private subscribers: (() => void)[] = [];

  subscribe(onStoreChange: () => void): () => void {
    this.subscribers.push(onStoreChange);
    return () => {
      const indexOf = this.subscribers.indexOf(onStoreChange);
      this.subscribers.splice(indexOf, 1);
    };
  }

  notifySubscribers() {
    for (const sub of this.subscribers) {
      sub();
    }
  }
}
