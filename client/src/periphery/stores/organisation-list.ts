import { sortBy } from 'lodash';
import { HTTPClient } from '../../api/http';
import { Organisation } from '../../data-schemas/organisations';
import { Store } from './base';

export class OrganisationListStore extends Store {
  private state: Organisation[] = [];
  private client = new HTTPClient();

  constructor() {
    super();
    this.fetch().catch((err) => {
      console.error("Couldn't fetch", err);
    });
  }

  getSnapshot(): readonly Organisation[] {
    return this.state;
  }

  private async fetch() {
    const result = await this.client.listOrganisations();
    this.state = sortBy(result.unwrap(), ['name', 'id']);
    this.notifySubscribers();
  }
}

export class FakeOrganisationListStore extends Store {
  constructor(private state: Organisation[] = []) {
    super();
  }
  getSnapshot(): readonly Organisation[] {
    return this.state;
  }
}

export type OrganisationStoreType = Omit<OrganisationListStore, ''>;
