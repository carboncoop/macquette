import { HTTPClient } from '../../api/http';
import { AssessmentMetadata } from '../../data-schemas/api-metadata';
import { Store } from './base';

export type AssessmentListQueryParams = {
  search: string;
  organisation: string;
};

type AssessmentListStoreState = {
  list: AssessmentMetadata[];
  fetchInProgress: boolean;
  lastQuery: AssessmentListQueryParams;
};

const statusOrder = [
  'In progress',
  'For review',
  'Complete',
  'Template',
  'Paused',
  'Test',
  'Abandoned',
];

export class AssessmentListStore extends Store {
  private state: AssessmentListStoreState = {
    list: [],
    fetchInProgress: false,
    lastQuery: { search: '', organisation: '__ALL' },
  };
  private client = new HTTPClient();

  constructor() {
    super();
    this.fetch().catch((err) => {
      console.error("Couldn't fetch", err);
    });
  }

  getSnapshot(): AssessmentListStoreState {
    return this.state;
  }

  private sortedList(result: AssessmentMetadata[]) {
    return result.sort((a, b) => {
      // Primarily sort by status (lowest first), then by last mod (highest first)
      if (a.status !== b.status) {
        const aStatusNum = statusOrder.indexOf(a.status);
        const bStatusNum = statusOrder.indexOf(b.status);

        return aStatusNum - bStatusNum;
      } else {
        if (b.updatedAt > a.updatedAt) {
          return 1;
        } else if (b.updatedAt === a.updatedAt) {
          return 0;
        } else {
          return -1;
        }
      }
    });
  }

  private async fetch() {
    this.state = { ...this.state, fetchInProgress: true };
    this.notifySubscribers();

    const query = this.state.lastQuery;
    const result = await this.client.listAssessments(
      query.organisation === '__ALL' ? undefined : query.organisation,
      query.search === '' ? undefined : query.search,
    );

    this.state = { ...this.state, fetchInProgress: false, list: result };
    this.notifySubscribers();
  }

  async query(query: AssessmentListQueryParams) {
    this.state.lastQuery = query;
    await this.fetch();
  }

  async create(name: string, organisationId: string | null): Promise<string> {
    const newAssessment = await this.client.createAssessment(
      name,
      '',
      organisationId ?? undefined,
    );

    this.state = {
      ...this.state,
      list: this.sortedList([...this.state.list, newAssessment]),
    };
    this.notifySubscribers();

    return newAssessment.id;
  }

  async rename(id: string, name: string) {
    const updatedAssessment = await this.client.updateAssessment(id, { name });
    if (updatedAssessment === null) {
      throw new Error('Failure updating');
    }

    const renamedAssessment = this.state.list.find((assessment) => assessment.id === id);
    if (renamedAssessment !== undefined) {
      // We don't replace the whole element because the server response has a slightly
      // different format than the metadata-only format that we use here. This could be
      // changed in future when there is less legacy code around.
      renamedAssessment.name = updatedAssessment.name;
    }

    // This is a hack to make sure React redraws - if the identity of the state doesn't
    // change, no re-render is triggered.
    this.state = { ...this.state };
    this.notifySubscribers();
  }

  async duplicate(id: string): Promise<string> {
    const newAssessment = await this.client.duplicateAssessment(id);
    this.state = {
      ...this.state,
      list: this.sortedList([...this.state.list, newAssessment]),
    };
    this.notifySubscribers();

    return newAssessment.id;
  }

  async delete(id: string) {
    await this.client.deleteAssessment(id);

    this.state = {
      ...this.state,
      list: this.state.list.filter((assessment) => assessment.id !== id),
    };
    this.notifySubscribers();
  }
}

export class FakeAssessmentListStore extends Store {
  private state: AssessmentListStoreState;

  constructor(state: Partial<AssessmentListStoreState>) {
    super();
    this.state = {
      list: [],
      fetchInProgress: false,
      lastQuery: { search: '', organisation: '__ALL' },
      ...state,
    };
  }

  getSnapshot(): AssessmentListStoreState {
    return this.state;
  }

  // We're don't do any async stuff in the mock versions.
  /* eslint-disable @typescript-eslint/require-await */
  async query(query: AssessmentListQueryParams) {
    this.state = { ...this.state, lastQuery: query };
    this.notifySubscribers();
  }

  async create(name: string, organisationId: string | null): Promise<string> {
    this.state = {
      ...this.state,
      list: [
        ...this.state.list,
        {
          id: 'ID',
          name: name,
          description: '',
          status: 'In progress',
          updatedAt: new Date(),
          createdAt: new Date(),
          owner: { id: 'USERID', name: 'name', email: 'email@fake.email' },
          organisation:
            organisationId === null ? null : { id: organisationId, name: 'Organisation' },
        },
      ],
    };
    this.notifySubscribers();
    return 'ID';
  }

  async rename(id: string, name: string) {
    const assessment = this.state.list.find((assessment) => assessment.id === id);
    if (assessment === undefined) {
      throw new Error("Assessment ID to rename didn't exist");
    }

    assessment.name = name;
    this.state = { ...this.state };

    this.notifySubscribers();
  }

  async duplicate(id: string): Promise<string> {
    const assessment = this.state.list.find((assessment) => assessment.id === id);
    if (assessment === undefined) {
      throw new Error("Assessment ID to duplicate didn't exist");
    }

    this.state = {
      ...this.state,
      list: [...this.state.list, { ...assessment, id: 'ID' }],
    };
    this.notifySubscribers();

    return 'ID';
  }

  async delete(id: string) {
    const assessment = this.state.list.find((assessment) => assessment.id === id);
    if (assessment === undefined) {
      throw new Error("Assessment ID to delete didn't exist");
    }

    this.state = {
      ...this.state,
      list: this.state.list.filter((assessment) => assessment.id !== id),
    };
    this.notifySubscribers();
  }
}

export type AssessmentListStoreType = Omit<AssessmentListStore, ''>;
