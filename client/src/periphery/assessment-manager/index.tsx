import React, { useMemo, useState, useSyncExternalStore } from 'react';
import { PlusIcon } from '../../ui/icons';
import {
  ActionsButton,
  Menu,
  MenuItem,
  MenuTrigger,
  Popover,
} from '../../ui/input-components/action-menu';

import debounce from 'lodash/debounce';
import { urls } from '../../api/urls';
import { AssessmentMetadata } from '../../data-schemas/api-metadata';
import { Button as OurButton } from '../../ui/input-components/button';
import { Select } from '../../ui/input-components/select';
import { Spinner } from '../../ui/output-components/spinner';
import {
  AssessmentListQueryParams,
  AssessmentListStoreType,
} from '../stores/assessment-list';
import { OrganisationStoreType } from '../stores/organisation-list';
import { NewAssessmentModal } from './new-assessment-modal';

function handleError(intent: string) {
  return (err: unknown) => {
    alert(`Error while ${intent}`);
    console.error(`Error while trying to ${intent}`, err);
  };
}

function Status({ value }: { value: AssessmentMetadata['status'] }) {
  let className = '';
  if (value === 'Complete') {
    className = 'label-success';
  } else if (value === 'For review') {
    className = 'label-warning';
  } else if (value === 'Abandoned' || value === 'Test') {
    className = 'label-secondary';
  } else {
    className = 'label-info';
  }

  return <span className={`label ${className}`}>{value}</span>;
}

function RecentTimestamp({ value: date }: { value: Date }) {
  const now = new Date();
  let formatter: Intl.DateTimeFormat;

  if (
    now.getFullYear() === date.getFullYear() &&
    now.getMonth() === date.getMonth() &&
    now.getDay() === date.getDay()
  ) {
    formatter = Intl.DateTimeFormat('en-GB', {
      timeStyle: 'short',
    });
  } else {
    formatter = Intl.DateTimeFormat('en-GB', {
      dateStyle: 'short',
      timeStyle: 'short',
    });
  }

  return <>{formatter.format(date)}</>;
}

function AssessmentRow({
  assessment,
  store,
  isNew,
  setNewId,
}: {
  assessment: AssessmentMetadata;
  store: AssessmentListStoreType;
  isNew: boolean;
  setNewId: (id: string) => void;
}) {
  const [alteringRow, setAlteringRow] = useState(false);

  function handleAction(actionType: string | number) {
    if (
      actionType === 'delete assessment' &&
      !window.confirm('Really remove delete this assessment?')
    ) {
      return;
    }

    setAlteringRow(true);

    let promise: Promise<void>;
    if (actionType === 'rename assessment') {
      const newName = window.prompt('Rename to', assessment.name);
      if (newName === null) {
        setAlteringRow(false);
        return;
      }
      promise = store.rename(assessment.id, newName);
    } else if (actionType === 'delete assessment') {
      promise = store.delete(assessment.id);
    } else if (actionType === 'duplicate assessment') {
      promise = store.duplicate(assessment.id).then((newId) => setNewId(newId));
    } else {
      throw new Error('unhandled case');
    }

    promise.catch(handleError(actionType.toString())).finally(() => {
      setAlteringRow(false);
    });
  }

  return (
    <tr className={isNew ? 'new-row' : ''}>
      <td>
        <a className="unpad-8" href={urls.assessmentHTML(assessment.id)}>
          {assessment.name}
        </a>
        <br />
        <small>{assessment.description}</small>
      </td>
      <td className="align-right">
        <Status value={assessment.status} />
      </td>
      <td>
        {assessment.owner.name}
        {assessment.organisation !== null && (
          <>
            <br />
            <small>{assessment.organisation.name}</small>
          </>
        )}
      </td>
      <td className="text-nowrap">
        <RecentTimestamp value={assessment.updatedAt} />
      </td>
      <td>
        <div className="d-flex gap-7 align-items-center">
          <Spinner visibility={alteringRow} />
          <MenuTrigger>
            <ActionsButton />
            <Popover>
              <Menu onAction={handleAction}>
                <MenuItem id="rename assessment">Rename</MenuItem>
                <MenuItem id="duplicate assessment">Duplicate</MenuItem>
                <MenuItem id="delete assessment">Delete</MenuItem>
              </Menu>
            </Popover>
          </MenuTrigger>
        </div>
      </td>
    </tr>
  );
}

type AssessmentManagerProps = {
  assessmentStore: AssessmentListStoreType;
  organisationStore: OrganisationStoreType;
};

type ModalType = 'new';

export function AssessmentManager({
  assessmentStore,
  organisationStore,
}: AssessmentManagerProps) {
  const assessments = useSyncExternalStore(
    assessmentStore.subscribe.bind(assessmentStore),
    assessmentStore.getSnapshot.bind(assessmentStore),
  );
  const organisations = useSyncExternalStore(
    organisationStore.subscribe.bind(organisationStore),
    organisationStore.getSnapshot.bind(organisationStore),
  );

  const [modal, setModal] = useState<null | ModalType>(null);
  const [newId, setNewId] = useState<string | null>(null);
  const [query, setQuery] = useState<AssessmentListQueryParams>({
    search: '',
    organisation: '__ALL',
  });

  const orgOptions = [
    { value: '__ALL', display: 'Any' },
    ...organisations.map((org) => ({
      value: org.id,
      display: org.name,
    })),
  ];

  const debouncedQuery = useMemo(
    () =>
      debounce(
        (newQuery: AssessmentListQueryParams) => assessmentStore.query(newQuery),
        400,
      ),
    [assessmentStore],
  );

  function updateQuery(update: Partial<AssessmentListQueryParams>) {
    const newQuery = { ...query, ...update };
    setQuery(newQuery);
    const promise = debouncedQuery(newQuery);
    if (promise !== undefined) {
      promise.catch((err) => handleError('querying assessments')(err));
    }
  }

  const assessmentsList = useMemo(() => {
    return assessments.list.map((assessment, idx) => (
      <AssessmentRow
        store={assessmentStore}
        key={idx}
        assessment={assessment}
        isNew={assessment.id === newId}
        setNewId={setNewId}
      />
    ));
  }, [assessments.list, assessmentStore, newId]);

  return assessments === null ? (
    <div className="mx-auto max-width-1100 pt-15">
      <Spinner />
    </div>
  ) : (
    <section className="mx-auto max-width-1100 d-flex flex-v gap-15">
      <section className="px-0 line-top">
        <h1 className="ma-0">Assessments</h1>
      </section>

      {modal === 'new' && (
        <NewAssessmentModal
          assessmentStore={assessmentStore}
          organisationStore={organisationStore}
          onClose={(id?: string) => {
            if (id !== undefined) {
              setNewId(id);
            }
            setModal(null);
          }}
        />
      )}

      <div className="d-flex gap-7">
        <OurButton
          title="Create assessment"
          icon={PlusIcon}
          className="btn-primary"
          onClick={() => setModal('new')}
        />
      </div>

      <div
        className="rounded border-box-sizing bg-white pa-15 d-flex gap-30 align-items-center"
        style={{ width: 'fit-content' }}
      >
        <div>
          <label
            className="small-caps"
            style={{ width: 'max-content' }}
            htmlFor="search_text"
          >
            Name/Description
          </label>

          <input
            id="search_text"
            type="text"
            value={query.search}
            onChange={(evt) => {
              updateQuery({ search: evt.target.value });
            }}
            className="mb-0"
          />
        </div>
        <div>
          <label
            className="small-caps"
            style={{ width: 'min-content' }}
            htmlFor="organisation"
          >
            Organisation
          </label>
          <Select
            id="organisation"
            options={orgOptions}
            value={query.organisation}
            onChange={(val) => {
              updateQuery({ organisation: val });
            }}
            className="mb-0"
          />
        </div>
        <Spinner visibility={assessments.fetchInProgress} />
      </div>

      {assessments.list.length > 0 && (
        <div
          className="rounded border-box-sizing bg-white pa-15"
          style={{ width: 'fit-content' }}
        >
          <table className="table table--vertical-middle" style={{ width: 'auto' }}>
            <thead>
              <tr style={{ backgroundColor: 'var(--grey-800)' }}>
                <th scope="col">Name</th>
                <th scope="col">Status</th>
                <th scope="col">Owner</th>
                <th scope="col">Modified</th>
                <th></th>
              </tr>
            </thead>
            <tbody>{assessmentsList}</tbody>
          </table>
        </div>
      )}
    </section>
  );
}
