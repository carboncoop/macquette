import React, { useId, useMemo, useSyncExternalStore } from 'react';
import { Button } from '../../ui/input-components/button';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from '../../ui/output-components/modal';
import type { LibraryListStoreType } from '../stores/library-list';
import {
  OrganisationListStore,
  OrganisationStoreType,
} from '../stores/organisation-list';

export function ShareModal({
  libraryStore,
  libraryId,
  organisationStore: orgStoreIn,
  onClose,
}: {
  libraryStore: LibraryListStoreType;
  libraryId: string;
  organisationStore?: OrganisationStoreType | undefined;
  onClose: () => void;
}) {
  const libraries = useSyncExternalStore(
    libraryStore.subscribe.bind(libraryStore),
    libraryStore.getSnapshot.bind(libraryStore),
  );
  const organisationStore = useMemo(
    () => (orgStoreIn !== undefined ? orgStoreIn : new OrganisationListStore()),
    [orgStoreIn],
  );
  const organisations = useSyncExternalStore(
    organisationStore.subscribe.bind(organisationStore),
    organisationStore.getSnapshot.bind(organisationStore),
  );
  const headerId = useId();

  const library = libraries.find((lib) => lib.id === libraryId);
  if (library === undefined) {
    throw new Error("Library doesn't exist");
  }
  const libraryOwnerId = library.owner.id;
  if (libraryOwnerId === null) {
    throw new Error("Library doesn't have an organisational owner");
  }
  const librarySharedWith = (library.shared_with ?? []).map((sharee) => sharee.id);

  return (
    <Modal headerId={headerId} onClose={onClose}>
      <ModalHeader title={'Share library'} onClose={onClose}>
        {library.name}
      </ModalHeader>
      <ModalBody>
        <p tabIndex={-1} autoFocus={true}>
          Sharing a library with another organisation allows them to use it but not to
          edit it.
        </p>

        <table className="table table--vertical-middle" style={{ width: 'auto' }}>
          <thead>
            <tr>
              <th>Organisation</th>
              <th style={{ minWidth: '6em' }}>Status</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {organisations
              .filter(
                (org) =>
                  library?.owner.type !== 'organisation' || org.id !== library?.owner.id,
              )
              .map((org, idx) => (
                <tr key={org.id}>
                  <th
                    scope="row"
                    id={`org-name-${idx}`}
                    style={{ verticalAlign: 'middle' }}
                  >
                    {org.name}
                  </th>
                  <td>{librarySharedWith.includes(org.id) ? 'shared' : 'not shared'}</td>

                  {org.permissions.canShareLibraries &&
                    (librarySharedWith.includes(org.id) ? (
                      <td>
                        <Button
                          title="Stop sharing"
                          aria-label={`Stop sharing with ${org.name}`}
                          onClick={() => {
                            libraryStore
                              .unshare(libraryOwnerId, library.id, org.id)
                              .catch((err) => {
                                alert('Error while unsharing library');
                                console.error('Error while unsharing library', err);
                              });
                          }}
                        />
                      </td>
                    ) : (
                      <td>
                        <Button
                          title="Share"
                          aria-label={`Share with ${org.name}`}
                          onClick={() => {
                            libraryStore
                              .share(libraryOwnerId, library.id, org.id)
                              .catch((err) => {
                                alert('Error while sharing library');
                                console.error('Error while sharing library', err);
                              });
                          }}
                        />
                      </td>
                    ))}
                </tr>
              ))}
          </tbody>
        </table>
      </ModalBody>
      <ModalFooter>
        <button onClick={onClose} className="btn">
          Done
        </button>
      </ModalFooter>
    </Modal>
  );
}
