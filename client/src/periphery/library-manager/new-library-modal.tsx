import React, { useId, useMemo, useState, useSyncExternalStore } from 'react';
import { FormGrid } from '../../ui/input-components/forms';
import { RadioGroup } from '../../ui/input-components/radio-group';
import { Select } from '../../ui/input-components/select';
import { TextInput } from '../../ui/input-components/text';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from '../../ui/output-components/modal';
import type { LibraryListStoreType } from '../stores/library-list';
import type { OrganisationStoreType } from '../stores/organisation-list';
import { OrganisationListStore } from '../stores/organisation-list';
import { LibraryType, libraryTypeOptions } from './types';

const privateValue = '__PRIVATE';

export function NewLibraryModal({
  libraryStore,
  organisationStore: orgStoreIn,
  onClose,
}: {
  libraryStore: LibraryListStoreType;
  organisationStore?: OrganisationStoreType | undefined;
  onClose: () => void;
}) {
  const organisationStore = useMemo(
    () => (orgStoreIn !== undefined ? orgStoreIn : new OrganisationListStore()),
    [orgStoreIn],
  );
  const organisations = useSyncExternalStore(
    organisationStore.subscribe.bind(organisationStore),
    organisationStore.getSnapshot.bind(organisationStore),
  );
  const headerId = useId();
  const [name, setName] = useState('');
  const [type, setType] = useState<LibraryType | null>(null);
  const [ownership, setOwnership] = useState(privateValue);

  const ownershipOptions = [
    { value: privateValue, display: 'Private' },
    ...organisations.map((organisation) => ({
      value: organisation.id,
      display: organisation.name,
    })),
  ];

  return (
    <Modal headerId={headerId} onClose={onClose}>
      <ModalHeader title={'New library'} onClose={onClose} />
      <ModalBody>
        <FormGrid>
          <label htmlFor="name">Name:</label>
          <div>
            <TextInput id="name" value={name} onChange={setName} autoFocus={true} />
          </div>
          <label htmlFor="type">Type:</label>
          <div>
            <Select
              id="type"
              options={libraryTypeOptions}
              value={type}
              onChange={setType}
              className="input--auto-width"
            />
          </div>
          <div id="ownership-label">Ownership:</div>
          <div>
            <RadioGroup
              options={ownershipOptions}
              ariaLabelledBy={'ownership-label'}
              value={ownership}
              onChange={setOwnership}
              radioClasses={['mr-3']}
            />
          </div>
        </FormGrid>
      </ModalBody>
      <ModalFooter>
        <button onClick={onClose} className="btn">
          Cancel
        </button>
        <button
          onClick={() => {
            if (name === '') {
              alert('Library name must be set');
              return;
            }
            if (type === null) {
              alert('Library type must be set');
              return;
            }
            libraryStore
              .create({
                name,
                type,
                ownership:
                  ownership === privateValue
                    ? { type: 'private' }
                    : { type: 'organisation', id: ownership },
              })
              .catch((err) => {
                alert('Error while creating library');
                console.error('Error while creating library', err);
              });
            onClose();
          }}
          className="btn btn-primary"
        >
          Create
        </button>
      </ModalFooter>
    </Modal>
  );
}
