import React, { useState, useSyncExternalStore } from 'react';

import { Organisation } from '../../data-schemas/organisations';
import { PlusIcon } from '../../ui/icons';
import {
  ActionsButton,
  Menu,
  MenuItem,
  MenuTrigger,
  Popover,
} from '../../ui/input-components/action-menu';
import { Button as OurButton } from '../../ui/input-components/button';
import { Spinner } from '../../ui/output-components/spinner';
import { OrganisationStoreType } from '../stores/organisation';
import { InviteUserModal } from './invite-user-modal';

type OrgMember = Organisation['members'][number];

function formatRoles(member: OrgMember) {
  const roles: string[] = [];

  if (member.isAdmin) {
    roles.push('Admin');
  }
  if (member.isLibrarian) {
    roles.push('Librarian');
  }

  return roles.join(', ');
}

function handleError(intent: string) {
  return (err: unknown) => {
    alert(`Error while ${intent}`);
    console.error(`Error while trying to ${intent}`, err);
  };
}

function MemberRow({
  store,
  organisation,
  member,
  userId,
}: {
  store: OrganisationStoreType;
  organisation: Organisation;
  member: OrgMember;
  userId: string;
}) {
  const [alteringRow, setAlteringRow] = useState(false);

  const memberIsUser = member.id === userId;

  function handleAction(actionType: string | number) {
    if (
      actionType === 'demote admin' &&
      memberIsUser &&
      !window.confirm(
        'Really remove yourself as admin? If you want to undo this action, you will' +
          'need another admin to make you an admin again afterwards.',
      )
    ) {
      return;
    }

    if (
      actionType === 'remove member' &&
      memberIsUser &&
      !window.confirm('Really remove yourself from this organisation?')
    ) {
      return;
    }

    setAlteringRow(true);

    let promise: Promise<void>;
    if (actionType === 'demote librarian') {
      promise = store.demoteLibrarian(member.id);
    } else if (actionType === 'promote librarian') {
      promise = store.promoteLibrarian(member.id);
    } else if (actionType === 'demote admin') {
      promise = store.demoteAdmin(member.id);
    } else if (actionType === 'promote admin') {
      promise = store.promoteAdmin(member.id);
    } else if (actionType === 'remove member') {
      promise = store.removeMember(member.id);
    } else {
      throw new Error('unhandled case');
    }

    promise.catch(handleError(actionType.toString())).finally(() => {
      setAlteringRow(false);
    });
  }

  return (
    <tr>
      <td>
        {member.name} {memberIsUser && <span>(you!)</span>}
      </td>
      <td>{member.email}</td>
      <td>
        {member.lastLogin === 'never'
          ? 'never'
          : Intl.DateTimeFormat().format(member.lastLogin)}
      </td>
      <td>{formatRoles(member)}</td>
      <td>
        <div className="d-flex gap-7 align-items-center">
          <Spinner visibility={alteringRow} />
          <MenuTrigger>
            <ActionsButton
              isDisabled={!organisation.permissions.canEditMembersAndRoles || alteringRow}
            />
            <Popover>
              <Menu onAction={handleAction}>
                {member.isLibrarian ? (
                  <MenuItem id="demote librarian">Demote librarian</MenuItem>
                ) : (
                  <MenuItem id="promote librarian">Make librarian</MenuItem>
                )}
                {member.isAdmin ? (
                  <MenuItem id="demote admin">Demote admin</MenuItem>
                ) : (
                  <MenuItem id="promote admin">Make admin</MenuItem>
                )}
                <MenuItem id="remove member">Remove</MenuItem>
              </Menu>
            </Popover>
          </MenuTrigger>
        </div>
      </td>
    </tr>
  );
}

type OrganisationEditorParams = {
  organisationStore: OrganisationStoreType;
  userId: string;
};

type ModalType = { type: 'invite' };

export function OrganisationEditor({
  userId,
  organisationStore,
}: OrganisationEditorParams) {
  const organisation = useSyncExternalStore(
    organisationStore.subscribe.bind(organisationStore),
    organisationStore.getSnapshot.bind(organisationStore),
  );
  const [modal, setModal] = useState<null | ModalType>(null);

  return organisation === null ? (
    <div className="mx-auto max-width-1100 pt-15">
      <span className="spinner" />
    </div>
  ) : (
    <section>
      <div className="mx-auto max-width-1100">
        <section className="px-0 line-top">
          <h1 className="ma-0">{organisation.name}</h1>
        </section>
      </div>

      {modal?.type === 'invite' && (
        <InviteUserModal
          organisationStore={organisationStore}
          onClose={() => {
            setModal(null);
          }}
        />
      )}

      <div className="mx-auto max-width-1100 pt-15 d-flex gap-7">
        <OurButton
          title="Invite user"
          icon={PlusIcon}
          className="btn-primary"
          disabled={!organisation.permissions.canEditMembersAndRoles}
          tooltip={
            organisation.permissions.canEditMembersAndRoles
              ? undefined
              : 'You do not have permission to invite users'
          }
          onClick={() => {
            setModal({ type: 'invite' });
          }}
        />
      </div>

      <div className="mx-auto max-width-1100 mt-15 rounded border-box-sizing">
        <table
          className="table table--vertical-middle bg-white"
          style={{ width: 'auto' }}
        >
          <thead>
            <tr>
              <th scope="col">User</th>
              <th scope="col">Email</th>
              <th scope="col">Last active</th>
              <th scope="col" style={{ minWidth: '12ch' }}>
                Role
              </th>
            </tr>
          </thead>
          <tbody>
            {organisation.members.map((member, idx) => (
              <MemberRow
                key={idx}
                member={member}
                organisation={organisation}
                store={organisationStore}
                userId={userId}
              />
            ))}
          </tbody>
        </table>
      </div>
    </section>
  );
}
