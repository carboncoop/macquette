import { cloneDeep, isEqual } from 'lodash';
import React, { ReactNode, useLayoutEffect, useReducer } from 'react';
import { partition } from '../../helpers/partition';
import { Result } from '../../helpers/result';
import { DeleteIcon, PlusIcon } from '../../ui/icons';
import { Button } from '../../ui/input-components/button';
import { TextInput } from '../../ui/input-components/text';

export type StoredBase = { tag: string };
export type EditableBase = { id: number; tag: string };

export type ValidationErrors<T extends object> = {
  [K in keyof T]?: string;
};

export type EditorDefinition<T extends EditableBase, U extends StoredBase> = {
  blank: T;
  fromStored: (stored: U, idx: number) => Result<T, Error>;
  toStored: (row: T) => Result<U, ValidationErrors<T>>;
  columns: {
    title: string;
    display: (item: T) => ReactNode;
    edit: (
      item: T,
      errors: ValidationErrors<T>,
      onUpdate: (item: T) => void,
    ) => ReactNode;
  }[];
};

type ErrorState = {
  mode: 'error initialising';
};

type ViewState<T extends EditableBase, U extends StoredBase> = {
  mode: 'view';
  dataset: T[];
  definition: EditorDefinition<T, U>;
};

type EditState<T extends EditableBase, U extends StoredBase> = {
  mode: 'edit';
  definition: EditorDefinition<T, U>;
  focusOn: number | null;
  originalDataset: T[];
  dataset: Array<[T, ValidationErrors<T>]>;
  deletedIds: number[];
  modifiedIds: number[];
};

type EditableDatasetType<T extends EditableBase> = EditState<T, never>['dataset'];

type State<T extends EditableBase, U extends StoredBase> =
  | ErrorState
  | ViewState<T, U>
  | EditState<T, U>;

type Action<T extends EditableBase> =
  | { type: 'start editing' }
  | { type: 'cancel editing' }
  | { type: 'save changes' }
  | { type: 'drop focus' }
  | { type: 'insert new row'; row: T }
  | { type: 'delete row'; id: number }
  | { type: 'replace row'; id: number; data: T };

function withRemovedItem<T>(arr: T[], id: number): T[] {
  return [...arr.slice(0, id), ...arr.slice(id + 1)];
}

function withReplacedItem<T>(arr: T[], id: number, item: T): T[] {
  return [...arr.slice(0, id), item, ...arr.slice(id + 1)];
}

function calcDeletedIds<T extends EditableBase>(
  originalDataset: T[],
  newDataset: EditableDatasetType<T>,
): number[] {
  const origIds = originalDataset.map((row) => row.id);
  const newIds = newDataset.map(([row]) => row.id);
  const deletedIds: number[] = [];

  for (const origId of origIds) {
    if (!newIds.includes(origId)) {
      deletedIds.push(origId);
    }
  }

  return deletedIds;
}

function calcModifiedIds<T extends EditableBase>(
  originalDataset: T[],
  newDataset: EditableDatasetType<T>,
): number[] {
  const modifiedIds: number[] = [];

  for (const [newItem] of newDataset) {
    const origItem = originalDataset.find((origItem) => origItem.id === newItem.id);
    if (origItem === undefined) {
      modifiedIds.push(newItem.id);
    }
    // Maybe this needs to be overridable on a per-editor basis
    if (!isEqual(origItem, newItem)) {
      modifiedIds.push(newItem.id);
    }
  }

  return modifiedIds;
}

function calcDuplicatedIds<T extends EditableBase>(
  dataset: EditableDatasetType<T>,
): number[] {
  const occurances: Record<string, number> = {};

  for (const [item] of dataset) {
    occurances[item.tag] = (occurances[item.tag] ?? 0) + 1;
  }

  const duplicatedIds = new Set<number>();
  for (const [tag, count] of Object.entries(occurances)) {
    if (count < 2) {
      continue;
    }

    for (const [item] of dataset.filter(([item]) => item.tag === tag)) {
      duplicatedIds.add(item.id);
    }
  }

  return [...duplicatedIds];
}

function reducer<T extends EditableBase, U extends StoredBase>(
  state: State<T, U>,
  action: Action<T>,
): State<T, U> {
  console.log(state, action);
  switch (action.type) {
    case 'start editing': {
      if (state.mode !== 'view') {
        return state;
      }
      return {
        mode: 'edit',
        definition: state.definition,
        originalDataset: state.dataset,
        dataset: state.dataset.map((row) => [row, {}]),
        deletedIds: [],
        modifiedIds: [],
        focusOn: null,
      };
    }
    case 'cancel editing': {
      if (state.mode !== 'edit') {
        return state;
      } else {
        return {
          mode: 'view',
          dataset: state.originalDataset,
          definition: state.definition,
        };
      }
    }
    case 'save changes': {
      if (state.mode !== 'edit') {
        return state;
      }

      return {
        mode: 'view',
        dataset: state.dataset.map(([data]) => data),
        definition: state.definition,
      };
    }
    case 'insert new row': {
      if (state.mode !== 'edit') {
        return state;
      }

      const allIds = [
        ...state.originalDataset.map((row) => row.id),
        ...state.dataset.map(([row]) => row.id),
      ];
      const highestId = allIds.reduce((prev, curr) => (curr > prev ? curr : prev), -1);

      const newItem = cloneDeep(action.row);
      newItem.id = highestId + 1;

      const newItemErrors: ValidationErrors<T> = state.definition
        .toStored(newItem)
        .mapOk(() => ({}))
        .coalesce();

      return {
        ...state,
        focusOn: newItem.id,
        dataset: [...state.dataset, [newItem, newItemErrors]],
      };
    }
    case 'drop focus': {
      if (state.mode !== 'edit') {
        return state;
      }
      return { ...state, focusOn: null };
    }
    case 'delete row': {
      if (state.mode !== 'edit') {
        return state;
      }
      const index = state.dataset.findIndex(([row]) => row.id === action.id);
      if (index === -1) {
        return state;
      }
      const newDataset = withRemovedItem(state.dataset, index);
      return {
        ...state,
        dataset: newDataset,
        deletedIds: calcDeletedIds(state.originalDataset, newDataset),
      };
    }
    case 'replace row': {
      if (state.mode !== 'edit') {
        return state;
      }
      const index = state.dataset.findIndex(([row]) => row.id === action.id);
      if (index === -1) {
        return state;
      }

      const errors: ValidationErrors<T> = state.definition
        .toStored(action.data)
        .mapOk(() => ({}))
        .coalesce();
      const newDataset = withReplacedItem(state.dataset, index, [
        action.data,
        errors,
      ] satisfies [T, ValidationErrors<T>]);
      const duplicatedIds = calcDuplicatedIds(newDataset);
      for (const row of newDataset) {
        if (duplicatedIds.includes(row[0].id)) {
          row[1]['tag'] = 'Duplicate tag';
        } else {
          delete row[1]['tag'];
        }
      }

      const newState = {
        ...state,
        dataset: newDataset,
        modifiedIds: calcModifiedIds(state.originalDataset, newDataset),
      };

      return newState;
    }
  }
}

export type ViewTableParams<T extends EditableBase, U extends StoredBase> = {
  state: ViewState<T, U>;
  dispatch: (action: Action<T>) => void;
};

export function ViewTable<T extends EditableBase, U extends StoredBase>({
  state,
  dispatch,
}: ViewTableParams<T, U>) {
  return (
    <div className="d-flex flex-v gap-7">
      <div>
        <button
          className="btn"
          onClick={() => {
            dispatch({ type: 'start editing' });
          }}
        >
          Edit
        </button>
      </div>
      <table className="table bg-white table--library-editor">
        <thead>
          <tr style={{ position: 'sticky', top: 0, backgroundColor: '#eee', zIndex: 2 }}>
            <th>Tag</th>
            {state.definition.columns.map((entry) => (
              <th key={entry.title}>{entry.title}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {state.dataset.map((row: T) => {
            return (
              <tr key={row.id}>
                <th>{row.tag}</th>
                {state.definition.columns.map((column) => (
                  <td key={column.title}>{column.display(row)}</td>
                ))}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export type EditTableParams<T extends EditableBase, U extends StoredBase> = {
  state: EditState<T, U>;
  onSave: (dataset: Record<string, U>) => Promise<void>;
  dispatch: (action: Action<T>) => void;
};

export function EditTable<T extends EditableBase, U extends StoredBase>({
  state,
  onSave,
  dispatch,
}: EditTableParams<T, U>) {
  useLayoutEffect(() => {
    if (state.focusOn === null) {
      return;
    }
    const elem: HTMLElement | null = document.querySelector('.just-inserted');
    if (elem !== null) {
      elem.focus();
      dispatch({ type: 'drop focus' });
    }
  }, [state.focusOn, dispatch]);

  return (
    <div className="d-flex flex-v gap-7">
      <div className="d-flex gap-7">
        <button
          className="btn btn-primary"
          onClick={() => {
            const toSaveR = Result.fromThrowing(() => {
              const duplicatedIds = calcDuplicatedIds(state.dataset);
              if (duplicatedIds.length > 0) {
                throw new Error('duplicate ids');
              }

              return Object.fromEntries(
                state.dataset.map(([data]) => [
                  data.tag,
                  state.definition.toStored(data).unwrap(),
                ]),
              );
            })();

            if (toSaveR.isErr()) {
              alert('There are validation errors - please fix before saving.');
              return;
            }

            const data = toSaveR.unwrap();
            onSave(data)
              .then(() => {
                dispatch({ type: 'save changes' });
              })
              .catch((err) => {
                alert('Error while saving library');
                console.error('Failed while saving library', err);
              });
          }}
        >
          Save
        </button>

        <button
          className="btn"
          onClick={() => {
            if (
              state.modifiedIds.length > 0 &&
              !window.confirm('Discard unsaved changes?')
            ) {
              return;
            }

            dispatch({ type: 'cancel editing' });
          }}
        >
          Cancel
        </button>
      </div>
      <table className="table bg-white table--library-editor">
        <thead>
          <tr style={{ position: 'sticky', top: 0, backgroundColor: '#eee', zIndex: 2 }}>
            <th>Tag</th>
            {state.definition.columns.map((entry) => (
              <th key={entry.title}>{entry.title}</th>
            ))}
            <th></th>
          </tr>
        </thead>
        <tbody>
          {state.dataset.map(([row, errors]) => {
            return (
              <tr key={row.id}>
                <th className="text-nowrap">
                  <TextInput
                    value={row.tag}
                    error={errors['tag']}
                    onChange={(tag) =>
                      dispatch({
                        type: 'replace row',
                        id: row.id,
                        data: { ...row, tag },
                      })
                    }
                    style={{ width: '3rem' }}
                    className={
                      state.focusOn !== null && state.focusOn === row.id
                        ? 'just-inserted'
                        : ''
                    }
                  />
                  {state.modifiedIds.includes(row.id) && '*'}
                </th>
                {state.definition.columns.map((column) => (
                  <td key={column.title}>
                    {column.edit(row, errors, (newData) =>
                      dispatch({
                        type: 'replace row',
                        id: row.id,
                        data: newData,
                      }),
                    )}
                  </td>
                ))}
                <td>
                  <Button
                    className="btn-danger"
                    icon={DeleteIcon}
                    onClick={() => {
                      dispatch({ type: 'delete row', id: row.id });
                    }}
                  />
                </td>
              </tr>
            );
          })}
          <tr>
            <td colSpan={5}>
              <Button
                icon={PlusIcon}
                title="New row"
                onClick={() => {
                  dispatch({
                    type: 'insert new row',
                    row: state.definition.blank,
                  });
                }}
              />
            </td>
          </tr>
          {state.deletedIds.length > 0 && (
            <tr>
              <td colSpan={8}>{state.deletedIds.length} deleted rows</td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
}

export type EditorTableParams<T extends EditableBase, U extends StoredBase> = {
  data: Record<string | symbol, U>;
  onSave: (dataset: Record<string, U>) => Promise<void>;
  definition: EditorDefinition<T, U>;
};

export function LibraryTable<T extends EditableBase, U extends StoredBase>({
  data,
  onSave,
  definition,
}: EditorTableParams<T, U>) {
  const [state, dispatch] = useReducer<
    React.Reducer<State<T, U>, Action<T>>,
    typeof data
  >(reducer, data, (data): State<T, U> => {
    for (const [key, item] of Object.entries(data)) {
      if (key !== item.tag) {
        console.error(`Object key tag ${key} doesn't match tag ${item.tag}`);
        return {
          mode: 'error initialising',
        };
      }
    }

    const parsed = Object.values(data)
      .map(definition.fromStored)
      .map((item) => item.coalesce());
    const [errors, dataset] = partition(
      parsed,
      (item): item is Error => item instanceof Error,
    );

    if (errors.length > 0) {
      console.error(errors);
      return {
        mode: 'error initialising',
      };
    } else {
      return {
        mode: 'view',
        dataset,
        definition,
      };
    }
  });

  return (
    <div className="d-flex flex-v gap-7">
      {state.mode === 'error initialising' && (
        <div>Error initialising: please contact support.</div>
      )}
      {state.mode === 'view' && <ViewTable state={state} dispatch={dispatch} />}
      {state.mode === 'edit' && (
        <EditTable state={state} dispatch={dispatch} onSave={onSave} />
      )}
    </div>
  );
}
