import React from 'react';
import { Library } from '../../../data-schemas/libraries';
import { Result } from '../../../helpers/result';
import { NumberInput } from '../../../ui/input-components/number';
import { Select } from '../../../ui/input-components/select';
import { TextInput } from '../../../ui/input-components/text';
import { EditorDefinition, ValidationErrors } from '../editor';
import {
  BasicCarbon,
  CommonMeasureFields,
  blankBasicCarbon,
  blankMeasure,
  carbonBasicDefinition,
  measureDefinition,
} from './--common-measures';

type StoredItemCollection = Extract<Library, { type: 'storage_type' }>['data'];
type StoredItem = StoredItemCollection[string];

export type EditableItem = {
  id: number;
  tag: string;
  name: string;
  category: 'Cylinders with inmersion' | 'Indirectly heated cylinders' | null;
  storageVolume: number | null;
  manufacturerLossFactor: 'unknown' | 'known';
  lossFactor: number | null;
  volumeFactor: number | null;
  temperatureFactor: number | null;
  source: string;
};

type StoredMeasureCollection = Extract<
  Library,
  { type: 'storage_type_measures' }
>['data'];
type StoredMeasure = StoredMeasureCollection[string];

type EditableMeasure = EditableItem & CommonMeasureFields & BasicCarbon;

function sharedColumns<T extends EditableItem>(): EditorDefinition<T, never>['columns'] {
  return [
    {
      title: 'Name',
      display: (item) => item.name,
      edit: (item, errors, onUpdate) => (
        <TextInput
          type="text"
          value={item.name}
          error={errors['name']}
          onChange={(name) => onUpdate({ ...item, name })}
          style={{ width: '20rem' }}
        />
      ),
    },
    {
      title: 'Category',
      display: (item) => item.category,
      edit: (item, errors, onUpdate) => (
        <Select
          className="input--auto-width"
          onChange={(category) => onUpdate({ ...item, category })}
          value={item.category}
          error={errors['category']}
          options={[
            { display: 'Cylinders with immersion', value: 'Cylinders with inmersion' },
            {
              display: 'Indirectly heated cylinders',
              value: 'Indirectly heated cylinders',
            },
          ]}
        />
      ),
    },
    {
      title: 'Storage volume',
      display: (item) => item.storageVolume,
      edit: (item, errors, onUpdate) => (
        <NumberInput
          value={item.storageVolume}
          error={errors['storageVolume']}
          onChange={(storageVolume) => onUpdate({ ...item, storageVolume })}
          style={{ width: '1.5rem' }}
          unit="litres"
        />
      ),
    },
    {
      title: 'Manufacturer loss factor',
      display: (item) => item.manufacturerLossFactor,
      edit: (item, errors, onUpdate) => (
        <Select
          className="input--auto-width"
          onChange={(manufacturerLossFactor) =>
            onUpdate({ ...item, manufacturerLossFactor })
          }
          value={item.manufacturerLossFactor}
          error={errors['manufacturerLossFactor']}
          options={[
            { display: 'known', value: 'known' },
            { display: 'unknown', value: 'unknown' },
          ]}
        />
      ),
    },
    {
      title: 'Loss factor',
      display: (item) => item.lossFactor,
      edit: (item, errors, onUpdate) => (
        <NumberInput
          value={item.lossFactor}
          error={errors['lossFactor']}
          onChange={(lossFactor) => onUpdate({ ...item, lossFactor })}
          style={{ width: '3rem' }}
          unit="kWh/litre/day"
        />
      ),
    },
    {
      title: 'Volume factor',
      display: (item) => item.volumeFactor,
      edit: (item, errors, onUpdate) =>
        item.manufacturerLossFactor === 'unknown' ? (
          <NumberInput
            value={item.volumeFactor}
            error={errors['volumeFactor']}
            onChange={(volumeFactor) => onUpdate({ ...item, volumeFactor })}
            style={{ width: '3rem' }}
          />
        ) : (
          '-'
        ),
    },
    {
      title: 'Temperature factor',
      display: (item) => item.temperatureFactor,
      edit: (item, errors, onUpdate) => (
        <NumberInput
          value={item.temperatureFactor}
          error={errors['temperatureFactor']}
          onChange={(temperatureFactor) => onUpdate({ ...item, temperatureFactor })}
          style={{ width: '2rem' }}
        />
      ),
    },
    {
      title: 'Source',
      display: (item) => item.source,
      edit: (item, errors, onUpdate) => (
        <TextInput
          type="text"
          value={item.source}
          error={errors['source']}
          onChange={(source) => onUpdate({ ...item, source })}
          style={{ width: '18rem' }}
        />
      ),
    },
  ];
}

export const hotWaterStorageDefinition: EditorDefinition<EditableItem, StoredItem> = {
  toStored: toStoredItem,
  fromStored: fromStoredItem,
  blank: {
    id: 0,
    tag: '',
    name: '',
    category: null,
    manufacturerLossFactor: 'unknown',
    lossFactor: null,
    storageVolume: null,
    volumeFactor: null,
    temperatureFactor: null,
    source: '',
  },
  columns: sharedColumns(),
};

export const hotWaterStorageMeasureDefinition: EditorDefinition<
  EditableMeasure,
  StoredMeasure
> = {
  toStored: toStoredMeasure,
  fromStored: fromStoredMeasure,
  blank: {
    ...hotWaterStorageDefinition.blank,
    ...blankMeasure,
    measureCostUnits: 'unit',
    ...blankBasicCarbon,
  },
  columns: [
    ...sharedColumns<EditableMeasure>(),
    ...measureDefinition<EditableMeasure, StoredMeasure>({
      includeBaseCost: false,
    }),
    ...carbonBasicDefinition<EditableMeasure, StoredMeasure>({ includeBaseCost: false }),
  ],
};

function fromStoredItem(data: StoredItem, idx: number): Result<EditableItem, Error> {
  return Result.ok({
    id: idx,
    tag: data.tag,
    name: data.name,
    category: data.category,
    storageVolume: data.storage_volume,
    manufacturerLossFactor:
      data.declared_loss_factor_known === true ? 'known' : 'unknown',
    lossFactor:
      data.declared_loss_factor_known === true
        ? data.manufacturer_loss_factor
        : data.loss_factor_b,
    volumeFactor: data.declared_loss_factor_known === true ? null : data.volume_factor_b,
    temperatureFactor:
      data.declared_loss_factor_known === true
        ? data.temperature_factor_a
        : data.temperature_factor_b,
    source: data.source,
  });
}

function fromStoredMeasure(
  data: StoredMeasure,
  idx: number,
): Result<EditableMeasure, Error> {
  return fromStoredItem(data, idx).chain((item) =>
    Result.ok({
      ...item,
      measureDescription: data.description,
      measurePerformance: data.performance,
      measureBenefits: data.benefits,
      measureBaseCost: data.min_cost,
      measureCostQty: data.cost,
      measureCostUnits: data.cost_units,
      measureInstaller: data.who_by,
      measureDisruption: data.disruption,
      measureAssociatedWork: data.associated_work,
      measureRisks: data.key_risks,
      measureNotes: data.notes,
      measureMaintenance: data.maintenance,
      measureLifetimeYears: data.lifetimeYears,
      carbonType: data.carbonType,
      carbonBaseUpfront: data.carbonBaseUpfront,
      carbonBaseBiogenic: data.carbonBaseBiogenic,
      carbonPerUnitUpfront: data.carbonPerUnitUpfront,
      carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
      carbonSource: data.carbonSource,
    } satisfies EditableMeasure),
  );
}

function toStoredMeasure(
  data: EditableMeasure,
): Result<StoredMeasure, ValidationErrors<EditableMeasure>> {
  const result = toStoredItem(data);

  return result.chain((item) =>
    Result.ok({
      ...item,
      associated_work: data.measureAssociatedWork,
      benefits: data.measureBenefits,
      cost: data.measureCostQty ?? 0,
      cost_units: data.measureCostUnits,
      min_cost: data.measureBaseCost ?? 0,
      description: data.measureDescription,
      disruption: data.measureDisruption,
      key_risks: data.measureRisks,
      maintenance: data.measureMaintenance,
      lifetimeYears: data.measureLifetimeYears,
      notes: data.measureNotes,
      performance: data.measurePerformance,
      who_by: data.measureInstaller,
      carbonType: data.carbonType,
      carbonBaseUpfront: 0,
      carbonBaseBiogenic: 0,
      carbonPerUnitUpfront: data.carbonPerUnitUpfront,
      carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
      carbonSource: data.carbonSource,
    } satisfies StoredMeasure),
  );
}

function toStoredItem(
  data: EditableItem,
): Result<StoredItem, ValidationErrors<EditableItem>> {
  const errors: ValidationErrors<EditableItem> = {};
  if (data.name === '') {
    errors.name = 'required';
  }
  if (data.category === null) {
    errors.category = 'required';
  }
  if (data.storageVolume === null) {
    errors.storageVolume = 'required';
  }
  if (data.lossFactor === null) {
    errors.lossFactor = 'required';
  }
  if (data.temperatureFactor === null) {
    errors.temperatureFactor = 'required';
  }
  if (data.manufacturerLossFactor === 'unknown' && data.volumeFactor === null) {
    errors.volumeFactor = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    category: data.category ?? 'Cylinders with inmersion',
    storage_volume: data.storageVolume ?? 0,
    source: data.source,
    ...(data.manufacturerLossFactor === 'known'
      ? {
          declared_loss_factor_known: true,
          manufacturer_loss_factor: data.lossFactor ?? 0,
          temperature_factor_a: data.temperatureFactor ?? 0,
        }
      : {
          declared_loss_factor_known: false,
          loss_factor_b: data.lossFactor ?? 0,
          volume_factor_b: data.volumeFactor ?? 0,
          temperature_factor_b: data.temperatureFactor ?? 0,
        }),
  });
}
