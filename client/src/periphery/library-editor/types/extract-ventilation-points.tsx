import React from 'react';
import { Library } from '../../../data-schemas/libraries';
import { Result } from '../../../helpers/result';
import { NumberInput } from '../../../ui/input-components/number';
import { TextInput } from '../../../ui/input-components/text';
import { EditorDefinition, ValidationErrors } from '../editor';
import {
  BasicCarbon,
  CommonMeasureFields,
  blankBasicCarbon,
  blankMeasure,
  carbonBasicDefinition,
  measureDefinition,
} from './--common-measures';

type StoredCollection = Extract<Library, { type: 'extract_ventilation_points' }>['data'];
type StoredEVP = StoredCollection[string];

type EditableEVP = {
  id: number;
  tag: string;
  name: string;
  ventilationRate: number | null;
  source: string;
} & CommonMeasureFields &
  BasicCarbon;

export const extractVentilationPointsDefinition: EditorDefinition<
  EditableEVP,
  StoredEVP
> = {
  toStored,
  fromStored,
  blank: {
    id: 0,
    tag: '',
    name: '',
    ventilationRate: null,
    source: '',
    ...blankMeasure,
    measureCostUnits: 'unit',
    ...blankBasicCarbon,
  },
  columns: [
    {
      title: 'Name',
      display: (measure) => measure.name,
      edit: (measure, errors, onUpdate) => (
        <TextInput
          value={measure.name}
          error={errors['name']}
          onChange={(name) => onUpdate({ ...measure, name })}
          style={{ width: '18rem' }}
        />
      ),
    },
    {
      title: 'Ventilation rate',
      display: (measure) => measure.ventilationRate,
      edit: (measure, errors, onUpdate) => (
        <NumberInput
          value={measure.ventilationRate}
          error={errors['ventilationRate']}
          onChange={(ventilationRate) => onUpdate({ ...measure, ventilationRate })}
          style={{ width: '2rem' }}
          unit={'m³/hm²'}
        />
      ),
    },
    {
      title: 'Source',
      display: (measure) => measure.source,
      edit: (measure, errors, onUpdate) => (
        <TextInput
          value={measure.source}
          error={errors['source']}
          onChange={(source) => onUpdate({ ...measure, source })}
          style={{ width: '18rem' }}
        />
      ),
    },
    ...measureDefinition<EditableEVP, StoredEVP>({
      includeBaseCost: false,
    }),
    ...carbonBasicDefinition<EditableEVP, StoredEVP>({ includeBaseCost: false }),
  ],
};

function toStored(data: EditableEVP): Result<StoredEVP, ValidationErrors<EditableEVP>> {
  const errors: ValidationErrors<EditableEVP> = {};

  if (data.name === '') {
    errors.name = 'required';
  }
  if (data.ventilationRate === null) {
    errors.ventilationRate = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    ventilation_rate: data.ventilationRate ?? 0,
    source: data.source,
    associated_work: data.measureAssociatedWork,
    benefits: data.measureBenefits,
    cost: data.measureCostQty ?? 0,
    cost_units: data.measureCostUnits,
    min_cost: data.measureBaseCost ?? 0,
    description: data.measureDescription,
    disruption: data.measureDisruption,
    key_risks: data.measureRisks,
    maintenance: data.measureMaintenance,
    lifetimeYears: data.measureLifetimeYears,
    notes: data.measureNotes,
    performance: data.measurePerformance,
    who_by: data.measureInstaller,
    carbonType: data.carbonType,
    carbonBaseUpfront: 0,
    carbonBaseBiogenic: 0,
    carbonPerUnitUpfront: data.carbonPerUnitUpfront,
    carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
    carbonSource: data.carbonSource,
  });
}

function fromStored(data: StoredEVP, idx: number): Result<EditableEVP, Error> {
  return Result.ok({
    id: idx,
    tag: data.tag,
    name: data.name,
    ventilationRate: data.ventilation_rate,
    source: data.source,
    measureDescription: data.description,
    measurePerformance: data.performance,
    measureBenefits: data.benefits,
    measureBaseCost: data.min_cost,
    measureCostQty: data.cost,
    measureCostUnits: data.cost_units,
    measureInstaller: data.who_by,
    measureDisruption: data.disruption,
    measureAssociatedWork: data.associated_work,
    measureRisks: data.key_risks,
    measureNotes: data.notes,
    measureMaintenance: data.maintenance,
    measureLifetimeYears: data.lifetimeYears,
    carbonType: data.carbonType,
    carbonBaseUpfront: data.carbonBaseUpfront,
    carbonBaseBiogenic: data.carbonBaseBiogenic,
    carbonPerUnitUpfront: data.carbonPerUnitUpfront,
    carbonPerUnitBiogenic: data.carbonPerUnitBiogenic,
    carbonSource: data.carbonSource,
  });
}
