import React from 'react';
import { Library } from '../../../data-schemas/libraries';
import { Result } from '../../../helpers/result';
import { NumberInput } from '../../../ui/input-components/number';
import { Select } from '../../../ui/input-components/select';
import { TextInput } from '../../../ui/input-components/text';
import { EditorDefinition, ValidationErrors } from '../editor';
import {
  CommonMeasureFields,
  HighLowCarbon,
  blankHighLowCarbon,
  blankMeasure,
  carbonHighLowDefinition,
  measureDefinition,
} from './--common-measures';

type StoredItemCollection = Extract<Library, { type: 'floors' }>['data'];
type StoredItem = StoredItemCollection[string];

export type EditableItem = {
  id: number;
  tag: string;
  name: string;
  description: string;
  type: 'custom' | 'solid (bs13370)' | 'suspended' | 'heated basement' | 'exposed' | null;
  uValue: number | null;
  kValue: number | null;
  source: string;
};

type StoredMeasureCollection = Extract<Library, { type: 'floor_measures' }>['data'];
type StoredMeasure = StoredMeasureCollection[string];

type EditableMeasure = EditableItem & CommonMeasureFields & HighLowCarbon;

function sharedColumns<T extends EditableItem>(): EditorDefinition<T, never>['columns'] {
  return [
    {
      title: 'Name',
      display: (item) => item.name,
      edit: (item, errors, onUpdate) => (
        <TextInput
          type="text"
          value={item.name}
          error={errors['name']}
          onChange={(name) => onUpdate({ ...item, name })}
          style={{ width: '20rem' }}
        />
      ),
    },
    {
      title: 'Description',
      display: (item) => item.description,
      edit: (item, errors, onUpdate) => (
        <TextInput
          type="text"
          value={item.description}
          error={errors['description']}
          onChange={(description) => onUpdate({ ...item, description })}
          style={{ width: '18rem' }}
        />
      ),
    },
    {
      title: 'Type',
      display: (measure) => measure.type,
      edit: (measure, errors, onUpdate) => (
        <Select
          className="input--auto-width"
          onChange={(type) => onUpdate({ ...measure, type })}
          value={measure.type}
          error={errors['type']}
          options={[
            { value: 'custom', display: 'custom' },
            { value: 'solid (bs13370)', display: 'solid' },
            { value: 'suspended', display: 'suspended' },
            { value: 'heated basement', display: 'heated basement' },
            { value: 'exposed', display: 'exposed' },
          ]}
        />
      ),
    },
    {
      title: 'U value',
      display: (measure) => (measure.type === 'custom' ? measure.uValue : '-'),
      edit: (measure, errors, onUpdate) =>
        measure.type === 'custom' ? (
          <NumberInput
            value={measure.uValue}
            error={errors['uValue']}
            onChange={(uValue) => onUpdate({ ...measure, uValue })}
            style={{ width: '1.5rem' }}
          />
        ) : (
          '-'
        ),
    },
    {
      title: 'k value',
      display: (measure) => measure.kValue,
      edit: (measure, errors, onUpdate) => (
        <NumberInput
          value={measure.kValue}
          error={errors['kValue']}
          onChange={(kValue) => onUpdate({ ...measure, kValue })}
          style={{ width: '1.5rem' }}
        />
      ),
    },
    {
      title: 'Source',
      display: (item) => item.source,
      edit: (item, errors, onUpdate) => (
        <TextInput
          type="text"
          value={item.source}
          error={errors['source']}
          onChange={(source) => onUpdate({ ...item, source })}
          style={{ width: '18rem' }}
        />
      ),
    },
  ];
}

export const floorDefinition: EditorDefinition<EditableItem, StoredItem> = {
  toStored: toStoredItem,
  fromStored: fromStoredItem,
  blank: {
    id: 0,
    tag: '',
    name: '',
    description: '',
    type: null,
    uValue: null,
    kValue: null,
    source: '',
  },
  columns: sharedColumns(),
};

export const floorMeasureDefinition: EditorDefinition<EditableMeasure, StoredMeasure> = {
  toStored: toStoredMeasure,
  fromStored: fromStoredMeasure,
  blank: {
    ...floorDefinition.blank,
    ...blankMeasure,
    measureCostUnits: 'sqm',
    ...blankHighLowCarbon,
  },
  columns: [
    ...sharedColumns<EditableMeasure>(),
    ...measureDefinition<EditableMeasure, StoredMeasure>({
      excludeFields: ['Description', 'Performance'],
      includeBaseCost: true,
      costUnits: { type: 'selectable', options: ['ln m', 'sqm'] },
    }),
    ...carbonHighLowDefinition<EditableMeasure, StoredMeasure>({ includeBaseCost: true }),
  ],
};

function fromStoredItem(data: StoredItem, idx: number): Result<EditableItem, Error> {
  return Result.ok({
    id: idx,
    tag: data.tag,
    name: data.name,
    description: data.description,
    type: data.type,
    uValue: data.uValue,
    kValue: data.kValue,
    source: data.source,
  });
}

function fromStoredMeasure(
  data: StoredMeasure,
  idx: number,
): Result<EditableMeasure, Error> {
  return fromStoredItem(data, idx).chain((item) =>
    Result.ok({
      ...item,
      measureDescription: data.description,
      measurePerformance: data.performance,
      measureBenefits: data.benefits,
      measureBaseCost: data.min_cost,
      measureCostQty: data.cost,
      measureCostUnits: data.cost_units,
      measureInstaller: data.who_by,
      measureDisruption: data.disruption,
      measureAssociatedWork: data.associated_work,
      measureRisks: data.key_risks,
      measureNotes: data.notes,
      measureMaintenance: data.maintenance,
      measureLifetimeYears: data.lifetimeYears,
      carbonType: data.carbonType,
      carbonHighBaseUpfront: data.carbonHighBaseUpfront,
      carbonHighBaseBiogenic: data.carbonHighBaseBiogenic,
      carbonHighPerUnitUpfront: data.carbonHighPerUnitUpfront,
      carbonHighPerUnitBiogenic: data.carbonHighPerUnitBiogenic,
      carbonLowBaseUpfront: data.carbonLowBaseUpfront,
      carbonLowBaseBiogenic: data.carbonLowBaseBiogenic,
      carbonLowPerUnitUpfront: data.carbonLowPerUnitUpfront,
      carbonLowPerUnitBiogenic: data.carbonLowPerUnitBiogenic,
      carbonSource: data.carbonSource,
    } satisfies EditableMeasure),
  );
}

function toStoredMeasure(
  data: EditableMeasure,
): Result<StoredMeasure, ValidationErrors<EditableMeasure>> {
  const result = toStoredItem(data);

  let measureCostUnits: StoredMeasure['cost_units'];

  const errors: ValidationErrors<EditableMeasure> = result.isErr()
    ? result.unwrapErr()
    : {};
  if (data.measureCostUnits !== 'sqm' && data.measureCostUnits !== 'ln m') {
    errors.measureCostUnits = 'should be sqm or ln m';
  } else {
    measureCostUnits = data.measureCostUnits;
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return result.chain((item) =>
    Result.ok({
      ...item,
      uValue: data.uValue ?? 0,
      type: data.type ?? 'custom',
      associated_work: data.measureAssociatedWork,
      benefits: data.measureBenefits,
      cost: data.measureCostQty ?? 0,
      cost_units: measureCostUnits,
      min_cost: data.measureBaseCost ?? 0,
      description: data.measureDescription,
      disruption: data.measureDisruption,
      key_risks: data.measureRisks,
      maintenance: data.measureMaintenance,
      lifetimeYears: data.measureLifetimeYears,
      notes: data.measureNotes,
      performance: `${data.uValue} W/m²·K`,
      who_by: data.measureInstaller,
      carbonType: data.carbonType,
      carbonHighBaseUpfront: data.carbonHighBaseUpfront,
      carbonHighBaseBiogenic: data.carbonHighBaseBiogenic,
      carbonHighPerUnitUpfront: data.carbonHighPerUnitUpfront,
      carbonHighPerUnitBiogenic: data.carbonHighPerUnitBiogenic,
      carbonLowBaseUpfront: data.carbonLowBaseUpfront,
      carbonLowBaseBiogenic: data.carbonLowBaseBiogenic,
      carbonLowPerUnitUpfront: data.carbonLowPerUnitUpfront,
      carbonLowPerUnitBiogenic: data.carbonLowPerUnitBiogenic,
      carbonSource: data.carbonSource,
    } satisfies StoredMeasure),
  );
}

function toStoredItem(
  data: EditableItem,
): Result<StoredItem, ValidationErrors<EditableItem>> {
  const errors: ValidationErrors<EditableItem> = {};
  if (data.name === '') {
    errors.name = 'required';
  }
  if (data.type === 'custom' && data.uValue === null) {
    errors.uValue = 'required for custom type';
  }
  if (data.kValue === null) {
    errors.kValue = 'required';
  }
  if (data.type === null) {
    errors.type = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    description: data.description,
    type: data.type ?? 'custom',
    uValue: data.uValue,
    kValue: data.kValue ?? 0,
    source: data.source,
  });
}
