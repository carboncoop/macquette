import React from 'react';
import { Library } from '../../../data-schemas/libraries';
import { Result } from '../../../helpers/result';
import { NumberInput } from '../../../ui/input-components/number';
import { Select } from '../../../ui/input-components/select';
import { TextInput } from '../../../ui/input-components/text';
import { EditorDefinition, ValidationErrors } from '../editor';

type StoredCollection = Extract<Library, { type: 'floor_insulation_materials' }>['data'];
type StoredMaterial = StoredCollection[string];

type EditableMaterial = {
  id: number;
  tag: string;
  name: string;
  source: string;
  type: string;
  mechanism: 'conductivity' | 'resistance';
  conductivity: number | null;
  resistance: number | null;
};

export const floorInsulationDefinition: EditorDefinition<
  EditableMaterial,
  StoredMaterial
> = {
  toStored,
  fromStored,
  blank: {
    id: 0,
    tag: '',
    name: '',
    source: '',
    type: '',
    mechanism: 'conductivity',
    conductivity: null,
    resistance: null,
  },
  columns: [
    {
      title: 'Name',
      display: (material) => material.name,
      edit: (material, errors, onUpdate) => (
        <TextInput
          type="text"
          value={material.name}
          error={errors['name']}
          onChange={(name) => onUpdate({ ...material, name })}
          style={{ width: '20rem' }}
        />
      ),
    },
    {
      title: 'Type',
      display: (material) => material.type,
      edit: (material, errors, onUpdate) => (
        <TextInput
          value={material.type}
          error={errors['type']}
          onChange={(type) => onUpdate({ ...material, type })}
          style={{ width: '8rem' }}
        />
      ),
    },
    {
      title: 'Mechanism',
      display: (material) => material.mechanism,
      edit: (material, errors, onUpdate) => (
        <Select
          className="input--auto-width"
          onChange={(mechanism) => onUpdate({ ...material, mechanism })}
          value={material.mechanism}
          error={errors['mechanism']}
          options={[
            { display: 'Conductivity', value: 'conductivity' },
            { display: 'Resistance', value: 'resistance' },
          ]}
        />
      ),
    },
    {
      title: 'Conductivity',
      display: (material) =>
        material.mechanism === 'conductivity' ? String(material.conductivity) : '-',
      edit: (material, errors, onUpdate) =>
        material.mechanism === 'conductivity' ? (
          <NumberInput
            value={material.conductivity}
            error={errors['conductivity']}
            onChange={(conductivity) => onUpdate({ ...material, conductivity })}
          />
        ) : (
          '-'
        ),
    },
    {
      title: 'Resistance',
      display: (material) =>
        material.mechanism === 'resistance' ? String(material.resistance) : '-',
      edit: (material, errors, onUpdate) =>
        material.mechanism === 'resistance' ? (
          <NumberInput
            value={material.resistance}
            error={errors['resistance']}
            onChange={(resistance) => onUpdate({ ...material, resistance })}
          />
        ) : (
          '-'
        ),
    },
    {
      title: 'Source',
      display: (item) => item.source,
      edit: (item, errors, onUpdate) => (
        <TextInput
          type="text"
          value={item.source}
          error={errors['source']}
          onChange={(source) => onUpdate({ ...item, source })}
          style={{ width: '18rem' }}
        />
      ),
    },
  ],
};

function fromStored(data: StoredMaterial, idx: number): Result<EditableMaterial, Error> {
  return Result.ok({
    id: idx,
    tag: data.tag,
    name: data.name,
    source: data.source,
    type: data.type ?? '',
    mechanism: data.mechanism,
    conductivity: data.mechanism === 'conductivity' ? data.conductivity : null,
    resistance: data.mechanism === 'resistance' ? data.resistance : null,
  });
}

function toStored(
  data: EditableMaterial,
): Result<StoredMaterial, ValidationErrors<EditableMaterial>> {
  const errors: ValidationErrors<EditableMaterial> = {};

  if (data.mechanism === 'conductivity' && data.conductivity === null) {
    errors.conductivity = 'required';
  }
  if (data.mechanism === 'resistance' && data.resistance === null) {
    errors.resistance = 'required';
  }
  if (Object.values(errors).length > 0) {
    return Result.err(errors);
  }

  return Result.ok({
    tag: data.tag,
    name: data.name,
    source: data.source,
    type: data.type,
    description: '',
    ...(data.mechanism === 'conductivity'
      ? {
          mechanism: data.mechanism,
          conductivity: data.conductivity ?? 0,
        }
      : {
          mechanism: data.mechanism,
          resistance: data.resistance ?? 0,
        }),
  });
}
