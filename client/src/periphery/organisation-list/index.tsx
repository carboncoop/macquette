import React, { useSyncExternalStore } from 'react';
import { Link } from 'wouter';
import { OrganisationStoreType } from '../stores/organisation-list';

type AssessmentManagerProps = {
  organisationStore: OrganisationStoreType;
};

export function OrganisationList({ organisationStore }: AssessmentManagerProps) {
  const organisations = useSyncExternalStore(
    organisationStore.subscribe.bind(organisationStore),
    organisationStore.getSnapshot.bind(organisationStore),
  );

  return (
    <section className="mx-auto max-width-1100 d-flex flex-v gap-15">
      <section className="px-0 line-top">
        <h1 className="ma-0">Organisations</h1>
      </section>

      <ul>
        {organisations.map((organisation) => (
          <li key={organisation.id}>
            <Link href={`/organisations/${organisation.id}/`}>
              {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
              <a>{organisation.name}</a>
            </Link>
          </li>
        ))}
      </ul>
    </section>
  );
}
