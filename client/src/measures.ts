import { Scenario } from './data-schemas/scenario';
import type { GenericMeasure } from './data-schemas/scenario/measures';
import { sum } from './helpers/array-reducers';

type CalcInputs =
  | {
      costUnits: 'sqm';
      area: number;
      costPerUnit: number;
      baseCost: number;
      isExternalWallInsulation: boolean;
    }
  | {
      perimeter: number;
      costUnits: 'ln m';
      costPerUnit: number;
      baseCost: number;
    }
  | {
      costUnits: 'unit';
      costPerUnit: number;
      baseCost: number;
    }
  | {
      costUnits: 'kWp';
      costPerUnit: number;
      kWp: number;
      baseCost: number;
    };

export function calcMeasureQtyAndCost(inputs: CalcInputs): [number, number] {
  let quantity = 1;

  switch (inputs.costUnits) {
    case 'sqm':
      // We use area rather than net area here, the idea being that the cost of the
      // area where the windows are, which you're not covering with EWI, is roughly
      // equivalent to the costs of the detailing around the windows - beads, trims,
      // cills etc.
      if (inputs.isExternalWallInsulation === true) {
        // We apply a multiple here because surveys use the internal dimensions of
        // the house, but EWI is applied to the external surface, and should also
        // run past at floor junctions etc.
        quantity = 1.15 * inputs.area;
      } else {
        quantity = inputs.area;
      }
      break;
    case 'ln m':
      quantity = inputs.perimeter;
      break;
    case 'kWp':
      quantity = inputs.kWp;
      break;
  }

  let totalCost = inputs.baseCost + quantity * inputs.costPerUnit;
  totalCost = parseFloat(totalCost.toFixed(2));

  return [quantity, totalCost];
}

function normalisePerformance(performance: string) {
  return performance
    .replace('WK.m2', 'W/m²·K')
    .replace('W/K.m2', 'W/m²·K')
    .replace('m3m2.hr50pa', 'm³/m²·hr50pa')
    .replace('m3/m2.hr50pa', 'm³/m²·hr50pa')
    .replace('W/msup2/sup.K', ' W/m²·K')
    .replace('msup3/sup/msup2/sup.hr50pa', 'm³/m²·hr50pa')
    .replace('na', 'n/a');
}

function normaliseLocation(location: string) {
  if (location === '') {
    return 'Whole house';
  }

  // Due to a historical mess, sometimes measure.location includes either '<br>' or
  // 'br'.  This should be normalised to whitespace.
  // This used to happen on bulk measures.
  location = location.replace(/,br/g, ', ').replace(/,<br>/g, ', ').trim();

  // The code used to also put excess commas at the ends of things.
  if (location[location.length - 1] === ',') {
    location = location.substring(0, location.length - 1);
  }

  return location;
}

function normaliseDisruption(disruption: string) {
  return disruption.replace('MEDIUMHIGH', 'MEDIUM / HIGH');
}

function normaliseCost(cost: number) {
  function roundToNearest(val: number, to: number) {
    return Math.ceil(val / to) * to;
  }

  if (cost < 500) {
    return roundToNearest(cost, 5);
  } else if (cost < 5000) {
    return roundToNearest(cost, 50);
  } else {
    return roundToNearest(cost, 500);
  }
}

function single(measure: GenericMeasure | undefined) {
  if (measure === undefined) {
    return [];
  } else {
    return [measure];
  }
}

function nested(measures: Record<string, { measure: GenericMeasure }> | undefined) {
  if (measures === undefined) {
    return [];
  } else {
    return Object.values(measures).map((row) => row.measure);
  }
}

function fabric(scenario: Scenario): GenericMeasure[] {
  const measures = scenario?.fabric?.measures;
  if (measures === undefined) {
    return [];
  } else {
    return Object.values(measures).map((row) => ({
      ...row.measure,
      tag: row.measure.lib,
      cost_total: row.measure.cost_total ?? 0,
      quantity: row.measure.quantity ?? 0,
    }));
  }
}

type MeasureWithNum = GenericMeasure & { num: number };

export function getScenarioMeasures(scenario: Scenario): MeasureWithNum[] {
  const measures = [
    ...fabric(scenario),
    ...nested(scenario?.measures?.ventilation?.extract_ventilation_points),
    ...nested(scenario?.measures?.ventilation?.intentional_vents_and_flues_measures),
    ...single(scenario?.measures?.ventilation?.draught_proofing_measures?.measure),
    ...single(scenario?.measures?.ventilation?.ventilation_systems_measures?.measure),
    ...nested(scenario?.measures?.ventilation?.clothes_drying_facilities),
    ...nested(scenario?.measures?.water_heating?.water_usage),
    ...single(scenario?.measures?.water_heating?.storage_type_measures?.measure),
    ...single(scenario?.measures?.water_heating?.pipework_insulation?.measure),
    ...single(scenario?.measures?.water_heating?.hot_water_control_type?.measure),
    ...nested(scenario?.measures?.space_heating_control_type),
    ...nested(scenario?.measures?.heating_systems),
    ...single(scenario?.measures?.LAC?.lighting?.measure),
    ...single(
      scenario?.use_generation === true
        ? scenario?.measures?.PV_generation?.measure
        : undefined,
    ),
  ];

  return measures.map(
    (measure, idx) =>
      ({
        ...measure,
        num: idx + 1,
        cost_total: normaliseCost(measure.cost_total),
        disruption: normaliseDisruption(measure.disruption),
        location: normaliseLocation(measure.location),
        performance: normalisePerformance(measure.performance),
      }) satisfies MeasureWithNum,
  );
}

export function totalCostOfMeasures(measures: GenericMeasure[]) {
  return sum(measures.map((row) => row.cost_total));
}

type AdditiveInput = {
  id: string;
  createdFromName: string | null;
  measures: GenericMeasure[];
};

export function additiveCostOfMeasures(scenarioId: string, scenarios: AdditiveInput[]) {
  let sum = 0;
  let thisScenarioId: string | null = scenarioId;
  while (thisScenarioId !== null && thisScenarioId !== 'master') {
    const scenario = scenarios.find(({ id }) => id === thisScenarioId);
    if (scenario === undefined) {
      throw new Error(`Couldn't find ${scenarioId}`);
    }
    sum += totalCostOfMeasures(scenario.measures);
    thisScenarioId = scenario.createdFromName;
  }
  return sum;
}

export function additiveEmbodiedCarbon(
  scenarioId: string,
  scenarios: Record<string, Exclude<Scenario, undefined>>,
) {
  const results = [];
  let thisScenarioId: string | null = scenarioId;
  while (thisScenarioId !== null && thisScenarioId !== 'master') {
    const scenario: Scenario = scenarios[thisScenarioId];
    if (scenario === undefined) {
      throw new Error(`Couldn't find ${scenarioId}`);
    }
    results.push(embodiedCarbonFromMeasures(scenarios));
    thisScenarioId = scenario.created_from ?? null;
  }
  return sumRecords(results);
}

interface EmbodiedCarbonResult {
  isComplete: boolean;
  upperUpfrontCarbon: number;
  upperBiogenicCarbon: number;
  lowerUpfrontCarbon: number;
  lowerBiogenicCarbon: number;
}

export function embodiedCarbonFromMeasure(measure: GenericMeasure): EmbodiedCarbonResult {
  const result = {
    isComplete: true,
    upperUpfrontCarbon: 0,
    upperBiogenicCarbon: 0,
    lowerUpfrontCarbon: 0,
    lowerBiogenicCarbon: 0,
  };

  if (measure.carbonType === 'none') {
    result.isComplete = false;
  } else if (measure.carbonType === 'basic') {
    if (
      measure.carbonBaseUpfront === null ||
      measure.carbonPerUnitUpfront === null ||
      measure.carbonBaseBiogenic === null ||
      measure.carbonPerUnitBiogenic === null
    ) {
      result.isComplete = false;
    }

    const upfrontCarbon =
      (measure.carbonBaseUpfront ?? 0) +
      (measure.carbonPerUnitUpfront ?? 0) * measure.quantity;
    const biogenicCarbon =
      (measure.carbonBaseBiogenic ?? 0) +
      (measure.carbonPerUnitBiogenic ?? 0) * measure.quantity;

    result.upperBiogenicCarbon = result.lowerBiogenicCarbon = biogenicCarbon;
    result.upperUpfrontCarbon = result.lowerUpfrontCarbon = upfrontCarbon;
  } else if (measure.carbonType === 'high/low') {
    if (
      measure.carbonHighBaseUpfront === null ||
      measure.carbonHighPerUnitUpfront === null ||
      measure.carbonLowBaseUpfront === null ||
      measure.carbonLowPerUnitUpfront === null ||
      measure.carbonHighBaseBiogenic === null ||
      measure.carbonHighPerUnitBiogenic === null ||
      measure.carbonLowBaseBiogenic === null ||
      measure.carbonLowPerUnitBiogenic === null
    ) {
      result.isComplete = false;
    }

    result.upperUpfrontCarbon =
      (measure.carbonHighBaseUpfront ?? 0) +
      (measure.carbonHighPerUnitUpfront ?? 0) * measure.quantity;
    result.lowerUpfrontCarbon =
      (measure.carbonLowBaseUpfront ?? 0) +
      (measure.carbonLowPerUnitUpfront ?? 0) * measure.quantity;
    result.upperBiogenicCarbon =
      (measure.carbonHighBaseBiogenic ?? 0) +
      (measure.carbonHighPerUnitBiogenic ?? 0) * measure.quantity;
    result.lowerBiogenicCarbon =
      (measure.carbonLowBaseBiogenic ?? 0) +
      (measure.carbonLowPerUnitBiogenic ?? 0) * measure.quantity;
  }

  return result;
}

export function embodiedCarbonFromMeasures(
  scenario: Exclude<Scenario, undefined>,
): EmbodiedCarbonResult {
  const measures = getScenarioMeasures(scenario);
  const calculated = measures.map(embodiedCarbonFromMeasure);
  const sum = sumRecords(calculated);
  return sum;
}

function sumRecords(records: EmbodiedCarbonResult[]): EmbodiedCarbonResult {
  const sum = {
    isComplete: true,
    upperUpfrontCarbon: 0,
    upperBiogenicCarbon: 0,
    lowerUpfrontCarbon: 0,
    lowerBiogenicCarbon: 0,
  };

  for (const result of records) {
    if (!result.isComplete) sum.isComplete = false;
    sum.upperUpfrontCarbon += result.upperUpfrontCarbon;
    sum.upperBiogenicCarbon += result.upperBiogenicCarbon;
    sum.lowerUpfrontCarbon += result.lowerUpfrontCarbon;
    sum.lowerBiogenicCarbon += result.lowerBiogenicCarbon;
  }

  return sum;
}
