import * as Sentry from '@sentry/browser';

declare global {
  interface Window {
    Sentry?: unknown;
    initSentry?: unknown;
  }
}

window.Sentry = Sentry;

window.initSentry = function (dsn: string) {
  Sentry.init({
    dsn,
    integrations: [Sentry.captureConsoleIntegration({ levels: ['error'] })],
    maxValueLength: 10 * 1024,
  });
};
