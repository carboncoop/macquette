import { createElement, StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import { z } from 'zod';
import { Router } from '../periphery';

const optionSchema = z.object({
  userId: z.string(),
  appName: z.string(),
});

export function init(mountpoint: HTMLElement, options: unknown) {
  const { userId, appName } = optionSchema.parse(options);

  const root = createRoot(mountpoint);
  const element = createElement(
    StrictMode,
    null,
    createElement(Router, { userId, appName }),
  );
  root.render(element);
}

declare global {
  interface Window {
    Router?: Record<string, unknown>;
  }
}

window.Router = { init };
