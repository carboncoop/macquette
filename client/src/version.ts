// SAFETY: BUILD_VERSION is substituted in by esbuild
/* eslint-disable */
// @ts-nocheck
export const VERSION: string =
  typeof BUILD_VERSION === 'string' ? BUILD_VERSION : 'unspecified';
