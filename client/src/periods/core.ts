import { positiveMod } from '../helpers/positive-mod';

/** Minutes since midnight */
export type TimeOfDay = number;

/** A period within a day.
 *
 * Includes start, excludes end; i.e. [start, end). Modulo 24 hours, so `{
 * start: 23 * 60, end: 1 * 60 }` is a valid period with length 2 hours. */
export type TimePeriod = { start: TimeOfDay; end: TimeOfDay };

export type Pattern = TimePeriod[];

export function periodLength({ start, end }: TimePeriod) {
  if (end === start) return 24 * 60;
  return positiveMod(end - start, 24 * 60);
}

export function timeIsInPeriod(t: TimeOfDay, { start, end }: TimePeriod): boolean {
  const t_ = positiveMod(t, 24 * 60);
  const start_ = positiveMod(start, 24 * 60);
  const end_ = positiveMod(end, 24 * 60);
  if (start_ >= end_) {
    // Period spans midnight
    return t_ >= start_ || t_ < end_;
  } else {
    return t_ >= start_ && t_ < end_;
  }
}

export function periodsOverlapping(a: TimePeriod, b: TimePeriod): boolean {
  return timeIsInPeriod(a.start, b) || timeIsInPeriod(b.start, a);
}
