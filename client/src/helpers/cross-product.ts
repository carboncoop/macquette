export function crossProduct<A, B>(a: A[], b: B[]): [A, B][] {
  const out: [A, B][] = [];
  for (const a_ of a) {
    for (const b_ of b) {
      out.push([a_, b_]);
    }
  }
  return out;
}
