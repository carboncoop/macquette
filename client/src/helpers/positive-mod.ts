export function positiveMod(a: number, b: number) {
  return ((a % b) + b) % b;
}
