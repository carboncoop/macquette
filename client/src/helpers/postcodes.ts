/**
 * Split a postcode into its two halves, convert to uppercase.
 */
export function parsePostcode(postcode: string): [string, string] {
  const parsed = postcode.replace(' ', '').toUpperCase();
  if (parsed.length > 7 || parsed.length < 5) {
    throw new Error('Postcode has wrong length');
  }
  const outcode = parsed.slice(0, -3);
  const incode = parsed.slice(-3);
  return [outcode, incode];
}
