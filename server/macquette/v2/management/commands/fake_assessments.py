from django.core.management.base import BaseCommand
from faker import Faker

from macquette.users.models import User
from macquette.v2.models.assessment import Assessment


class Command(BaseCommand):
    help = "Create fake assessments"

    def add_arguments(self, parser):
        parser.add_argument(
            "num_assessments", type=int, help="Number of assessments to create"
        )

    def handle(self, *args, **kwargs):
        num_assessments = kwargs["num_assessments"]

        self.stdout.write(self.style.SUCCESS("Creating assessments..."))

        fake = Faker()
        user = User.objects.get(pk=1)  # Change as necessary

        for _ in range(num_assessments):
            assessment_name = " ".join(fake.words())
            Assessment.objects.create(name=assessment_name, owner=user)

        self.stdout.write(self.style.SUCCESS("Assessment creation finished."))
