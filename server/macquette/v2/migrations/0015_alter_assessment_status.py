# Generated by Django 5.1.3 on 2024-12-04 13:38

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("v2", "0014_report_reproducibility_data"),
    ]

    operations = [
        migrations.AlterField(
            model_name="assessment",
            name="status",
            field=models.CharField(
                choices=[
                    ("Complete", "Complete"),
                    ("In progress", "In progress"),
                    ("For review", "For review"),
                    ("Template", "Template"),
                    ("Test", "Test"),
                    ("Paused", "Paused"),
                    ("Abandoned", "Abandoned"),
                ],
                default="In progress",
                max_length=20,
            ),
        ),
    ]
