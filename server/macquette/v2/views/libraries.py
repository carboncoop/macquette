import json
import logging

from rest_framework import exceptions, generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ..permissions import CanReadLibrary, CanWriteLibrary, IsReadRequest, IsWriteRequest
from ..serializers import LibraryItemSerializer, LibrarySerializer
from .exceptions import BadRequest
from .helpers import build_static_dictionary, get_libraries_for_user

STATIC_URLS = build_static_dictionary()


class ListCreateLibraries(generics.ListCreateAPIView):
    serializer_class = LibrarySerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self, *args, **kwargs):
        return get_libraries_for_user(self.request.user).prefetch_related(
            "owner_user",
            "owner_organisation",
            "owner_organisation__librarians",
            "owner_organisation__admins",
        )


class RetrieveUpdateDestroyLibrary(
    generics.UpdateAPIView,
    generics.DestroyAPIView,
    generics.RetrieveAPIView,
):
    serializer_class = LibrarySerializer
    permission_classes = [
        # IsAuthenticated will ensure we can filter (using get_queryset) based on User.libraries
        # (which is the reverse of Library.owner)
        IsAuthenticated,
        (IsReadRequest & CanReadLibrary) | (IsWriteRequest & CanWriteLibrary),
    ]

    def get_queryset(self, *args, **kwargs):
        return get_libraries_for_user(self.request.user)


class CreateUpdateDeleteLibraryItem(generics.GenericAPIView):
    serializer_class = LibraryItemSerializer
    permission_classes = [
        IsAuthenticated,
        (IsReadRequest & CanReadLibrary) | (IsWriteRequest & CanWriteLibrary),
    ]

    def get_queryset(self, *args, **kwargs):
        return get_libraries_for_user(self.request.user)

    def post(self, request, pk):
        serializer = self.get_serializer_class()(data=request.data)
        if not serializer.is_valid():
            print(serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        tag = serializer.validated_data["tag"]
        item = serializer.validated_data["item"]

        library = self.get_object()

        if isinstance(library.data, str):
            d = json.loads(library.data)
        else:
            d = library.data

        if tag in d:
            logging.warning(f"tag {tag} already exists in library {library.id}")
            raise BadRequest(f"tag `{tag}` already exists in library {library.id}")

        d[tag] = item
        library.data = d
        library.save()
        return Response("", status=status.HTTP_204_NO_CONTENT)

    def delete(self, request, pk, tag):
        library = self.get_object()

        if isinstance(library.data, str):
            d = json.loads(library.data)
        else:
            d = library.data

        if tag not in d:
            raise exceptions.NotFound(f"tag `{tag}` not found in library {library.id}")

        del d[tag]
        library.data = d
        library.save()
        return Response("", status=status.HTTP_204_NO_CONTENT)

    def put(self, request, pk, tag):
        library = self.get_object()

        if isinstance(library.data, str):
            d = json.loads(library.data)
        else:
            d = library.data

        if tag not in d:
            raise exceptions.NotFound(f"tag `{tag}` not found in library {library.id}")

        d[tag] = request.data
        library.data = d
        library.save()
        return Response("", status=status.HTTP_204_NO_CONTENT)
