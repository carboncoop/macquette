import pytest
from factory import Faker

from macquette.users.forms import UserCreationForm

pytestmark = pytest.mark.django_db


class TestUserCreationForm:
    def test_clean_username(self):
        username = Faker("user_name").evaluate(None, None, extra={"locale": None})
        password = Faker("password").evaluate(None, None, extra={"locale": None})

        form = UserCreationForm(
            {
                "username": username,
                "password1": password,
                "password2": password,
            }
        )

        assert form.is_valid()
        assert form.clean_username() == username

        # Creating a user.
        form.save()

        # The user with proto_user params already exists,
        # hence cannot be created.
        form = UserCreationForm(
            {
                "username": username,
                "password1": password,
                "password2": password,
            }
        )

        assert not form.is_valid()
        assert len(form.errors) == 1
        assert "username" in form.errors
