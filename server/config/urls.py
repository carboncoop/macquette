from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.storage import staticfiles_storage
from django.http import HttpResponseRedirect
from django.urls import include, path, re_path
from django.views import defaults as default_views
from django.views.generic.base import RedirectView

admin.site.site_header = "Macquette"
admin.site.site_title = "Backend admin"
admin.site.index_title = "Macquette administration"


def redirect_to_path(request, path):
    return HttpResponseRedirect("/" + path)


urlpatterns = [
    path(
        "favicon.ico", RedirectView.as_view(url=staticfiles_storage.url("favicon.png"))
    ),
    path(settings.ADMIN_URL, admin.site.urls),
    path("", include("macquette.users.urls")),
    path("address-search/", include("macquette.address_search.urls")),
    path("", include("macquette.v2.urls", namespace="v2")),
    re_path(r"^v2/(?P<path>.*)$", redirect_to_path),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
